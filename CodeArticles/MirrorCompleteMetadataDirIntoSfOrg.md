
# Mirror a Complete Metadata Directory into Salesforce Org

## Foreword

I'm planning on making an app that will turn a Salesforce org into a mirror of a local metadata directory. This is different from what the Force.com Deployment Tool currently does.

Current functionality in Force.com Deployment Tool:
- Deploy a new metadata file or component
- Update an existing metadata file or component
- Delete an existing metadata file or component

These are all low-level operations. I want to compose these functions to create something more interesting. I want to assume that my local directory contains every piece of metadata that I want in my org. With this assumption, I want a single function that will make the Salesforce org mirror my local directory. This will include removing fields, classes, workflow rules, etc. that do not exist in my local directory.

Since a tool like this makes it very easy to accidentally delete things from your Salesforce org, the user must understand the power and responsibility. I intend to use this tool in a programmatic manner, however, so I can carefully craft its use, and I will not have to consider a user's simple mistakes.

## Strategy Overview

Presumed Players
- 'Master Copy' of code - a Git repo
- 'Execution Environment' for metadata - a Salesforce org
- 'Deployment Tool' with commands to move code - the app we will write (Ruby)

Basic Use-cases
1) Retrieve all metadata types and components from Execution Environment
2) Deploy local metadata instance to Execution Environment

Related functionality
- Commit to Git repo (local or remote)
- Deploy instance from Git revision or tag

Level 2 Use-cases (remove?)
1) Initialize Git repo from Execution Environment (or manually create git repo and repo link is parameter?)
2) 

## Implementation

### Retrieve all metadata types and components

#### Looking at available tools

To decide the algorithm to use to retrieve every piece of metadata from an Execution Environment, we must know the capabilities of our interface. I chose Metaforce to be my interface to the Metadata API, so let's enumerate its relevant capabilities.

The only function in Metaforce that we need is the `Metaforce::Metadata::Client.retrieve_unpackaged` method. This is how it's used:

        client = Metaforce.new :username => 'username@domain.com', :password => ...
        manifest = Metaforce::Manifest.new(:custom_object => ['Account'])
        retrieveJob = client.retrieve_unpackaged(manifest)
          .extract_to('./tmp')
          .perform

The result of calling this function is the metadata defined in the manifest being placed in the `./tmp` directory.

We want a function that retrieves *all* metadata in a Salesforce org, rather than just the metadata we define in the manifest. It appears, then, that we must write our own function to do this.

#### Writing a retrieveAllMetadata function

A retrieveAllMetadata function will have two main responsibilities:
1) Create a manifest that lists all metadata types and components.
2) Use this created manifest to retrieve all metadata.

##### Create a manifest that lists all metadata types and components

The easiest way to create this Manifest object is to give it a hash, from desired metadata type to an array of desired metadata members/components.

So, our first challenge is to get a list of all metadata types in a Salesforce org. We have two options:
1) Maintain a static list of all metadata types, since they rarely change.
2) Request this list of types from Salesforce using an API.

It looks like Metaforce can help with this, by using the `Metaforce::Metadata::Client.describe` method. This is how it's used:

        client = Metaforce.new :username => 'username@domain.com', :password => ...
        response = client.describe
        response.metadata_objects.collect { |t| t.xml_name }
        => ["CustomLabels", "StaticResource", "Scontrol", "ApexComponent", ...]

The `describe` method will return a Hashie::Mash object with the response body, so we can do Hashie::Mash things with the result.

So, it looks like we can use the API to get a list of all Metadata types, but we also need a list of all Metadata components of these types as well. Because components are the things that a Salesforce really uses to customize their org, metadata components will be drastically different between orgs. Therefore, we must use the API to get a list of components in an org. I see Metaforce has a `Metaforce::Metadata::Client.list_metadata` method. Will this give us the information we need?

        response = client.list_metadata('StaticResource')
        response.collect { |t| t.full_name }
        => ["TASKRAY__trsharedjs", "backbone053full", "styles", ...]

As with all API responses in Metaforce, it is wrapped in a Hashie::Mash object. I noticed something strange here - I requested "CustomObject" metadata, but I also received these standard objects. After reading [the listMetadata API docs](http://www.salesforce.com/us/developer/docs/api_meta/Content/meta_listmetadata.htm), it looks like this is the intended functionality.

Great, it looks like I can loop over the results of the `describe` API call, then use the types in the response to get all members of these types. I believe this should be all we need to generate a complete manifest of an org.

I suppose I should test this...



Or should I test another dependency first...





