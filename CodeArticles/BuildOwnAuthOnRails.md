
# Build Your Own Auth for Rails App


## Foreword

I previously spent some time to prepare my Ubuntu 12.04 machine to build Rails apps, then I built a very basic Rails app. I am dreaming of building an web app in which anybody can store their login credentials and settings for my web app to use.

To do this, I believe I need user accounts to which to attach these login credentials and other generated information. I would like to use a third-party library such as [Devise](https://github.com/plataformatec/devise), but they claim their solution is too complex and recommend first building one's own authentication system to learn the problem domain. They specifically recommend two resources:

- [RailsTutorial - Modeling and viewing users, part II](http://ruby.railstutorial.org/chapters/modeling-and-viewing-users-two#top)
- [RailsCasts - #250 Authentication from Scratch](http://railscasts.com/episodes/250-authentication-from-scratch)

It seems that RailsTutorial is best consumed as a whole, from start to finish, so I'll save that for another time. Therefore, I will view, review, and take notes on the RailsCasts episode #250.


## Begin


- Create a new Rails app

        $ rails new BasicAuthApp
        $ cd BasicAuthApp

We first need to make a sign-up process. We will need some logic to create users, so let's add a new controller for that. We also need to store our user's information somewhere, so let's create a database table and model for that. Note that we don't want to have the responsibility of having our user's passwords. In fact, it is a _best practice_ to (read: **must**) always store only the hash, or fingerprint, of sensitive data like this. We will use an off-the-shelf hashing algorithm, but we want to ensure our hash values are not the same as our neighbor's hash values, so we will add a bit our own personality into the hashing algorithm. This 'bit of personality' is called a 'salt', and it makes it doubly difficult for an attacker to reverse our hashes into our user's real passwords.

        $ rails g controller users new
        $ rails g model user email:string password_hash:string password_salt:string
        $ rake db:migrate # Modify our fresh database to reflect our new model definition.

We now have a place to create users and a place to store them.

Let's direct our attention to the Users controller. It has no logic there, so let's implement it. It is very simple - it creates a new, blank user object in the controller's `new` method, and then saves it to the database in the `create` method.

- Implement the users controller, located at `app/controllers/user_controller.rb`

        class UsersController < ApplicationController
          def new
            @user = User.new
          end
          
          def create
            @user = User.new(params[:user])
            if @user.save
              redirect_to root_url, :notice => "Signed up!"
            else
              render "new"
            end
          end
        end

We also need to improve the view for this model, which will be our sign-up form. We will add a list of any sign-up errors, as well as an email field, password field, and password confirmation field. Note we want the password field types to be 'password_field'. The password and password_field aren't columns in our database, but this is ok because we will add them as properties in our model.

- Implement the sign-up form, located at `app/views/users/new.html.erb`.

        <h1>Sign Up</h1>
        
        <%= form_for @user do |f| %>
          <% if @user.errors.any? %>
            <div class="error_messages">
              <h2>Form is invalid</h2>
              <ul>
                <% for message in @user.errors.full_messages %>
                  <li><%= message %></li>
                <% end %>
              </ul>>
            </div>
          <% end %>
          
          <p>
            <%= f.label :email %><br />
            <%= f.text_field :email %>
          </p>
          <p>
            <%= f.label :password %><br />
            <%= f.password_field :password %>
          </p>
          <p>
            <%= f.label :password_confirmation %><br />
            <%= f.password_field :password_confirmation %>
          </p>
          <p class="button"><%= f.submit %></p>
        <% end %>

To complete our view, we want to add an entry to our routes file. Rather than having a user navigate to a url like 'users/new', we want them to see a url like 'sign_up' that navigates to `new` action on our users object. We'll also make the default route be the sign-up page.

- Add a "sign_up" route

        get "sign_up" => "users#new", :as => "sign_up"
        root :to => "users#new"
        resources :users

We can start our Rails app with `$ rails s` and navigate to `0.0.0.0:3000/sign_up` to see the view we just made. Before we start playing with it too much, we must return to finish implementing it.

Our model has the `database_h` field, but we aren't using that in the view. We want to capture the password entered by the user, calculate the password's hash value, then save it into the `database_h` field. To capture the user's entered password on the server side, we need to create a property on our model into which Rails can place it. So, let's modify our User model, located at `app/models/user.rb`.

- Add properties and validation to the User model

        class User < ActiveRecord::Base
          attr_accessor :password

          validates_confirmation_of :password
          validates_presence_of :password, :on => :create
          validates_presence_of :email
          validates_uniqueness_of :email
        end

To calculate our user's password's hash value, we will use a library called `bcrypt`. When we need to use a third-party library like this, we should define it next to our other dependencies, which is in our app's `Gemfile` file.

- Add `bcrypt` library as app dependency

    gem "bcrypt-ruby", :require => "bcrypt"

Now let's use the hash generator function in this library. We can add logic to the User model that will execute before the model is saved, so let's add the hash generator there.

- Add before save logic to User model

        before_save :encrypt_password

        def encrypt_password
          if password.present?
            self.password_salt = BCrypt::Engine.generate_salt
            self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
          end
        end











