
# Dissecting a Java Spring App

## Foreword

I recently wrote a [blog post](http://alexdberg.blogspot.com/2012/11/sunday-project-forcecom-spring-app-on.html) that contains notes about creating a Java Spring app from a Heroku project template. While I am capable of debugging and updating an existing Spring app, I am not familiar enough to create one from scratch. I want to dissect this app to answer some questions I have about how the Spring framework works.

### How does Spring serve an HTTP GET request?

My understanding
------

Here is how I understand a web application should work. An HTTP GET request arrives at an certain IP address and port. A process is attached to this port, called a web server, and 'handles' the GET request. A web server, in its most basic configuration, looks at the requested URL and transforms it into a file directory path relative to a root directory given to the web server. For example, a request for `127.0.0.1:8080/docs/ls.man` will be interpreted as a request for the `(webroot)/docs/ls.man` file on your local file system.

That is how the static web works. This has limited usefulness, so the ability to delegate this file resolution was added to web servers. So, instead of the web server deciding what file is requesting, retrieving the file, and sending the file to the requestor, the web server passes the request to a program, which is expected to return a file to the web server to send to the requestor. Have you heard of 'mod_php'? This is a web server plugin that allows the server to delegate the HTTP response to a PHP script.

A web server usually sends information in the HTTP request to these scripts in a standard format. The oldest protocol is [CGI](https://en.wikipedia.org/wiki/Common_Gateway_Interface), almost all Ruby web servers use the [Rack](https://en.wikipedia.org/wiki/Rack_(web_server_interface)) interface, and the Python world of web servers use the [WSGI](https://en.wikipedia.org/wiki/Web_Server_Gateway_Interface) interface.

The Java world, on the other hand, uses the Java Servlets specification, which is complex beyond comprehension. Well, it's complex until you understand the entire model and how its different from the above web server app models. The Java Servlets model adds a third player in the web app game, called a 'web container'.

What is a web container? It does almost the same thing as web server like Nginx, but the difference is that all features of its behavior are defined by a specification. Thus, a Java web app server is split into 1) a web server, which does nothing more than attach to an IP address and port and listen for HTTP requests, and 2) a web container, which has logic to route the HTTP request to a Java object handler. Web containers have received a few other responsibilities over time, however, as can be seen on [its Wikipedia page](https://en.wikipedia.org/wiki/Web_container). A web container 1) 'manages the lifecycle of servlets', 2) maps the URL to a servlet, and 3) ensures the requestor has correct access rights.

Why are Java Servlets so complex? There are lots of really smart people in the Java community, and smart people like to make fast and flexible apps. With these values in mind, the Java Servlets specification was designed to be highly componentized and configurable to satisfy many use-cases. Also, Java Servlets is a bit more complex to save on memory and request handling time. While CGI apps create a new OS process for each request, which takes time, a web container creates a single process for each servlet, which can more quickly create a new thread for each request.

So, with this added complexity, how does a Java Servlet handle an HTTP request? Like before, a web server attaches to an IP address and port on the host computer and waits for an HTTP request. The web server does nothing more than forward the request to the web container. The web container looks at its configuration, the `web.xml` file, to decide which servlet to delegate the HTTP request, and then initialize the servlet, if it has not yet been initialized. Once the servlet is ready, the container passes the request to the servlet by calling its `service` method. The servlet uses this method to inspect the information in the request and then take appropriate action. Often, it will create an HTTP response to return to the web container.

So, it seems that a web container is different from a normal web server request handler in two ways: 1) Rather than simply forwarding the request to a handling script, it manages a single instance of each Java handling object, for efficiency. Also, 2) the protocol between the web container and the servlet is hardcoded into the web container. Said another way, the web container can only hear one phrase, an incoming HTTP request object, and speak a few phrases, calling the `init`, `service`, and `destroy` methods of a Java object.

A few things about this design concerns me. 1) Both the web container and the Java servlet object have configuration that routes a request to handling code. 2) The web app server is split into the HTTP listener and the web container. These seem like unnecessary complexity. I will keep this Java Servlet design in mind as I further study the Node.js and Rails web servers. Like Java Servlets, these two web servers also boast efficiency, but unlike Java Servlets, they also advertise simplicity. I would also like to compare this to Nginx web apps.


Learned from Spring
------

Having clearly stated my understanding of how web servers pass HTTP requests to code, let's look at how Spring handles HTTP requests.

According to [official Spring MVC docs](http://static.springsource.org/spring/docs/3.2.x/spring-framework-reference/html/mvc.html), the Spring MVC framework receives HTTP requests from the web container via a servlet named `DispatcherServlet`. As a Spring app developer, you specify one or more URL routes to handle in an XML configuration file for the framework to parse. When your Spring app receives a request, that is, when the `DispatcherServler` receives a request, it consults the URL routes map it built to find a Java class designated to handle the request. This handler can decide how to respond to the request, with its options including writing to response stream, JSP view templates, or other templating languages.










