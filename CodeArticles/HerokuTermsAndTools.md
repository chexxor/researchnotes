
# Heroku Terms and Tools

## Foreword

By reading opinions and docs, I have some preconceptions of Heroku. I would like to dig deeper to clarify my understanding of the platform. I want to know how app development on Heroku is different from traditional app development on an OS.

When developing on a normal OS, you own the hardware resources, so you are free to use it however you feel. Your only limit is the OS and your own knowledge. Heroku development, on the other hand, is a slight inversion of control; Heroku manages hardware resources and mediates your app's access to them. To keep its operation prices low and apps running fast, it is in Heroku's best interest to most efficiently use the resources it governs. It makes sense that Heroku should want to limit unsafe or inefficient processes. It should also want to encourage its users to create manageable code that performs well.

What is Heroku doing to support this idea? On my first deeper read of the Heroku docs, I see many new concepts and tools, and I also see normal tools that aren't available. Where they removed normal tools, they added alternatives that better respect hardware resources. As a side effect of respecting hardware resouces, these alternative tools obey app architectures that are more efficient and scalable. It is my belief, therefore, that if I write a Heroku app as encouraged by the platform, I have learned to be a better software engineer and I have built a higher quality app. Because Heroku doesn't use proprietary APIs, my app is still portable, so I can take my well-designed app and newly gained knowledge and host it elsewhere.


## OS Target vs Heroku Target



## Heroku Architecture

### Stack

Several months ago, Heroku announced that it had a new 'stack' available to use, called Cedar, which is upgraded from Bamboo. What changed? According to the [stack migration dev article](https://devcenter.heroku.com/articles/cedar-migration), it seems that the Cedar stack was developed to give the developer many more tools options.

#### From Bamboo to Cedar

While the Bamboo stack prescribed the language, web server, and logging complex, the Cedar stack allows a developer to choose anything. Gathered from [here](https://devcenter.heroku.com/articles/cedar-migration), here is a list of some interesting differences:

Languages
- Bamboo = Ruby EE 2011.03 1.8.7, and MRI 1.9.2
- Cedar = Any language with a [Buildpack](https://devcenter.heroku.com/articles/buildpacks). Heroku maintains buildpacks for the following languages: Ruby, Python, Node.js, Java, Clojure, and Scala (with rumors of PHP)

Web Server
- Bamboo = [Thin](http://code.macournoyer.com/thin/)
- Cedar = Any web server

Preinstalled Apps/Libraries
- Bamboo = Gems: Bundler, Rake, Rack, [etc.](https://devcenter.heroku.com/articles/gems-on-aspen-bamboo)
- Cedar = None

App Monitoring
- Bamboo = New Relic
- Cedar = Anything

App Definition
- Bamboo = Presumed Ruby
- Cedar = Any processes defined in Procfile ([Foreman](http://blog.daviddollar.org/2011/05/06/introducing-foreman.html) concept, not custom to Heroku)

#### Significance of the Stack

Why do we care that our stack is Cedar? Well, while our stack's name doesn't really matter, the stack is our app's environment. As a programmer, we care about two things: our app's code and our app's container. On Heroku, the stack manages our app, it is our platform, in a way. So, because we are targeting the Cedar stack with our app, it's important to know its rules, limitations, and capabilities.
Specifically, important things to know about the Cedar stack when developing your app include:
- where to divide your app when designing it (running in the dyno manifold)
- how the Cedar stack sends an HTTP request to your app (routing mesh)
- how your app is started and stopped (dyno phases and idling)
- how to scale your app
- 





