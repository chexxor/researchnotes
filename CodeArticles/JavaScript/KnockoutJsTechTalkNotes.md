﻿
Knockout.js Tech-talk


Intro
=====

HTML and CSS are relatively easy to learn and understand, possibly because their vocabulary is limited and produces expected results. JavaScript, on the other hand, is the door to the abstract world, so it's a bit more difficult to understand.

What's JavaScript for? Well, like Java or any other programming language, it can do almost anything.


Data in Java
=====

This is how we work with data in Java. This code is compiled to bytecode and is interpreted by the JVM. The JVM is responsible for evaluating expressions, persisting values to memory, and managing variables, among other things.

        String userName = "Alex" + "Berg";
		String userCity = "Fargo";
		System.out.println(userName + " from " + userCity + " says Hello.");


Data in JavaScript
=====

JavaScript can also work with data. This code is understood by a web browser and is provided special APIs. One API is the DOM API, which will change the current web page on-the-fly. Like the JVM, a browser's JavaScript engine is responsible for evaluating expressions, persisting values to memory, and managing variables, among other things.

        var userName = "Alex" + "Berg";
		var userCity = "Fargo";
		console.log(userName + " from " + userCity + " says Hello.");

So, JavaScript can do most of the same things as a traditional application language, such as Java.


Popular JavaScript
=====

However, JavaScript is most commonly used to quickly add sugar to a web page, such as adding slick behavior or reacting to user interactions.

		$( "#datepicker" ).datepicker();
		
		var hiddenBox = $( "#banner-message" );
		$( "#banner-controls button.show" ).on( "click", function( event ) {
		  hiddenBox.show();
		});

Using JavaScript as simple behavior additions to HTML is fine, but it hides the abilities of JavaScript as a full programming language. It's easy to forget that JavaScript can create relatively complex applications.


Why JavaScript Apps?
=====

Some websites make me fear interaction.

* "I just want to quickly sort this table, PLEASE don't reload the entire page."
* "I just want a tooltip, PLEASE don't pop-up a browser window."
* "I just want to quickly save this, PLEASE don't lose my place in this big page."

These make me want to keep as much as possible in a single web page. I want my browsing to be fast and I want to keep control.


Keeping Snappy
=====

As a user, we want data, but we want to view it in a friendly layout.

By clicking traditional links, a server is repeatedly sending a heavy web page which has style and layout definitions. Browser and web caching helps, but if we can do better then we should try.

Rather than the server rendering data-filled views, let's try rendering them in the browser.

JavaScript can cache data, and it can send HTTP requests to quickly fetch new data. 


Fast Apps on Salesforce
=====

Visualforce has convenient ways of reloading page regions: actionFunction, reRender.

JavaScript Remoting is faster than VF reRender.

        myController.someRemotingMethod([a=1, b='test'] callbackFunction);

However, Javascript Remoting (JSR) just gives us data, which means we have to add markup and style.


Two View-rendering Techniques
=====

JSP = Generate an HTML string by interpolating logic and variables in an HTML file.
JavaScript = Generate an HTML string. Developer's choice of templating tools. (e.g. Handlebars, Dust.js, jQuery Templates)
[LinkedIn's Templating Throwdown](http://engineering.linkedin.com/frontend/client-side-templating-throwdown-mustache-handlebars-dustjs-and-more)


Rendering a View in Java
=====

With Java, a view can be rendered with JSPs. A JSP is an XML document with embedded Java expressions. This sounds like JavaScript, but the difference is that Java expressions in JSP are either equal citizens or more tightly controls the markup around it.

        <html>
			<head>
				<title>Conditional Example</title>
			</head>
			<body>
				<% if (request.getParameter("username") == null || 
						  request.getParameter("username").equals("") )
				{ %>
					Please log in.
			    <% } else { %>
					Hello: ${param.username}! How are you.
			    <% } %>
			</body>
		</html>

		
		<html>
			<head>
				<title>While Loop Example</title>
			</head>
			<body>
				<h1>While Loop Counter</h1>
				<%  int n = 0;
					out.println("Number:");
					while (n < 10) {
						out.println(" , "+ n);
						n++;
					} %>
			</body>
		</html>

		
Rendering a View in JavaScript
=====

With JavaScript, the view is master and must be always valid. JavaScript simply has the privilege of modifying the view definition.

		<html>
			<body>
			</body>
			
			<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
			<script>
			$(document).ready(function() {
				// After resources are all loaded, build the page.

				// Build HTML string.
				var pageHtml = '<div><h2>My Title</h2><p>Some content.</p></div>';

				// Paste it into the right place.
				$('body').html(pageHtml);
			});
			</script>
		</html>

To add new elements to the view using JavaScript, we write the new markup as a string and use the browser's API to append it to the right element/node.
[HTML-Rendered View](https://c.na9.visual.force.com/apex/VFRendered)
[JS-Rendered View](https://c.na9.visual.force.com/apex/JSRendered)
[Template-Rendered View](https://c.na9.visual.force.com/apex/JQM_Dynamic_List)


When to use JavaScript view rendering?
=====

* A user visits a web page to view and interact with data.
* A user visits a data-driven web page daily and app speed increases productivity.


KnockoutJS
=====

Rather than create separate templates, why not copy a page's in-use elements to use as templates?
Rather than manually rendering a view, why not automatically render the view when the JS data changes?
This is called "data binding", and is popular in the MVVM pattern.


Basic KnockoutJS Example
=====

		<!-- View, as HTML -->
        <p>First name: <strong data-bind="text: firstName">todo</strong></p>
		<p>Last name: <strong data-bind="text: lastName">todo</strong></p>

		/* Model, as a JavaScript object */
		function AppViewModel() {
			this.firstName = "Bert";
			this.lastName = "Bertington";
		}

		// Activates knockout.js
		ko.applyBindings(new AppViewModel());


Internal Page Using Knockout
=====

I use our internal Timesheet page quite often, and I thought the task searching was terribly slow and unresponsive. It was using normal Visualforce rerendering to perform task searches, so I wanted to see if the newer JavaScript Remoting would speed up task searching. After finding success, I want to share the results of my research.

The key is to make a Task Search widget that is has JavaScript logic.


Record Search Widget with Knockout
=====

- *Search Term* input
- *Text Change Event* handler, which queries Salesforce
- *Queried Record List* output
- *Click Event* handler, which copies selected record id into VF action rerender world to support inputField








