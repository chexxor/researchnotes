
# Building an AngularJS Hybrid Mobile App on Ionic

## Foreword

Lately, I spend most of my time developing a JavaScript application, built using AngularJS, which acts as a GUI to manipulate related objects in a Salesforce database.

### Visualforce is Great, Until...

"What? Why?", say some Salesforce developers. I can understand their skepticism. One can argue that using Salesforce's Visualforce as a Salesforce application platform is a better solution. It provides the look and feel of a native Salesforce page, which means the app maintainer doesn't need expert web development skills. Also. its code is highly maintainable, as it provides simple abstractions around most web app functionality, such as data-binding, presentation styles, and client-side updating using AJAX.

If Visualforce provides everything a business application needs, why would a business want to enter the messy world of JavaScript to build an application? To answer simply, application performance. Most of Visualforce is implemented on the server, which means that an application must request new HTML code from the server before it can update the UI. This is terribly slow, and helplessly slow due to its design.

### Speeding Up Visualforce Apps

How can we make a faster web app? The key is rendering HTML code on the client-side, which must be done using JavaScript.

While most of Visualforce consists of XML components which render as HTML, it's domain was extended to include JavaScript. Because the web app market has many UI toolkits, Visualforce's JavaScript tools accept only the responsibility of moving data to and from Salesforce. And they are great for that! However, after your Salesforce is on the client-side as raw JavaScript objects, how do you nicely display it on a web page? That problem is easily solved by pressing it into an HTML template, but how, then, do you change that data? These questions are asked by developers on all software platforms, so a variety of answers exist under this category, which I'll call "view frameworks" in this article.

### JavaScript View Frameworks

JavaScript web app developers have many options when choosing a view framework, but the most popular ones I'm aware of are [KnockoutJS](http://knockoutjs.com/), [EmberJS](http://emberjs.com/), and [AngularJS](http://angularjs.org/). KnockoutJS is great for simple apps, because it solves just one problem - binding JavaScript objects to HTML views. AngularJS is great for bigger apps, because it prescribes methods of JavaScript testing and URL navigation. (I haven't tried EmberJS.)

### JavaScript UI Frameworks

There are a variety of areas that these JavaScript view frameworks do not address. One problem is HTML templates. That is, given a list of records, which HTML do you use to display and manipulate them in a useful manner? Another problem is optimizing for user's device. That is, how can we make our app useful on both an iPhone *and* a laptop? There are yet more problems, such as graphing data, choosing color schemes, and so on. These problems are addressed by another category of software solutions, which I'll call "UI frameworks" in this article. 

Some examples of Javascript UI frameworks which I've seen and are popular are [Kendo UI](http://www.telerik.com/kendo-ui), [jQuery UI](http://jqueryui.com/) and [jQuery Mobile](http://jquerymobile.com/), [Sencha ExtJS](http://www.sencha.com/products/extjs/) and [Sencha Touch](http://www.sencha.com/products/touch/), [Bootstrap](http://getbootstrap.com/), and dozens of others. I have not worked with Salesforce's [Aura Framework](https://github.com/forcedotcom/aura), but it also seems to fall in this category.

I recently heard about the [Ionic Framework](http://ionicframework.com/), which strongly associates with AngularJS, so the goal of this article, for me, is to try out Ionic to see how it feels.

## Using AngularJS and Ionic for a Salesforce App

### Intro to Ionic Framework

The Ionic Framework was designed as the framework for big, hybrid mobile apps. A "hybrid mobile app" is simply a JavaScript app which can use smartphone-specific features, such as the camera. To this end, Ionic brings together the best of multiple technologies, Cordova to provide hardware APIs and AngularJS to provide view framework and app structure, and then adds a custom UI framework on top. Our time spent learning AngularJS and Cordova won't be wasted, because they are used in many other technology stacks. The UI framework, however, may be a non-transferable technology. But no worries, it looks pretty straight-forward, kinda like Bootstrap.

### Set up Ionic App

Now, let's try using the Ionic Framework to expose the data we manage in our AngularJS app. Ionic has a [Github repository](https://github.com/driftyco/ionic-angular-cordova-seed) which has the code for this exact app pre-built for us, so let's try it out. Ionic has a [Getting Started Guide](http://ionicframework.com/getting-started/) which explains how to set up this app, so I'll follow that.

#### Install Prerequisites

The Ionic Framework has a few dependencies, such as Cordova and AngularJS. Why do we need NPM? Well, it appears that Ionic delegates a few tasks to Cordova, such as building and running apps. These are written in JavaScript, as is the Ionic command line tool, so they need NodeJS to run them.

##### Install NodeJS.

I'm using Windows 8, and I already installed NodeJS by using Chocolatey to install the [`nodejs.install` NuGet package](http://chocolatey.org/packages/nodejs.install). I'll let you figure out how to install [NodeJS](http://nodejs.org/) for your platform.

##### Install Cordova and Ionic

        $ npm install -g cordova
        $ npm install -g ionic

I hit some errors when trying to run the Ionic project later. To prevent these errors, install a few more dependencies that the Getting Started guide didn't document.

##### Install JDK

I already had JDK 1.7 installed. I can't recall when or how I installed it.

##### Install Apache Ant

I installed Apache Ant by using Chocolatey to install the [`apache.ant` NuGet package](http://chocolatey.org/packages/apache.ant). Then, I set the `JAVA_HOME` and `ANT_HOME` environment variables (grr... I hate Ant and Java). 

##### Install Android SDK

If you get an error which says "`Unable to add platform android. Please see console for more info.`", you'll probably need to install an Android SDK. (I didn't have this when I started this project, and the "Getting Started" guide added a link to [Cordova's Installation docs](http://cordova.apache.org/docs/en/3.3.0/guide_platforms_android_index.md.html#Android%20Platform%20Guide) only after I delivered my feedback to them via Twitter. Points for Ionic for pushing fast fix.)

I installed the Android SDK by using Chocolatey to install the [`android-sdk` NuGet package](http://chocolatey.org/packages/android-sdk). Then, I set the `ANDROID_HOME` environment variable to its installation location, at `C:\Users\%USERNAME%\AppData\Local\Android\android-sdk`, and added `%ANDROID_HOME%\tools;%ANDROID_HOME%\platform-tools` to the `Path` system variable. This was trial-and-error on Windows, and was quite annoying. The key for successful `ionic` and `cordova` command line behavior? The `android` command must be available from the command line.

After installing the `android-sdk` package, I used the "SDK Manager" app, also available by running the `android` command from the command line to install the following packages:

- Android SDK Platform-tools
- Android SDK Build-tools
- Android 4.4.2 (API 19)
- Android Support Library
- Google USB Driver
- Google Web Driver
- Intel x86 Emulator Accelerator (HAXM)

##### Create Android AVD

I created an Android Virtual Device by running the "AVD Manager" app to create a virtual Nexus 4 device with 500MB RAM. It's pretty straight-forward.

#### Create New Ionic Project

Make a new Ionic project by cloning the [`ionic-angular-cordova-seed` project](https://github.com/driftyco/ionic-angular-cordova-seed) from GitHub. The `ionic` command line tool can do this for us.

        $ cd ~/CodeRepos
        $ ionic start myApp
        ###
        ### OR - the manual way ###
        ###
        $ cd ~/CodeRepos
        $ git clone https://github.com/driftyco/ionic-angular-cordova-seed.git myApp
        $ cd myApp/

#### Test the App in an Android Emulator

Next, set the app to run for a certain mobile device. These commands will download Cordova libraries for the specified device and start the emulator.

        $ ionic platform android
        $ ionic emulate android
        $ ionic run android

I tried setting `ios` as a platform, but I'm on Windows, so of course that didn't work. Next, I tried setting `android` as a platform, which worked after some addedwork. When I first followed the "Getting Started" guide, there were no instructions to install an Android SDK or Java or Ant. To help you out, I've listed those above as I installed them.

I had troubles with the `ionic emulate android` app. After some monkeying around, I managed to launch the hybrid app on the Android emulator, but not by following the guide. In the "AVD Manager" app, I created a device and clicked the "Start..." button there. After the Android emulator booted, I used the `ionic emulate android` command, which eventually launched our hybrid app in the emulator.

*optional performance enhancement*

Also, I installed the [Intel HAXM](http://software.intel.com/en-us/android/articles/installation-instructions-for-intel-hardware-accelerated-execution-manager-windows) program, which promises to launch the Android emulator faster by reserving a chunk of memory for it. Basically, you install the HAXM package through the "SDK Manager" app, then run the executable (found in the SDK's "extras" folder). It worked as promised! I can't say that about Android and Ionic, however. Big points for Intel for restoring my faith in software!

Yowza - this was a much bigger project than the "Getting Started" guide made it seem. Several hours and an exercise in frustration. Good luck to you, reader! There be dragons!

## Conclusion

I'll stop this article here. I want to modify this Ionic Framework app to display Saleforce data, but this article is already too long. To be continued!
