## Productions

I  have more productions than are hosted here on my blog. Here are links to them for your convenience.


### Sundog Blog

- [The Comet Model: The Yang to the Yin That is Client-side Polling](http://www.sundoginteractive.com/sunblog/posts/the-comet-model-the-yang-to-the-yin-that-is-ajax-client-side-polling) - July 05, 2011
- [Why Did Heroku Choose Clojure? And Why Would You?](http://www.sundoginteractive.com/sunblog/posts/why-did-heroku-choose-clojure-and-why-would-you) - July 13, 2011
- [Alternatives to the Average AJAX ActionStatus](http://www.sundoginteractive.com/sunblog/posts/alternatives-to-the-average-ajax-actionstatus) - July 20, 2011
- [A Pattern for Portable Apex Unit Tests](http://www.sundoginteractive.com/sunblog/posts/a-pattern-for-portable-apex-unit-tests) - August 05, 2011
- [The Responsibility of Choosing Salesforce Apps for Your Org](http://www.sundoginteractive.com/sunblog/posts/the-responsibility-of-choosing-salesforce-apps-for-your-org) - August 17, 2011
- [Force.com - Application Platform or Database Platform](http://www.sundoginteractive.com/sunblog/posts/force-application-platform-or-database-platform) - August 28, 2011
- [Field-update Workflows and Twice-firing Triggers](http://www.sundoginteractive.com/sunblog/posts/field-update-workflows-and-twice-firing-triggers) - September 08, 2011
- [The Necessity of Version Control for Cloud Platforms](http://www.sundoginteractive.com/sunblog/posts/the-necessity-of-version-control-for-cloud-platforms) - September 19, 2011
- [The Social Force.com Community](http://www.sundoginteractive.com/sunblog/posts/the-social-salesforce-community) - October 20, 2011
- [The New, Yet Nascent, Debugger for Force.com Code](http://www.sundoginteractive.com/sunblog/posts/the-new-yet-nascent-debugger-for-force-com-code) - October 29, 2011
- [The Problematic Deployment Process for Internal Salesforce Development](http://www.sundoginteractive.com/sunblog/posts/the-problematic-deployment-process-for-internal-salesforce-development) - November 06, 2011
- [StratoSource - A Salesforce Deployment Tool](http://www.sundoginteractive.com/sunblog/posts/stratosource-a-salesforce-deployment-tool) - November 10, 2011
- [Salesforce’s Two Application Platforms and Target Markets](http://www.sundoginteractive.com/sunblog/posts/salesforces-two-application-platforms-and-target-markets) - November 25, 2011
- [Mobile Apps for Salesforce Made Simple with Mobile SDK](http://www.sundoginteractive.com/sunblog/posts/mobile-apps-for-salesforce-made-simple-with-mobile-sdk) - December 10, 2011
- [Salesforce’s Apex Runtime Design - Old vs New](http://www.sundoginteractive.com/sunblog/posts/salesforces-apex-runtime-design-old-vs-new) - December 13, 2011
- [App Architectures and Data Delivery when Moving Mobile](http://www.sundoginteractive.com/sunblog/posts/app-architectures-and-data-delivery-when-moving-mobile) - December 28, 2011
- [Visualforce Variety of MVC](http://www.sundoginteractive.com/sunblog/posts/visualforce-variety-of-mvc) - January 07, 2012
- [MVC-structured Visualforce Example](http://www.sundoginteractive.com/sunblog/posts/mvc-structured-visualforce-example) - January 13, 2012
- [Switch to DVCS Shows Branching Behavior Changes](http://www.sundoginteractive.com/sunblog/posts/switch-to-dvcs-shows-branching-behavior-changes) - January 17, 2012
- [Chatter Becomes More Social with Spring 12 Release](http://www.sundoginteractive.com/sunblog/posts/chatter-becomes-more-social-with-spring-12-release) - January 30, 2012
- [Automated Salesforce Unit Test Execution Using Scheduled Apex](http://www.sundoginteractive.com/sunblog/posts/automated-salesforce-unit-test-execution-using-scheduled-apex) - January 31, 2012
- [Mitigating Maintenance Cost with Open Source Software](http://www.sundoginteractive.com/sunblog/posts/mitigating-maintenance-cost-with-open-source-software) - February 14, 2012
- [Salesforce MVP Brings Multi-file Uploading to Salesforce](http://www.sundoginteractive.com/sunblog/posts/salesforce-mvp-brings-multi-file-uploading-to-salesforce) - February 20, 2012
- [Incredible Statistics of Salesforce’s Growth](http://www.sundoginteractive.com/sunblog/posts/incredible-statistics-of-salesforces-growth) - February 24, 2012
- [Various Force.com Development Editors](http://www.sundoginteractive.com/sunblog/posts/various-force.com-development-editors) - February 29, 2012
- [Force.com vs OS - Complexity of Web App Platforms](http://www.sundoginteractive.com/sunblog/posts/force.com-vs-os-complexity-of-web-app-platforms) - March 15, 2012
- [Salesforce’s Vision of HR’s Social Future with Rypple](http://www.sundoginteractive.com/sunblog/posts/salesforces-vision-of-hrs-social-future-with-rypple) - March 16, 2012
- [Uninterrupted Workflows with Open Source Tools](http://www.sundoginteractive.com/sunblog/posts/uninterrupted-workflows-with-open-source-tools) - March 21, 2012
- [Salesforce DE Org - Great Javascript Sandbox](http://www.sundoginteractive.com/sunblog/posts/salesforce-de-org-great-javascript-sandbox) - March 30, 2012
- [Shared Salesforce Sandbox Issues](http://www.sundoginteractive.com/sunblog/posts/shared-salesforce-sandbox-issues) - May 01, 2012
- [Designing Solutions with Visualforce Mobile Components](http://www.sundoginteractive.com/sunblog/posts/designing-solutions-with-visualforce-mobile-components) - May 17, 2012
- [Apex Scheduling Manager - Skoodat Relax](http://www.sundoginteractive.com/sunblog/posts/apex-scheduling-manager-skoodat-relax) - June 30, 2012
- [Consuming a JSON API - Force.com vs. Java](http://www.sundoginteractive.com/sunblog/posts/consuming-a-json-api-force.com-vs.-java) - July 30, 2012
- [A Developer’s Perspective on Dreamforce 2012](http://www.sundoginteractive.com/sunblog/posts/a-developers-perspective-on-dreamforce-2012) - August 30, 2012
- [My Choice of Developer Sessions at Dreamforce 2012](http://www.sundoginteractive.com/sunblog/posts/my-choice-of-developer-sessions-at-dreamforce-2012) - September 15, 2012
- [Inspiring Developers at Dreamforce 2012](http://www.sundoginteractive.com/sunblog/posts/inspiring-developers-at-dreamforce-2012) - September 27, 2012
- [Salesforce Auto-Number Field Skips Numbers?](http://www.sundoginteractive.com/sunblog/posts/salesforce-auto-number-field-skips-numbers) - November 10, 2012


### Developerforce.com Technical Library

- [Creating a Mobile Component for Visualforce](http://wiki.developerforce.com/page/Creating_a_Mobile_Component_for_Visualforce) - June 7, 2012

### Conferences

- [Creating and Using Visualforce Mobile Components](Creating a Mobile Component for Visualforce)- Dreamforce 2012 - September 19, 2012




