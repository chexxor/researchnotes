
# Installing Gitit on Raspberry Pi

## Foreword

The Raspberry Pi is a tiny, super-low power computer which is perfect for a toy home-server. What is a toy home server good for? A web server, SSH-tunnelling, SSH host for small coding projects, backup for code repositories, and maybe even a personal wiki host. In a previous [blog post](http://alexdberg.blogspot.com/2012/11/creating-public-web-server-on-raspberry.html), I spent an afternoon learning how to set up a web server on the Raspberry Pi. Today, I'd like to try to set up a personal wiki.

If you want to create a wiki, there are many choices. I tried [TiddlyWiki](http://tiddlywiki.com/) for a while, which is a unique type of wiki implemented in JavaScript and HTML that has many forks. It's attractive attribute is that there is nothing to install, but I decided this is bad for me because it is client-side only and can't be hosted on a central server without some complicated configuration.

So, I continue looking at other options for wiki software. There is [MediaWiki](https://www.mediawiki.org/wiki/Main_Page), which must be stable and useful since Wikipedia uses it, but it is implemented in PHP. I don't trust PHP simply because of its reputation, so I will not use any PHP wiki software. Same goes for Perl. So, what about wiki software implemented in other languages? Java isn't interesting to me, so I that's out. Python has [MoinMoin](https://en.wikipedia.org/wiki/MoinMoin) and [Zwiki](https://en.wikipedia.org/wiki/ZWiki). Ruby has a few, but I can't find a list of popular ones at time of writing.

The most interesting wiki software I found is implemented in Haskell, called [Gitit](http://gitit.net). This is attractive because it uses Git for persistence, not a full-on complicated SQL database, and it also renders markdown-formatted text documents, which is what I've been using lately. So, Haskell! I choose you!


## References

LLVM
- [GHC's LLVM Backend - Author Notes](http://blog.llvm.org/2010/05/glasgow-haskell-compiler-and-llvm.html)
- [GHC - Installing LLVM](http://ghc.haskell.org/trac/ghc/wiki/Commentary/Compiler/Backends/LLVM/Installing)
- [Raspberry Pi - LLVM Build Options](http://www.raspberrypi.org/phpBB3/viewtopic.php?f=31&t=36724)

GHC
- [GHC - Building for Raspberry Pi](http://ghc.haskell.org/trac/ghc/wiki/Building/Preparation/RaspberryPi)
- [ARM ELF Author's Status Notes](http://haskell.1045720.n5.nabble.com/Re-Haskell-GHC-7-on-ARM-td5711616.html)
- [ARM ELF Author's Notes - Building GHC for ARM](http://bgamari.github.io/posts/ghc-llvm-arm.html)

Trac
- [Dynamically Link GHCi](http://ghc.haskell.org/trac/ghc/ticket/3658)
- [Dynamic GHC programs](http://ghc.haskell.org/trac/ghc/wiki/DynamicGhcPrograms)



### Installing Haskell on Raspberry Pi

I'll be following the official [Gitit installation guide](http://gitit.net/Installing).

Install Haskell
  - SSH into the Raspberry Pi
  - Install the Haskell Platform package, which includes several essential Haskell [development tools](http://www.haskell.org/platform/contents.html), by running the following command:
  
      sudo apt-get install ghc haskell-platform
  
  - Verify we can compile Haskell files by checking the existance of the Haskell compiler and checking its version:
  
      which ghc && ghc --version
  
  - I will use Git to persist the wiki content. I already have version 1.7.10.4 installed.
  

### Problems Ahead

After [reading ahead](http://www.haskell.org/haskellwiki/Raspberry_Pi) and confirming in the #Haskell IRC channel, I became quite sad. While GHC does work on ARM, GHCi, the GHC interactive environment, does not yet work on ARM. Why do we need GHCi? The Gitit app happens to use Template Haskell, which depends on GHCi. This means that if the `GHCi` program does not work from the command line, the Gitit app can not work. So, I run `ghci` on the command line to see if I should proceed with installing Gitit. It responds with `-bash: ghci: command not found`, which tells me that I need a new approach.

After some internet searching, the [GHC Status Report May 2012](http://hackage.haskell.org/trac/ghc/wiki/Status/May12) shows that the 7.4.2 version of GHC supports GHCi on ARM. This GHC release is not yet available in the official Raspbian repositories, so the adventure of this project has now shifted from "Installing Gitit" to "Building GHC from Source". The [latest official build](http://www.haskell.org/ghc/download) shows that [7.6.1](http://www.haskell.org/ghc/download_ghc_7_6_1) is the latest stable release, so I'll be using that. I hope this release includes the patch to support GHCi on ARM.

After researching and testing, it seems that GHCi is still not supported on Raspberry Pi's ARMv6. I believe the issue is related to the GHC linker, but I'm less than 50% sure. The GHC 7.8 release is planning a big move away from GHC's custom linker towards OS linkers for run-time linking. I'm attempting to build from the GHC master branch to see if the fix is committed, but I still have no luck completing a build on the Raspberry Pi.


### Building GHC 7.6.1 from Source on ARM Machine

I'll be following the [official build instructions](http://hackage.haskell.org/trac/ghc/wiki/Building).

  - Install [build dependencies](http://hackage.haskell.org/trac/ghc/wiki/Building/Preparation/Linux) by running the following command:
  
      sudo apt-get install haskell-platform git autoconf libtool make ncurses-dev g++
  
  - Get the latest GHC source code and get any sub-repositories this project requires:
  
      mkdir ~/repos
      cd ~/repos
      git clone http://darcs.haskell.org/ghc.git/
      cd ghc
      ./sync-all --testsuite get //trying pull
  
  - Change to the latest stable branch, which is 7.6, and get any new sub-repositories this branch requires:
  
      // Trying it from master...
      git checkout ghc-7.6
      ./sync-all --testsuite get -b ghc-7.6
  
  - Verify that the code patch that adds support for ARM is included in this release:
  
      // Actually there are several new patches which better-support ARM
      git log --grep=ARM --oneline
      
    should show these lines
  
      ...
      3144f85 add support for ARM hard-float ABI (fixes #5914)
      b22501b Add linker support for ELF on ARM
      ...
  
  - [Building and installing](http://hackage.haskell.org/trac/ghc/wiki/Building/QuickStart) the latest stable source code:
  
    - I had to get a new session to pick up latest PATH changes after installing the build dependencies:
    
      exit
      ssh username@raspberrypi
    
    - Configure the build scripts:
    
      perl boot
      //Modify mk/build.mk to use quick
      ./configure
    
    - Build the binaries from the source, which takes many hours. If the build fails, just run this command again:
    
      make
      

Still having compile issues...
    
  
  
  
  - The Gitit app is packaged and distributed with [Cabal](http://www.haskell.org/cabal). We got this when we installed the Haskell Platform package, so we can use it right away to install Gitit by running the following command:
  
      cabal update
      cabal install gitit
  
  
  

