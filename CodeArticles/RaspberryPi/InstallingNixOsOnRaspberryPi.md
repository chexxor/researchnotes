
# Install NixOS on Raspberry Pi

Recently, I successfully installed NixOS on a VM. I saw the [Raspberry Pi page on the NixOS wiki](http://nixos.org/wiki/Raspberry_Pi), and it made me want to pull out my Raspberry Pi again and try it out. That wiki page helps us install NixOS on a Raspberry Pi, but it is rather unorganized and incomplete. Is it easy? Difficult? I wasn't sure, but it seemed promising.

NixOS user "viric" did most of the work to add Raspberry Pi support to NixOS. He created a pre-built image we can directly write to an SD card and use. Installing this image was a success, with only a few snags. Overall, it was a satisfying several-hour-long project which taught me a lot.

## Download the NixOS-RaspberryPi Image

Why use a NixOS image, rather than using the `configuration.nix` fild and `nixos-rebuild` command? The Raspberry Pi has unusual hardware; not only is the Raspberry Pi an ARM processor, it is a rather old version which uses the ARMv6 instruction set, not the more common, better supported ARMv7. Also, and more uniquely, it has a floating point unit in hardware, also called "hard-float". When discussing software for Raspberry Pi, we ask if it can be compiled to run on the "armhf" architecture. Many large Linux distrubutions no longer officially support ARMv6, so if NixOS can run on Raspberry Pi, I will be pleasantly surprised.

Find the magnet link on the [Raspberry Pi page on the NixOS wiki](http://nixos.org/wiki/Raspberry_Pi). It can be used by torrent clients to find the file to download directly from peers. This magnet link will download the NixOS-for-RasperryPi image, created and shared by user "viric". This was my first time using a magnet link, so I'll share how it works here.

In Transmission (or another torrent client)

- File > Open URL...
- Enter `magnet:?xt=urn:btih:3a1273137a31ef65453ddc44eafcf8e0ede24731&dn=nixos-raspberrypi-ef28e8e70e96-0236cc5d8826-sd2g.img.xz`
- Click the "Open" button, and then click the next "Open" button.
- Wait for the download to finish.
- Unzip it any way you wish. From the terminal, you can use `unzip nixos-raspberrypi*.img.xz`

## Write the NixOS-RaspberryPi Image

The following instructions will overwrite everything on your SD card. If you want to boot two operating systems, this guide is not for you.

Find the device name of your SD card.

    df -l
    # Insert SD card into computer
    df -l
    # Note the name of the newly added device. e.g. sdb1, sdc1

Now that we know the device name of the SD card, we can write the OS image to it. This command will overwrite everything on the card. Alternatively, it could overwrite your current machine's directories if you specify the wrong device. Careful! I will use `/dev/sdb` to refer to the SD card, but your card's device name might be different.

    sudo apt-get install dcfldd
    sudo umount /dev/sdb1
    # Double-check the following command. It's powerful!
    sudo dcfldd bs=1M if=/home/alex/Downloads/nixos-raspberrypi-ef28e8e70e96-0236cc5d8826-sd2g.img of=/dev/sdb
    # Wait for writing to complete.

When the writing is complete, verify its success. Eject the SD card and re-insert it into your computer. Open a file browser and navigate to the SD card to quickly visually verify its contents. The results of my writing produced two partitions, one with a dozen boot files and one with the standard NixOS directory hierarchy.

### Expand Partitions to Fill SD Card

I am using an 8 GB SD card, but I see the size of the NixOS partition is only 2 GB. I want to expand this to fill the SD card, and I also want to add a small swap partition. I used the `gparted` program as instructed by this [elinux.com wiki page](http://elinux.org/RPi_Resize_Flash_Partitions#Manually_resizing_the_SD_card_using_a_GUI_on_Linux). Its instructions are pretty good, so follow that link if you want to expand your partition.

## Insert the SD Card into the Raspberry Pi

This is the easy part. Just put the card into the Raspberry Pi and plug it in.

## Attempt to Log-in

I don't have a monitor, so I don't know if the OS is properly booted and running. The lights are on, but how do I know the system is properly running? My Raspberry Pi is connected to my router. If it successfully booted, maybe I can see it as a device on my network.My router is a DHCP server, which means it knows the Raspberry Pi's IP address, because the router created one for it. There are various ways to discover the IP addresses a DHCP assigned, but I will login to my router's web UI using a web browser.

- Open a web browser window
- Navigate to the router's IP address.
  - Run the following command to find the router's IP: `route -n` Mine is "192.168.0.1".
- Explore the router's web UI to find the DHCP page. This page should list all IP addresses it has given out. I see that a client named `nixos` was given an IP address of `192.168.0.104`.

Now that we know the Raspberry Pi's IP address, we can SSH into it. Currently, the only user account it has is root, with no password.

    ssh root@192.168.0.104
    # Enter empty password.

I am not allowed to log in. I believe SSH doesn't allow logging in as root with empty password, so we need to provide set a password for the root account.

### Set a Password for Root

Now, in most Linux distributions, passwords aren't stored as plaintext, such as "mypass" or "12345". Rather, they are encrypted, using a random salt and a hash of the chosen password. I need to choose a password, calculate this encrypted representation, and save it in the Raspberry Pi's `/etc/shadow` file, which is where these passwords are stored. I can choose a password, but I don't know how to generate its encrypted reprentation. Luckily "viric" in the #nixos IRC channel suggested an alternative idea: I copy my user's encrypted password from my laptop and paste it into the NixOS Raspberry Pi. That should work. Let's try it.

First, unplug the Raspberry Pi, remove the SD card, and plug it into your laptop.

Where is my encrypted password stored? In the `/etc/shadow` file. Let's copy it and paste it into the same file on the SD card.

    # On your Linux laptop:
    sudo nano /etc/shadow
    # Find your username, copy the long text between the first and second colons.
    # On the mounted SD card:
    sudo nano /media/alex/9cbfb8dc-ee41-478a-a0c6-b695d464dd60/etc/shadow
    # Find the row for "root", paste between the first and second colons.
    # Save and exit.

Now, unmount the SD card, remove it, insert it into the Raspberry Pi again, and plug in the power. Try to SSH into it again:

    ssh root@192.168.0.104
    # Enter your user's password from your laptop.

You should be connected! You can keep this password, or create a new password for Raspberry Pi's root account by using the `passwd root` command.

## What's Next?

I want to play around with the `configuration.nix` file a bit. I know the Raspberry Pi has a slow processor, so I believe it will take many hours to build some packages.

More usefully, I want to see if I can declaratively set up a web server on the Raspberry Pi. Can I use only the `configuration.nix` file? Or must I install individual packages and perform manual configuration? I believe declarative set up is possible, but how difficult is it?




