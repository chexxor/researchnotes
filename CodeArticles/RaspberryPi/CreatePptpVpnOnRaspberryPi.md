
# Setting Up PPTP VPN on Raspberry Pi

## Foreword

I have a friend in China who wants to use some websites that are blocked by the Chinese government. She sometimes uses a paid VPN, but I decided I could help a friend out by setting up a simple, free VPN for her to use on my Raspberry Pi.

There are a few different ideas about how to set up a VPN, some simple and some complex. According to my research, OpenVPN is the most secure, but more complex to set up, according to the voices on the internet. PPTP, on the other hand, is easy to set up, but lacks encryption. Therefore, as this is my first adventure in VPN-land, I'll start with the simple type of VPN. Also, my friend has an iPhone. I took a look at the iPhone's built-in VPN configuration, and it seems it doesn't support OpenVPN. So, let's continue setting up PPTP VPN. I'll use the [Ubuntu's PPTP wiki](https://help.ubuntu.com/community/PPTPServer) and this [blog post on StuffAboutCode.com](http://www.stuffaboutcode.com/2012/08/raspberry-pi-use-as-vpn-server.html) and this [larmeir.com blog post](http://www.larmeir.com/2010/03/setting-up-a-pptp-vpn-server-on-debian-and-ubuntu/), and [this AskUbuntu post](http://askubuntu.com/questions/56559/how-do-i-set-up-a-pptp-vpn-on-my-own-ubuntu-server) as a guide. http://pptpclient.sourceforge.net/howto-diagnosis.phtml  http://cnedelcu.blogspot.tw/2011/10/pptp-server-fix-for-ios-problems.html https://discussions.apple.com/thread/2778039?start=120&tstart=0 

## Install PPTP Server on Ubuntu

Install the 'pptpd' package

        sudo apt-get install pptpd

Configure `pptpd`

        sudo nano /etc/pptpd.conf

Add the following lines to the end this file, similar to the file's comments.

        localip 192.168.1.12
        remoteip 192.168.1.100-110

My laptop's IP address is 192.168.1.12, so I use this value for `localip`. I discovered this by logging into my router and looking for my machine name in the DHCP client list. The `remoteip` value should specify a pool of IP addresses the PPTP program can give to clients. We need a unique IP address for each person who will use this VPN, so pick a number and range of IP addresses that aren't being used by your home router.

Configure some extra `pptpd` options. We want to specify the DNS servers for clients.

        sudo nano /etc/ppp/options.pptp

Add the following to the bottom of the file.

        ms-dns 192.168.1.1
        nobsdcomp
        noipx
        mtu 1490
        mru 1490

`ms-dns` is IP address of the DNS server we want to use. Nearly every router is designed to handle this task, so I am using the IP address of my router.

Now, we need to define a user name and password for each PPTP client.

        sudo nano /etc/ppp/chap-secrets

This is a tab-separated list of PPTP users, so be sure to use the tab key when editing this file.

        summer  *   123abc456def    *

Now restart PPTPD to load the new settings.

        sudo service pptpd restart

I am using my laptop to test this, which has no firewall, so I don't need to change the iptables. If you do, follow these steps.

We also need to configure our machine to forward traffic, which is the basic idea of a VPN.

        sudo nano /etc/sysctl.conf

Find the setting for `net.ipv4.ip_forward` and enable it by removing the '#' comment character.

        net.ipv4.ip_forward=1

We changed the settings file, so we need to restart the process.

        sudo sysctl -p


PPTP VPN uses ports 1723 and 47, so I need to tell my router to forward these ports to 192.168.1.12. See portforward.com if you need help configuring your router.
Also in the router, I needed to go to the Application Layer Gateway (ALG) settings to enable "PPTP passthrough".




