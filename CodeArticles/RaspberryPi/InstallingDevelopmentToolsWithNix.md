
# Installing Development Environment with Nix

## Foreword

Nix is quite versatile. It was originally designed to be a package manager which ensures its packages are immutable. Why is immutable an important trait? When I declare a package as a dependency of my application, I can be confident that its behavior never changes. When my application fails, I can confidently ignore any changes in its libraries or other dependencies, and focus on changes inside my application.

Nix can do other things. In the NixOS project, it uses a single configuration file to compose a complete OS by generating its every file. Is this useful in the software development lifecycle? I haven't found a place where it solves a generic problem, but it is a valuable option in application deployment and automated testing. Perhaps it also has a place in application development.

What else can it do? I've heard that Nix can help deploy an entire development environment as well. "Development environment" is a little unclear, so let's clarify. When I want to start hacking on an app for the first time, I can simply run a Nix command or two to give me everything I need to get started on it. What does one need to start hacking on a new app? Build tools and coding tools.

Build tools means everything needed to turn source into executable programs. Essentially, this is only a compiler, but it commonly includes other utilities, such as utilities which enable cross-platform customization like GCC and CMake. Sometimes, a language will need no compilation, which means it only needs an interpreter program and any libraries each app references.

Coding tools means everything to needed to modify source code. Essentially, this is only a simple text editor, such as Nano or SublimeText, but it also could be a complete IDE, such as Eclipse, NetBeans, IntelliJ, or RubyMine. In addition, these editors and IDEs are often enhanced with plugins and user scripts, which means these also must be installed to be considering a complete development environment.

## Goal

If I package my app with Nix, the app's build tools and compile-time dependencies will be installed. So, those are handled. What's left? Coding tools. An app's development team chose a set of tools to use for the app, so we want to quickly set up new developers with these same coding tools. How can we do that? I like Nix's philosophy of ensuring dependencies are immutable. Am I wrong if I say coding tools are dependencies? Maybe not.

Nix is good at managing dependencies, so I'd like to use Nix to manage my coding tools. On one OS, can I set up Nix to manage multiple sets of coding tools, one for each app in development? Even better, can Nix manage them by keeping them isolated, updated, and easy to install?

## Case Study and Scope

To start, I will identify one specific case. I have developed lots of Salesforce projects, so I am familiar with setting up Salesforce development environments on a few OS's. Salesforce development is unusual because code is not compiled on the dev's machine, but rather uploaded to Salesforce servers via SOAP or REST APIs to be compiled and executed. This means its coding tools are few and they should be relatively easy to package with Nix.

What tools does a Salesforce developer use? Most developers use the Eclipse & Force.com plugin or SublimeText 3 & MavensMate plugin. I believe the latter is "lighter", and therefore will be easier to package, so this case study will be using Nix to package SublimeText 3 and the MavensMate plugin.

## Packaging SublimeText 3 and Plugin with Nix

### Make a Plan

How do we install SublimeText? SublimeText is not an open source application, but we can still freely [download its binaries](http://www.sublimetext.com/3). Nix can do that pretty easily. I'll download the Linux tarball release from this URL.

        ### SublimeText 3 - Linux 64-bit
        http://c758482.r82.cf2.rackcdn.com/sublime_text_3_build_3047_x64.tar.bz2

How do we install MavensMate? MavensMate is a SublimeText 3 package. How do we install a SublimeText package? According to the [SublimeText docs](http://docs.sublimetext.info/en/latest/extensibility/packages.html#types-of-packages),

> "Packages are simply folders under `Packages`, or zip archives with the .sublime-package extension saved under `Installed Packages`."

Looking at my installation of MavensMate, the [MavensMate GitHub project](https://github.com/joeferraro/MavensMate-SublimeText)'s directory structure looks identical to the directory structure inside the `Packages/MavensMate` directory. So, we should be able to download a MavensMate package from GitHub and unpack it into that subdirectory of SublimeText, once that is installed. I'll download this directory tarball from this URL.

        ### MavensMate - Tarball
        https://github.com/joeferraro/MavensMate-SublimeText/tarball/fc5f2ec9
        ### Or master
        https://github.com/joeferraro/MavensMate-SublimeText/tarball/master

### Copy Similar Attempt

#### Find SublimeText 2 Package

After browsing the NixPkgs source code, I found a [SublimeText 2 Nix package](https://github.com/NixOS/nixpkgs/blob/master/pkgs/applications/editors/sublime/default.nix). This is SublimeText 2, however, and I want SublimeText 3, so I will have to make a new package. I presume its dependencies didn't change too much, so maybe I can just use the same Nix expression with some modifications.

Here's the steps I followed to fork the NixPkgs project in which to create a `sublime-text-3` package.

        ### Go to GitHub, push "Fork" button on NixOS/NixPkgs repo.
        > cd /
        > git clone https://github.com/chexxor/nixpkgs.git my-sources
        > cd /my-sources
        > git branch add-mavensmate-package
        > cd /my-sources/pkgs/applications/editors/sublime
        > cp default.nix sublime-text-3.nix
        > sudo nano sublime-text-3.nix
        ### Change name to "sublimetext-3b3047"
        ### Change URLs to
        # http://c758482.r82.cf2.rackcdn.com/sublime_text_3_build_3047_x32.tar.bz2
        ### ...and
        # http://c758482.r82.cf2.rackcdn.com/sublime_text_3_build_3047_x64.tar.bz2
        ### Find SHA256 value with "nix-prefetch-url" command.
        > nix-prefetch-url http://c758482.r82.cf2.rackcdn.com/sublime_text_3_build_3047_x32.tar.bz2
        # 06rd3mghn0c090xshxr88mvbi7r7rj48f99cw604qy7hy7b6mk94
        > nix-prefetch-url http://c758482.r82.cf2.rackcdn.com/sublime_text_3_build_3047_x64.tar.bz2
        # 15snrnz3xd8a8b5hr4wfan9nvpnvmn24xhzm4y3cpcqkkr4g9qi0
        ### Copy these SHA values to `sublime-text-3.nix`.

Awesome, this package should work. Let's test it. How do we test it? After reading the [NixPkgs manual: Quick Start to Adding a Package](http://nixos.org/nixpkgs/manual/#chap-quick-start), I should be able to test it like this:

        > nix-build -A sublime-text-3
        ### I just want to test, so I'll use `dry-run`.
        ### Also, I believe I must specify `my-sources`.
        ### So, I'll use this command:
        > nix-build -A sublime-text-3 --dry-run -I nixpkgs=/my-sources -vvv
        # error: getting status of `/home/chexxor/default.nix': No such file or directory

But, this command doesn't work for me, as you can see. Why doesn't it work? This error message doesn't make sense to me. Working with Nix can be so frustrating.






#### Find Another Plugins Package

Now, how do we use Nix to install a plugin?





