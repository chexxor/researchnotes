
# Testing Ruby Metaforce

## Foreword

I am planning on writing a tool to help manage Salesforce metadata outside of Salesforce, as is every other Salesforce dev. I plan to make a Rails app, so I need to use the Metadata API from Ruby. Looks like [ejholmes on GitHub](https://github.com/ejholmes) has a Ruby gem named [Metaforce](https://github.com/ejholmes/metaforce) that does exactly that. I looked through the code, and it looks fairly clean. I am new to Ruby, which is full of idioms and techniques new to me, so I can't read and understand some parts. From a user's point of view, however, it looks like a dream to use, so I'll try it out on the command line to get a feel for it.

## Prerequisites

I'm using
- Ubuntu 12.04
- RVM 1.18.6
- Ruby 1.9.3

## Install Metaforce

- Make a directory for our Metaforce test

        $ mkdir ~/dev/RubySandbox/
        $ cd ~/dev/RubySandbox/

- Set the Ruby version to use, and install Metaforce

        $ rvm use 1.9.3-head
        $ gem install metaforce

## Test Retrieving Metadata

- Now that we have the Metaforce gem installed, let's use it from the Interactive Ruby Shell (irb)

        $ irb
        irb(main):001:0> require 'metaforce' # Load the Metaforce gem
        => true

- I'll follow the docs for the `retrieve_unpackaged` command on the Readme on Metaforce's [GitHub page](https://github.com/ejholmes/metaforce). We need to define a Metaforce instance, a manifast that defines what to retrieve, and finally send the retrieve request.

        irb(main):002:0> client = Metaforce.new :username => 'username@domain.com', :password => 'myPass123', :security_token =>'piFJ664SJIoBicgStOGQuQQM'
        => #<Metaforce::Client @options={:username=>"username@domain...
     
        irb(main):003:0> manifest = Metaforce::Manifest.new(:custom_object => ['Account'])
        => {:custom_object=>["Account"]}
     
        irb(main):009:0> retrieveJob = client.retrieve_unpackaged(manifest)
        => #<Metaforce::Job::Retrieve @id=nil>
        irb(main):010:0> retrieveJob.extract_to('./tmp')
        => #<Metaforce::Job::Retrieve @id=nil>
        irb(main):011:0> retrieveJob.perform
        SOAP request: https://login.salesforce.com/servi...

- We requested the metadata file for the Account object. The Metadata API sent us this information in a Zip file, which we immediately extracted to the `./tmp` directory. Let's verify this by opening a new terminal.

        $ cd ~/dev/RubySandbox/tmp
        $ ls
        objects  package.xml
        $ ls objects
        Account.object

- That was really easy. I like this library.

# Conclusion

The Force.com Migration Tool is almost as easy to use at Metaforce, so why is Metaforce a better option?
- We don't have to create a `build.xml` file, which the Force.com Migration tool requires.
- Ant build-files is much more difficult to maintain.
- Ruby and this library is easier to use in a Ruby app.

The only thing I don't like is that, like every other deploy tool, making a Salesforce org look exactly like your local metadata directory is still difficult. Why? The Salesforce Metadata API is primed for changes and additions, not deletions. It requires a separate file that lists all destructive changes, and each metadata type has a different protocol for specifying these deletions.



