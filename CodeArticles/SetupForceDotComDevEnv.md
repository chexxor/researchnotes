# Setup Windows 8 for Force.com Development

## Foreword

I just received a new laptop I will use for work. It's a Lenovo Yoga 13, which has an IPS touchscreen which makes Windows 8 quite enjoyable. I do Force.com development, so I need to set up all the tools I will need.

## Install Desktop Apps

After playing around with Modern UI apps and the Store app, I see that there aren't any Modern UI apps I can use for development. Let's switch to the desktop to do all our development.

### Install Chocolatey Package Manager

When learning Ruby development on Ubuntu, I came across a wonderful thing called package managers. [RubyGems](http://rubygems.org/) is one, and [APT](http://en.wikipedia.org/wiki/Advanced_Packaging_Tool) is another. It seems that this concept has finally crossed the borders of Windows-land and has manifested as [Chocolatey](http://chocolatey.org/).

To install Chocolatey, I followed the instructions on the [Chocolatey wiki](https://github.com/chocolatey/chocolatey/wiki/Installation) on their GitHub site. I am using PowerShell, so I opened PowerShell and ran `iex ((new-object net.webclient).DownloadString("http://bit.ly/psChocInstall"))` to install it. If you want to use the old-school command line, you can read the wiki yourself.

With Chocolatey, you can easily install many Windows software packages. There's a full [list of Chocolatey packages](http://chocolatey.org/packages) on their site, so feel free to install other interesting packages. I'm not quite sure how it is related to [Nuget](http://nuget.org/), which is a package manager for Visual Studio, but it seems that Chocolatey is just another front-end to Nuget packages. Some brief reading indicates that Nuget is for library packages while Chocolatey is for executables/application packages.

### Install Git

Git has always been easy to install in Linux and Mac, but Windows users have always had a more difficult experience. GitHub is trying to fill this void in Git platform support by creating a friendly UI and easy installer for Windows. Let's try it out.

I went to the [GitHub for Windows site](http://windows.github.com/), used the download link for v1.0, and followed the instructions. In just a few minutes, I had Git installed without it asking any questions. I have a few repositories in GitHub, so I can use the GitHub app that was installed. However, I also have repositories hosted elsewhere.

To clone my Bitbucket repository locally, I used the Git Shell app that was just installed. I had a problem using the SSH protocol, but I found success by using the Git protocol - I'll investigate later.

### Install SublimeText 2

I need a text editor for writing code and documentation. I've used several editors, but I recently found that SublimeText handles 90% of my file editing tasks, and it's quite easy to use. Let's install SublimeText 2 using Chocolatey.

Open PowerShell, run `cinst sublimetext2`.

### Install SublimeText 2 Package Manager and Packages

SublimeText is extra-great because it has many user-created plugins. What's even more awesome is that it has a package manager, like Chocolatey, for these plugins.

To install the package manager, 

A list of SublimeText packages are on [wbond.net](http://wbond.net/sublime_packages/community). I'll follow [that site's installation instructions](http://wbond.net/sublime_packages/package_control/installation) for the package manager. Open the SublimeTex2 Console (ctrl + `) then paste and execute the following command:

        import urllib2,os; pf='Package Control.sublime-package'; ipp=sublime.installed_packages_path(); os.makedirs(ipp) if not os.path.exists(ipp) else None; urllib2.install_opener(urllib2.build_opener(urllib2.ProxyHandler())); open(os.path.join(ipp,pf),'wb').write(urllib2.urlopen('http://sublime.wbond.net/'+pf.replace(' ','%20')).read()); print('Please restart Sublime Text to finish installation')

 This froze the running instance of SublimeText for a minute or so, then completed. After restarting SublimeText, verify the installation by opening the Command Pallete (ctrl + shift + p) and start typing "Package". This didn't give me any "Package Control" commands, so I re-installed the plugin, this time following the "Manual Instructions", which asked me to download the `PackageControl.sublime-package` file to the "Installed Packages" directory.

Let's install a few plugins. Open the Command Pallete, search for and choose "Package Control: Install Package", then search for packages by name.

- [BracketHighlighter](https://github.com/facelessuser/BracketHighlighter)
- [Git](https://github.com/kemayo/sublime-text-2-git)
- [ApplySyntax](https://github.com/facelessuser/ApplySyntax)
- [MarkdownEditing](https://github.com/ttscoff/MarkdownEditing)

### Install Eclipse and Force.com IDE

I do Force.com development, so I need to install the Force.com IDE, which is Eclipse plus an Eclipse plugin. The DeveloperForce website has an [installation wiki](http://wiki.developerforce.com/page/Force.com_IDE_Installation) and a page for [Eclipse 3.6](http://wiki.developerforce.com/page/Force.com_IDE_Installation_for_Eclipse_3.6).

Force.com IDE Prerequisites
- [Java 7 JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
- [Eclipse 3.6](http://www.eclipse.org/downloads/packages/eclipse-ide-java-developers/heliossr1)

After installing Java 7 and Eclipse 3.6, launch Eclipse and install the Force.com IDE plugin. These are the same instructions as on the DeveloperForce wiki.
- Click `Help`, then choose `Install New Software...`
- Click `Add Site`
    - Name: "Force.com IDE"
    - Location: "http://www.adnsandbox.com/tools/ide/install/"
- Check "Force.com IDE", click `Next`
- Restart Eclipse

### Install MavensMate

MavensMate is a SublimeText plugin to add Force.com IDE features. It was developed for OSX, so Windows was not supported, originally. [amtrack](https://github.com/amtrack) user, however, made customizations to add Windows support to this plugin.

Here is install instructions for [amtrack's MavensMate for Windows](https://github.com/amtrack/MavensMate-SublimeText/wiki/Install-Help#wiki-windows), please reference his instructions for now. Later, I will add my personal experience, to share my path to success.

