
# Learning PureScript Compiler

## Foreword

After using JavaScript exclusively for my full-time project of the last 15+ months, I can confidently say that JavaScript is a terrible language for a "Serious Project". An explanation for that statement is best left for its own post. This post is to introduce a language called PureScript and detail how it compiles to JavaScript. 

## Compile PureScript to JS

I believe not many people know how to use a compiler directly. That is, they use a build tool like GNU Make rather than using GCC directly. This is understandable, as most compilers don't have a friendly human interface. Instead of being a function in a programming language, which accepts arguments in a more normal way, compilers use relatively cryptic command line arguments, like `-o`, `-Wall`, and `-j4`. In addition to explicitly provided command line arguments, compilers commonly also use environment variables as arguments, without the user even having an idea that it's a possibility.

While you can be quite successful with a programming language without ever learning how to directly use its compiler, it's sometimes very helpful to understand it, such as when trying to figure out how to speed up a build or why a third-party library isn't being found.

I'm learning PureScript, so let's look at how the current PureScript compiler works.

### Using `psc` to Compile One PureScript Module to JavaScript

Compilers can be pretty complex, so let's start with a very simple PureScript program.

Start by making a project directory, named "simple-psc", into which we'll put a "src" and "dist" directory. We'll be compiling a "hello.purs" file into a "hello.js" file.

    $ mkdir simple-psc
    $ cd simple-psc
    $ mkdir src
    $ vim src/hello.purs

Put the following PureScript code into `main.purs`.

    -- simple-psc/src/hello.purs
    module MyModule where
    
    import Debug.Trace
    
    main = trace "Hello, World!"

Now compile that into JavaScript using the `psc` command, like this.

    $ psc src/hello.purs
    (lots of JavaScript printed here)

It prints it to the console by default, which isn't too useful to us. I guess it could be useful to other console tools, like piping the output to a file or line counting program. When humans are compiling PureScript, we usually want the results to appear in files. Here's how we do that.

    $ psc src/hello.purs --output dist/hello.js

Nothing is printed to the console this time, and when we check the contents of the `dist/hello.js` file we will find the JavaScript we expected. Very good!

What do you think about the JavaScript which was created? There's a lot of JavaScript there! Way more than we need. It looks like the JavaScript translation of our `trace "Hello, World!"` code was put at the bottom of that file, and the `trace` function's module was put just before that. The trace module uses some other library code, which, like before, implies placing a copy of the referenced modules before they are used.

So, it looks like PureScript's `import` keyword is interpretted by the compiler as meaning "put a copy of the JavaScript translation of the imported module here. A pretty simple translation. I can hear you thinking - "But I only used one function from one module! Putting the module's unused code there is such a waste of space! This is a browser app, where every byte matters!" Yes, you're absolutely right - I agree. But don't worry - read the documentation a bit and you'll find that `psc` has a `--module` option, which will remove all code unreachable from the specified module. Here's how we can use it to greatly reduce the size of our "hello.js" program.

    $ psc src/hello.purs --output dist/hello.js --module MyModule

If you look at the JavaScript in the `dist/hello.js` file now, you can't complain. I count two tiny JavaScript functions - it's hard to get better.

### Using `psc` to Compile Two PureScript Modules to JavaScript

We saw how `psc` compiles one PureScript file into one JavaScript file. But what does it do when we tell it to compile *TWO* PureScript files? Let's try. Let's make a second module which does the same as our first.

    $ cp src/hello.purs src/hello2.purs

And change the content of that file to the following.

    -- simple-psc/src/hello2.purs
    module MyModule2 where
    
    import Debug.Trace
    
    main = trace "Hello, World 2!"

Then compile both of these PureScript files and see what happens.

    $ psc --output dist/hello.js src/hello.purs src/hello2.purs

If we look at the JavaScript in the `dist/hello.js` file now, we will see an almost identical file to that produced by compiling just `src/hello.purs`; the only difference between the previous JavaScript file and the new one is the addition of the JavaScript translation of MyModule2 added the very end of the file. (Notice that the `trace` function's definition wasn't added twice? That's pretty great.)

### What is `psc` Making?

Let's stop here and think. Does it make sense that these two unrelated modules were compiled into the same output file? What does it mean to compile two PureScript modules which don't call each other? A similar question has been asked of C - What happens if we compile a C program which has two `main` functions? The [StackOverflow answer](http://stackoverflow.com/questions/1990932/two-main-functions-in-c-c) is "No. All programs have a single main(), that's how the compiler and linker generate an executable that start somewhere sensible." and "What do you mean by 'main function'? If you mean the first function to execute when the program starts, then you can have only one."

If we are expecting the product of `psc` to be executable, I think it doesn't make sense to pass two independent modules to the compiler, as an executable program has exactly one entry point. Judging by the behavior of `psc`, it seems to not be trying to make an executable JavaScript program. I didn't mention it, but `psc` has a `--main` flag which allows us to choose a module's `main` function to execute when the resulting JavaScript is executed. However, even if we use `--main=MyModule` as an argument, MyModule2 is still placed into the resulting JavaScript, which implies that even if we try to tell `psc` to make an executable, it doesn't really know what that means, as it still included MyModule2, which was completely unused.

If `psc` isn't making an executable program, could it be making a library? If it's a library, then it needs to be able to be linked to an executable. In JavaScript, linking is only possible if our program runner is Node. Node allows libraries to be linked to an executable by using a `require` function to load a library which conforms to the CommonJS specification. Current-day web browsers have a hacky way of linking libraries to an executable, which is described by the AMD specification, and will be more properly supported by the ES6 module loader. I don't see traces of either of those specifications in the `psc`-created JavaScript, which means that it is not trying to create a JavaScript library.

### Using `psc-make` to Compile PureScript to CommonJS Libraries

So, while `psc` *does* translate PureScript into JavaScript (which is **awesome**), it doesn't really have a preconception of the product it's making.

But wait - a *different* PureScript compiler exists, called `psc-make`, which has different behavior. What? A *different* compiler? Yeah, `psc-make` is quite different from `psc` - it's reasonable to call it a different compiler, rather than a different compilation mode. `psc-make` advertises itself as a PureScript compiler which produces CommonJS modules, which is exactly correct. It's name is terribly misleading, in my opinion, as it evokes ideas of the GNU Make build tool or the `ghc --make` compilation mode. Note that `psc-make` is *not* like GNU Make at all, and also not like `ghc --make`.

`psc-make` takes a set of PureScript files and translates it to a set of JavaScript CommonJS modules. It doesn't try to make an executable, it just makes libraries. Because it is easy to understand what it is trying to make, I believe it's the only sensible PureScript compiler to use right now.

An important note: If you use `psc`, you get super-minified JavaScript code, but if you use `psc-make` you don't get any minification at all. If respond by saying, "We want small JavaScript, so of course we should use `psc` instead of `psc-make`," then you aren't wrong. I think it's ok to use `psc` if your goal is to get small, working JavaScript. I, however, care about how a compiler should work. A compiler not knowing what it's making doesn't inspire confidence.

### The Second Half of PureScript Compiler

Let me finish my thoughts on `psc-make`, as I think it's quite nice, but missing an important part. The JavaScript produced by `psc-make` is very nice JavaScript, formatted as CommonJS modules. I like CommonJS, as it uses the concept of inter-module linking, a concept used by lots of other programming languages. This is a pretty meaty concept which allows us to finish the job which `psc-make` started.

CommonJS modules are kinda like C's "object files", which are produced by a C compiler before being compiled into a single executable program. An object file is machine code, like the final executable program, but it can't be directly executed as it still has references to subroutines in other object files. The second half of a C compilation process is to pull these object files together into a single file using a linker program, which replaces these references to other modules with working code. The linker can produce a statically linked, self-contained, binary executable, or it can leave some references in the executable which a run-time linker resolves before executing the program.

As this analogy shows, it's ok that `psc-make` doesn't produce minified/linked single file, as we can write a special tool to perform this job. We can consider these CommonJS modules as "JS object files" which can be linked together into a single executable. Haskell's GHC compiler has a `-split-objs` argument, which will split each object file it produces into many - one for each function in the module. When the linker combines these split-object files, it links to the specific functions it needs, rather than pulling in the function's entire module. This space-saving linking process seems to be well-suited to how JavaScript programs are distributed and run.


