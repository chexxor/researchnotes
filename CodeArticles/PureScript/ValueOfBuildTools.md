
## Building PureScript Project

Let's see what we have to do if we don't use a build tool.

    ### Instead of
    $ grunt
    ### We do
    $ psc --output dist/index.js src/index.purs

That's not too bad. I can just press <up> to recall and execute from my terminal's command history.

Let's see what happens when we add more files to our project

    ### Instead of
    $ grunt
    ### We do
    $ psc --output dist/index.js src/index.purs src/Module2/index.purs src/Module3/index.purs

Now it's starting to get a little ugly. Most "Real" projects have more than a dozen modules.

However, we can simplify that command a bit.

    $ psc --output dist/index.js src/**/*.purs

We can take advantage of our shell's, Bash's, path globbing to select all of our PureScript files in one go. This is no worse than our first command!

Let's not forget that most "Real" projects use standard libraries or modules written by third-parties. Let's add some to our project and include those in our build.

    ### Instead of
    $ grunt
    ### We do
    $ psc --output dist/index.js src/**/*.purs thirdparty_libs/jquery/dist/jquery.js thirdparty_libs/lodash/dist/lodash.js

A project's reliance on third-party libraries grows linearly with time, but our compile command's complexity will stay simple if we use path globbing again.

    $ psc --output dist/index.js src/**/*.purs thirdparty_libs/**/*.purs

So which is better between remembering a single build command and using a build tool? Using a build tool is simple if it's already built, but a build tool can become a build *system*, and systems are complex. Keeping a list of shell commands, on the other hand, is simple.

After seeing how the complexity scales for each option, I might prefer using simple shell scripts as long as possible, until they become complex. Why are build tools so widely used, then? Looking at how a previous generation language, C/C++, is compiled might help answer this question.

## Building C Project

Create a new directory for our C project and put "src" and "dist" directories in it. Put the following C code in "src/helloworld.c"

    // src/helloworld.c
    #include <stdio.h>

    int main(void){
        printf("My first C program\n");
        return 0;
    }

We can build this using the "gcc" program (GNU Compiler Collection).

    $ gcc src/helloworld.c -o dist/helloworld

It's easy to compile this simple C program, but as you add more C files, the complexity of the compilation process skyrockets. To build a two-file C program, you need to compile each C file, then link them together.

    $ gcc -c src/helloworld.c -o dist/helloworld.o
    $ gcc -c src/square.c -o dist/square.o
    $ gcc -o dist/helloworld dist/helloworld.o dist/square.o

Not only is the number of commands large, but there are dependencies between these commands which imply command ordering. Keeping this script up-to-date is error-prone. Not only is it error-prone, but it's slow, as every C file in the project is re-compiled on every compilation, which can take minutes for large projects.

To alleviate these pains, the GNU Make build tool was created. It creates a dependency tree of the input files, and compiles a file only if its dependencies have changed (judging on last-modified time-stamps). Let's create a Make recipe and have Make manage the build steps. Copy the following lines into a "Makefile" file in the project directory.

    SRCDIR=src
    ODIR=dist
    IDIR=../include

    CC=gcc
    CFLAGS=-I$(IDIR)

    # Include Math library
    LIBS=-lm

    _OBJ = helloworld.o square.o
    OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

    $(ODIR)/%.o: $(SRCDIR)/%.c
        $(CC) -c -o $@ $^ $(CFLAGS)

    $(ODIR)/helloworld: $(OBJ)
        $(CC) -o $@ $^ $(CFLAGS) $(LIBS)

    .PHONY: clean

    clean:
        rm -f $(ODIR)/* *~ core $(INCDIR)/*~ 

A Makefile has a recipe for building each file, including resulting executable file and each intermediate file. The file to the left of the colon is created by running the command below it, which is only executed if the files listed to the right of the colon are modified. The Makefiles aren't easy to read as they use shell syntax, but they work pretty well and the idea makes sense.

Now we can compile our program by running `make dist/helloworld`, and GNU Make will produce that file by building each file it depends on, in order, and only if it isn't already created and up-to-date.

I believe Make is the first build system which you *had* to use, and only because the C compiler/language was really not helpful when trying to build multi-file projects. I believe the C compiler created the idea that you need to have a build system as a compiler's interface.

## Building a Haskell Project

Most people who have used GHC will likely agree that it is a pretty advanced Haskell compiler. It's type-checker is very featureful and the language is well-designed, but the GHC project compilation process is also very pain-free. Let's give it a try.

Like before, create "src" and "dist" directories in a new project directory. Put the following Haskell code in "src/hello.hs".

    module Main where

    import MySquare

    main = putStrLn "Hello, World!" >>
        putStrLn ("5 squared = " ++ (show $ mysquare 5))

To compile it, we can run the following commands.

    $ ghc --make src/hello.hs -isrc -o dist/hello -keep-tmp-files -outputdir dist

Note that we didn't need to tell GHC about MySquare.hs; GHC finds and loads the referenced module automatically. Not only that, but GHC will only re-compile MySquare.hs if its compiled artifacts are still valid! This almost completely replaces a build system, like GCC needs Make.




