
# Environments: The Essence of Nix's Domain

When we start using the Nix package manager, one of the first commands we work with is the `nix-env` command. "Nix" makes sense, but why is "env" part of this command name? Why isn't it "nix-install" or "nix-package --install"? To answer this question, we need to understand OS shells and the variety of "environments" in a computer. The term "environment" is overloaded, so read this article to better understand when this term is used in the Nix project.

## Defining "Environment"

In the Nix project, an "environment" refers to an OS shell as it is used by a person or program. When we think of an OS shell, we think of using it to access data files and executable files, but an easily overlooked component is the variables defined in the shell. When talking about environments, it is these variables, called "environment variables", which require the most work to safely manage. In addition to this, the structure of the OS's file system is another source of problems, as each user and OS has a different idea of how to best organize files.

Why do we say that these things cause problems when managing an environment? Application portability. If an application is developed to use specific file system paths or specific environment variables, it will be difficult for other people to build, install, and use it. Users expect new software to comform to their system's file organization scheme, which means the software's build and install scripts must consider the file system of every possible user to be considered portable. Likewise, environment variables are freely modifiable by any person or program, which means that building new software may fail because the end-user has an unexpected value for an environment variable. Also, install scripts of new software may change environment variables used by existing software, which can cause existing software to behave unexpectedly.

So, we can see how an environment plays a big role when building software, installing software, and running software. The goal of the Nix project is to solve these problems, to obtain the ability to reproduce properly-functioning software in every OS. Let's see how Nix solves these problems in each of these three phases of a software development life-cycle.

## Variety of Environments

### User Environment 

A "user environment" is an OS shell from the point-of-view of a typical OS-using person. We assume that a typical user is primarily concerned with using programs. Convenience is essential to user experience, so beyond installing an application, Nix must ensure that it is easily accessible from the command line. To do this, Nix must ensure that it is added to the `PATH` environment variable, which is used by most Linux OS's to find a program without considering the user's current directory.

There are a few ways to solve this problem. Rather than adding each package's Nix store directory to the `PATH`, which isn't scalable, Nix uses symlinks to merge a user's packages into a single directory, the `~/.nix-profile` directory, then adds this one directory to the `PATH`.

### Build Environment

A "build environment" is an OS shell from the point-of-view of an OS process while it builds software. Nix wants to guarantee that Nix-packaged software will reliably build on all platforms, so each build environment must be identical. To do this, Nix creates an empty environment in which to build. A benefit of doing this is that programs or libraries which aren't available on users' systems won't accidentally be found when building on the developer's system. This feature forces these dependencies to be explicitly declared in the package, on the `buildInputs` attribute, which tells the package builder to build and include them into the empty build environment so they are ready to use when the software is built. What does "ready to use" mean? This means the `bin` and `lib` directories of these dependent programs and libraries are placed where the software builder expects them, which usually involves the `PATH` environment variable.

!!! Find where and when Nix adds package's to the `PATH` when building `buildInputs` packages.

Here lies a problem. Some build scripts depend on other environment variables to find executables of libraries, such as Ruby, which uses the `RUBYLIB` and `GEM_PATH` environment variables to find its libraries and Gems. How can Nix support these environment variables in a build process? If these packages tell Nix which of their directories must be added to which environment variables, Nix can look at this information and include the packages in more than just the `PATH` environment variable after they are built.

Now, an important thing to consider is code reuse and feature overlap. How can we manage a package's environment variable exports in its build environment and its runtime environment/user environment? Well, the question to ask here is what's different?

In a user environment, the user's environment variables are not touched. Instead, Nix places links in the existing search path to directories in the Nix store. It does this by collecting a list of links to persist in the Nix store to a package named "user-env", then linking to this package with the `~/.nix-profile` directory.

In a build environment, I believe all environment variables are cleaned before the build, then rebuilt after all `buildInput` packages are built and installed. Nix has built-in behavior which adds the `bin` and `lib` directories of each input package to the `PATH` environment variable. Nix will also execute each script in each package's `nix-support` directory. The expected scripts and their expected behavior is not defined.

!!! What are they expected to do?

## Runtime Environment

Another kind of environment in Nix's domain is called a "runtime environment". Some programs need information which is only available at runtime, such as dynamically linked libraries, files, or environment variable values. Consider how these depend on the operating system. Shared libraries or files are often located in a different directory from the executing program's, so how are they found? This problem is solved differently by different software projects, including linux distributions. Therefore, when packaged to run in a Nix system, a software project may not work.

Because Nix uses a non-standard filesystem layout, Nix is responsible for modifying applications to conform to its system. There are a few ways to modify a program's runtime behavior to conform to Nix's system. One way is to patch a program, to update hard-coded filesystem paths to point to the equivalent location in Nix. Another way is to set environment variables, which some applications use to override hard-coded filesystem paths. Environment variables may be read at runtime in addition to at build-time. Nix's build-time environment is described above, but how does Nix manage an applications's runtime environment?

An application's runtime environment, as it seems, was not considered when Nix was originally designed. As a result, a Nix package has no place to describe runtime modifications. Lacking support in a Nix package, the Nix community's solution for runtime modifications is a shell script named `makeWrapper`. This simple solution ensures a program's shell environment will have certain values when it is executed. It works by renaming the application's original executable, then creating an executable shell script with the same name as the original. This replacement script sets environment variables, then executes the original application in this modified enviroment. See the [`makeWrapper` wiki documentation]() for more information.




** Edited out **



### Environment Variables

Now, these environment variables are used by not only programs, but the OS itself. When a user issues a command in a shell, such as `bundle install`, the shell searches for the command in several places. First, it checks shell-native programs, then it search for an executable file which has a name matching the command. Where does it search? It checks each directory defined in the `PATH` environment variable.

I think this is a terrible solution. It's evident that Linux was designed by people who use the shell for fun. I, however, want reliable tools, and this "PATH" solution is not reliable.

The shell's program finder is not the only program which uses environment variables in this manner, many programs use them. For example, the Ruby interpreter's `require` command uses the `RUBYLIB` environment variable to find libraries, and the Rubygems' `gem` program uses the `GEM_HOME` and `GEM_PATH` environment variables to find `*.gem` files.

To make these environment variables more reliable, we need to write a program to manage them. When we install a program, the program should be automatically available without modifying the `PATH` environment variable. Likewise, when we install a Gem, it should be automatically available without modifying the `GEM_PATH` environment variable. How can we do this?

Rather than modifying a global variable, we can manage the visibility of these files by managing links to them in a central location, a location which is already added to the global environment variable. 

This is exactly the solution used by Nix. When a user uses Nix to install a package, it is built and installed into the system. Then, a link to its executable file is placed in a location already on the `PATH` environment variable, which is the `~/.nix-profile` directory.

This works very well, but it only works for the `PATH` environment variable. Why? Currently, Nix only adds every package's `bin` directory to the `~/.nix-profile` linked directory.

!!! Consider adding more thoughts about Shell User Environments



** End Edited Out **




`setup.sh` script has a [addToNativeEnv()](https://github.com/NixOS/nixpkgs/blob/bee4c41e13a9276db4f4a582234c670d74edfa1a/pkgs/stdenv/generic/setup.sh#L179) function. 






How does Nix build a package? We can see this implementation by reading the [`DerivationGoal` class](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libstore/build.cc#L854).

It looks like Nix, the language, does one thing: evaluate a Nix package into a plain text file. I believe this because I see a plain text file in the Nix store, named like `/nix/store/<hash>-*.drv`, which supports this idea. I believe this file is a description of a package's closure, which is the software's dependent packages and its build script. Here's an example:

        Derive(
            [("out","/nix/store/fr0x2jsa8b4yh7bbngq4ncr1q0q5cfv4-foreman","","")],
            [
                ("/nix/store/cdgi55148mgkd5cac3pwg5crgvidn7ix-bash-4.2-p42.drv",["out"]),
                ("/nix/store/hxdq49zv0x3acq2mwqjlp6hjqbmp0a1r-stdenv.drv",["out"]),
                ("/nix/store/pg77zqbmlsp7wjxxpgv7p6w3r1sd1r1q-foreman-1.3.1.tar.bz2.drv",["out"])
            ]
            ,
            ["/nix/store/9krlzvny65gdc8s7kpb6lkx8cd02c25b-default-builder.sh"],
            "x86_64-linux",
            "/nix/store/7mjplwq1r0agngqj2058hl5z8kb3w15g-bash-4.2-p42/bin/bash",
            ...
        )

Nix, the C program, will [parse this derivation's description](https://github.com/NixOS/nix/blob/7cf539c728b8f2a4478c0384d3174842e1e0cced/src/libstore/derivations.cc#L65) to build an object to pass to the derivation building algorithm.



What is the format of this derivation description? Here's how I can explain it.

        // [Parse](https://github.com/NixOS/nix/blob/7cf539c728b8f2a4478c0384d3174842e1e0cced/src/libstore/derivations.cc#L65) the following into a [Derivation](https://github.com/NixOS/nix/blob/7cf539c728b8f2a4478c0384d3174842e1e0cced/src/libstore/derivations.hh#L43) object.
        Derive(
            // Outputs (e.g. "/nix/store/<hash>-foreman")
            [drvId, out.path, out.hashAlgo, out.hash] // [DerivationOutput](https://github.com/NixOS/nix/blob/7cf539c728b8f2a4478c0384d3174842e1e0cced/src/libstore/derivations.hh#L18)
            ,
            // Inputs: sub-derivations
            [inputDrv, [drvId]] // [DerivationInputs](https://github.com/NixOS/nix/blob/7cf539c728b8f2a4478c0384d3174842e1e0cced/src/libstore/derivations.hh#L39)
            ,
            // Inputs: sources (e.g. "default-builder.sh")
            [drv.inputSrcs] // [PathSet](https://github.com/NixOS/nix/blob/7cf539c728b8f2a4478c0384d3174842e1e0cced/src/libstore/derivations.hh#L47)
            ,
            // Platform (e.g. "x86_64-linux")
            drv.platform // string
            ,
            // Builder program (e.g. ${bash}/bin/bash)
            drv.builder // Path
            ,
            // Builder arguments (e.g. ["-e", "/nix/store/...default-builder.sh"])
            [drv.args] // Strings
            ,
            // Environment variables (e.g. ("buildInputs",""), ("name","foreman"))
            drv.env[name] // StringPairs
        )





[writeDerivation](https://github.com/NixOS/nix/blob/7cf539c728b8f2a4478c0384d3174842e1e0cced/src/libstore/derivations.cc#L29)

[computeStorePathForText](https://github.com/NixOS/nix/blob/7cf539c728b8f2a4478c0384d3174842e1e0cced/src/libstore/store-api.cc#L214)

[makeStorePath](https://github.com/NixOS/nix/blob/7cf539c728b8f2a4478c0384d3174842e1e0cced/src/libstore/store-api.cc#L168)


Derivation's path =
    "/nix/store/" + s + "-" + name; [source](https://github.com/NixOS/nix/blob/7cf539c728b8f2a4478c0384d3174842e1e0cced/src/libstore/store-api.cc#L177)
    where
        s = type + ":sha256:" + printHash(hash) + ":" + "/nix/store" + ":" + name [source](https://github.com/NixOS/nix/blob/7cf539c728b8f2a4478c0384d3174842e1e0cced/src/libstore/store-api.cc#L172)
        type = "text" (or something else) [source](https://github.com/NixOS/nix/blob/7cf539c728b8f2a4478c0384d3174842e1e0cced/src/libstore/store-api.cc#L221)
        hash = unparseDerivation(drv) [source](https://github.com/NixOS/nix/blob/7cf539c728b8f2a4478c0384d3174842e1e0cced/src/libstore/store-api.cc#L217) from [source](https://github.com/NixOS/nix/blob/7cf539c728b8f2a4478c0384d3174842e1e0cced/src/libstore/derivations.cc#L40)
        name = drv.name + ".drv" [source](https://github.com/NixOS/nix/blob/7cf539c728b8f2a4478c0384d3174842e1e0cced/src/libstore/derivations.cc#L39)




The Nix installer creates an empty environment in the [`DerivationGoal::startBuilder` method](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libstore/build.cc#L1621), then calls the `DerivationGoal::initChild` method to run the package's build script.


The Nix installer calls a package's build script in the [`DerivationGoal::initChild` method](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libstore/build.cc#L2142).



A Nix Store path is created by the [LocalStore::buildPaths method](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libstore/build.cc#L3213).
- 



[Implementation](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libstore/build.cc#L1621) of derivation build:
- Set [hardcoded environment variables](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libstore/build.cc#L1636): `PATH`, `HOME`, `NIX_STORE`, `NIX_BUILD_CORES`
- Set [user-defined environment variables](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libstore/build.cc#L1657) from the `drv.env` property
- Create and [set build directory](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libstore/build.cc#L1662): "nix-build-" + storePathToName(drvPath)
- Set environment variables to this build directory
    - `BUILD_TOP`, `TMPDIR`, `TEMPDIR`, `TMP`, `TEMP`, `PWD`
- [Compute references](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libstore/build.cc#L1704), which is primarily used by NixOS and virtualization
    - Gets the `exportReferencesGraph` value from `drv.env`
    - Includes all references in the build's closure
    - Write references to file in build directory
- Find a user account to build as: `settings.buildUsersGroup`
- [Set up "chroot"](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libstore/build.cc#L1788), if using one
- [Set up a process fork](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libstore/build.cc#L1963) in which to build
- Do "chroot" stuff
    - Setup IP socket and hostname stuff
    - Setup filesystems as private
    - Bind-mount dependencies, Nix store directories, from host
    - Mount directory and set it as root
- [Set up child process](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libstore/build.cc#L369)
    - Create separate shell session for child
    - Set up process' input, output, and error handles
    -
 



Points of Interest in `build.cc`
- [in Worker::run, goal->work](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libstore/build.cc#L3026)

- [DerivationGoal's States](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libstore/build.cc#L802): Each state has matching function.






