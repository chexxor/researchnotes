
# The Value of Nix in App Management

## Discovering Nix

I first heard about Nix, the package manager, a year or two ago. It sells itself as "the functional package manager", which I thought was an interesting sales point. In the programming world, "functional" is a type of programming language, so why does a package manager call itself this?

The stars aligned recently, and I found myself with free time, curiosity, and sufficient knowledge of software management to see that this project has high potential value in the software development process. After playing with it for awhile, I found it was difficult to use. I still felt the value, but it was covered by layers of hacks and patches. It's main problem was that it lacked consistent design. Why was that?

## Learning Nix by Playing with NixOS

To start learning Nix, I chose to dive to the deepest part of the project, which was NixOS, a Linux distrubution based on Nix. Now, not only is Nix its sole package manager, but it also uses Nix to generate every file on the Operating System. I started by playing with the OS-level Nix code, which consists of modifying a single configuration file to add and remove software to the OS, such as web servers, daemons/services, desktop and window managers, and so on. I was fascinated by the idea of controlling every aspect of my OS from a single configuration file.

I discovered a configuration option that didn't behave as I expected, related to enabling SSL on a web server. I decided to help the project by modifying the NixOS modules to mitigate the damage of this misconfiguration for other users. NixOS guides and documentation is quite poor, so progress was slow. After many attempts and much time spent, I finally fixed the problem I found. It was quite painful, and my pull request is still not merged into the community repository. Ugh.

### Using Nix Package Manager

Playing with OS-level configuration was powerfull, but modifying it is quite painful. I decided to leave OS-level configuration alone for awhile and move my attention to the other part of Nix, which is the Nix package manager.

Using the Nix package manager is relatively straight-forward. Install a software package, such as Firefox, by using the `nix-env -i firefox` command, and remove a package by using the `nix-env -e firefox` command. After using the install command, I watch hundreds of lines of text fly by as Nix works hard on my behalf to build and install it. It's quite magical and inspiring. At the end, I have a perfect installation of Firefox. It is amazing how everything lined up so perfectly. Alright, what next?

### Preparing my OS for Development

As a developer, I use an OS to develop software. I have been wanting to write a Rails app and deploy it to Heroku, so I started reading Heroku's guide to writing and deploying a Rails 4 app. It instructs me to install some software: Ruby, Rails, Git, Heroku client, Foreman, and a few other small things.

#### Installing Heroku Client

Alright, let's start with the Heroku client. I want to continue using Nix, so I want to install it with Nix. However, after searching for Heroku tools in the Nix central repository, I see that nothing has been packaged. Bummer. Well, I'll just package with my own two hands, then. I delegated most of the build process to the app's quick-install script. Then, I had to customize the configuration and installation steps bit to ensure it fits into the Nix directory structure. This was not too difficult. I believe I have this mostly working, and I added this to my fork of the NixPkgs repository to share when I finish and test it.

#### Installing Heroku Toolbelt and Dependencies

After that, I saw that the Heroku Toolbelt includes Foreman, so I started trying to package that. This became very complicated when I saw that it includes Puppet as a dependency. Puppet is not yet packaged with Nix, so I had to also package this with Nix. I found the "build from source" instructions on the Puppet website and followed them as I added the steps to the Nix expression's build and install phases. The Heroku client is a simple Ruby script, but Puppet consists of a client app, a server app, and has a separate custom app or two as dependencies. This was *very* complicated and my experience and knowledge of building OS-level software is low. I eventually gave up.

Some people in the Nix community suggested that I use the Nix-Gem interface to install the Foreman Gem, rather than building it from source. This idea was completely foreign to me, and their ideas had no supporting documentation and sounded like hacks. This made me feel like I completely wasted my time by trying to create a Nix package for it, and I lost motivation to continue.

I started asking myself, what is the right way to do this? What is "The Nix Way"? I don't want to spend another three weeks on this problem before discovering that the Nix Way is completely different.

## A Better Way to Start Using Nix

After spending lots of time thinking about why I'm having so much trouble with finding the value in Nix, I think I've found an answer.

Until now, I've been using Nix to set up and manage software on my OS. This means adding software to my user's global account, useable from anywhere.

When I was reading about Nix, its most interesting feature is package visibility. I was focused on using this feature of Nix to manage development environments. My goal was to create a process to easily switch from one set of tools to another set. For example, I want to easily change from Ruby 1.9 to Ruby 2.0 to Salesforce. When using one tool set, the other tool sets are invisible. This sounds really interesting to me. While I may still be able to use Nix to manage tool sets, I have seen a better way to start realizing the power of Nix.

### Nix as a Dependency Manager

After reviewing Zef's [Setting up Development Environments with Nix post](http://zef.me/5966/setting-up-development-environments-with-nix) for the Nth time, I've realized that his method of using Nix is so much better. What does Zef do differently from me? While I wanted to use Nix mainly to install tools into my OS, Zef is using Nix mainly to manage his software application's dependencies.

Managing an app's dependencies, this is truly Nix's raison d'être. Here's the idea: we start by automating the installation of our app's libraries. From there, we write and share only the code we build on top of these libraries. This makes sense, right? Any sufficiently useful application is built on top of other people's efforts. If we can write a file which sufficiently defines a specification of other people's software we build on, we can use a tool to manage their installation so a new user or developer can instead focus on using our additions, our software. It makes so much sense.

The concept makes so much sense, why is this not taught in college or used in the industry? Well, the software development industry *does* understand this concept, but they use different terms. What does the industry call code written by other people which we use in our app? This is called a "dependency" of our app. And what does the industry call a dependency which provides sufficient information in a format required by a package manager? This is called a "package".

#### Domains of Dependency Management

So, when we look for a tool to manage our app's dependencies, we are looking for a "package manager" or a "package management tool". I don't like these terms. Because this term doesn't connote "dependencies", the tools are allowed much more freedom. With such loose terms, these tools may not solve the entire problem. Instead, their mission may be satisfied by simply copying and moving files around.

I'm not familar with every package manager, but my experience with Nix, both its easy and difficult parts, leads me to believe that Nix properly handles the problem of dependency management, or package management if you prefer. A software developer will recognize that many software tools advertise themselves as the solution to this problem. They advertise "dependency resolution", "dependency management", "package management", "dependency injection", and so on. What does this tell us? Either this problem is not well-understood, difficult to solve, or the problem exists in many domains of software development. I believe the latter is the most common case.

A dependency manager must exist for each platform on which software is built. For example, OS-level applications use package managers like Aptitude, language-level applications use dependency managers like Rubygems and NPM, internet browser-level applications use dependency managers like Jam and Bower, mobile device package managers like APK and iOS App Store, and application-level package managers like Emacs' Package.el and Sublime Text's Package Control.

Nix is currently used for many of these domains. Until now, the domains of software in which I have used Nix are OS-level configuration management and OS-level package management. Using Nix for OS configuration management is workable, but I found it painful to use due to its many rough edges, such as severe lack of documentation and poor interface design.

## Managing Rails App's Dependencies with Nix

When I consider using Nix as a language-level dependency manager, I am very hopeful. It looks like this domain uses only "The Good Parts" of the Nix project. Let's try to create a Rails app which uses Nix to manage its dependencies.

### Starting With Basic Nix Package

Zef seems to use Nix correctly, so I'll follow the path he made. Using Zef's [NodeJs-Nix app's Nix expression](https://github.com/zefhemel/nodejs-nix/blob/master/default.nix) as a guide, I created this empty Nix expression to start my new Rails 4 app.

        ### ~/dev/rubyHeroku/default.nix ###
        { pkgs ? import <nixpkgs> {} }:
        let
          stdenv = pkgs.stdenv;
        in rec {
          app = stdenv.mkDerivation {
            name = "test-rails4-heroku";
            src = ./app;
            buildInputs = [  ];
            phases = [  ];
            meta = {
              description = "A test Rails 4 application to deploy to Heroku.";
              platforms = stdenv.lib.platforms.linux;
            };
          };
        }

We want our app's build to never fail as we move towards our goal, so let's build our app now.

        $ nix-shell -A app

My first build failed because Nix couldn't get the package's source code, which I specified is in the `./app` directory. This directory didn't exist initially, so I created it and built the app again, which was successful.

### Questioning the `mkDerivation` Function

This initial Nix expression isn't correct, however. We are using the `stdenv.mkDerivation` function to build this app, which is composed of standard steps of building Linux software. Linux software commonly uses the GCC to produce runnable code, and uses AutoConf and CMake to produce a build and install script which works on any platform. As a result, this Nix function will use AutoConf and CMake commands, which include `./configure`, `make`, and `make install`.

Our app is written in Ruby, which doesn't use a build process like this. So, rather than heavily customizing the `stdenv.mkDerivation` function, we should create a new Nix function which creates a derivation for Ruby applications. Let's call this new function the `rubyDerivation` function or the `rakeDerivation`, which assumes the "mk" part of "mkDerivation" refers to Make, the Linux build process.

### Creating a `rakeDerivation` Function

If I recall correctly, Ruby already has a build tool, called Rake. I believe the name of this tool is a portmanteau of "Ruby Make". Because the `mkDerivation` function is based on CMake, I wonder if we can create a `rakeDerivation` function which is based on Rake.

Let's look at an [example Rails application](https://github.com/heroku/rails_12factor) which Heroku recommends. This Rails app has a `Rakefile`, which implies that Heroku's Rails apps use Rake. But, [its Rakefile](https://github.com/heroku/rails_12factor/blob/master/Rakefile) is nearly empty. Is this really a proper Ruby build script? Well, Ruby doesn't need to be compiled, so maybe this is correct. But, when I check the directory of my previous Rails 4 installation on another machine, I see that the Rakefile requires the `config/application.rb` file. With that installation, executing the `rake -T` command displays dozens of administrative tasks, such as asset, database, and test management. I don't, however, see a "build" or "install" task. Well, maybe Rake won't be useful, but for now we'll continue by following Heroku's guide.

### Following Rails 4 Quick Start Guide

#### Adding Ruby as a Dependency

The [Local Workstation Setup](https://devcenter.heroku.com/articles/getting-started-with-rails4#local-workstation-setup) section of Heroku's guide says we must install Ruby and Rails installed, in addition to the Heroku Toolbelt. I want to use Ruby 2 and Rails 4. Let's start by specifying Ruby as a dependency in our app's Nix expression.

        ### ~/dev/rubyHeroku/default.nix ###
        ...
        in rec {
          app = stdenv.mkDerivation {
            ...
            buildInputs = [ pkgs.ruby2 ];
            ...
          };
        }

#### Adding Rails as a Dependency

Next, let's add Rails as a dependency. It looks like Rails isn't packaged by Nix, as I don't see it in the `all-packages.nix` file. Or is it? After asking the Nix community, I learned about the query operation's `-P` flag, which will make a list of all available derivations and return their complete attribute paths. Let's use this to see if any derivation includes the "rails" word.

        $ nix-env -qaP | grep rails
        nixos.pkgs.rubyLibs.autotest_rails   ruby-autotest-rails-4.1.2
        ...
        nixos.pkgs.rubyLibs.rails   ruby-rails-4.0.0
        ...

Awesome! Rails 4 is already packaged with Nix! Lucky! We can use the `pkgs.rubyLibs.rails` attribute path to get the Nix package.

        ### ~/dev/rubyHeroku/default.nix ###
        ...
        in rec {
          app = stdenv.mkDerivation {
            ...
            buildInputs = [ pkgs.ruby2 pkgs.rubyLibs.rails ];
            ...
          };
        }

Wait a minute. Gems are not part of the Nix packaging system, so how does this work? How does one write a Nix expression to install a Gem? I was curious, so I spent many hours tracing the Nix expressions which implement this functionality, which located in the `/development/interpreters/ruby` directory. This directory has three files, `rubyGems.nix`, `generated.nix`, and `libs.nix`, which use Nix expressions to magically install Gems. I don't like magic. I wish I could explain to a Nix noob how to declare an unpackaged Gem as a dependency, but with the current system, I could not. I propose an alternative design in the following section.

##### Exploring a New Idea: Language-Owned Dependency Resolution

Which solution do I think is better than using this magic Nix expressions for Gems? Well, in this case, the Ruby community has its own system for dependency resolution, which is the Rubygem and Bundler packaging systems. Ruby isn't unique in having this facility, almost every programming language has its own way to obtain libraries from the language's community, its own dependency resolution system.

Confusion is created when a Ruby developer wants to use two separate dependency resolution systems. Should we repackage every Gem with a Nix expression? Adopting this solution implies a Ruby on Nix developer must leave the Ruby community, because the Ruby community relies *very* heavily on Rubygems and Bundler. Also, some Ruby services require the use of Gems, such as Heroku's deployment platform.

If we don't repackage every Gem, can we tell Nix to execute a few shell commands as part of its dependency resolution phase? That is, after it builds the Nix packages specified in the `buildInputs` attributes and configures them, and before running the standard build phases, can we ask Nix to run the `bundle install` command? If we place this command in the `buildPhase` attribute, which isn't part of Nix's dependency resolution phase, the Gems won't be installed when we use the `nix-shell` command to work on the app.

Ruby 2 includes Rubygems, so we don't need to explictly include the Rubygems package. We want to execute the `gem install rails --version=4.0` command as part of the package's dependency resolution. Nix uses "input" to mean "dependency", because Nix is designed in terms of "derivations", not software. Leaving the attribute's name undecided for now, Nix should use it's value as a shell command or shell function, and execute it after it finishes building and configuring the package's Nix package inputs.

See the following example.

        ### ~/dev/rubyHeroku/default.nix ###
        ...
        in rec {
          app = stdenv.mkDerivation {
            ...
            buildInputs = [ ruby2 ];
            # runtimeInputs = "installGemsPhase";
            # - or -
            # manualBuildInputs = "installGemsPhase";
            # - or -
            # postHookBuildInputs = "installGemsPhase";
            installGemsPhase = ''
              gem install rails --version=4.0
            '';
            ...
          };
        }

#### Adding Bundler as a Dependency

Bundler is another Gem, so we can use the same method as finding Rails' to find Bundler's attribute path.

        $ nix-env -qaP | grep bundler
        nixos.pkgs.rubyLibs.bundler   ruby-bundler-1.3.5

Then, add Bundler to our Nix expression like this.

        ### ~/dev/rubyHeroku/default.nix ###
        ...
        in rec {
          app = stdenv.mkDerivation {
            ...
            buildInputs = [ pkgs.ruby2 pkgs.rubyLibs.rails pkgs.rubyLibs.bundler ];
            ...
          };
        }







By classifying every component of an app as a dependency, we can use Nix's most reliable feature to install them, its package manager. If we are a software app, what is a dependency? A dependency is code we use, but it is owned by someone else. Because we must use it, we must install it and correctly obtain references to it. Because it must be installed, it must also be configured and built. Nix does these all, and it does these completely. In particular, it's most unique and unique feature is that it clears the terminal environment before building each package. This means that each package can use nothing unless it's declared - no library, application, or source code.






