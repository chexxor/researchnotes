
# Installing NixOS on Windows in VirtualBox

## Foreword

Awhile back, I heard about an awesome software package manager, named [Nix](http://nixos.org/nix/). How is it different and interesting? Others package managers don't sandbox their packages and dependencies, which leads to modifying shared files/libraries/programs. Nix ensures this never happens by making every dependency immutable. If a library/program on which we depend is updated, a whole new package is installed for us to use. This method leaves the old dependent library/program untouched, which ensures other packages which use it are unaffected.

But that's inefficient, you say? Yes, you may accumulate dozens of versions of a single package on your machine, but old, unused packages can be garbage-collected later. Many programming languages do it, and also applications like Git. Nix is like Git in another way. Git has dozens of versions of a file, but only one is used at a time. It does this by using a commit object which lists pointers to a single version. Nix is similar. It uses a profile/generation directory to select a single version of each package to use. What's more, it ensures that each package always points to exactly the packages it required at build-time, which ensures its behavior never changes and it never breaks when upgrading. Using this strategy, Nix enables you to easily have many versions of a package on a system.

Nix seems like a well-designed system. Curious and want to try it? You can install and use Nix and keep using your existing package manager, so it's safe to play to with. How to install it? It's not super-simple to install, but you shouldn't have problems if you follow the [Nix manual's Quick Start](http://nixos.org/nix/manual/#chap-quick-start).

After testing out Nix as a package manager, I decided I wanted to use it exclusively for awhile, to see how it works in practice. Can I use it as the exclusive package manager for Ubuntu? I don't know. I believe a Linux distro's identity is largely tied to its package manager, so I assume it's not possible. So how to use it exclusively? The Nix community created and maintains its own Linux distribution named [NixOS](http://nixos.org/nixos/), which uses Nix as its package manager. It seems to be what I'm looking for, so I'll install it to try it out.

Following is my log when installing, learning, and playing with NixOS in a VM.

## Get NixOS ISO

In a web browser, navigate to the [NixOS downloads homepage](http://nixos.org/nixos/download.html) to choose an appropriate ISO to download.

A more manual means is to choose an ISO directly from the [NixOS build server](http://hydra.nixos.org/jobset/nixos/trunk#tabs-jobs) to download. I chose the "iso_graphical.x86_64-linux" build.

The ISO is about 770 MB, so the download might take some time.

## Create New VM in VirtualBox

After you have a NixOS ISO, create a virtual machine to use it. I am using Windows 8. I believe Microsoft's Hyper-V is the best option for Windows users, but it seems like this feature is only available in Windows 8 Pro edition. As I have Windows 8 Basic, I must use another solution. I chose VirtualBox.

Install VirtualBox

- Navigate to the [VirtualBox Downloads page](https://www.virtualbox.org/wiki/Downloads).
- Download, install, and start VirtualBox for Windows hosts. (I installed VirtualBox 4.3)

Create a VM in VirtualBox

- In VirtualBox, click the "New" button and use the following settings:
    - Name => "NixOS" (name your machine anything)
    - Type => "Linux"
    - Version => "Other Linux (64-bit)"
    - Memory Size => 1 GB (I tried 512 MB first, but found this caused `nixos-rebuild` to run out of memory and fail)
- Create a virtual hard drive and use the following settings:
    - Hard drive file type => "VHD" (Because VHD is Microsoft's format, but VDI or VMDK also work)
    - Storage on hard drive => "Dynamically allocated"
    - File location and size => "8.00 GB" (The NixOS ISO is nearly 4 GB, so 8 GB should leave enough room to play)

## Install NixOS in VM

### Create a New VM

When your NixOS ISO download is complete, load it into your new VM in VirtualBox.

- In VirtualBox, select your new VM. Mine is named "NixOS".
- Click the "Settings" button and customize the following settings:
    - System > Processor => "Enabled"
    - Display > Video > Video Memory => "64 MB"
    - Display > Video > Extended Features > Enable 3D Acceleration => "Enabled"
    - Click "OK" to save and return to the main window.

### Load NixOS ISO on New VM and Log In

Now we must choose an ISO to load when then VM starts.

- Select your VM and click the "Start" button.
- If a window does not automatically appear to ask you to select one, use the menu: Devices > CD/DVD Devices > Choose a virtual CD/DVD disc file...
- Choose the NixOS ISO and click "OK".
- Reset the VM by using the menu: Machine > Reset

After the machine boots, it should find and boot from the NixOS ISO. It starts the Linux kernel and a bunch of other stuff. When this is complete, the screen says "Welcome to NixOS". Now we must use this in-memory instance of NixOS to install NixOS to the VM's virtual hard disc (VHD). The [NixOS manual](http://nixos.org/nixos/manual/) has helpful instructions, particularly the ["Commands for installing NixOS on /dev/sda" example](http://nixos.org/nixos/manual/#ex-install-sequence).

Log in to the in-memory instance of the NixOS live CD:

        root

(optional) Type `start display-manager` to see a Windows-like desktop.

### Prepare Virtual Hard Disc

NixOS doesn't have a user-friendly GUI to install, like Windows, Mac, or Ubuntu, so we must use the command line. We must do three things: Create a hard disc partition, format it as ext4, and mount it.

Use the terminal to enter the following commands to prepare a partition in the VHD:

        fdisk /dev/sda
        # In sequence:
        m, n, p, 1, <enter>, <enter>, w
        mkfs.ext4 -j -L nixos /dev/sda1
        mount LABEL=nixos /mnt

### Create NixOS Configuration

In other Linux distributions, a user will first log in to the OS, then install packages to obtain apps. This is a problem. Today's common packaging system doesn't ensure packages cooperate. While modifying shared files may be desireable in some cases, it is undesirable if a user wants a deterministic OS.

NixOS doesn't use Nix in the same way as Nix packaging system. How do you mean? Well, the big challenge in NixOS is to generate a standard set of system files, but ensure they are slightly customized to cooperatively comply with a declared configuration. Here, a "declared configuration" means, mostly, a set of options/arguments which are used by the NixOS's complex build script.

Similar to how the Nix packaging system manages a single package in a single place, `default.nix`, NixOS manages an OS configuration in a single place, the `configuration.nix` file. NixOS will compile this file to obtain the set arguments passed to the NixOS build process.

Why do I mention this now? We must define the system configuration now, before we install NixOS. We can manually create and modify this `configuration.nix` file, or we can generate it using another utility program. On my first attempt to install NixOS, I modified several arguments to create a rather complex initial installation. The installation failed and it was rather difficult to debug. Eventually, I deleted the partition and reinstalled. So, my suggestion is to start from a simple configuration which works, then incrementally add features. So, to start, we will generate a simple configuration file to use to install NixOS.

Use the following commands to create the `configuration.nix` file in the new hard drive partition we created.

        mkdir -p /mnt/etc/nixos
        nixos-generate-config --root /mnt

Add the following variables to the `configuration.nix` file which was generated. If you have problems, ensure any other auto-generated Nix files in the `/mnt/etc/nixos/` directory don't redefine these values.

        ### /mnt/etc/nixos/configuration.nix ###
        # Ensure the following lines are not commented:
        boot.loader.grub.device = "/dev/sda";
        services.virtualbox.enable = true;

### Install NixOS

After you finish creating and modifying the `configuration.nix` file, install NixOS with this command.

        nixos-install

Now wait for the Nix program to set up NixOS. It will download, build, and install all packages which are required to create the system configuration you declared. This might take awhile, depending on the complexity of the system configuration.

To use this new instance of NixOS, reboot the system.

        reboot

## Boot into NixOS

After rebooting the VM, boot into the hard disc. We will be shown a list of every NixOS configuration we created. Right now there's only one, but in the future you may see many. Why many?  A new entry will be added each time we modify the `configuration.nix` file and execute the `nixos-rebuild` command to reconfigure the OS. Choose the default entry to start NixOS.

After booting my instance of NixOS, the terminal asks me to login. Login as "root", which still has no password. I installed NixOS with a GUI, so where's that? I believe we have to rebuild the OS to include those packages.

## Add KDE to NixOS Configuration

Let's rebuild our NixOS configuration to include the KDE desktop packages. We don't have to boot from the NixOS install CD, we can reconfigure and rebuild the NixOS instance even if we are logged in and using it. After logging in, modify the `configuration.nix` file and modify the following options.

        ### /etc/nixos/configuration.nix ###
        # Ensure the following lines are not commented:
        services.xserver.enable = true;
        services.xserver.displayManager.kdm.enable = true;
        services.xserver.desktopManager.kde4.enable = true;

Save that file and close it, then rebuild NixOS.

        nixos-rebuild switch

After all the packages are downloaded, built, and installed, we will find our newly-created configuration listed in GRUB as the default option. When I did this, I believe it built lots of stuff from source, so it took a long time. After it finished, you may see the KDE login screen without needing a reboot. Nice! If not, just reboot.

## Add Initial Users

After booting into the the KDE system configuration, I wasn't able to login. Why? The system had only one user, `root`, and the default KDE login system doesn't allow root logins. We must add some users, which we do after logging in as root. To do that, we must use the terminal. There's a button on the KDE login screen which allows us to return to a terminal.

From here, we are expected to manually create users by using `useradd -m alice` and `passwd alice` commands. But, I want to automate this. If I use NixOS to sandbox processes, such as a web server, they should run under separate user accounts. I want these common user accounts to be created when the system is first created.

Nix packages can't create users, only NixOS's `nixos-rebuild` command can do this. _(A Nix package's build script can't create users?)_ The [NixOS user management docs](http://nixos.org/nixos/manual/#idp324464) says this about automatically creating users command:

> Warning: Currently declarative user management is not perfect: nixos-rebuild does not know how to realise certain configuration changes. This includes removing a user or group, and removing group membership from a user.

That's ok, I only care about ensuring the user account exists, I won't often delete them. Right now, I'll just add a user for myself. Later, I can add "apache", "nginx", or "git" users. To create users using Nix's declarative style, we modify the `configuration.nix` file to contain the following lines.

        ### /etc/nixos/configuration.nix ###
        users = {
            extraGroups = [ { name = "chexxor"; } { name = "dev"; } ];
            extraUsers  = [ {
                description     = "chexxor";
                name            = "chexxor";
                group           = "chexxor";
                extraGroups     = [ "users" "wheel" ];
                home            = "/home/chexxor";
                createHome      = true;
                useDefaultShell = true;
            } ];
        };

Note: I did not specify a password. As a user on the `#nixos` IRC channel told me, the `configuration.nix` file is insecure/unprotected. Besides, if I want to share this file with friends, I don't want them to see my password.

(Ideas: 1) Specify a password here to act as default, but remember to change it later. 2) Move the user initialization to another Nix file and import it as a module. This way, if I pass this file to a friend, they can update the users module to add themselves.)

Now, run `nixos-rebuild switch` again to realize the new user account. If we look at the home directories, run the `ls /home` command, we can see that my user account has been created. Nice!

How do we return to the KDE login screen? The only way I found was to `reboot`. Let me know if you find a better way. From here, I still could not log in with my new user account because it still had no password. To add a password, exit to the terminal, log in as root, and use the `passwd chexxor` command to set a password, then `reboot` the system again to login.

## What's the Goal?

This seems like a lot of work to set up an OS. Actually, it isn't too much more difficult than setting up an Ubuntu or Windows system. How so, you say? While they do have nice GUIs and wizards which install the OS, after it's installed, they still need to be customized. By using NixOS, this customization time is spent once, then we can recreate it many times.

### Alternative Options

But still, is this useful? Are there no better alternatives? Here are some ideas for uses.

### Completely Recreate Production Issues Locally?

What's so interesting here? We can easily recreate a development environment. From a single `configuration.nix` file, we can deterministically create an entire OS! Because it's deterministic, we can confidentally and completely recreate a production environment locally to debug a production issue. Likewise, we can be confident that a continuous integration server is testing software in the same environment as production.

Alternative solutions?  VMs and continuous integration servers are used by many teams to recreate production environments. Is Nix better at this than VMs? Unsure.

### Recreate Legacy Applications?

What if an old application breaks in production? If it runs in a NixOS environment which has a `configuration.nix` file, I can use that to recreate it in a local VM to attempt to recreate the bug locally. This is better than manually setting up web servers, configuring firewalls, and installing old versions of a language or IDE.

Alternative solutions? Language-specific package managers and OS-specific package managers are currently used. Is Nix better at this? Unsure. But using Nix as a unified packaging tool seems like an improvement.

### Highly Portable Application Code

A Nix-packaged application is guaranteed to be unaffected by existing packages on a system. This means it is unlikely to accidentally have an undeclared dependency, which is code not managed by the package manager.

I want a super-simple way to clone a software project from GitHub, install all its dependencies, and then run it, all without failing.

Alternative solutions? PaaS, application containers, and language-specific package managers are currently used to make applications more portable. Is Nix better at this than containers? Unsure.

## What's Next?

Can we skip NixOS and instead use only the Nix package manager to mostly accomplish thes same goals? What can NixOS do that Nix packages can not?

Suppose I give my Nix-packaged application to a friend who wants to work with me. Can Nix help him quickly recreate my development environment, including application code and tools? Can Nix do that? Or only NixOS? Or a combination?

For now, I will continue to test out NixOS as a development OS. To test more system configurations, I need to know all the options. Here's how we can do that:

- Run the `man configuration.nix` command.
- Read the source code. The `module-list.nix` file is the list of configurable modules. See the [`nixpkgs/nixos/modules` directory](https://github.com/NixOS/nixpkgs/tree/master/nixos/modules).
- Reference other people's `configuration.nix` files. Here are some existing files to check out:

    - [Configs of NixOS maintainers](http://lastlog.de/wiki/index.php/Nixos_configuration.nix)
    - [VirtualBox, Haskell, XMonad](https://gist.github.com/dysinger/5054240)
    - [VirtualBox, Nix manual on TTY8](https://github.com/ecarreras/nixos_dotfiles/blob/master/configuration.nix)
    - [Many system packages](https://github.com/bleuscr/NixOS/blob/master/configuration.nix)
    - [Interesting xserver](https://bitbucket.org/goibhniu/7rl/src/bb5435ed9c69eac5b3fe097891ebae4789b84289/nixos/configuration.nix)
    - [Reasoning and two modules for HW or VM](https://github.com/i-GrCe/nixos-configurations)
    - [Complex and detailed modules](https://github.com/aszlig/nixconf)
    - [Nginx and complex modules](https://github.com/offlinehacker/offline.x-truder.net)
    - [Many Nix files for dev](https://github.com/coreyoconnor/nix_configs)
    - [NixOS configurations for nixos.org and its servers](https://github.com/NixOS/nixos-org-configurations)
    - [Organized modules](https://github.com/aristidb/dotfiles)
    - [Basic, but organized modules](https://github.com/drvirgilio/nixos-config)
    - [Modules for many systems](https://gitorious.org/goibhnix)


