
# How Nix Installs a Package

"What happens when I use Nix to install a Nix package?" is a question that an average Nix user may ask. It's a fair question, as Nix is a fair bit different from anything else. "How is my package integrated into the end-user's environment?" is a question that a Nix package maintainer may ask. This is also a fair question, as Nix has a special way of enabling packages for use after they are installed which is unlike anything else.

The goal of this article is to shed light onto the package installation and uninstallation process. If you still have questions after reading this, please leave it in the "Questions?" section at the bottom.

Package maintainers should note that resolving a package's build inputs/dependencies is not covered in this article. To read about dependency resolution, read the "How Nix Builds a Package" article. (!!! Explain this process a bit, and link to that page.)

## Package Installation Process

The question is, what does Nix do when we execute the `nix-env -i` command? We might think this is an easy answer: It builds the package and adds it to the current environment. The package installation process isn't quite so simple. Where do the complications arise? Nix's implementation of user environments are entries in the Nix Store, and Nix never modifies contents of the Nix Store, which means that an entirely new entry which contains the new package must be created.

Knowing that an entirely new user environment will be created, the answer becomes relatively simple. Nix parses the `nix-env -i` command's flags and options, loads the package from a variety of sources, then builds a list of all packages which the new user environment should contain. It builds this list of packages by first adding the new packages, specified in the `nix-env -i` command, then adding all currently installed derivations. Then, the Nix builder once again builds all of these packages, which is fast, as all packages in the previous user environment are already built. To read more about Nix's build process, see the "How Nix Builds a Package" article.

Now that all of its packages are built, the new user environment is built. Nix builds a user environment using a package called `buildenv`. One of the parameters of the `buildenv` package is a list of all the Nix packages it should include. This parameter is called a "manifest", and it is a list which contains the full output path of each package. This package's builder is a Perl script which creates simlinks in the output directory to the contents of each derivation listed in the manifest. In addition to the derivations explicitly listed in the manifest, each derivation listed in their `nix-support/propagated-user-env-packages` files also are also included to be symlinked. The following directories, however, are not symlinked to be included in the user environment: `propagated-build-inputs`, `nix-support`, `perllocal.pod`, `info/dir`, `log`.

!!! Find sources and add links in-line.


## Package Uninstallation Process





I traced the code and summarized the installation process below.

- The `nix-env -i` command's flags and options are parsed. [source](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/nix-env/nix-env.cc#L82)
- The specified sources are queried to create the user-specified derivation objects. [source](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/nix-env/nix-env.cc#L340)
- A list of derivations for the new user environment is created by first adding the user-specified derivations, then adding all currently installed derivations. [source](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/nix-env/nix-env.cc#L497)
- All derivations in the new user environment are built. [source](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/nix-env/user-env.cc#L48)
- A manifest of the new user environment, which consists of the output paths of its derivations, is created and saved in the Nix store.
- This manifest is passed as an argument to the derivation defined by the [`nix/buildenv.nix`](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/corepkgs/buildenv.nix) Nix expression.
- The `buildenv` derivation's builder is a Perl script. It creates symlinks in this derivation's output directory to the contents of each derivation listed in the manifest, as well as to the contents of all derivations listed in all dependent package's `nix-support/propagated-user-env-packages` files. The following directories are not included in the user environment: `propagated-build-inputs`, `nix-support`, `perllocal.pod`, `info/dir`, `log`.

## Methods of Interest

[run](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/nix-env/nix-env.cc#L1293) - Find the functionality which corresponds to the specified options and execute it.

[opInstall](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/nix-env/nix-env.cc#L529) - Install the derivations specified in the opArgs string according to the options specified in the opFlags string by using the `installDerivations` method.

[parseInstallSourceOptions](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/nix-env/nix-env.cc#L82) - Parse the package's source option from the specified args string and store them in the specified globals object.

[installDerivations](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/nix-env/nix-env.cc#L468) - Create a new user environment which adds the specified derivations, found by analyzing the specified args using the `queryInstSources` method, to the current user environment by using the `createUserEnv` method.

[queryInstSources](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/nix-env/nix-env.cc#L340) - Find derivations and create `DrvInfo` objects from the specified source. Be default, derivations are found using the `loadDerivations` method. If the source is specified as a Nix expression, the `loadSourceExpr` method is used. If a path is specified as a Nix expression, a `DrvInfo` object is constructed for each path specified in the args.

[loadDerivations](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/nix-env/nix-env.cc#L177) - Load all derivations found at the specified attribute path prefix in the Nix expression which is found in the file at the specified Path. Derivations which don't match the specified system filter are removed. By default, the specified Path is `~/.nix-defexpr`.

[EvalState::evalFile](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libexpr/eval.cc#L453) - Evaluate the Nix expression located in the specified file and store the parsed Nix expression in the specified `Value` object.

[getDerivations](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libexpr/get-drvs.cc#L184) - Create `DrvInfo` objects by recursively searching the specified Value, an evaluated Nix expression, for all attributes named "derivation", then add them in the specified `DrvInfos` object.

[getDerivation](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libexpr/get-drvs.cc#L126) - Create a `DrvInfo` object by searching the specified Value, an evaluated Nix expression, for an attribute named "derivation", then add it to the specified `DrvInfos` object.

[createUserEnv](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/nix-env/user-env.cc#L36) - Create a new user environment which consists of the specified derivations as elements. First, all invalid derivations are built. Then, the new user environment is created by passing a list of all derivation's outputs, called its manifest, to the derivation defined by the [`nix/buildenv.nix`](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/corepkgs/buildenv.nix) Nix expression. This derivation's builder is a Perl script which creates symlinks from this derivation's output directory to the contents of each derivation listed in the manifest, as well as the derivations listed in their `nix-support/propagated-user-env-packages` files. The following directories are not included in the user environment: `propagated-build-inputs`, `nix-support`, `perllocal.pod`, `info/dir`, `log`.



## Questions?


