 
# What is a Nix Package?

A software package is a file which describes how to obtain a working piece of software. How does Nix implement this behavior? The creators of Nix deeply consider this question, how does one reliably produce a working piece of software, and their answer considers the very nature of software development.

## Derivation: The Nature of Software

The nature of software is that no piece stands independently. Rather, all software is built on other software, which are called frameworks, platforms, or libraries. We have a word to describe this kind of relationship, a thing which builds on another thing - "derivative". All software is a derivation of existing software.

### Useable Software

Nix implements this idea as a function named `derivation`. This "derivation function" will produce a useable piece of immutable software by passing everything it needs to run as parameters. What does a piece of software need to be useable? Source code, it needs to be compiled.

When a software's dependencies and build process are explicitly defined, they can be passed to the derivation function to reliably produce the software.

### Derivations as Packages



### Keeping Modularity

In the interest of modularity and reducing scope of code ownership, we want to stay separate from the software we build on. That is, rather than manually copying or installing another project's software, we would rather reference its APIs and its implementation be automatically installed. References to another project's software could be in source code form or in binary form. In source code form, we might want to compile our code with a shared library. In binary form, we might want to dynamically link with libraries or use programs and services while running our program.

### Descriptions of Modular Software

If we want to use software owned and managed by someone else, it must exist locally when we either build or run our software. We don't want to own and manage someone else's code, so we shouldn't include it as part of our code base. We still need their software to build or run ours, so we need to include their software's installation as part of our software's build process and install process.

What is the scope of a tool which must All good software uses scripting languages to automate their software's build and install process, so we need to ensure their software is installed just before we execute the build script.


### Describing Software by Defining Derivations

What should a package manager do? A package manager should be able to install any packaged application, where a package is a description of the software's build and install process. How can Nix be used to implement a package manager?

Nix implements an idea called a "derivation function", which is a function that produces a complete application by ensuring its dependencies are explicitly stated as function parameters. Once described, an application can be produced by simply evaluating its derivation function. To answer the question, Nix can be used to implement a software package by simply writing a derivation function for the packaged application.

Why not use the term "package" instead of the more unusual term "derivation"? While very similar, one could say that the terms are important because the details are important. A more practical answer is to say that Nix is an academic project, which means it implements scientific terms and ideas.

If we want to discuss packaging software, we must consider the application's complete description, including its dependencies. We use the Nix language to create a key-value set which describes the packaged software's dependencies and build scripts, then pass this software description to the `derivation` function.

### Creating a Derivation

What attributes does a `derivation` set have? The Nix builder specially uses the values of a few attributes, which are the `name`, `system`, `builder`, and `args` attributes. This is the minimum amount of information the Nix build system needs to build a piece of software. What will happen if we add more attributes to this set? Most of the complication of building software lies in configuring the compiler and installation options, which often reference dependencies or the platform. Instead of hardcoding these values 

http://nixos.org/nix/manual/#ssec-derivation

How is a `derivation` set used?


!!! To do: Heavily revise above.


A Nix package requires a few essential elements:

- Name of packaged software's resulting directory
- List of packaged software's dependencies, which must be Nix packages
- A program which will build and install the packaged software (e.g. Bash script, Perl script, or a program to execute from Bash, such as Make)
- A build file, which is passed to the build program
- Extra arguments to pass to the build program
- Environment variables which will be used during the build

## Derivations

When we are writing a Nix package, we are actually 

## Example Derivation Definition

What is a "derivation"? This term is used a lot in the Nix project, so actually seeing one will help us understand.

Look at the derivation produced by evaluating the `hello` package.

        Derive(
        [("out","/nix/store/d4ccnxjm73yiqg85nyyyqhybrhrqykn3-hello-2.8","","")],
        [
            ("/nix/store/cdgi55148mgkd5cac3pwg5crgvidn7ix-bash-4.2-p42.drv",["out"]),
            ("/nix/store/hxdq49zv0x3acq2mwqjlp6hjqbmp0a1r-stdenv.drv",["out"]),
            ("/nix/store/kzr1q8i10a4lpxgyjg4zpf76rh2mzjr8-hello-2.8.tar.gz.drv",["out"])
        ],
        ["/nix/store/9krlzvny65gdc8s7kpb6lkx8cd02c25b-default-builder.sh"],
        "x86_64-linux",
        "/nix/store/7mjplwq1r0agngqj2058hl5z8kb3w15g-bash-4.2-p42/bin/bash",
        ["-e","/nix/store/9krlzvny65gdc8s7kpb6lkx8cd02c25b-default-builder.sh"],
        [
            ("buildInputs",""),
            ("builder","/nix/store/7mjplwq1r0agngqj2058hl5z8kb3w15g-bash-4.2-p42/bin/bash"),
            ("doCheck","1"),
            ("name","hello-2.8"),
            ("nativeBuildInputs",""),
            ("out","/nix/store/d4ccnxjm73yiqg85nyyyqhybrhrqykn3-hello-2.8"),
            ("propagatedBuildInputs",""),
            ("propagatedNativeBuildInputs",""),
            ("src","/nix/store/nsk10m1gz7cnlzri9y5kxpyv7s9r5vi1-hello-2.8.tar.gz"),
            ("stdenv","/nix/store/3xxyrlzf3bj3mlqw1419j7lyzmpzqx0j-stdenv"),
            ("system","x86_64-linux"),
            ("userHook","")
        ]
        )

A derivation contains a lot of hashes and text, which is rather difficult to read. What does it all mean? Let's replace the data with descriptions and types to more clearly see what a derivation is.

        Derive(
            [drvId, out.path, out.hashAlgo, out.hash] // Derivation output :: DerivationOutput
            ,
            [inputDrv, [drvId]] // Derivation inputs :: DerivationInputs
            ,
            [drv.inputSrcs] // Source inputs :: Path Set
            ,
            drv.platform // Description of target platform :: String
            ,
            drv.builder // Builder program :: Path
            ,
            [drv.args] // Arguments for builder program :: String Set
            ,
            drv.env[name] // Environment variables :: String Pairs
        )

When Nix evaluates a Nix expression, the derivation file you see above will be produced. This derivation should contain a complete description of a useable piece of software. From a derivation, a useable piece of software can be "realized", which means built and installed.



### Edited stuff from wiki

== What is a Nix package? ==

A Nix package requires a few essential elements:

* Name of packaged software's resulting directory
* List of packaged software's dependencies, which must be Nix packages
* A program which will build and install the packaged software (e.g. Bash script, Perl script, or a program to execute from Bash, such as Make)
* A build file, which is passed to the build program
* Extra arguments to pass to the build program
* Environment variables which will be used during the build

== What is a Nix expression? ==

A Nix expression has all information required to build a piece of software. Generally, this means a link to the software's source code, a list of other software required to build and/or run it, and a scripts of shell commands required to compile its source code and install its binaries.

When a Nix expression is evaluated, Nix will use the software's SHA hash to find its pre-compiled binaries by searching for it in all binary caches registered with the Nix installation. If it isn't found, Nix will create the software's binaries by building them from source code, then installing them into the appropriate directory structure. Once complete, Nix will copy this resulting structure into the read-only Nix Store, which ensures it can be used as a reliable dependency for other software packages. This entry in the Nix Store created by a Nix expression is called a "derivation". Finally, Nix will add a link to this derivation to our environment, which makes it visible on the command line for us to use.

== Where can I get Nix expressions? ==

Writing a Nix expression can be quite a challenge. Luckily, many other people have shared their expressions by adding them to a central list, named NixPkgs, available on the [https://github.com/NixOS/nixpkgs/ NixPkgs GitHub repository]. If you installed Nix, you can register the NixPkgs repository by using the <code>nix-channel</code> command.



