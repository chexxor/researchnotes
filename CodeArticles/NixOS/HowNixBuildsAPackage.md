
# How Nix Builds a Package

When we execute the `nix-env -i myPackage` command, the Nix package installer does many things, such as parsing Nix expressions to build a derivation for the package. In this article, we will skip most of the steps and look only at how Nix creates the build environment and builds the packaged software.

## Derivation - A Software's Complete Build Recipe

In this topic, we are only interested in how Nix builds the software in a Nix package. When Nix starts building a package, the package's Nix expression has already been compiled into a "derivation". While a package's Nix expression is maintainer-friendly, the [derivation object](https://github.com/NixOS/nix/blob/7cf539c728b8f2a4478c0384d3174842e1e0cced/src/libstore/derivations.hh#L43) organizes the package's information into a format suitable to the Nix build process. To build software, Nix requires: a list of its dependencies, most notably the application's source files and dependant packages, its builder application and its arguments, and the expected state of environment variables.

To see an example, here's the derivation produced by evaluating the `hello` package.

        Derive(
        [("out","/nix/store/d4ccnxjm73yiqg85nyyyqhybrhrqykn3-hello-2.8","","")], /* Derivation output */
        [
            ("/nix/store/cdgi55148mgkd5cac3pwg5crgvidn7ix-bash-4.2-p42.drv",["out"]), /* Derivation inputs/dependencies */
            ("/nix/store/hxdq49zv0x3acq2mwqjlp6hjqbmp0a1r-stdenv.drv",["out"]),
            ("/nix/store/kzr1q8i10a4lpxgyjg4zpf76rh2mzjr8-hello-2.8.tar.gz.drv",["out"])
        ],
        ["/nix/store/9krlzvny65gdc8s7kpb6lkx8cd02c25b-default-builder.sh"], /* Source inputs */
        "x86_64-linux", /* Target platform */
        "/nix/store/7mjplwq1r0agngqj2058hl5z8kb3w15g-bash-4.2-p42/bin/bash", /* Builder program */
        ["-e","/nix/store/9krlzvny65gdc8s7kpb6lkx8cd02c25b-default-builder.sh"], /* Arguments for builder program */
        [
            ("buildInputs",""), /* Environment variables */
            ("builder","/nix/store/7mjplwq1r0agngqj2058hl5z8kb3w15g-bash-4.2-p42/bin/bash"),
            ("doCheck","1"),
            ("name","hello-2.8"),
            ("nativeBuildInputs",""),
            ("out","/nix/store/d4ccnxjm73yiqg85nyyyqhybrhrqykn3-hello-2.8"),
            ("propagatedBuildInputs",""),
            ("propagatedNativeBuildInputs",""),
            ("src","/nix/store/nsk10m1gz7cnlzri9y5kxpyv7s9r5vi1-hello-2.8.tar.gz"),
            ("stdenv","/nix/store/3xxyrlzf3bj3mlqw1419j7lyzmpzqx0j-stdenv"),
            ("system","x86_64-linux"),
            ("userHook","")
        ]
        )

## Setting Up the Software's Build Environment

Once we have a derivation, the Nix builder can do its thing. First, Nix must prepare the environment in which to build the software. This is a pretty straight-forward task: add all desired environment variables and their values to a list, then pass it to the [`execve`](http://man.cx/execve) function, which creates a thread that has only the specified list of environment variables.

it [creates a shell which has no environment variables](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libstore/build.cc#L1621), then [sets the environment state](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libstore/build.cc#L1657) expected by the software. The initial environment state [includes some hard-coded values](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libstore/build.cc#L1636), such as `HOME`, `TMPDIR`, and `PWD`, and the environment variables listed in the derivation, such `name` and `out`.

## Executing the Software's Builder

Now that the build environment is prepared, Nix can finally build the software. Every piece of software is built differently, so we rely on the software to provide a script or program which builds it.

Now, before directly continuing to build the software, Nix will first create a new OS thread in which to run the builder. This threading functionality is implemented in Nix by using the `fork` function, which is available on all [POSIX-compliant systems](https://en.wikipedia.org/wiki/POSIX#POSIX-oriented_operating_systems). If building in `chroot` environments is enabled, the `clone` function will be used, rather than the `fork` function, to create a new thread. The difference is that `clone` allows us to set up namespaces, such as a networking namespace. (To see the extra steps which occur to set up a `chroot` environment, see the following section.)

Finally, now that the build environment is prepared and is operating in a new thread, Nix [executes the derivations's builder and build script](https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libstore/build.cc#L2142) by using the `execve` function. The builder is the program specified by the package's "builder" attribute.

### Building in a `chroot` Environment

In the future, Nix will execute all builds on the Linux platform in a `chroot` environment. (Note that `chroot` is function unique to Linux, so this function will never be used on non-Linux platforms.) Let's see what extra steps occur when a package is built with the `chroot` feature enabled. 

Nix will create a new directory in the Nix store, named "{derivationPath}.chroot", to activate as a `chroot` directory. In this directory, a `/tmp` is created, as many builders need this, and `/etc/passwd`, `etc/group`, and `etc/hosts` files are also created and populated with records for `nixbld` and `nobody` users. Next, a network socket is set up as a loopback interface, the hostname is set to "localhost", and the domain name is set to "(none)".

Then, directories are bind-mounted. The directories listed in the `/proc/self/mountinfo` file, the `/proc` directory, the `/dev/shm` directory, and the directories listed in the `settings.dirsInRoot` and `buildInputs` variables, and mounts them using the `mount` function. If any of the values in the `buildInputs` variable are not directories, such as files, hard-links are created. Finally, the directory is activated as a `chroot` directory by using the `chroot` function.




When


Edited is the default builder called when installing a package?

When are package's dependencies resolved? (Input sources and input derivations.)
- See `addWaitee` - https://github.com/NixOS/nix/blob/a478e8a7bb8c24da0ac91b7100bd0e422035c62f/src/libstore/build.cc#L1041




