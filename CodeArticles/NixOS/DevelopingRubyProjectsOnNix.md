
# Ruby on Nix

I spent several hours consider this issue - Ruby on Nix.

I think that Nix is a great complement for Ruby.

- Nix can manage Ruby versions, which means no need for RVM or RbEnv.
- Nix can manage Ruby libraries, which means no need for Gem. Gem has problems with dependency management. It's issues are related to dependencies of dependencies, and storing all Gems in a single place. These problems motivated the creation of RVM and RbEnv, which are similar to Profiles/User Environments in Nix.
- Nix can manage Ruby apps. Create Nix expressions and add them to NixPkgs.

Here's my vision, from a developer's point-of-view.

Could manage Ruby versions using "Nix Profiles":

    ### Create empty project in which to install Ruby version.
    $ nix-env --switch-profile rails-project-at-work-env
    $ nix-env -i ruby2
    $ nix-env -i gem-rails

Could manage Ruby versions using "Nix Shell", assuming your app is a Nix package:

    $ nix-env -i some-rails-project
    $ nix-shell some-rails-project
    ### Now have project's Ruby version in scope.

A Nix Ruby project declares its library dependencies in Nix expression (I'm still new with Nix expressions):

    some-rails-project = buildRubyPackage rec {
      name = "some-rails-project-1.4";
      src = fetchurl {
        url = "http://www.someurl.com/repo/${name}.tar.gz";
        sha256 = "0xql73jkcdbq4q9m0b0rnca6nrlvf5hyzy8is0crdk65bynvs8q1";
      };
      propagatedBuildInputs = [
        ruby2 rails3 gem-1 gem-2 gem-3
      ];
    };

Now, if our app must leave the Nix world to return to the Ruby world, we only need to create a Gemfile. We can find its gem dependencies in the project's Nix expression (above):

    $ nix-env -i nix-bundler
    $ nix-bundle toGemFile some-rails-project/default.nix
    ### Creates Gemfile based on Nix expression.



