
# NixOS: Creating a Binary Cache

## Foreword

Lately, I have been trying to use NixOS on my Raspberry Pi. I installed and set up a web server on Raspbian before now, and it was not easy. Setting up a web server involves editing disparate configuration files and using the command line a lot. I wish I could set all this up in a single configuration file.

NixOS is exactly that. With one configuration file, I can set up most everything I need. So, I want to install and use NixOS on my Raspberry Pi, just to see how it goes.

## Problem

Earlier, I installed NixOS on my Raspberry Pi. Now, I am trying to change the configuration and rebuild the OS. But, rebuilding takes *forever*. Like, I am currently installing one package, and now, 15 hours later, Nix is still installing it. Why does installing one package take so long?

1) Nix builds it from source code.
2) Raspberry Pi has a very slow CPU.

When I install a package on NixOS in my VM, it is *very* fast. Why? The VM uses a more common platform, based on the Intel x86 CPU. When NixOS in my VM installs a package, it sees that a pre-built version exists, so it skips building from source to download these pre-built binaries.

Where do these binaries come from? Compiling source code produces binaries. These binaries can be used by anybody on the same platform. That's awesome. I don't want to spend hours and hours building these packages from source. Can't I find another Raspberry Pi user who has already built these packages? Well, NixOS is a small niche, and Raspberry Pi is also a small niche. I'm probably the only person who sees this problem.

That's ok, I want a place to store these binaries. My Raspberry Pi is working so hard to produce them, so of course I want to save them. Hey, maybe we need to reinstall them later, right? So, let's create a cache of all the binaries we produce.

## Initial Investigation

It already has this functionality built-in, so how does NixOS currently find pre-built binaries? After initial investigation, I found a configuration setting in `configuration.nix`, called `nix.binaryCaches`. Here's the documentation in the NixOS manual.

        nix.binaryCaches
        List of binary cache URLs used to obtain pre-built binaries of Nix packages.
        Default: [ "http://cache.nixos.org/" ]
        Declared by:
        <nixpkgs/nixos/modules/services/misc/nix-daemon.nix>

I followed the code to find a Perl script which queries the specified cache. The cache is actually a SQLite database, called `binary-cache-v2.sqlite`, which can be accessed by HTTP. Actually, I already have one of these binary cache databases locally, in my Nix "stateDir", which is here: `/nix/var/nix`.

https://github.com/NixOS/nix/blob/2d9bb56e554b17488c0f8984f34c026a66cdce67/scripts/download-from-binary-cache.pl.in#L128
https://github.com/NixOS/nix/blob/2d9bb56e554b17488c0f8984f34c026a66cdce67/scripts/download-from-binary-cache.pl.in#L221

## Low-level Binary Cache

SQLite has a command-line utility, called `sqlite3`, which allows us to interactively talk to a SQLite database. It's a binary file which should be included in the standard SQLite library package. Let's find it.

		> which sqlite3
		<no result>
		## Looks like it's not in the path. ##
		## Let's check the contents of the package. ##
		> ls /nix/store | grep sqlite
		> j1zg5vji2cclnlzpsm5ibsgpa5q90k2w-sqlite-3.8.0.2
		> cd j1zg5vji2cclnlzpsm5ibsgpa5q90k2w-sqlite-3.8.0.2
		> ls bin
		sqlite3
		## Found it! ##

Ok, now let's connect to a NixPkgs binary cache. After some searching, I discovered I have one locally.

		> ls /nix/var/nix
		binary-cache-v2.sqlite          channel-cache  daemon-socket  gc.lock  manifests  temproots
		binary-cache-v2.sqlite-journal  chroots        db             gcroots  profiles   userpool
		## Let's use sqlite3 on the "*.sqlite" file. ##
		> ./bin/sqlite3 /nix/var/nix/binary-cache-v2.sqlite
        QLite version 3.8.0.2 2013-09-03 17:11:13
        > Enter ".help" for instructions
        > Enter SQL statements terminated with a ";"
        sqlite> .tables
        BinaryCaches  NARExistence  NARs        
        sqlite> .quit


Now, hold on. Let's stop here. Let's review what we've learned.

- How do we download binaries instead of building source code? When we do `nix-env -i`, `nixos-rebuild`, or anything that will build a package, before downloading the package's source code, Nix will first query its binary cache database. If it has an entry for the requested package's hash value, Nix will download and install the binaries and finish.
- Where is the binary cache database? By default, we have two: one local and one central (`http://cache.nixos.org/`).
- How do we create our own binary cache? It is a standard SQLite database. We can copy the schema easily enough. But, we can't copy how Nix uses it. Plus, if Nix changes its format, our hack will fall apart. This is a big problem.

## Using Nix to Create Cache

That was interesting, but it's a lot of work. We have to worry about protocols, database schema, etc. I wish Nix had a command we can use to create a binary cache. That command would create the database and related files in a format which it understands. Does Nix have a command like this? Looking that the Nix docs didn't immediately show this, but a "nikstut" in the #NixOS IRC channel directed me to the [`nix-push` command](http://nixos.org/nix/manual/#sec-nix-push).

This is perfect! Nix will do almost all the work for us. This became so much easier. Here's what I need to do:

1) Use the `nix-push` command to create the SQLite database.
2) Set up a web server to share it.

### Using `nix-push` to Create Binary Cache

How do we use `nix-push` to build a package and add it to a binary cache?

        nix-push --dest /tmp/binary-cache/ $(nix-instantiate /nix/var/nix/profiles/per-user/chexxor/channels/nixpkgs/default.nix -A vim)

After the "vim" package completely rebuilds itself, it's binary composition will be placed inside the `tmp/binary-cache/` directory. If we look there, we will find dozens, if not hundreds, of `*.nar.xz` and `*.narinfo` files. In theory, if we set up an HTTP server to share this directory, a similar machine can use my binaries instead of building source code.

### Creating an HTTP Server to Share Binary Cache

Now, NixOS has an `httpd` module, which is a relatively simple HTTP server. It's easy to enable it, but when I have problems enabling the SSL. When I rebuild, the `httpd` service won't start because I haven't set up "SSL Certificate" and "SSL Key" files. I am stubborn, so I don't want to give up on SSL. I don't want to manually create these files, so I want to add an option called `genSelfSignedSSL` which will declaratively generate these two files.

We can create these two files by using this command:

        openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout mysitename.key -out mysitename.crt

#### Creating a File From a Module

How can we execute this shell command from the `httpd` module? Let's read the [NixOS module docs](http://hydra.nixos.org/build/6706167/download/2/nixos/manual.html#module-syntax) to discover the intended abilities of a module.

##### What is a Module?

A module...

- Must define 3 attributes which are used by the NixOS framework: **`imports`, `options`, `config`**.
- Evaluates to an **attribute set** or a function which returns an attribute set.
- Defines **options*** and default values, which are read by other modules.
- Wraps options with **properties**, which are attributes in the expression's `config` argument, to change option values.
- Merges option values with changes from other modules by using **types**. (see: `nixpkgs/lib/types.nix`)

In this definition of a module, what does it say about generating a file? I don't see it. I only see information about managing options and their values. Other modules create configuration files and start services, this must be true. So, if we look at the source code of other modules, maybe we can copy their methods.

#### How to Start a Cron Job From a Module?

Look at "Example 5.2. Locate Module Example". Think for a minute, a cron job can only be started through the shell, right? I'll copy it here from the NixOS manual.

        config = mkIf cfg.enable {
            services.cron = {
              enable = true;
              systemCronJobs = "${cfg.period}  root ${cmd}";
            };
        };

This module uses a `mk*` function to define two new options, `enable` and `systemCronJobs`, then exports it by adding it to the global `config` attribute set.

But, these are only values, I want to know how to issue a shell command. Here's the key: while the `systemCronJobs` option does not apparantly do anything here, it is exported! This means a different module can use this value and possibly issue a shell command. Let's search the code.

After searching the entire `nixpkgs` repository, I see this option is used in the `nixos/modules/services/scheduling/fcron.nix` and `nixos/modules/services/scheduling/cron.nix` modules. It looks like the latter has the code I'm looking for, so I'll copy this interesting part of the `cron.nix` file here:

        # Put all the system cronjobs together.
        systemCronJobsFile = pkgs.writeText "system-crontab"
            ''
              SHELL=${pkgs.bash}/bin/bash
              PATH=${config.system.path}/bin:${config.system.path}/sbin
              ${optionalString (config.services.cron.mailto != null) ''
                MAILTO="${config.services.cron.mailto}"
              ''}
              NIX_CONF_DIR=/etc/nix
              ${pkgs.lib.concatStrings (map (job: job + "\n") config.services.cron.systemCronJobs)}
            '';

Here, the `pkgs.writeText` function writes that big string to a file named "system-crontab". How? It uses the `writeTextFile` function, defined in the `pkgs/build-support/trivial-builders.nix` file. Take a look:

        # Create a single file.
        writeTextFile =
            { name # the name of the derivation
            , text
            , executable ? false # run chmod +x ?
            , destination ? ""   # relative path appended to $out eg "/bin/foo"
            }:
            runCommand name
              { inherit text executable;
                # Pointless to do this on a remote machine.
                preferLocalBuild = true;
              }
              ''
                n=$out${destination}
                mkdir -p "$(dirname "$n")"
                echo -n "$text" > "$n"
                (test -n "$executable" && chmod +x "$n") || true
              '';

The `writeTextFile` function uses the `runCommand` function to actually issue the shell command, passing the commands as a string. To see that there isn't anything deeper, I'll also copy this function here:

        # Run the shell command `buildCommand' to produce a store path named
        # `name'.  The attributes in `env' are added to the environment
        # prior to running the command.
        runCommand = name: env: buildCommand:
            stdenv.mkDerivation ({
              inherit name buildCommand;
            } // env);

#### Creating a  `writeText` Function

Let's not forget our goal. We want to add an option named `genSelfSignedSSL` which specifies whether to automatically create a certificate and key file for our web server's SSL. User `nietonfir` in [this StackOverflow thread](http://askubuntu.com/questions/49196/how-do-i-create-a-self-signed-ssl-certificate) has good suggestions. Here's the shell commands we can use to accomplish this:

        ### Make key.
        > openssl genrsa -des3 -out /var/host.key 2048
        # Enter password.
        ### Make password-less key.
        > openssl rsa -in server.key -out /var/host.key.insecure
        # Enter password.
        ### Make CSR (Certificate Signing Request)
        > openssl req -new -key host.key -out host.csr
        # Enter password.
        # Enter info, called Distinguished Name (DN).
        > openssl x509 -req -days 365 -in host.csr -signkey host.key -out host.crt
        > ls /var
        host.crt   host.csr   host.key

I wonder if the interactive part will be a problem. A quick web search says we can also use command, which won't prompt the user for inputs:

        ### Generate self-signed SSL certification, non-interactive.
        # http://www.openssl.org/docs/apps/req.html
        # -x509 means self-signed certificate.
        # -nodes means non-encrypted key.
        # rsa :4096 means 4096-bit RSA key.
        > openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -subj "/C=TW/ST=Taiwan/L=Taipei/O=/CN=localhost" -keyout /var/host.key -out /var/host.crt

Now, where can we put this shell command? This seems like a feature separate from web servers, so I chose to make a new option to use in the `configuration.nix` file called `config.security.ssl.genSSLCert`. Likewise, I'll create a new module in the `nixos/modules/security` directory, which seems appropriate, and name the file `ssl.nix`. You can look at my code later.

Now, how do we test this new module?

        > nixos-rebuild dry-run -I /my-sources --show-trace

This didn't work. It was complaining about a missing option, saying `Option 'security.ssl' defined without option declaration`. This means that my configuration file used `security.ssl`, but this option wasn't found in the modules. After some searching, I discovered that my `ssl/default.nix` module wasn't found because it wasn't added to the `module-list.nix` file.

#### Fixing Small Issues

##### Type Coercion Issue?

Now I get this error: `error: attribute 'service.ssl.genSSLCert' missing`. If I knew where the evaluator was looking for this, this would be easier to fix. After toggling some code on and off, I discovered the issue was caused by my `system.activationScripts` expression. The type of the option was `types.path`, but I was trying to cast it to a String. Nix should be able to automatically coerce that, but I was forced to use the `toString` built-in function.

##### Module Mistake

I encountered another issue: The options set in my config module, `common-http.nix` were not used when rebuilding. Why? I had trouble with this until I discovered I can use the `nixos-option` command as a debugging tool.The `nixos-option services.httpd.sslServerCert` command will tell me the value NixOS sees when evaluating the `configuration.nix` file. With this tool, I found an evaluation error in my `common-http.nix` module, which was silently broke the configuration build.

##### Option Type Updated

Another error appeared after I updated NixPkgs. After running `nixos-rebuild`, the HTTPD service failed to start. Using the `systemctl status httpd.service` command didn't help. After spending lots of time digging around, I discovered I can use the `journalctl -b` command to debug this, which gives chronological logging. I found this in the journal:

        httpd[3294]: Syntax error on line 169 of /nix/store/bv3xf0fzhjld68ml2spz2r2djx
        httpd[3294]: RedirectPermanent takes two arguments, a document to be redirecte

This told me to look at line 169 of the httpd conf file, which led me back to my `configuration.nix` file, where I set the `globalRedirect` option to a blank string. This rendered the `RedirectPermanent` option in the httpd conf file with only one argument, which is a syntax error. This error appeared after I updated my NixPkgs, which tightened the type system by updating the types of many options.

After fixing that issue and playing with option values to test my new module, I found success. Finally! My Apache HTTPD service is up and running with SSL!

### Next Steps

This was only a test. I performed all this configuration in a VM, but I still have to transfer this to my Raspberry Pi. I expect building this configuration on the Raspberry Pi to take a long time, possibly impossibly long. I'll probably need to consider doing distributed builds. I wonder if I can do this across in the internet, to share the workload with other members of this niche community.

Other needs:

- Domain name
- Configure router to expose Raspberry Pi
- Configure firewall on Raspberry Pi







