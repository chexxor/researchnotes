
# Upgrading a Nix Package


## Foreword

I've been playing with NixOS for awhile, which is a complicated beast. I have it installed in a VM and on my Raspberry Pi (not good at building packages). My opinion of the utility of NixOS is still undecided, but playing with it has been educational. Why am I playing with NixOS? I like it's package manager, Nix. It sounds like The Best package manager, and who doesn't want to use the best tools?

So, using the Nix package manager on Xubuntu on my netbook, I installed the "virt-manager" package. After installing it, I immediately hit a roadblock. When I run `virt-manager` from the terminal, I get an error. The error said `Failed to contact configuration server; the most common cause is a missing or misconfigured D-Bus session bus daemon`, which is caused when trying to connect to the GConf daemon on the same machine. I filed a [bug report on NixPkgs project](https://github.com/NixOS/nixpkgs/issues/1119) with the details. I searched high and low, but it seems nobody on the internet knows how to solve this issue.

I'm stubborn, so I won't give up. I see on the [virt-manager website](http://virt-manager.org/download/) that a new version has been released, 0.10.0, which ported the app from "gconf" to "gsettings". Perfect! Maybe I can upgrade the app's Nix package from 0.9.1 to 0.10.0 to fix my problems. I'll give it a try and keep a log in this post.

## Upgrading a Nix Package

So, looking at the `virt-manager/default.nix` file, I see this line:

        let version = "0.9.1"; in
        
Can I just change this to "0.10.0"? I'll try.

### Creating a Personal Copy of NixPkgs

I can't develop the master copy, because everyone uses it. I must change a personal copy of the the NixPkgs project, test it, and then ask someone else to verify my work. Then, after it's tested, I can submit the upgrade to the master copy. Let's make a personal copy.

        ### Clone the master project into a local directory.
        > cd ~/dev/git
        > git clone git://github.com/NixOS/nixpkgs.git
        ### Navigate to the file we want to change and edit the package.
        > cd pkgs/applications/virtualization/virt-manager/
        > gedit default.nix

Now, we can edit this file to change the version to "0.10.0". Looking at the code, this will download a different file, so we have to update the SHA hash value. To do this, download the new file, `virt-manager.et.redhat.com/download/sources/virt-manager/virt-manager-0.10.0.tar.gz`, then use a program to calculate its SHA-256 value.

        > curl -o ~/Downloads/virt-manager-0.10.0.tar.gz virt-manager.et.redhat.com/download/sources/virt-manager/virt-manager-0.10.0.tar.gz
        > sha256sum ~/Downloads/virt-manager-0.10.0.tar.gz
        4001a248801f90be3ea572d62fbf7a9613509433d8c106f2384c1a9eeceef890  /home/alex/Downloads/virt-manager-0.10.0.tar.gz

Now change these values in the `virt-manager/default.nix` file.

        ### virt-manager/default.nix ###
        # Set version to "0.10.0".
        # Set sha256 to "4001a248801f90be3ea572d62fbf7a9613509433d8c106f2384c1a9eeceef890".

### Building Personal Copy of Nix Package

I wonder if that will work. Let's try to build it. Now, by default, Nix will use its own copy of NixPkgs. Which is that? When you connected Nix to the central NixPkgs repository, it downloaded them all, placing them safely into its own Nix Store, which is always placed here, `/nix/store`. If you're curious, you can inspect these things with the following commands.

        ### Check for connections to otherrepositories.
        > nix-channel --list
        nixpkgs http://nixos.org/channels/nixpkgs-unstable
        ### Check for Nix-downloaded NixPkgs.
        > ls ~/.nix-defexpr/channels
        binary-caches  manifest.nix  nixpkgs
        ### Verify its location in the Nix Store.
        > ls -l ~/.nix-defexpr/channels/nixpkgs
        lrwxrwxrwx 1 alex alex 81 Jan  1  1970 /home/alex/.nix-defexpr/channels/nixpkgs -> /nix/store/d9xb1dnfijcllz0v3ijq3idvv4cn95rg-nixpkgs-13.10pre35428.9668294/nixpkgs

When we use Nix commands, such as `nix-env`, Nix will use this copy of NixPkgs by default. However, we can tell Nix to use a different one by using the `-I` flag when using Nix commands. Let's try it out.

        ### Check whether Nix can see our new version of virt-manager.
        # q=query, a=available, c=compare, f=from
        > nix-env -qcf /home/alex/dev/git/nixpkgs virt-manager --dry-run --show-trace
        (dry run; not doing anything)
        virt-manager-0.9.1  < 0.10.0

Looks good! Let's do it. If it fails, nothing on the system should break. Remove the `-dry-run` flag and tell it to install by using the `-i` flag.

        > nix-env -if /home/alex/dev/git/nixpkgs virt-manager --show-trace

I got the following error:

        output path `/nix/store/ghszkrnkbqw9qfhdz8kfbwpr971gaysi-virt-manager-0.10.0.tar.gz' should have sha256 hash `147qxvn9w6jc73r0dhfq6fa504wngazjzmkjllzbx40zh14a40a0', instead has `0v9l2ic3q4bq9jbzrxfd0870pfxdkbmwn7hs4s2qh8hpd4qqzgar'

Strange that Nix's calculated hash is different from what I calculated. I'll just copy Nix's hash, "0v9l2ic3q4bq9jbzrxfd0870pfxdkbmwn7hs4s2qh8hpd4qqzgar", into the `default.nix` file and try again.

Now it's making more progress, but I still expect this to fail. Why? The virt-manager 0.10.0 release notes said they ported from gconf to gsettings. This is a change in dependencies. Nix handles dependencies, so if we don't note these dependencies in the Nix expression, they won't be included in the build process and the compilation should fail. How do we know which dependencies to add to the package, and how to add them? This will be the difficult part.

Indeed, it looks like this build failed. Here's the error trace:

        building path(s) `/nix/store/c1miq4lp5y7amycykkwaqdrczd880vgx-virt-manager-0.10.0'
        building /nix/store/c1miq4lp5y7amycykkwaqdrczd880vgx-virt-manager-0.10.0
        unpacking sources
        unpacking source archive /nix/store/kl6ffy36crx1y28f43ncrqqyypqhdfsb-virt-manager-0.10.0.tar.gz
        source root is virt-manager-0.10.0
        patching sources
        /nix/store/mpbwif9nvavl4k203i1b8m8g0w6vhq8i-stdenv/setup: line 853: src/virt-manager.in: No such file or directory
        builder for `/nix/store/15m9i8128akdh42fcz3grm639h79d0bc-virt-manager-0.10.0.drv' failed with exit code 1
        error: build of `/nix/store/15m9i8128akdh42fcz3grm639h79d0bc-virt-manager-0.10.0.drv' failed

What does this mean?

### Helping WIP from Other User

Another user finally commented on the issue I created for this virt-manager problem. He suggested that I manually download `libvirt` and manually start the `libvirtd` service. I could try this, but I decided that my time is better spent to first upgrade the `virt-manager` package to 0.10.0, then manually install the `libvirt` package, if I must.

Let's stop working on my upgrade attempt and start working on his.

        ### Get Bjorne's code.
        > cd dev/git
        > git clone https://github.com/bjornfor/nixpkgs.git bjornefor-nixpkgs
        > cd bjornefor-nixpkgs/
        > git status
        ### Let's move to the virt-manager branch from this master branch.
        > git branch -a
        > git checkout virt-manager-version-bump
        ### Check that Nix sees this latest version.
        > nix-env -qcf /home/alex/dev/git/bjornefor-nixpkgs/ virt-manager --show-trace --dry-run
        (dry run; not doing anything)
        virt-manager-0.9.1  < 0.10.0

Looks good. Let's try to build it.

        nix-env -if /home/alex/dev/git/bjornefor-nixpkgs/ virt-manager --show-trace

I get a long stacktrace, which lists files which can't be found. This occurs when trying to build the `libvirt-glib-0.1.7` package, which was added in this same branch because it is a new dependency of the `virt-manager` package. 

Here's some more information from the failed installation.

        compilation terminated.
        linking of temporary binary failed: Command '['libtool', '--mode=link', '--tag=CC', '--silent', 'gcc', '-o', '/tmp/nix-build-libvirt-glib-0.1.7.drv-0/libvirt-glib-0.1.7/libvirt-glib/tmp-introspectvevojt/LibvirtGLib-1.0', '-export-dynamic', '/tmp/nix-build-libvirt-glib-0.1.7.drv-0/libvirt-glib-0.1.7/libvirt-glib/tmp-introspectvevojt/LibvirtGLib-1.0.o', '-L.', './libvirt-glib-1.0.la', '-Wl,--export-dynamic', '-pthread', '-L/nix/store/34dgabmzipdsjjwz87xkylc4z8wg7nyi-glib-2.36.1/lib', '-lgio-2.0', '-lgobject-2.0', '-lgmodule-2.0', '-lgthread-2.0', '-lglib-2.0']' returned non-zero exit status 1
        make[2]: *** [LibvirtGLib-1.0.gir] Error 1

Looks like building the `virt-manager` package failed because one of its dependencies, `libvirtGlib` failed its build. I want to try to build that single package alone.

(...time passes...)

I've been working on other things for about a month. I am now returning to this project. Let's see if Bjorne has made progress. I pulled his latest code locally, and I'm now trying to install it.

        Error starting Virtual Machine Manager: Requiring namespace 'Gtk' version '2.0', but '3.0' is already loaded
        Traceback (most recent call last):
          File "/nix/store/vvgc3kv1iz225kzk00sz5s9jnfaccib4-virt-manager-0.10.0/share/virt-manager/.virt-manager-wrapped", line 303, in <module>
            main()
          File "/nix/store/vvgc3kv1iz225kzk00sz5s9jnfaccib4-virt-manager-0.10.0/share/virt-manager/.virt-manager-wrapped", line 267, in main
            from virtManager.engine import vmmEngine
          File "/nix/store/vvgc3kv1iz225kzk00sz5s9jnfaccib4-virt-manager-0.10.0/share/virt-manager/virtManager/engine.py", line 45, in <module>
            from virtManager.details import vmmDetails
          File "/nix/store/vvgc3kv1iz225kzk00sz5s9jnfaccib4-virt-manager-0.10.0/share/virt-manager/virtManager/details.py", line 37, in <module>
            from virtManager.console import vmmConsolePages
          File "/nix/store/vvgc3kv1iz225kzk00sz5s9jnfaccib4-virt-manager-0.10.0/share/virt-manager/virtManager/console.py", line 27, in <module>
            from gi.repository import GtkVnc
          File "/nix/store/vdrx2h72dbazs2flks80rfsz4pxy8bg7-pygobject-3.0.4/lib/python2.7/site-packages/gi/importer.py", line 76, in load_module
            dynamic_module._load()
          File "/nix/store/vdrx2h72dbazs2flks80rfsz4pxy8bg7-pygobject-3.0.4/lib/python2.7/site-packages/gi/module.py", line 222, in _load
            version)
          File "/nix/store/vdrx2h72dbazs2flks80rfsz4pxy8bg7-pygobject-3.0.4/lib/python2.7/site-packages/gi/module.py", line 90, in __init__
            repository.require(namespace, version)
        RepositoryError: Requiring namespace 'Gtk' version '2.0', but '3.0' is already loaded



        
Dang.

After some quick internet searches, [this email message](http://osdir.com/ml/libchamplain-list/2010-11/msg00005.html) says that this error will appear when one process wants to use both Gtk 2 and Gtk 3. What does this tell us? This means that if the virt-manager developers are successfully running the latest version of `virt-manager`, their dependencies must all be using Gtk 3.

In our case, the `virt-manager` Nix package is using Gtk 3 while its dependency, `gtk-vnc`, is using Gtk 2. This is the problem. We need to update the gtk-vnc Nix package. A better idea is to make a new version of this package, because other packages might need a gtk-vnc which uses Gtk 2.

I see in Bjorne's public NixPkgs fork that he is currently using "gtk-vnc v0.4.2". The [gtk-vnc project page](https://wiki.gnome.org/Projects/gtk-vnc) has a link to the [gtk-vnc Git repository](https://git.gnome.org/browse/gtk-vnc/), which has a [gtk-vnc v0.5.3 tree](https://git.gnome.org/browse/gtk-vnc/tree/?id=v0.5.3). Maybe this version of gtk-vnc is using Gtk 3.

In summary, my opinion is that we must make a new Nix package, named `gtkvnc5`. Then, we must upgrade the `virt-manager` expression's dependency to `gtkvnc5`.

I can't find build instructions for gtk-vnc, so my attempt to upgrade this package will be rough.

For now, I just copied the `gtk-vnc/default.nix` file to a new file named `gtk-vnc/gtk-vnc-5.nix`. I updated the version to 0.5.3 and its Nix expression's parameter from "gtk" to "gtk3". Let's try to build it.

        $ nix-build /home/alex/dev/git/bjornefor-nixpkgs/pkgs/top-level/all-packages.nix -A gtkvnc5

Looks like there were no errors. Let's try to update the `virt-manager` dependency to use this new version of gtk-vnc. Simply update the `virt-manager/default.nix` file to replace every "gtkvnc" with "gtkvnc5". Now let's try to install virt-manager again.

        $ nix-env -if /home/alex/dev/git/bjornefor-nixpkgs/ virt-manager
        Error starting Virtual Machine Manager: Requiring namespace 'Gtk' version '2.0', but '3.0' is already loaded
        Traceback (most recent call last):
          File "/nix/store/qpm8rva4m1mavb8byjj9s50yhk4m0l2s-virt-manager-0.10.0/share/virt-manager/.virt-manager-wrapped", line 303, in <module>
            main()
          File "/nix/store/qpm8rva4m1mavb8byjj9s50yhk4m0l2s-virt-manager-0.10.0/share/virt-manager/.virt-manager-wrapped", line 267, in main
            from virtManager.engine import vmmEngine
          File "/nix/store/qpm8rva4m1mavb8byjj9s50yhk4m0l2s-virt-manager-0.10.0/share/virt-manager/virtManager/engine.py", line 45, in <module>
            from virtManager.details import vmmDetails
          File "/nix/store/qpm8rva4m1mavb8byjj9s50yhk4m0l2s-virt-manager-0.10.0/share/virt-manager/virtManager/details.py", line 37, in <module>
            from virtManager.console import vmmConsolePages
          File "/nix/store/qpm8rva4m1mavb8byjj9s50yhk4m0l2s-virt-manager-0.10.0/share/virt-manager/virtManager/console.py", line 27, in <module>
            from gi.repository import GtkVnc
          File "/nix/store/vdrx2h72dbazs2flks80rfsz4pxy8bg7-pygobject-3.0.4/lib/python2.7/site-packages/gi/importer.py", line 76, in load_module
            dynamic_module._load()
          File "/nix/store/vdrx2h72dbazs2flks80rfsz4pxy8bg7-pygobject-3.0.4/lib/python2.7/site-packages/gi/module.py", line 222, in _load
            version)
          File "/nix/store/vdrx2h72dbazs2flks80rfsz4pxy8bg7-pygobject-3.0.4/lib/python2.7/site-packages/gi/module.py", line 90, in __init__
            repository.require(namespace, version)
        RepositoryError: Requiring namespace 'Gtk' version '2.0', but '3.0' is already loaded

Same error as before. I'll have to let someone else make an attempt.




