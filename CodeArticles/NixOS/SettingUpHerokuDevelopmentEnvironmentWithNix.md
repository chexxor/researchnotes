
# Setting Up Heroku Development Environment with Nix

## Foreword

I want to create a Rails app, develop it locally, and use Heroku to host it. To deploy it to Heroku, I need to install some tools in my OS-level environment, which is explained in the [Local Workstation Setup](https://devcenter.heroku.com/articles/getting-started-with-rails4#local-workstation-setup) section of the Heroku docs for using Rails 4.

The Heroku workstation setup guide assumes the developer is using a common OS, such as Ubuntu, MacOS, or Windows, but I am using NixOS. What's the difference? The packaging system. I like NixOS's philosophy of setup-by-configuration. That is, everything in my development environment should be declared, which implies that anything else is not available to use. Why is this a good thing? It ensures consistency and repeatability. Repeatability is good because I can pass this configuration file to a coworker who can quickly and deterministically receive the same development tools as me.

So, the goal of this blog is to document setting up a repeatable development environment which has tools required for projects which use Rails and Heroku.

## Intro to Nix and User Environments

Nix is a packaging system. But, it is not a normal packaging system because much thought was spent when designing it. It ensures each package's dependencies are immutable, therefore reliable. Not only that, but it ensures package upgrades are worry-free, which is accomplished by offering a rollback. This rather uncommon feature is accomplished by keeping every build of a package, then selecting the "active" one. To summarize, a user can install dozens of packages, and select a subset to be visible.

How does a user select a subset of packages to use? Nix has a concept called a "profile", explained in the [Nix manual: Profiles](http://nixos.org/nix/manual/#sec-profiles) section, which is a directory of links to the desired subset of programs, libraries, and other installed packages which were installed by Nix. Every Nix user is given a default "profile", whose name corresponds to each user's username. For this reason, a "profile" is also called a "user environment", because an "environment", in practice, means a set of apps and libraries available for use.

The interesting bit of Nix's profiles is that a user is not restricted to their default profile. Rather, a user can create a new, empty profile, and install a different set of apps and libraries. How is this useful? Consider having a set of possible profiles named `chexxor` (my username), `rails-heroku`, `nodejs`, `forcedotcom`, `ruby-sandbox`, `company-app-dev-env`, `freelancing-app-dev-env`, each with the apps and tools required for each projects.

You may ask, Why not install all these languages and tools into the same environment? My answer: Repeat-ability and Share-ability. If I want to include another person in my development team, I can simply share the definition of my development environment and he should have all tools my project requires. This becomes more practical when a team chooses a set of tools as required for any development on their project.

So, with the goal of share-ability in mind, let's use Nix to define a development environment for a normal Rails on Heroku app.

## Heroku Tools

## Adding Heroku Toolbelt to Environment

Back to the [Local Workstation Setup](https://devcenter.heroku.com/articles/getting-started-with-rails4#local-workstation-setup) section of the Heroku docs, let's start from the top.

To use Heroku, we need the [Heroku Toolbelt](https://toolbelt.heroku.com/) in our environment, which gives access to the Heroku command-line client, Foreman, and Git. To install a tool into a Nix environment, it must be packaged using Nix. So, let's make a `heroku-toolbelt` Nix package.

### Creating a `heroku-toolbelt` Package

The NixPkgs manual's [Quick Start](http://nixos.org/nixpkgs/manual/#chap-quick-start) section got me started. First, we need to make a fork of the NixPkgs project from GitHub and install it locally. Then, we add the package.

        ### Go to GitHub, push "Fork" button on NixOS/NixPkgs repo.
        $ cd /
        $ git clone https://github.com/chexxor/nixpkgs.git my-sources
        $ cd /my-sources
        $ git branch add-heroku-toolbelt
        $ mkdir /my-sources/pkgs/development/tools/misc/heroku-toolbelt
        $ cd /my-sources/pkgs/development/tools/misc/heroku-toolbelt
        $ touch default.nix
        $ sudo nano default.nix

Now, we need to create a Nix expression which defines how to get the Heroku Toolbelt source code and build it. It's also a good idea to add information about the package in the meta attributes. I wrote the following Nix expression.

        { stdenv, fetchurl }:
        stdenv.mkDerivation rec {
          name = "heroku-client";
          src = fetchurl {
            url = "https://toolbelt.heroku.com/install.sh";
            #url = "https://s3.amazonaws.com/assets.heroku.com/heroku-client/heroku-client.tgz";
            ### Find SHA256 value with "nix-prefetch-url" command.
            # In another shell, use `nix-prefetch-url <url>`
            sha256 = "10i5wgqfka6yifh6p4hmvzvfkm0k1w4s59fwz3vip3knpyi6abcz";
            #sha256 = "00zqj9jwcklfyq5k3v81dwlzrrjm8axkgx7mixcb1kghjjlsdzv2";
          };
          nativeBuildInputs = [  ];
          meta = {
            homepage = https://toolbelt.heroku.com/;
            description = "All the tools you need to get started using Heroku at the command line.";
            longDescription = ''
              The Heroku Toolbelt is a package of the Heroku CLI, Foreman, and Git — all the tools
        you need to get started using Heroku at the command line.
            '';
            license = "bsd";
            maintainers = [ "chexxor <chexxor@gmail.com>" ];
            platforms = stdenv.lib.platforms.linux;
          };
        }

With this file in place, we need to add it to the set of packages queried by the `nix-env` and `nix-build` commands, which is defined by the `/top-level/all-packages.nix` file. Add the following line in an appropriate place there.

        heroku-client = callPackage ../development/tools/misc/heroku { };

### Testing a Nix Package Build

Our package should be lined up to be found by Nix commands. Now we can use Nix commands to test this package.

Normally, we don't need to specify a file or directory when we use Nix commands, so why do we need to do this now? Like the OS uses the `PATH` environment variable to find executable files, Nix uses the `NIX_PATH` environment variable to find Nix expressions. We didn't add our fork of the NixPkgs project to the `NIX_PATH`, so Nix won't find our new package.

So, how do we tell Nix about our fork? Several Nix expressions have a flag, `-I <path>`, which will prepend a path to the `NIX_PATH`. While this should work, I was never successful with it.

Why can't we just directly pass the `/my-sources/pkgs/development/tools/misc/heroku-toolbelt/default.nix` path to a Nix command? We can, but this Nix expression requires arguments, because it is a function. (See the `{ stdenv, fetchurl }:`?) So, if we want to directly call it, we must pass values for these using the `--arg` or `--argstr` flags. The problem is, I don't know the correct values or syntax. So, the easiest way I've found is to evaluate a Nix function which calls our package, because it will pass these arguments for us. The Nix expression which does this is in the `top-level/all-packages.nix` file.

This file contains a list of package names. To select our package from this function, use the `-A <attrPath>` flag. So, after many troubles, I discovered that the following command will build our package.

        # Optionally, use the `--dry-run` flag first, to be safe.
        $ nix-build /my-sources/pkgs/top-level/all-packages.nix -A heroku-toolbelt

Out build failed. Here's the error:

        ...
        installing
        install flags: install    
        make: *** No rule to make target `install'.  Stop.
        builder for `/nix/store/x06xqiffvlkqv0m1i31igy4mqw6qi4p8-heroku-toolbelt.drv' failed with exit code 2

### Packaging an App Which Doesn't Use "Make"

What's the problem? Well, it's using a default build process. What is the default? We can find out by reading the [Using stdenv](http://nixos.org/nixpkgs/manual/#idp166512) section of the NixPkgs Manual. Let's see what's happening.

        "For Unix packages that use the standard ./configure; make; make install build interface, you don’t need to write a build script at all; the standard environment does everything automatically.""

To be clear, this default build process is only used because we used the `stdenv.mkDerivation` function in the package's Nix expression. The default build process is the conventional build process, which is the standard process of using Make or CMake tools, like this:

        ### Conventional build process.
        $ ./configure
        $ make
        $ make install

The Heroku Toolbelt doesn't use Make, so we must customize this process a bit. It's just a Ruby script, which needs no compilation, rather only unpacking/extracting. The docs say we also need to ensure the `PATH` environment variable points to the app's `/bin` directory, but before we change that now, we can see if the default build will handle this for us.

After reading the docs and experimenting, I came up with the following Nix expression

        stdenv.mkDerivation {
          name = "heroku-client";
          ...
          # Necessary? We don't need to compile, so shouldn't need Ruby.
          #nativeBuildInputs = [ ruby ];
          phases = [ "unpackPhase" "installPhase" "fixupPhase" ];
          installPhase = ''
            mkdir -p $out
            mv ./* $out
          '';
          # Necessary? The Ruby script has a `#!` which must be changed to point to the Nix store.
          #postInstall = "patchShebangs $out;";
        }

### Explaining the Package's Nix Expression

There are several phases, but we only need the "unpackPhase" and "installPhase". If we set `phases` attribute's value, only those phases will be executed, skipping the other phases. To override the default "installPhase" to ignore it, we can put some Bash commands in the "installPhase" attribute.

An important thing to mention is the `$out` environment variable. In the context of a build by Nix, the `$out` environment variable will be set to the derivation path in the Nix Store, such as`/nix/store/ah3gzb34jj23b2ba5k5cwn3zhgzqqlsy-heroku-client`.

Besides the `$out` environment variable, every attribute in the result of the `stdenv.mkDerivation { }` function will be turned into an environment variable. These variables are accessible to the build scripts, most commonly Bash scripts. From this, we can infer that this function has an `out` attribute which is defined elsewhere. Likewise, `$src`, `$name`, and `$phases` should all be available as environment variables. If you read the [3.1. Using stdenv](http://nixos.org/nixpkgs/manual/#idp166512) section of the NixPkgs Manual, you might be able to see this.

There is much knowledge a Nix package developer is assumed to have. What's worse, most of it is undocumented. For example, if we want to deviate from the standard build, which is `./configure; make; make install;`, we can do this by overriding the appropriate stage. While this sounds simple, what isn't mentioned is that our override code must create the `$out` directory and move the files we want to keep into it.

Where can we learn about how to build Nix packages? The [NixPkgs manual](http://nixos.org/nixpkgs/manual/#idp166512) has *some* documentation, but it will not teach us the basics of software building. A much better introduction is the [5.1. A simple Nix expression](http://nixos.org/nix/manual/#idp24712128) section of the Nix Manual, which explains each part of the `hello` Nix package. Another source is the [Create and Debug Nix Packages](https://nixos.org/wiki/Create_and_debug_nix_packages) page on the NixOS Wiki, but it is quite unorganized and random.

### Testing a Nix Package Installation

Back to the `heroku-client` package. We can further test it by actually installing it. The following command will install a package from a local fork of NixPkgs. Optionally, you can first test the command by using the `-qa` and/or `--dry-run` flags. (In hindsight, placing my fork in the `/` directory was a bad decision, because now I must use `sudo` to install packages from there.)

        $ nix-env -i -f /my-sources/pkgs/top-level/all-packages.nix -A heroku-client
        $ which heroku
        /nix/var/nix/profiles/default/bin/heroku
        $ heroku
        /nix/store/dz9pvnjmvnhikfwyf624zzkj3cv7mbqi-coreutils-8.21/bin/env: ruby: No such file or directory

Awesome! The build worked! We know because the `heroku` command is available to use. However, we can't execute it because we haven't installed Ruby yet.

How should we handle this problem? Can we tell Nix that Ruby is a run-time dependency of this package, which means Ruby should be installed and made available? This was my first thought. After thinking about it, however, I maybe Ruby should be installed separately. Why? `heroku` is a single command, so it should be installed by a `nix-env --install` command. Likewise, `ruby` is a single command, so it should also be manually installed by a `nix-env --install` command.

So, to use the `heroku` command, we need to manually install the `ruby` package. I can do that.

        nix-env -i -f /my-sources/pkgs/top-level/all-packages.nix -A ruby

Now, let's try to use Heroku again.

        $ heroku
        /nix/store/m1mgqmkx23hyqr8sp1533x4mhcv6fpqf-ruby-1.9.3-p429/lib/ruby/1.9.1/rubygems/custom_require.rb:36:in `require': cannot load such file -- readline (LoadError)
        from /nix/store/m1mgqmkx23hyqr8sp1533x4mhcv6fpqf-ruby-1.9.3-p429/lib/ruby/1.9.1/rubygems/custom_require.rb:36:in `require'
        from /nix/store/fm6x4lh4ic40b7gig2zddvjhlh8gpc12-heroku-client/lib/heroku/command/run.rb:1:in `<top (required)>'
        from /nix/store/m1mgqmkx23hyqr8sp1533x4mhcv6fpqf-ruby-1.9.3-p429/lib/ruby/1.9.1/rubygems/custom_require.rb:36:in `require'
        from /nix/store/m1mgqmkx23hyqr8sp1533x4mhcv6fpqf-ruby-1.9.3-p429/lib/ruby/1.9.1/rubygems/custom_require.rb:36:in `require'
        from /nix/store/fm6x4lh4ic40b7gig2zddvjhlh8gpc12-heroku-client/lib/heroku/command.rb:14:in `block in load'
        from /nix/store/fm6x4lh4ic40b7gig2zddvjhlh8gpc12-heroku-client/lib/heroku/command.rb:13:in `each'
        from /nix/store/fm6x4lh4ic40b7gig2zddvjhlh8gpc12-heroku-client/lib/heroku/command.rb:13:in `load'
        from /nix/store/fm6x4lh4ic40b7gig2zddvjhlh8gpc12-heroku-client/lib/heroku/cli.rb:27:in `start'
        from /nix/var/nix/profiles/default/bin/heroku:24:in `<main>'

Another error. What's the problem? Seems it can't find "readline". After some googling, other people fix this by installing a "libreadline-dev" package, then rebuilding and reinstalling Ruby. Looking at Nix's Ruby package, it already tries to handle this problem by including a "readline" package as a buildInput, which includes the C headers. I don't know what the problem is.

So, I'm stuck. I'll have to continue this later.

### Alternative: Install Heroku Gem

An alternative to installing the Heroku command-line client from Nix is to install it as a Gem. Let's try it.

        $ gem install heroku
        Fetching: excon-0.25.3.gem (100%)
        ERROR:  While executing gem ... (Gem::FilePermissionError)
            You don't have write permissions into the /nix/store/m1mgqmkx23hyqr8sp1533x4mhcv6fpqf-ruby-1.9.3-p429/lib/ruby/gems/1.9.1 directory.

Dang, looks like Gem wants to install its Gems into the Nix store. Rather than installing Gems into a system-wide place, we can install them into our home directory, `~/.gem/`. To do this, I believe we only need to set the `GEM_HOME` environment variable to this value. What is the default `GEM_HOME` and `GEM_PATH` values?

        $ echo $GEM_HOME
        # <Nothing>
        $ echo $GEM_PATH
        # <Nothing>

Why aren't these set? When are supposed to be set? When I run `gem env`, it shows a Gem path, so this environment variables must be optional. I'll set GEM_HOME to my home directory so I can install and test the Heroku gem.

        $ export GEM_HOME=/home/chexxor/.gemrepo
        $ gem install heroku
        $ which heroku
        # <Nothing>
        $  ~/.gemrepo/gems/heroku-3.0.1/bin/heroku 
        /nix/store/m1mgqmkx23hyqr8sp1533x4mhcv6fpqf-ruby-1.9.3-p429/lib/ruby/1.9.1/rubygems/custom_require.rb:36:in `require': cannot load such file -- readline (LoadError)
          from /nix/store/m1mgqmkx23hyqr8sp1533x4mhcv6fpqf-ruby-1.9.3-p429/lib/ruby/1.9.1/rubygems/custom_require.rb:36:in `require'
          from /home/chexxor/.gemrepo/gems/heroku-3.0.1/lib/heroku/command/run.rb:1:in `<top (required)>'
          from /nix/store/m1mgqmkx23hyqr8sp1533x4mhcv6fpqf-ruby-1.9.3-p429/lib/ruby/1.9.1/rubygems/custom_require.rb:36:in `require'
          from /nix/store/m1mgqmkx23hyqr8sp1533x4mhcv6fpqf-ruby-1.9.3-p429/lib/ruby/1.9.1/rubygems/custom_require.rb:36:in `require'
          from /home/chexxor/.gemrepo/gems/heroku-3.0.1/lib/heroku/command.rb:14:in `block in load'
          from /home/chexxor/.gemrepo/gems/heroku-3.0.1/lib/heroku/command.rb:13:in `each'
          from /home/chexxor/.gemrepo/gems/heroku-3.0.1/lib/heroku/command.rb:13:in `load'
          from /home/chexxor/.gemrepo/gems/heroku-3.0.1/lib/heroku/cli.rb:27:in `start'
          from /home/chexxor/.gemrepo/gems/heroku-3.0.1/bin/heroku:17:in `<main>'

### Solved with Ruby Implementation of `readline`

The error is the same as the Heroku I packaged with Nix. The problem must be with the Ruby version I installed with Nix. I'll try one more thing...

        $ gem install rb-readline
        $ heroku
        # <Heroku command list - Success!>

So, it looks like the system must have the `rb-readline` gem installed to run the `heroku` Ruby script. How is this gem different from the `readline` Nix package?  I read the docs, which say that `rb-readline` is a Ruby implementation of the OS-level `readline` package, which is written in C. Users have trouble So, the `rb-readline` package is a simpler cross-platform solution.

### Compiling Ruby with OS-native Readline

Why can't Ruby find the OS-level `readline` package? Is this a bug in the "Ruby" Nix package? It looks like I have the Readline header files, here: `/nix/store/06p7jmzc26sv6x6j2s9lrlslmw2vmvsi-readline-6.2/include/readline/readline.h`. But, this obviously isn't the key.

Some other NixOS users looked at Ruby's Nix expression and found that the `readline` and `curses` packages are mutually dependent. That is, to add `readline` to the Ruby build, the Ruby package's `cursesSupport` option must be true. To do that, we can create a new version of that Ruby package which has curses support. Add the following attribute in the `top-level/all-packages.nix` file, right below the `ruby` attribute.

        rubyCurses = ruby.override { cursesSupport = true; };

Then, let's install this version of Ruby, which has `curses` support. Then let's reinstall the Heroku Toolbelt. It seems the Nix packaged Heroku was fine and the problem was in the Ruby interpreter, so I'll try to install the Heroku package again.

        $ nix-env --uninstall ruby
        $ nix-env --uninstall rubygems
        $ nix-env --install -A rubyCurses -f /my-sources/pkgs/top-level/all-packages.nix
        $ sudo nix-env --install -A heroku-client -f /my-sources/pkgs/top-level/all-packages.nix
        $ heroku
        # <Heroku command list - Success!>

    rubyCurses = ruby.override { cursesSupport = true; };



    $ nix-env --uninstall ruby
    $ nix-env --uninstall rubygems
    $ nix-env --install -A rubyCurses -f /my-sources/pkgs/top-level/all-packages.nix
    $ sudo nix-env --install -A heroku-client -f /my-sources/pkgs/top-level/all-packages.nix
    $ heroku
    # <Heroku command list - Success!>

### Creating `heroku-toolbelt` Nix Package

Until now, I've packaged only the Heroku CLI. The [Heroku Toolbelt](https://github.com/heroku/toolbelt), on the other hand, includes [Foreman](https://github.com/ddollar/foreman). So, I have to create a Nix package for Foreman as well.

#### Creating a `foreman` Nix Package

##### Initial Install Script

So, besides the `heroku-client` app, the Heroku Toolbelt includes the [foreman](https://github.com/ddollar/foreman) app. A web app usually consists of multiple processes, such as a web server process, a database process, and utility processes. Because this architecture is great idea, this tool was created to support this practice. Foreman will create, update, and kill an app's constituent processes as a unit. How does it know which processes to create? Like every application, Foreman has a configuration file, named `Procefile` to receive these as arguments. Here's an example `Procfile`.

        ### Example Procfile ###
        web:    bundle exec thin start -p $PORT
        worker: bundle exec rake resque:work QUEUE=*
        clock:  bundle exec rake resque:scheduler

When an application has this file in its root, simply tell Foreman to start it.

        $ foreman start

Let's add Foreman to NixPkgs. To create a `foreman` Nix package, I'll copy the `heroku-client` Nix expression and change a few values.

        { stdenv, fetchurl }:
        stdenv.mkDerivation rec {
          name = "foreman";
          src = fetchurl {
            # Tarball link fetched from here: http://projects.theforeman.org/projects/foreman/files
            url = "http://projects.theforeman.org/attachments/download/642/foreman-1.3.1.tar.bz2";
            sha256 = "0jq48nswwgx7lk79cpdm2zmn40rxf1jjnng63qmam09m4pbypj3r";
          };
          phases = [ "unpackPhase" "installPhase" "fixupPhase" ];
          installPhase = ''
            mkdir -p $out
            mv ./* $out
          '';
          meta = {
            homepage = https://github.com/ddollar/foreman;
            description = "Manage an app's constituent processes as a whole using a Procfile.";
            longDescription = ''
              Declare an application's constituent processes in a Procfile to easily start and stop the application.
            '';
            license = "bsd";
            maintainers = [ "chexxor <chexxor@gmail.com>" ];
            platforms = stdenv.lib.platforms.linux;
          };
        }

Now let's try to install it.

        $ nix-env --install -A foreman -f /my-sources/pkgs/top-level/all-packages.nix

That worked. Now let's run Foreman.

        $ ./result/script/rails s -e production
        /nix/store/pckbbvmmr38h5gv32m05lj6k8jm20vnc-ruby-1.9.3-p429/lib/ruby/1.9.1/psych.rb:297:in `initialize': No such file or directory - /home/chexxor/result/config/settings.yaml (Errno::ENOENT)

Error. Looks like it can't find a `settings.yaml` file. I see a `./result/config/settings.yaml.example` file, maybe that is a template to use to create a `settings.yaml` file.

##### Add Install Commands from Documentation

Before we go further, let's read Foreman's documentation for [installing Foreman from source code](http://theforeman.org/manuals/1.3/index.html#3.4InstallFromSource). Foreman has a bunch of dependencies: `gcc-c++ git libvirt-devel mysql-devel pg-devel openssl-devel libxml2-devel sqlite-devel libxslt-devel zlib-devel readline-devel postgresql-devel`. And it also requires the following terminal commands.

        ### Foreman Install Commands ###
        git clone https://github.com/theforeman/foreman.git -b develop
        cd foreman
        cp config/settings.yaml.example config/settings.yaml
        cp config/database.yml.example config/database.yml
        gem install bundler
        # depending on database configured in yml file you can skip others
        # (we are assuming sqlite to be configured)
        bundle install --without mysql mysql2 pg test --path vendor # or postgresql
        # set up database schema, precompile assets and locales
        RAILS_ENV=production bundle exec rake db:migrate assets:precompile locale:pack

Let's add these shell commands to the install phase.

        { stdenv, fetchurl, rubygems }:
        stdenv.mkDerivation rec {
          name = "foreman";
          ...
          buildInputs = [ rubygems ];
          installPhase = ''
            mkdir -p $out
            cp config/settings.yaml.example config/settings.yaml
            cp config/database.yml.example config/database.yml
            export GEM_HOME=`pwd`/__gems__
            gem install bundler --install-dir ./__gems__ --no-ri --no-rdoc
            # depending on database configured in yml file you can skip others
            # (we are assuming sqlite to be configured)
            ln `pwd`/__gems__/gems/bundler-1.3.5/bin/bundle bundle
            ./bundle install --standalone --without mysql mysql2 pg test --path vendor
            # set up database schema, precompile assets and locales
            RAILS_ENV=production bundle exec rake db:migrate
            rm bundle
            mv ./* $out
          '';
          ...
        }

Error when doing the bundler step. It couldn't build a PostgreSQL gem because it couldn't find C headers.

##### Add Build Dependencies to `buildInputs`

This means we need to include the PostgreSQL Nix package as a build input. This is documented in the install directions, so I'll add all these dependencies in the `buildInputs` attributes.

        { stdenv, fetchurl, rubygems, gcc, git, libvirt, mysql,
          postgresql, openssl, libxml2, sqlite, libxslt, zlib, readline
        }:
        stdenv.mkDerivation rec {
          name = "foreman";
          ...
          buildInputs = [ rubygems gcc git libvirt mysql
            postgresql openssl libxml2 sqlite libxslt zlib readline ];
          installPhase = ''
            mkdir -p $out
            cp config/settings.yaml.example config/settings.yaml
            cp config/database.yml.example config/database.yml
            export GEM_HOME=`pwd`/__gems__
            gem install bundler --install-dir ./__gems__ --no-ri --no-rdoc
            # depending on database configured in yml file you can skip others
            # (we are assuming sqlite to be configured)
            ln `pwd`/__gems__/gems/bundler-1.3.5/bin/bundle bundle
            ./bundle install --standalone --without mysql mysql2 pg test --path vendor
            # set up database schema, precompile assets and locales
            RAILS_ENV=production bundle exec rake db:migrate
            rm bundle
            mv ./* $out
          '';
          ...
        }

That looks good. Let's test it. (Note: I spent a lot of time in this part. I think I skipped a few steps. If you spot a missing thing, it probably happened here, when I took a break from logging my progress.)

##### Can't Download Gem

I still had problems with the `bundle install ...` step, so I'll try to step through the installation using `nix-shell`. I'll execute each step of the `installPhase` definition.

        $ nix-shell /my-sources/pkgs/top-level/all-packages.nix -A foreman
        [nix-shell]$ unpackPhase
        [nix-shell]$ cd foreman-1.3.1/
        [nix-shell]$ cp config/settings.yaml.example config/settings.yaml
        [nix-shell]$ cp config/database.yml.example config/database.yml
        [nix-shell]$ export GEM_HOME=`pwd`/__gems__
        [nix-shell]$ gem install bundler --install-dir ./__gems__ --no-ri --no-rdoc
        ERROR:  While executing gem ... (Gem::RemoteFetcher::FetchError)
            SocketError: getaddrinfo: Name or service not known (http://rubygems.org/gems/bundler-1.3.5.gem)

Error. Looks like Rubygems can't reach that URL. I can reach that URL when I copy-paste it into a browser, so why does it have problems now? The `nix-shell` command should create a pure environment, which has no environment variables, apps, or libraries available except those which are explicitly included with the packages in the `buildInputs` attribute.

Maybe an environment variable is causing Rubygems to fail. I'll try to reach this url from inside that Nix Shell

        [nix-shell]$ curl http://rubygems.org/gems/bundler-1.3.5.gem
        curl: (5) Couldn't resolve proxy 'nodtd.invalid'

This verifies the problem is only in the Nix Shell. Curl gives us more information than Rubygems by telling us it is proxy-related. Let's check our environment for that.

        [nix-shell]$ env | grep -i proxy
        http_proxy=http://nodtd.invalid/
        ftp_proxy=http://nodtd.invalid/

That's the problem. What is setting this environment variable? I search NixPkgs for this URL, and I see the [`libxml2`](https://github.com/NixOS/nixpkgs/blob/master/pkgs/development/libraries/libxml2/setup-hook.sh) package defines it.

Let's clear these variable and try again.

        [nix-shell]$ unset http_proxy
        [nix-shell]$ unset ftp_proxy
        [nix-shell]$ curl http://rubygems.org/gems/bundler-1.3.5.gem
        # <Returns HTTP snippet>

After fixing this issue, our `installPhase` script looks like this.

        { stdenv, fetchurl, rubygems, gcc, git, libvirt, mysql,
          postgresql, openssl, libxml2, sqlite, libxslt, zlib, readline
        }:
        stdenv.mkDerivation rec {
          name = "foreman";
          ...
          buildInputs = [ rubygems gcc git libvirt mysql
            postgresql openssl libxml2 sqlite libxslt zlib readline ];
          installPhase = ''
            mkdir -p $out
            cp config/settings.yaml.example config/settings.yaml
            cp config/database.yml.example config/database.yml
            export GEM_HOME=`pwd`/__gems__
            unset http_proxy # libxml2 sets this, but Rubygems fails if proxy is set.
            unset ftp_proxy
            gem install bundler --install-dir ./__gems__ --no-ri --no-rdoc
            # depending on database configured in yml file you can skip others
            # (we are assuming sqlite to be configured)
            ln `pwd`/__gems__/gems/bundler-1.3.5/bin/bundle bundle
            ./bundle install --standalone --without mysql mysql2 pg test --path vendor
            # set up database schema, precompile assets and locales
            RAILS_ENV=production bundle exec rake db:migrate
            rm bundle
            mv ./* $out
          '';
          ...
        }

With that fixed, let's continue to install.

        [nix-shell]$ gem install bundler --install-dir `pwd`/__gems__ --no-ri --no-rdoc
        # <Success!>
        # Let's continue.
        [nix-shell]$ ln `pwd`/__gems__/gems/bundler-1.3.5/bin/bundle bundle
        [nix-shell]$ ./bundle install --standalone --without mysql mysql2 pg test --path vendor

Error! Here's the message:

        Installing ruby-libvirt (0.4.0) 
        Gem::Installer::ExtensionBuildError: ERROR: Failed to build gem native extension.
        ...
        extconf.rb:83:in `<main>': libvirt library not found in default locations (RuntimeError)

This is strange; we included the `libvirt` package as a build input, so the Gem should be able to find its C headers. We can try to explicitly specify the path to the virtlib headers path.

        # Find the headers.
        [nix-shell]$ find /nix/store -name libvirt
        ...
        /nix/store/vpbsz1qz5a0p91r3mzkckdkl0s0mb1j7-libvirt-1.1.4/include/libvirt
        /nix/store/vpbsz1qz5a0p91r3mzkckdkl0s0mb1j7-libvirt-1.1.4/lib/libvirt
        ...

        [nix-shell]$ gem install ruby-libvirt -v '0.4.0' -- --with-libvirt-include=/nix/store/vpbsz1qz5a0p91r3mzkckdkl0s0mb1j7-libvirt-1.1.4/include/ --with-libvirt-lib=/nix/store/vpbsz1qz5a0p91r3mzkckdkl0s0mb1j7-libvirt-1.1.4/lib/

Success! Now, why can't this Ruby gem automatically find the `libvirt` headers? 

After fixing this issue, our Nix expression looks like this.

        { stdenv, fetchurl, rubygems, gcc, git, libvirt, mysql,
          postgresql, openssl, libxml2, sqlite, libxslt, zlib, readline
        }:
        stdenv.mkDerivation rec {
          name = "foreman";
          ...
          buildInputs = [ rubygems gcc git libvirt mysql
            postgresql openssl libxml2 sqlite libxslt zlib readline ];
          installPhase = ''
            mkdir -p $out
            cp config/settings.yaml.example config/settings.yaml
            cp config/database.yml.example config/database.yml
            export GEM_HOME=`pwd`/__gems__
            unset http_proxy # libxml2 sets this, but Rubygems fails if proxy is set.
            unset ftp_proxy
            gem install bundler --install-dir ./__gems__ --no-ri --no-rdoc
            # depending on database configured in yml file you can skip others
            # (we are assuming sqlite to be configured)
            ln `pwd`/__gems__/gems/bundler-1.3.5/bin/bundle bundle
            # Add build options to address build issues for specific gems.
            ./bundle config build.ruby-libvirt --with-libvirt-include=${libvirt}/include/ --with-libvirt-lib=${libvirt}/lib/
            ./bundle install --standalone --without mysql mysql2 pg test --path vendor
            # set up database schema, precompile assets and locales
            RAILS_ENV=production bundle exec rake db:migrate
            rm bundle
            mv ./* $out
          '';
          ...
        }

Now, when executing the `bundle install` command, it runs until completion.

##### Running RAILS_ENV=production ./bundle exec rake db:migrate

Let's continue to finish running the install commands.

        [nix-shell]$ RAILS_ENV=production ./bundle exec rake db:migrate
        rake aborted!
        cannot load such file -- facter
        /home/chexxor/foreman-1.3.1/vendor/ruby/1.9.1/gems/activesupport-3.2.15/lib/active_support/dependencies.rb:251:in `require'
        ...

It can't find a thing called "facter". A quick internet search tells me that "facter" is a piece of Puppet. I believe we need to include Puppet as a build dependency. I don't see a Nix expression for Puppet in the `top-level/all-packages.nix` file, so I must create one. Dang. :-(

Idea: In this `foreman` package, I might be able to do `gem install puppet`, instead of making a new Nix package for it.

#### Creating a `puppet` Package

Let's create a Puppet package in NixPkgs. Puppet is a configuration management tool

Let's follow Puppet's [Install from Source documentation](http://docs.puppetlabs.com/guides/installation.html#installing-from-a-tarball-not-recommended). It looks like Puppet consists of two components, Puppet and Facter, so I think we should make two Nix expressions.

We need:
- lsb_release
- rgen gem
- Download Puppet and Facter tarballs
- Unpack Facter; $ sudo ruby install.rb
- Unpack Puppet; $ sudo ruby install.rb
- sudo puppet resource group puppet ensure=present
- sudo puppet resource user puppet ensure=present gid=puppet shell='/sbin/nologin'
- Create and install init scripts
- Create an /etc/puppet/puppet.conf file
- Continue here: http://docs.puppetlabs.com/guides/installation.html#post-install

##### Initial Install Script for Facter

Let's start by creating a Nix package for Facter. After downloading the tarball and looking inside, it looks like a Ruby app like Foreman. Let's copy the Nix expression we wrote for the `foreman` package and start from there.

        { stdenv, fetchurl, ruby
        }:
        stdenv.mkDerivation rec {
          name = "facter";
          src = fetchurl {
            # Tarball link fetched from here: http://downloads.puppetlabs.com/facter/
            url = "http://downloads.puppetlabs.com/facter/facter-1.7.3.tar.gz";
            sha256 = "1iyy3km4zbf5gnjh7l094703429yk6iw3q244p6ch5x5ssaamyri";
          };
          buildInputs = [ ruby ];
          phases = [ "unpackPhase" "installPhase" "fixupPhase" ];
          installPhase = ''
            mkdir -p $out
            # Failed build motivated these installation options.
            ruby install.rb --destdir=$out
            mv ./* $out
          '';
        meta = {
            homepage = http://puppetlabs.com/;
            description = "An automated administrative engine for your Linux, Unix, and Windows
        systems.";
            longDescription = ''
              An automated administrative engine for your Linux, Unix, and Windows systems,
              performs administrative tasks (such as adding users, installing packages,
              and updating server configurations) based on a centralized specification.
            '';
            license = stdenv.lib.licenses.asl20;
            maintainers = [ "chexxor <chexxor@gmail.com>" ];
            platforms = stdenv.lib.platforms.linux;
          };
        }

Let's try to build this.

        $ nix-build /my-sources/pkgs/top-level/all-packages.nix -A facter
        <...build output...>
        $ ./result/bin/facter
        <...list of my system's stats from facter...>

Success!

##### Initial Install Script for Puppet

Let's continue by writing a Nix package for Puppet. According to Puppet's [Install from Source documentation](http://docs.puppetlabs.com/guides/installation.html#installing-from-a-tarball-not-recommended), the installation procedure for Puppet is identical to Facter's. Great! Let's just copy Facter's Nix expression.

        { stdenv, fetchurl, ruby
        }:
        stdenv.mkDerivation rec {
          name = "puppet";
          src = fetchurl {
            # Tarball link fetched from here: http://downloads.puppetlabs.com/puppet/
            url = "http://downloads.puppetlabs.com/puppet/puppet-3.3.1.tar.gz";
            sha256 = "117214y1381x6x4sd1v45x6hfknm3f4zzvrbsp5zdib7cym9wbqm";
          };
          buildInputs = [ ruby ];
          phases = [ "unpackPhase" "installPhase" "fixupPhase" ];
          installPhase = ''
            mkdir -p $out
            # Failed build motivated these installation options.
            ruby install.rb --destdir=$out
            mv ./* $out
          '';
        meta = {
            homepage = http://puppetlabs.com/;
            description = "An automated administrative engine for your Linux, Unix, and Windows
        systems.";
            longDescription = ''
              An automated administrative engine for your Linux, Unix, and Windows systems,
              performs administrative tasks (such as adding users, installing packages,
              and updating server configurations) based on a centralized specification.
            '';
            license = stdenv.lib.licenses.asl20;
            maintainers = [ "chexxor <chexxor@gmail.com>" ];
            platforms = stdenv.lib.platforms.linux;
          };
        }

Let's try to build it.

        $ nix-build /my-sources/pkgs/top-level/all-packages.nix -A puppet
        ...
        Could not load facter; cannot install
        builder for `/nix/store/r75x24kjzia8p7ag59rlwkhslyaqavwq-puppet.drv' failed with exit code 255
        error: build of `/nix/store/r75x24kjzia8p7ag59rlwkhslyaqavwq-puppet.drv' failed

Looks like it wants to use "facter", but the installer couldn't find it. Looks like we didn't specify it as a dependency, so of course the builder doesn't have it. That's an easy fix.

        { stdenv, fetchurl, ruby, facter
        }:
          ...
          buildInputs = [ ruby facter ];
          ...

Let's try to build it again. 

        $ nix-build /my-sources/pkgs/top-level/all-packages.nix -A puppet
        ...
        Could not load facter; cannot install
        qbuilder for `/nix/store/0iii8qdz5aripscv0xbhv8li846msq1p-puppet.drv' failed with exit code 255
        error: build of `/nix/store/0iii8qdz5aripscv0xbhv8li846msq1p-puppet.drv' failed

Looks like we didn't fix the issue.

After using `nix-shell` to investigate, it looks like like a `require "facter"` expression is failing in the `install.rb` script. In this `nix-shell`, it looks like the `facter` binary *is* in the `PATH` environment variable, but not in the `RUBYLIB` or `GEM_PATH` environment variables. I believe Ruby uses the `RUBYLIB` environment variable when evaluating a `require` function in a Ruby script to find libraries and modules. It appears that the `facter` Nix package does not add itself to this path. How can we solve this?

When writing packages, the important thing to keep in mind is that each package should be responsible for ensuring it, itself, can properly run. This means that we should not change the `factor` package to add itself to the `RUBYLIB` environment variable. Instead, the problem is that the `puppet` package needs its environment variable set to see `facter.` So, let's add code to add `facter` to the right path variable in the `puppet` package's Nix expression.

        { stdenv, fetchurl, ruby, facter
        }:
        stdenv.mkDerivation rec {
          ...
          installPhase = ''
            ...
            addToSearchPath RUBYLIB ${facter}/lib
            ...

Let's try to build `puppet` package again.

        $ nix-build /my-sources/pkgs/top-level/all-packages.nix -A puppet
        <...build info...>
        install.rb:138:in ``': No such file or directory - which gzip (Errno::ENOENT)

Looks like we missed another dependency. Add the `which` package exactly as we added the `facter` package as a dependency. Now, let's try again.

Looks like that fixed the issue! It completely built and produced a `result` file, which points to the package's derivation in the Nix store. Let's try to run `puppet`.

        $ result/bin/puppet 
        /nix/store/pckbbvmmr38h5gv32m05lj6k8jm20vnc-ruby-1.9.3-p429/lib/ruby/1.9.1/rubygems/custom_require.rb:36:in `require': cannot load such file -- puppet/util/command_line (LoadError)
          from /nix/store/pckbbvmmr38h5gv32m05lj6k8jm20vnc-ruby-1.9.3-p429/lib/ruby/1.9.1/rubygems/custom_require.rb:36:in `require'
          from result/bin/puppet:3:in `<main>'

Dang, it is still missing something. It can't find its own libraries. Ruby libraries are found by `require` by adding their directories to the `RUBYLIB` environment variable. Looks like we need to add Puppet's own library directory to that path.

!!! To do: Continue editing from here.


After I install the package, the value of the `RUBYLIB` environment variable is empty. It holds the value we give it during the build, but it seems to be untouched outside the build. We installed the `facter` package, so Nix should add it to the `RUBYLIB` environment variable.

We want this Ruby library to be considered in a way similar to executable binaries. In a Bash shell, an executable program is found by searching all directories (but not subdirectories) listed in the `PATH` environment variable. In the same way, the Ruby interpreter finds libraries by searching all directories listed in the `RUBYLIB` environment variable. Nix manages the `PATH` environment variable, so why does it not manage the `RUBYLIB` environment variable?

How does Nix manage the `PATH` environment variable? If a package lists packages in its `buildInputs` attribute, Nix will ensure their `bin` directories are added to the `PATH` environment variable. Why can't Nix ensure these library directories are added to the appropriate environment variable?

It looks like Nix supports this idea, albeit at a non-native level. Documented in a [footnote for the generic builder](http://nixos.org/nix/manual/#ftn.idp24844384) in the Nix documentation, the generic builder will execute the `$out/nix-support/setup-hook` file as part of a package's build process. When is this called and are the effects restricted to the scope of the package's builder? The footnote says "For instance, the setup hook for Perl sets the PERL5LIB environment variable to contain the lib/site_perl directories of all inputs". This leads me to believe this is only useful during the build.






(Snippets from previous revisions. Remove later.)

The Heroku Toolbelt package, on the other hand, is installed using a [custom shell script](https://toolbelt.heroku.com/install.sh), which we specified as the package's URL. Can we just use this?

According to the Nix manual, we can specify a custom build script by using the `builder` attribute. Let's add that attribute to see if it works.

        stdenv.mkDerivation {
          name = "heroku-toolbelt";
          ...
          builder = ./install.sh;
        }

And test it by building the package again.

        $ nix-build /my-sources/pkgs/top-level/all-packages.nix -A heroku-toolbelt



How is this package intended to be build? On a normal Linux distribution, Heroku Toolbelt uses a 

NixOS is not normal, so we need to customize it a bit. Looks like it just unpacks it into `heroku-client` directory, move the contents out of it, and deletes that directory. That sounds like an "install" phase thing. Nix can unpack it with its default behavior, and there's no building, so we can just 



When Ruby was compiled from source code, what compiler flags were used? We can check this by using:

        ruby -r rbconfig -e 'puts RbConfig::CONFIG["configure_args"]'






