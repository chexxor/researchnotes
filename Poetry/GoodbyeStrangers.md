

### Goodbye Strangers

I said goodbye to a circle to which I once belonged.

I was excited to finally be rid of the responsibilities and commitments.
So why, now, do I feel like a hole has opened in my chest?
What does my heart feel that my mind can not?

These people, I did not know well.
My goal was to learn techniques, but they cared only for politeness.
My goal was to meet people, but the circle allowed no such leisures.

The techniques required a competent partner.
How practical when learnt under false conditions?
To what goal when no ideal is given?
What to believe when advice is inconsistent and unclear?

With such a hollow environment, what could be missed?

Could it be the look in the manager's eye, when I delivered my decision?
Surprised at the possibility that the circle could be unfulfilling.
Assuming friendship, when it actually was nothing more than business.

So could it be my own feeling, of a lost mission and a failed commitment?
That my mind decided towards other goals while my heart didn't foresee the feeling?

