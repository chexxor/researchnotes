
# Node.JS Intro

So, I finally found time to get hands on with [Node.js](http://nodejs.org/) a few months ago, and I was pretty blown away by the different way of thinking. I want to take some time to crystallize my thoughts on the paradigm shift you must undertake to understand Node

# Foreword

Apache is the most popular web server, and for many good reasons. It is fast, configurable, and can delgate request handling to almost any language. With an awesome tool like Apache, why is Node.js worth a look? Because it is interesting. Node is interesting because it exclusively uses JavaScript. The language makes that big of a deal? Well, no. the platform is what is significantly different, but it can't be so without JavaScript. This language and platform combination forces a programmer to think differently. Thinking differently is good - it teaches you different vectors from which to attack problems.

You can spend a lot of time digging into Node and building different things, but rather than diving deep, I just want to introduce you to the different way of thinking that Node applications require to handle a request in a scalable way. All my knowledge, and this entire post, is based on [The Node Beginner Book](http://www.nodebeginner.org/), so if your interest in Node.js is piqued, I do recommend you continue reading there.

# Creating Basic HTTP Server

Node uses a module system to pull more functionality into your application. The first one that you will undoubtedly learn about is the `http` module, which is how you listen for requests send to a web socket. Here is how you import and use the `http` module to create a working HTTP server:


    var http = require("http");
    
    var requestHandler = function(request, response) {
      console.log("Request received.");
      response.writeHead(200, {"Content-Type": "text/plain"});
      response.write("Hello World");
      response.end();
    };
    
    var server = http.createServer(requestHandler);
    server.listen(8888);


So, when an HTTP request arrives at socket 8888, whatever code we put into the `requestHandler` function is called. This is a pretty straight-forward way of handling an HTTP request, so how is this interesting? Well, it isn't. What *is* interesting, however, is how you are forced to organize your request handling code. I use 'forced' because Node has a few rules around which a programmer must bend his solution. In Node, every request is handled by a single OS process. This means that if we design our solution wrong, one slow request handler will slow the entire process, which is the web server, and cause every other request to wait for the slow code block to finish. Therefore, Node programmers have to learn to follow a few rules to ensure their application scales and stays fast. **This** is why Node.js is interesting.

# Designing Scalable Request Handlers

Node doesn't handle long-running operations very well. That is, if we query a database in Node, its entire process will wait until the single database request returns with a response.


    var result = database.query("SELECT * FROM hugetable");
    console.log("Hello World");


Suppose the database query takes 7 seconds to respond, we won't see "Hello World" displayed until 7 seconds pass. What's worse, all other requests that Node is handling will be paused until the end of that 7 seconds. Would you use an application like this? I wouldn't. Slow apps are bad, so let's make it fast.

# Working Around Slow Operations

One way to fix this database request is to fix our platform. Let's re-program Node to automatically handle each request in a separate thread, so one slow thread doesn't affect another slow thread. With this solution, the programmer doesn't have to think about making fast code. Woah, slow down. We're talking about re-writing the platform. If we did that, we wouldn't learn anything new! So, rather than being lazy and sweeping the problem under the rug, let's face this challenge head on and take responsibility for handling slow operations.

How would we fix the database query above? Well, what's the problem? The problem is that one line of code takes a long time to execute and is slowing the rest of the application. Why can't the program just continue on while that operation finishes? Why does it have to wait? The rest of the program wants to use the result of this operation. It is being placed into a variable for later reference, so the rest of the program has to wait.

This is quite a restriction. Every line of code has dependent lines of code that follow it. All programs that I've written run from start to finish, changing behavior by using if-else blocks. So what can we do? Well...can we manage this dependency somehow? Maybe take these dependent lines of code and wrap them up into a function? If we did this, we can use a lightweight function pointer to use to continue execution when the database result returns.

Let's see how this kind of solution might look:


    var slowOperationHandlingCode = function(rows) {
      var result = rows;
      console.log("Got database rows");
    };
    database.query("SELECT * FROM hugetable", slowOperationHandlingCode);
    console.log("Hello World");

**or**

    database.query("SELECT * FROM hugetable", function(rows) {
      var result = rows;
      console.log("Got database rows");
    });
    console.log("Hello World");


The first thing to notice is that the `database.query` function's signature has changed. Instead of just the query string as a parameter, we are also giving it a pointer to its handling code. This is called a 'callback' or 'callback function'. Put another way, we are telling this slow beast of a function what to do when it's finished.

However, just because added a callback function doesn't mean that this function will be faster. This function could be doing the exact same thing as the first example, which is firing off the query and pausing while it waits for a response. So, what are we really hoping will happen in this function's implementation? I want it to fire off the query, continue on with the rest of the program, and pick up the callback function whenever the query result returns.

If we were to implement this query function to behave like this, how would we do it?

# The Glue of Asynchronous Code

So what is the empirical difference between the problem code and the solution code? Events. We are assuming that this new `database.query` function has been implemented using events. Events seem to be a core part of the Node.js platform. In its essence, it seems that the power of Node as a platform is that it allows you to define a long-running operation and attach a piece of code to its completion event.

We have already seen two examples of this at work. In our first example, Node listens for an HTTP request to arrive, then calls the `requestHandler` function with the HTTP request information. In our `database.query` example, Node listens for a database response to arrive, then calls the `slowOperationHandlingCode` function with the query results.

So far, I have learned how to use these event-driven functions that are in existing modules, but I would like to know how to implement an event-driven function. That is, I would like to know how to set up a raw event in Node. If you want to write an asynchronus module, it seems you can choose between [Events](http://nodejs.org/api/events.html) or [callbacks](http://caolanmcmahon.com/posts/asynchronous_code_in_node_js/). You can use Events to define multiple events for your module's user to handle, or accept a single function to use as a callback. I'm satisfied with the depth of this post, so continue your reading there, if you are curious.

# Conclusion

So, what have we learned? We learned that there are multiple ways to prevent long-running operations from affecting other concurrent requests. Apache can be configured to spawn new processes, new threads, or a combination. It sounds like Nginx is similar to Node in that it uses Events, but it triggers these events in an existing process (don't quote me on that). Node sends each request to a single JavaScript event handler and pushes other long-running operations into its event-callback facilities.












