
## Week 2 Notes

Source: [Algebraic Data Types](http://www.cis.upenn.edu/~cis194/lectures/02-ADTs.html)

### Enumeration Types

#### Why Use Enumeration Types?

_Enumeration types_ is feature which many programming languages have. We call it a feature because people believe it is a Good Thing. Why? What does it do for us? Let's look at what it is.

An enumeration is this: A data type which has a pre-defined set of named values. Another way of looking at it: it allows us to easily assign types to arbitrary values, which allows us to use them as alternatives. We do this by defining the values as members of a named set.

Here is why this feature is a Good Thing. If we instead use a normal Set of String values, its type would be a Set of Strings. This makes it legal to place the values in any function which uses String typed values. This might allow those values to be used in unintended places, forcing developers to be careful.

To solve this typing problem, (I am guessing) we could create a custom data type for the values before adding them to the set. When we declare the type of these values in referencing functions, we limit the acceptable values to what I intended when writing the function.

I imagine an enumeration type as a shortcut for the latter. An enumeration type allows the programmer to add meaning; "This value is a different TYPE of data, not just values, which are semantically comparable." By adding types to these static values, the compiler can check for bugs and code editors can have strong refactoring and code completion features.

#### Enumerations Types in Haskell

Here is how to use Haskell's implementation of the concept of enumeration types.

        data Thing = Shoe 
                   | Ship 
                   | SealingWax 
                   | Cabbage 
                   | King
          deriving Show

What's the English translation? Declare a new type called `Thing`. It has five data constructors (Shoe, Ship, etc.), which are the only possible values of `Thing`. Also, add `Show` functionality to this type to add useability, which will enable conversion to the String type in various places.

Now, how do we use this enumeration type?

        shoe :: Thing -- Our enum is a new type, of course.
        shoe = Shoe   -- Use enum value as literal value, like '15'.

        listO'Things :: [Thing] -- Can make List of our things.
        listO'Things = [Shoe, SealingWax, King, Cabbage, King]

        isSmall :: Thing -> Bool  -- Can use in functions.
        isSmall Shoe       = True -- Can match enum values.
        isSmall Ship       = False
        isSmall SealingWax = True
        isSmall Cabbage    = True
        isSmall King       = False

### Beyond Enumerations

In other languages, "enumeration" is an idea that requires explicit support in the compiler, so you will see that 'enum' is a keyword in other languages. To use this idea of enumerations, Haskell doesn't  use an 'enum' keyword, but a `data` keyword. What is `data`?

The Haskell `data` keyword defines a new "algebraic data type", or just "data type". An enumeration type is just one way to define a Haskell data type, a data type which has alternative values. Haskell data types have many more features than defining a simple set of alternative types, most powerful of which is combining types (algebra) to define a new type.

Let's look at using the advanced features of Haskell's data types.

### Algebraic Data Types

        data FailableDouble = Failure
                    | OK Double
        deriving Show

What does this mean? Declare a new data type called `FailableDouble`, which has two alternative data constructors. The first data constructor, called `Failure`, takes no arguments. This is called a "nullary constructor", which means it doesn't use another value, However, `Failure` itself is still a value, but it behaves as a constant in practical use. The second one, called `OK` takes an argument of type `Double`. This means that we must give the `OK` type a value of `Double` type to be valid. We'll see how to create an OK type in a minute.

How do we use the `FailureDouble` data type? Let's write a function that performs a save division, which allows us to divide by zero without seeing an error.

        safeDiv :: Double -> Double -> FailableDouble
        safeDiv _ 0 = Failure -- If y is 0, this a Failure.
        safeDiv x y = OK (x / y)

This function must return `FailableDouble` typed value, so we must create an `OK` typed value to return. We can create an `OK` typed value by giving it an argument, like this: `OK (x / y)`.

#### Data Constructors

That OK data constructor is interesting.

        > :type OK -- For fun: What is the type of OK?
        OK :: Double -> FailableDouble

Notice the arrow is a single arrow `->`, which means OK is a function. So, give 'OK' one data type (Double), and it will create `FailableDouble` typed value, a different data type. So a data constructor is a function... It has no arguments, so it simply maps from one type to another type. Looking at the definition of the `OK` data constructor, I see a space. Remember that functions use a space to apply it to arguments - interesting. I've read a bit about Category Theory, and this sounds like a Functor. I wonder if my guess is right.

Anyways, here is how we can use our data constructors. These are the names we created in the `FailableDouble` type constructor.

        ex01 = Failure -- Failure is a type
        ex02 = OK 3.4  -- OK is valid type only when given a Double type

As you can see, they are used just like functions. Unlike `OK`, `Failure` has no arguments, so it is used like a constant. It's behaves like a function, so can we use data constructors in pattern matching?

        failureToZero :: FailableDouble -> Double
        failureToZero Failure = 0 -- Can match no-arg data constructor.
        failureToZero (OK d)  = d -- Can reference data constructor's argument.

Can data constructors can have more than one argument?

        -- Store a person's name, age, and favourite Thing.
        data Person = Person String Int Thing -- Look, two 'Person'
          deriving Show

This one is tricky. `Person` is both a *type* constructor (first one), and a *data* constructor (second one). How can we differentiate them? I believe we can call this entire expression a type constructor. But to create data that lies in this type's domain, we use a data constructor, which is the second `Person`.

        brent :: Person -- Notice no arrows. So data, not a function.
        brent = Person "Brent" 31 SealingWax -- Data constructor.

Here, we declare a datum which is Person *type*. Then, we define its *data value*, which must be a Person. To make a `Person`, we must use one of the data constructors used in its type constructor.

On the second line, are we using *data* constructor or *type* constructor? A type constructor is a type definition, so this must a data constructor. If you want to read more, Haskell.org has [constructor wiki page](http://www.haskell.org/haskellwiki/Constructor). The teacher says this is a common idiom - to use the same name for a type and its data constructor.

### A Generic Look at Algebraic Data Types

In general, an algebraic data type has one or more data constructors, and each data constructor can have zero or more arguments.

        data AlgDataType = Constr1 Type11 Type12
                | Constr2 Type21
                | Constr3 Type31 Type32 Type33
                | Constr4

A data of `AlgDataType` type can be constructed in one of four ways. Depending on the data constructor, an `AlgDataType` typed value may contain other values. For example, if it was constructed by using `Const1`, it has two other values, of type `Type11` and `Type12`.

### A Generic Look at Pattern-Matching

Now that we've learned about constructing data types using data constructors, we can explain how pattern-matching works. Pattern-matching works by inspecting a value to see which data constructor built it. Actually, Haskell bases all decisions on a value's data constructor.

For example, let's see all the pattern-matchers we can create for a AlgDataType typed value.

        foo (Constr1 a b)   = ...
        foo (Constr2 a)     = ...
        foo (Constr3 a b c) = ...
        foo Constr4         = ...

Note:
- We give names to the values that come with each constructor.
- Parentheses are required if a pattern has more than a single data constructor. a, b, and c are data, so they also have data constructors.

### Advanced Pattern-Matching

In general, the following grammar defines what can be used as a pattern:

        pat ::= _      -- Underscore is a pattern.
             |  var    -- A variable is a pattern.
             |  var @ ( pat ) -- A variable and pattern combined.
             |  ( Constructor pat1 pat2 ... patn ) -- Data constructor with args.

1. An underscore `_` matches anything.

2. A pattern of the form `x@pat` is equivalent to the `pat` pattern, but you can then use `x` to get the pattern's value(s).

For example:

        baz :: Person -> String
        baz p@(Person n _ _) = "The name field of (" ++ show p ++ ") is " ++ n
        *Main> baz brent
        "The name field of (Person \"Brent\" 31 SealingWax) is Brent"

3. Patterns can be nested.

For example:

        checkFav :: Person -> String
        checkFav (Person n _ SealingWax) = n ++ ", you're my kind of person!"
        > checkFav brent
        "Brent, you're my kind of person!"

Here, we nest the pattern "SealingWax" inside the pattern for Person. Why do we call SealingWax a pattern? It probably became a symbol or literal when we declared it as a data constructor, but I'm not sure.

Another thing we can use in patterns is literal values. Literal values like 2 or 'c' can be thought of as constructors with no arguments.

### Case Expressions

One secret thing about pattern matching: It's built using another Haskell construct: "case expressions"!

Here's what a case expression looks like:

        case exp of
          pat1 -> exp1
          pat2 -> exp2
          -- etc.

To evaluate a case expression, `exp` is matched against `pat1`, `pat2`, etc. to find the first matching one. The corresponding `exp1`, `exp2`, etc. is then evaluated to become the value for the entire case expression.

Here's an example:

        ex03 = case "Hello" of
           []      -> 3
           ('H':s) -> length s -- If array starts with 'H'.
           _       -> 7

As you can see, this decides what to do with a non-variable, "Hello". It will return 4, because "Hello" starts with "H", and the length of "ello" is 4. The underscore will also match this pattern, but it matched an earlier pattern, so the underscore wasn't reached.

This sounds like the pattern-matching in functions. Remember the FailureToZero function?

        failureToZero :: FailableDouble -> Double
        failureToZero Failure = 0
        failureToZero (OK d)  = d

We can use a case expression as an alternative definition:

        failureToZero' :: FailableDouble -> Double
        failureToZero' x = case x of
                     Failure -> 0
                     OK d    -> d

What do you think? Very interesting.

### Recursive Data Types

We forgot to mention the best part about Abstract Data Types - they can be recursive! You know the List type? It is a recursively defined type.

It's kinda like this:

        data IntList = Empty | Cons Int IntList

See how an `IntList` can be created in two ways: it has an Empty value, or it is an Int consed to the front of anoter `IntList`. That's so cool.

This same idea can be used to create many data types, such as traditional data structures, like trees.

        data Tree = Leaf Char
                  | Node Tree Int Tree
          deriving Show




