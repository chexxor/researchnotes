

## Notes

Source: [Intro Notes](http://www.cis.upenn.edu/~cis194/lectures/01-intro.html)

### What is Haskell

- Functional
- Pure
- Lazy
- Statically typed

(Blah blah, the theory will become when we use it)

### Course Themes

- Types
    - Clarify thinking, express structure
    - One form of documentation
    - Move errors from run-time to compile-time
- Abstraction
    - DRY - each idea, algorithm, piece of data should appear only once
    - Haskell tools - Parametric polymorphism, higher-order functions, type classes
- Wholemeal Programming
    - Consider list rather than elements
    - Develop entire solution space, rather than single solution

### Haskell Syntax

#### Declatarations and Variables

`x :: Int` -- means "x has type Int"
`x = 3` -- means "x is defined to be 3"
`-- some comment` -- denotes comment line
`{- multi-line comment -}` -- denotes multi-line comment

A variable can only be defined once.

#### Basic Types

No surprises.

`i :: Int` -- Integer (Value domain is determined by CPU)
`n :: Integer` -- Integer (Value domain determined by memory amount)
`d1 :: Double` -- Floating-point
`d2 :: Float` -- Single-precision floating-point also available
`b :: Bool`
`c :: Char`
`s :: String` -- String (List of characters)

#### Arithmetic

No surprises if comparing to other programming languages.

`ex01 = 3 + 2`
`ex05 = mod 19 3` -- Function name, then parameters
`ex06 = 19 \`mod\` 3` -- A function can be infixed by using backticks
`ex08 = (-3) * (-7)` -- Negative numbers may need paren wrappers to distinguish from subtract operator

`badArith1 = i + n` -- Addition does not implicitly convert types. Use functions to change types, like `fromIntegral` or `round`, `floor`, `ceiling`

`badArith2 = i / i` -- Divide operator only uses floating-point numbers.
`intDiv = i \`div\` i` -- Must use `div` function to divide integers

#### Boolean logic

Boolean operators: `&&`, `||`, `not`, `==`, `/=`, `<`, `>`, `<=`, `>=`

`ex12 = not (False || True)`
`ex13 = ('a' == 'a')`

Haskell has "if-expressions". Both branches must be defined, `then` and `else`. This Haskell `if` symbol is **not*** used for flow control, but for choosing a value.

`if b then t else f`

#### Defining basic functions

Compared to other languages Haskell's functions have tons of features. It's also important to note that Haskell designers decided that Data Types are a Very Good Thing, so we have to think about Types when defining functions, as we'll see.

How do we write a Haskell function? We should start with a simple one.

        -- Compute the sum of the integers from 1 to n.
        sumtorial :: Integer -> Integer
        sumtorial 0 = 0
        sumtorial n = n + sumtorial (n-1)

To write a function, we need three things:
- **Function name**
- **Type declaration**, denoted by the `::` and `->` symbols.
- **Function definition**, denoted by the `=` symbol.

The `sumtorial` function returns an Integer (far right side of the type declaration), and takes one parameter of type Integer (left of arrow). More explanation later.

If you want, you can define _multiple_ function definitions. This sounds illogical - how can one name have multiple values? Wait, this is a function, so it's job is to return varying values if its parameter values change, right? We can organize a function's definition into groups to handle different cases of parameter values.

Each definition for a function is called a "clause". A simple clause consists of:
    - A parameter pattern matcher (left of `=`)
    - A function definition

This makes sense. But we must mention another of Haskell's tools which allow us to further refine a function's definition. For each clause, we can an arbitrary Boolean expression. If it evaluates to True, the function definition is chosen; if False, the next function definition for the clause is checked. That's right, we can define _multiple_ function definitions in a single clause. This Boolean expression is called a "guard", and is denoted by a `|` symbol. It's called a guard because it allows you to preclude the function definition from being used.

So, we have two ways to choose a function definition:
- Simple pattern-matching on parameters
- Complex analysis on parameter values

Choosing a function definition is relatively complex, so please notes these rules for clauses and guards:
- Checked from top to bottom
- Clauses choose first matching pattern
- Guards chooses first True expression
- If no True guards in a clause, skips this clause and continues matching following clauses

        hailstone :: Integer -> Integer
        hailstone n
          | n `mod` 2 == 0 = n `div` 2
          | otherwise      = 3*n + 1  -- `otherwise` is sugar for True

#### Pairs

        p :: (Int, Char)
        p = (3, 'x')

Just pairing two values together. The syntax is used both a) to declare the pair's type, and b) to refer to the pair's values

Haskell also has triples, quadruples, etc., but don't use them because there are better ways.

#### Functions with multiple arguments

        f :: Int -> Int -> Int -> Int
        f x y z = x + y + z
        
        ex17 = f 3 17 8

Why is the type definition not `Int Int Int -> Int`? This is for a deep and beautiful reason, which we will learn about later.

#### Lists

Unlike some languages, Haskell designers consider lists essential to defining functions, so it is a very basic data type in Haskell. This means it has some some special syntax we should note.

        nums, range, range2 :: [Integer]
        nums   = [1,2,3,19]
        range  = [1..100]
        range2 = [2,4..100]

        emptyList = []
        listCons = 1 : []
        listCons2 = 3 : (1 : [])
        listCons3 = 2 : 3 : 4 : []

#### Functions on lists

How do we use lists in functions, as parameters and such? Pattern matching.

        -- Compute the length of a list of Integers.
        intListLength :: [Integer] -> Integer
        intListLength []     = 0 -- If length is zero.
        intListLength (x:xs) = 1 + intListLength xs -- Don't use `x`, so could replace with `_`.

Here we can see the `:` operator is also used in pattern matching. In this case, the second clause will match all of the following cases: `1 : []`, `1 : [2]`, `1 : [2, 3]`

This snippet shows how to match on first two elements of a list parameter.

        sumEveryTwo (x:(y:zs)) = (x + y) : sumEveryTwo zs

#### Combining functions

Haskell forces us to make functions rather simple. So, to add more complex functionality, we must write functions which use functions.

Nothing strange here. Just note that `hailstoneSeq` is evaluated before `intListLength` here.

        -- The number of hailstone steps needed to reach 1
        -- from a starting number.
        hailstoneLen :: Integer -> Integer
        hailstoneLen n = intListLength (hailstoneSeq n) - 1

### Haskell Style

The course teacher suggests we follow the following rules when writing Haskell. The teacher suggests style guidelines help to increase creativity. Also, I believe it helps other Haskell programmers understand our code.

Functions
- DO use `camelCase` for functions and variable names.
- DO use descriptive, yet short, function names.
- DO give every top-level function a type signature. Error messages will appear closer to the problem line.
- DO precede every top-level function with a comment which explains functionality.

Layout
- DON'T use tab characters. Haskell is layout-sensitive.
- DO try to keep every line under 80 characters. Don't rely on editor's line-wrapping

Running Code
- DO use `-Wall`. Can use `ghc -Wall` on command line or put `{-# OPTIONS_GHC -Wall #-}` at the top of the `.hs` file.
- DO break program into small functions, and compose them into more complex functions.
- DO make all functions _total_, which means they should sensibly handle **all** inputs.

### Resources

Installation and Coding Environment
- [Haskell Platform](http://hackage.haskell.org/platform/)
- [haskell-mode for emacs](http://projects.haskell.org/haskellmode-emacs/)
- [haskell-mode for vim](http://projects.haskell.org/haskellmode-vim/)

Books
- [Learn You a Haskell For Great Good](http://learnyouahaskell.com/)
- [Read World Haskell](http://book.realworldhaskell.org/)
- [Haskell Wikibook](http://en.wikibooks.org/wiki/Haskell)

Reference
- [Typeclassopedia](http://haskell.org/haskellwiki/Typeclassopedia)
- [Haskell Cheatsheet](http://cheatsheet.codeslower.com/)
- [Hoogle](http://www.haskell.org/hoogle/)
- [Hayoo](http://holumbus.fh-wedel.de/hayoo/hayoo.html)

