
## Week 5 Notes

Source: [More Polymorphism and Type Classes](http://www.cis.upenn.edu/~cis194/lectures/05-type-classes.html)

Supplemental Reading: Learn You a Haskell for Great Good: [Making Our Own Types and Typeclasses](http://learnyouahaskell.com/making-our-own-types-and-typeclasses)

### Parametricity

Polymorphism is a Good Thing. What's polymorphism? The ability to use a single subroutine/function in a variety of contexts. Suppose you write a great `sort` function for your list - good for you. However, suppose that function is specific to your problem and it can't be used by your friends for their lists - bad for everybody else. More polymorphic units of code means less rewriting of basic or common code.

Haskell has polymorphism, in an incarnation called "parametric polymorphism". You've seen type signatures like `[a] -> a`, right? In type signatures, a one-letter variable like `a` is called a "type variable", which means ANY type of parameter is allowed. If you create such a polymorphic function, *your friends* can also use it, not only yourself. Free bonus! Haskell makes it super-easy to write such polymorphic functions which can benefit all of mankind. Thanks Haskell, for helping me feel altruistic.

So, we should use these `a`s and `b`s in *every* function, right? Well, that's certainly easier said that done. It's easy to use `a`s and `b`s in the type signature, but we will probably meet resistance when implementing the function. For an example, see this simple function:

        f :: a -> a -> a
        f x y = x && y

While it looks innocent, the function's type signature and its implementation disagree. Look at the `&&` operator's type signature: `(&&) :: Bool -> Bool -> Bool`. So, the type signature says "We accept anything!", but the implementation says "Bools only!" What happens if we call this function with two Strings? Well, we will never find out, because the Haskell compiler sees the type disagreement and throws an error: `Couldn't match type 'a' with 'Bool'`. This can be fixed by changing the type signature to `Bool -> Bool -> Bool` or by removing the `&&` operator.

Suppose we originally intended this function to accept any type, which means we won't change the type signature, how could we change the implementation to accept any type? Can we explicitly write a case to handle every possible type, something like this?

        -- If not Int or Bool, then fallback to just return x.
        f :: a -> a -> a
        f x y case (typeOf x) of
            Int  -> x + y
            Bool -> x && y
            _    -> x

This looks promising, but this is impossible in Haskell because it does not have any incarnation of the `typeOf` idea. (It does have pattern matching on data constructors, however.) Why not? One problem is a technical matter. During compilation, Haskell uses type signatures to verify program completeness, but then promptly discards them. This means a compiled Haskell program has no type information to inspect when choosing one of our type-predicated paths. However, the problem is also a philosophical matter, which we might later see.

### Two Views on Parametricity

Other popular programming languages, such as Java, have the `instanceOf` or `typeOf` facilities to support polymorphism. Specifically, these facilities enable changing behavior depending on an input's specific type. Is custom behavior for each input type a fundamental trait of polymorphism? Maybe so.

Haskell allows custom behavior for various input types, but it doesn't use `typeOf` and branching logic. Let's illustrate this by looking at an example.

### Partially Polymorphic Functions

The `(+)` operator is an example of a polymorphic function. When given two `Integer` values, this operator behaves very differently than when given two `Double` values. This means it is polymorphic and therefore it can be receive any type, right? Well, no, because it can't receive `String` types, as you know. Interesting, so it appears the `(+)` function can receive some types, but not others. This sounds like a conditional polymorphism. How is this implemented?

The truth is that `Double` and `(+)` were not given special permissions by the compiler due to being basic functionality. The `(+)` operator can be partially polymorphic like this by using a Haskell feature called "type classes".

### Type Classes

Let's see type class information in the wild. Open up GHCI and use the `:t` command on some type-classed functions, like this:

        -- Examples of functions which have type classes.
        > :t (+)
        (+)  :: Num a => a -> a -> a
        (==) :: Eq a   => a -> a -> Bool
        (<)  :: Ord a  => a -> a -> Bool
        show :: Show a => a -> String

There they are! Type classes in the wild, hiding in the type signature. `Num`, `Eq`, `Ord`, and `Show` are type classes, common ones defined in the basic library. Note: we say that type-classed functions like these are "type-class polymorphic" functions.

#### Type Class Definition

We've seen them, but what is a type class? How do they help us write a conditionally polymorphic function? Let's look at a cleaned-up version of the `Eq` type class definition.

        class Eq a where
          (==) :: a -> a -> Bool
          (/=) :: a -> a -> Bool

This is the Haskell code which defines the `Eq` type class. Let's translate this into English. "Eq" is a type class, it has one parameter "a". Any type which wants to be an instance of "Eq" must define the "(==)" and "(/=)" functions. These function's type signatures must be like this.

Let's back up to look at the `(==)` signature again: `(==) :: Eq a  => a -> a -> Bool`. The stuff just in front of the `=>` symbol is a "type class constraint". This bit restricts the `a` type variable to types which are instances of the `Eq` type class. From another point of view, the `(==)` function will accept data of *any* type, on one small condition - the type you choose must be an instance/member of the `Eq` type class.

In other words, a type class declares that all its members have a minimum skill-set, that they have a minimum set of functions. We say that this set of functions are "methods" of communicating with instances of a type class.

#### Type Class Instantiation

Let's learn by practicing, by adding a type to a type class. Let's make a new type, 'Foo', and add it to the `Eq` type class.

        -- Define the type. Ours can be made by using F or G,
        --   which, we say, can be constructed with Int and Char.
        data Foo = F Int | G Char

        -- Add our type to the Eq type class,
        --   then define (==) and (/=) for each constructor.
        instance Eq Foo where
          (F i1) == (F i2) = i1 == i2
          (G c1) == (G c2) = c1 == c2
          _      == _      = False
          foo1   /= foo2   = not (foo1 == foo2)

That's all! To add a type to a type class, we use `instance`, then implement the required methods of interacting with that type. As with any function, the compiler uses pattern matching on type constructors to choose which function implementation to use.

#### Type Class Shortcuts

Let's look at the `Foo` type we just defined. Maybe you noticed that we shouldn't be required to define both `(==)` and `(/=)` functions, as one is just the opposite of the other. That is, if one of the type class' functions is defined, the other can be derived.

Actually, type classes offer a shortcut for defining their required functions. If one required function can be defined in terms of another, we can specify a default definition in the type class definition. We can see this by looking at an expanded version of the `Eq` type class definition:

        class Eq a where
          (==), (/=) :: a -> a -> Bool
          x == y = not (x /= y)
          x /= y = not (x == y)

With `Eq` defined like this, to instantiate it, we only need to define one of its two required functions. If we define `(==)`, when the compiler resolves a reference to `(/=)`, it simply refers to our `(==)` definition.

One other super-awesome shortcut. `Eq` and a few other standard type classes **are** special. How? Their definitions are simple enough that the compiler can automatically generate instances for most any type. That means what? That means that you can add your own types to these type classes without using the `instance` keyword and explicitly defining their functions. You can freely use them as if they were keywords in the language!

This means, to add `Foo` to `Eq`, we can throw away all the above code, and just use the `deriving` keyword, like this:

        data Foo = F Int | G Char
          deriving (Eq, Ord, Show)


#### Type Class Summary

We never answered the question: The `(+)` function behaves differently for `Integer` and `Double` types. How does this work? Both `Integer` and `Double` types defined that function with nearly identical signatures: `(+) :: a -> a -> a`. How does the compiler choose?

Here's my interpretation of the process, which is possibly incorrect:

        (+) 2.2 3.3
        -- (+) :: Num a => a -> a -> a

Finding the connection from the parameter's type to the appropriate function definition.
1) `(+)` is a function. Where's the implementation?
2) Here, `a` is `Double`.
3) `Double` was added to the `Num` type class.
4) `Num` defines a `(+)` function: `(+) :: a -> a -> a`.
5) Result: parameter type -> type class -> function: `Double` -> `Num` -> `(+)`.

You might find the [`Num` class type documentation](http://hackage.haskell.org/packages/archive/base/3.0.3.1/doc/html/GHC-Num.html) interesting.

Simon Peyton Jones, a core author of the Haskell language, has a better explanation of the implementation of type classes in his talk, [Classes, Jim, but not as we know them](http://channel9.msdn.com/posts/MDCC-TechTalk-Classes-Jim-but-not-as-we-know-them).

#### Type Classes and Java's Interfaces

After understanding Haskell's type classes, you may believe they are similar to interfaces in languages like Java or C#. And you are right. However, type classes are a more flexible solution:

- Java: Declare class, must also declare its interfaces.
- Haskell: Declare type, can create type class instance elsewhere (such as module).

- Java: Interface methods are quite rigid.
- Haskell: Type class methods' types can be general and flexible. Multi-parameter type classes are quite easy.

#### Standard Type Classes

Several type classes are part of the standard library. You should recognize the following type classes as standard so that you aren't surprised by each new type class in the wild you see.

**Ord** type instances can be compared, which means they can be totally ordered. Methods include `(<)`, `(<=)`, and `compare`.

**Num** type instances can be counted. Methods include `(+)`, `(*)`, and `(-)`.

Note: integer literals are `Num` type instances. `Integer`, `Double`, `Complex Double`, etc. are also `Num` type instances, which means integer literals can be used as any of these types.

**Show** type instances convert values into `String` types.

**Read** type instances convert `String` types into values.

**Integral** type instances represent whole numbers, such as `Int` and `Integer`. Methods include `quot`, `rem`, and `toInteger`, which perform integer division with remainders.

#### Create New Type Class: Example

Let's make a new type class. Here's the scenario: I want to turn things into `[Int]` types. So, I need a function like this: `toList :: a -> [Int]`, right?

Well, let's think about that "a" type. What types are possible? Well, "a" means anything. Can we convert anything to `[Int]` type? Let's think about possible inputs. If I had to guess, I predict these types might be passed: `Int`, `String`, `Double`. We can convert these to `[Int]`, but, what about trees or other custom-made data types?

By looking at each input type, we can see that the algorithm, converting each input type to `[Int]` type, will be radically different. How do we organize these algorithms? Should we make seperate functions for each input type, like this: `doubleToList :: Double -> [Int]` and `stringToList :: String -> [Int]`? While the idea seems right, the organization seems wrong. How to re-organize? Consider this, why do we want to add the input data's type to the function name? It helps a programmer choose the correct function. However, consider Haskell's faculties. Haskell knows the input data's type during compilation. Haskell should be able to choose the correct implementation, right? We just need to help by attaching each implementation to its type. We do this with type classes.

        -- We want a function like this.
        -- But, the implementation will be different
        --   for each type of "a", so...
        -- toList :: a -> [Int]
        
        -- So, declare this method inside a type class,
        --   and implement an instance for each type of "a".
        -- That is, create a class of types which support
        --   becoming a list.
        -- Call them "Listable" types.
        class Listable a where
          toList :: a -> [Int]
        
        -- Implement `toList Int -> [Int]`
        instance Listable Int where
          toList x = [x]
        
        -- Implement `toList Bool -> [Int]`
        instance Listable Bool where
          toList True = [1]
          toList False = [0]
        
        -- Implement `toList [Int] -> [Int]`
        instance Listable [Int] where
          toList = id
        
        -- A friend made a custom data type. Let's make it Listable.
        data Tree a = Empty | Node a (Tree a) (Tree a)
        -- Implement `toList Tree -> [Int]`.
        instance Listable (Tree Int) where
          toList Empty        = []
          toList (Node x l r) = toList l ++ [x] ++ toList r

#### Building on Type-Classed Functions

What happens if we want to use a type-classed function inside another function? Here's an example:

        sumL :: Listable a => a -> Int
        sumL x = sum (toList x)

Notice the type signature. Because its parameter is used in the `toList` function, the `sumL` function also receives the type constraint.

What happens if a function uses two type-class polymorphic functions?

        foo :: (Listable a, Ord a) => a -> a -> Bool
        foo x y = sum (toList x) == sum (toList y) || x < y

The type signature indicates that the `a` type variable must be Listable *and* Ordinal. Why Ordinal? Notice the last part, `x < y`, which is a function requiring comparable data types.

One final question. Can we use a type-class polymorphic function inside a type class instantiation? Take a look:

        instance (Listable a, Listable b) => Listable (a,b) where
          toList (x,y) = toList x ++ toList y

This instantiation refers to the functions we defined earlier. This is *not*, as it may seem, a recursive definition. This adds *pairs* of Listable types to the Listable class.

