
## Introduction to Category Theory 1: Course Overview

Source: youtube, user error792


### History

Has roots in algebraic topology. Trying to associate algebraic objects to topologic objects.
Want to take algebraic results to get deep meaning on topologic objects.

First efforts was Homology, holes in space
Next was Homotopy, how to move around in a space

If we want to talk about the homology of a torus, this one thing.
But what if we want to talk about the general process of mapping from topology to algebra?

After a few functions from a few topologic objects to algebraic objects, study shifted to find the relationships between these functions.
Mathematics in that time did not have tools to describe these relationships. This is where category theory was born.

### First Category Theory Mathematicians

Category Theory was slow to catch on. People thought it was nonsense, just intellectual fun.
1945, Eilenberg Mosbore

1960s, Grothendieck
- algebraic geometry
- actually used these abstract ideas to solve real problems

Lawvere - Tyrreny
- Applied these ideas to logic
- Alternatice foundations in Mathematics
- Instead of set theory, how about category theory

Now
- homological algebra	
- combinatorial
- graph theory
- haskell language
- describe data theory
- physicists find uses, string theory
- generalize category theory: higher categories and enriched categories


### What is Category Theory? How to use it?

Category Theory is a language and a philosophy, a way of looking at math.

Set Theory is not used to prove problems, only to show thoughts.
- What are mathematical objects? A set with some operations.

Category Theory
- What are mathematical objects does not matter.
- How do mathematical objects behave? How do they relate to each other?

### Example 1

To understand a circle, look at two semi-circles. If we can understand the components, we can understand the original question.

We want to relate Complex numbers with Real numbers. Set theory can not make a union between these two. Category theorists have found an operator to say this.

Category Theory is not a toolset, but a mindset.

If you are a mathematician and only care about your own field, should you still care about Category Theory?

### Example 2

Simplicial Set

Set Theory says Simplicial Set has tons of operations and equations
Category Theory says it is just one equation, just one concept.

## Things to Study to Understand Category Theory

- Functors, Duality, Comma Categories
- Colimits, limits
- Adjunctions and adjoints
- Monads
- Kan Extensions

Three Theories
- Yoneda Lemma
- Freyds Adjoint Functor Theorem
- Becks Theorem



## Monoids

Monoid is a set

Monoid has
- a binary operation
- associative
- has an identity/neutral element (symbol: e, 1, 0)

Monoid does not have
- commutativity
- inverses/negative

Notation is isomorphic to the objects we are considering
If our situation can be modeled by sticking symbols together, regardless of what the symbols mean, then we have a monoid.

To fully understand monoids, we must understand a monoid as it relates to another monoid, called Morphisms

Like a drink monoid and a money monoid. How can we relate these two monoids?
We find a map between drinks and money.

A Morphism consists of a function f:M -> N
Mx = Ny
Mfx = Nfy
M1 = N1






## Categories, Functors, and Natural Transformation

A category C consists of:
- set of objects ob(C)
- set of arrows ar(C)
- way to compose arrows (associativity)
- identity arrow

An 'arrow' is a morphism

### Types of categories

Represent areas of study or branches of mathematics

Set
- sets
- functions

Top
- topological spaces
- continuous maps

Ring
- rings
- ring homomorphisms

Grp
- groups
- group homomorphisms

R-Mod or Mod-R
- (left) R-modules
- module homomorphisms

K-Vec
- K-vector spaces
- K-linear transformations

Hask
- types
- functions


### Functors

Relations between categories
A functor F from category C -> category D consists of two functions
- object functions ob(F): ob(C) -> ob(D) 
- arrow function ar(F): ar(C) -> ar(D)

domains and codomains of arrows
identity arrows
composition of arrows

A bunch of examples a non-math speaker can not understand


### Natural Transformation

A Natural Transformation is a relationship between Functors

A bunch of examples that a non-math speaker can not understand.


These lectures are terrible. A math newb has no chance, because the speaker speaks in math, not English.




