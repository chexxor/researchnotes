
## Week 7 Notes

Source: [Folds and Monoids](http://www.cis.upenn.edu/~cis194/lectures/07-folds-monoids.html)

Supplemental Reading:

- Learn You a Haskell, [Only folds and horses](http://learnyouahaskell.com/higher-order-functions#folds)
- Learn You a Haskell, [Monoids](http://learnyouahaskell.com/functors-applicative-functors-and-monoids#monoids)
- Haskell wiki, [fold](http://haskell.org/haskellwiki/Fold)
- Heinrich Apfelmus, [Monoids and Finger Trees](http://apfelmus.nfshost.com/articles/monoid-fingertree.html)
- Dan Piponi, [Haskell Monoids and their Uses](http://blog.sigfpe.com/2009/01/haskell-monoids-and-their-uses.html)
- Data.Monoid [documentation](http://haskell.org/ghc/docs/latest/html/libraries/base/Data-Monoid.html)
- Data.Foldable [documentation](http://haskell.org/ghc/docs/latest/html/libraries/base/Data-Foldable.html)

### Generic Folds on Non-list Types

We've already seen how to define a function which folds a list. We can generalize this idea to non-list types as well.

How would we write a function which folds a binary tree? Let's use this one, which has two constructors:

    data Tree a = Empty
            | Node (Tree a) a (Tree a)
        deriving (Show, Eq)
    leaf :: a -> Tree a
    leaf x = Node Empty x Empty

For many functions which analyze the tree, we need to use recursion. Consider these tasks: a) How do we find its size, its number of nodes? b) How do we find its sum, the data in the nodes? c) How do we find its depth? d) How do we transform it into a list?

    -- Recurse over internal data structures,
    --   stop recursion on value-holding constructor.
    treeSize Empty        = 0
    treeSize (Node l _ r) = 1 + treeSize l + treeSize r
    treeSum Empty     = 0
    treeSum (Node l x r)  = x + treeSum l + treeSum r
    treeDepth Empty        = 0
    treeDepth (Node l _ r) = 1 + max (treeDepth l) (treeDepth r)
    flatten Empty        = []
    flatten (Node l x r) = flatten l ++ [x] ++ flatten r

What's the pattern? Different strategies of recursion and summation. Let's transform this pattern into a function. Its parameters are 1) the return type, 2) stopping case, and 3) how to recurse.

    -- e         == Value at stopping case.
    -- f         == Operation to perform at each node.
    -- Empty     == Constructor on which to stop.
    -- (Node...) == Constructor on which to recurse.
    treeFold :: b -> (b -> a -> b -> b) -> Tree a -> b
    treeFold e _ Empty        = e
    treeFold e f (Node l x r) = f (treeFold e f l) x (treeFold e f r)

If we assume this function is correct and generic, we should be able to rewrite the earlier functions using `treeFold`.

    -- Value at root == 0
    -- Func on nodes == (\l _ r -> 1 + l + r)
    -- Input Tree    == zero-point style, so invisible
    treeSize'  = treeFold 0 (\l _ r -> 1 + l + r)
    treeSum'   = treeFold 0 (\l x r -> l + x + r)
    treeDepth' = treeFold 0 (\l _ r -> 1 + max l r)
    flatten'   = treeFold [] (\l x r -> l ++ [x] ++ r)
    -- Can also easily write a new one.
    -- Just pass in what to do at each node.
    treeMax    = treeFold minBound (\l x r -> l `max` x `max` r)

This `treeFold` function is pretty awesome! Why can't we use the old `foldl` function? That is made for list types, so it doesn't know how to pattern match complex data structures, presumably.

#### Folding Multiple Constructors

In an earlier exercise, we defined and worked with the `ExprT` type. It's type signature is more complex, because it has three constructors. Let's look at it and explore a folding strategy.

    data ExprT = Lit Integer
           | Add ExprT ExprT
           | Mul ExprT ExprT

How do we write a fold function for this? For help, look at the "Folds in General" section.

    -- f        == Operation for Lit constructor
    -- g        == Operation for Add constructor
    -- h        == Operation for Mul constructor
    -- (Lit...) == Get references to variables
    -- (Add...) == Get references to variables
    -- (Mul...) == Get references to variables
    exprTFold :: (Integer -> b) -> (b -> b -> b) -> (b -> b -> b) -> ExprT -> b
    exprTFold f _ _ (Lit i)     = f i
    exprTFold f g h (Add e1 e2) = g (exprTFold f g h e1) (exprTFold f g h e2)
    exprTFold f g h (Mul e1 e2) = h (exprTFold f g h e1) (exprTFold f g h e2)

Let's test this more complex fold. Where can we use it? We can rewrite our original `eval` function. We can also create a whole-new function which summarizes an expression.

    -- Evaluate an expression.
    eval2 = exprTFold id (+) (*)
    -- Number of literals in an expression.
    numLiterals = exprTFold (const 1) (+) (+)

#### Folds in General

A fold can be implemented for most data types, and it is a very useful utility to have. Here's the general formula to write a fold on any type `T`. A fold function needs one argument, a function, for each of `T`'s constructors, which should translate the constructor's values into the result. 

### Monoids

Now, let's move on to discuss "monoids". In Haskell, `Monoid` is a standard type class. You can find it in the `Data.Monoid` module. Take a look at the definition of the `Monoid` type class.

    class Monoid m where
      mempty  :: m
      mappend :: m -> m -> m    -- `(<>)` is shorter alias.
      mconcat :: [m] -> m
      mconcat = foldr mappend mempty

The idea is that a monoid means that two things of a type are "join-able". Here's how it's defined in math-y terms:

1) mempty <> x == x
2) x <> mempty == x
3) (x <> y) <> z == x <> (y <> z)

So, in Haskell, `Monoid` instances have three methods: 1)an identity function, named `mempty`, and 2) a combination operation, named `mappend` or `(<>)`, which associatively combines two values, and 3) `mconcat`, which uses `(<>)` to combine a list of values. Associative means that order of evaluation doesn't change the result, so no need to use parentheses when using `(<>)`.

Why are we looking at monoids? It's a useful way to look at data, and they appear in lots of Haskell code. Their properties look quite basic, so how can they be so useful? Let's learn by using it in a few places.

#### Using Monoids

Let's make some monoids. Where can we use them? Well, what's join-able? We can join lists, so let's make a monoid under lists. To do that, we need to make `[]` an instance of `Monoid` and implement its two methods.

        instance Monoid [a] where
          mempty  = []
          mappend = (++)

What else can we join? Numbers, we can join numbers together, by using addition or multiplication. But, before we start to implement this, can you see that this is actually two different ways of joining numbers together? This means we need to make two different monoids, `Sum`, and `Product`. This implies that we split the `Num` type into two, which can be accomplished using `newtype`.

        -- Define numbers as monoids: Summable or Product-able.
        newtype Sum a = Sum a
          deriving (Eq, Ord, Num, Show)
        newtype Product a = Product a
          deriving (Eq, Ord, Num, Show)
        -- Make Sum and Product instances of Monoid.
        instance Num a => Monoid (Sum a) where
          mempty  = Sum 0     -- Choose identity: 0 + 0 = 0
          mappend = (+)
        instance Num a => Monoid (Product a) where
          mempty  = Product 1 -- Choose identity: 1 * 1 = 1
          mappend = (*)

What else can be a monoid? Pairs? How would we join pairs? This is interesting because a pair is a kind of data structure. How do we join data structures? Interesting question. In the case of pairs, joining two pairs seems pretty simple. Take a look:

        instance (Monoid a, Monoid b) => Monoid (a,b) where
          mempty = (mempty, mempty)
          (a,b) `mappend` (c,d) = (a `mappend` c, b `mappend` d)




