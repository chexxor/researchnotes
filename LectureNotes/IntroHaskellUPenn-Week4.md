
## Week 4 Notes

Source: [Higher-order Programming and Type Inference](http://www.cis.upenn.edu/~cis194/lectures/04-higher-order.html)

Supplemental Reading: Learn You a Haskell for Great Good: [Higher-Order Functions](http://learnyouahaskell.com/higher-order-functions)

### Anonymous Functions

Suppose we want to write relatively simple function:

        greaterThan100 :: [Integer] -> [Integer]
        greaterThan100 [1,9,349,6,907,98,105] = [349,907,105].

How can we implement this?

Our first attempt might look like this, which uses `filter`:

        gt100 :: Integer -> Bool
        gt100 x = x > 100

        greaterThan100 :: [Integer] -> [Integer]
        greaterThan100 xs = filter gt100 xs

However, I think this solution is not-so-pretty. Why? We create a function, which is necessary, but we will likely never use it again. So the ugliness lies in giving a name to this function, which treats it as equally important to `greaterThan100`, a function which is essential to our greater problem.

#### Lambda Abstractions

One way to remove this minor function's name from our program is to use a thing called a "lambda abstraction". This is more widely known as an "anonymous function", because its unique characteristic lies in using a function to which we haven't given a name. Normally we define a name for a unit of functionality, and we use that functionality by using its name. So the uniqueness is in its idea: if a function has no name, how can we use it? We can see the answer by refactoring the `greaterThan100` function above to remove the minor `gt100` function:

        greaterThan100_2 :: [Integer] -> [Integer]
        greaterThan100_2 xs = filter (\x -> x > 100) xs

The backslash is intended to be lambda, a Greek letter used in mathmatics to denote a pure function. We don't have a lambda key on our keyboard, so a backslash "\" is the best we can do. `x` is an argument and the stuff after the `->` is the function definition.

A multiple-argument lambda abstraction looks like this: `\x y -> x + y`

#### Operator Sections

Now, lambda abstraction is one way to produce an anonymous function in Haskell, but another method also exists. Take a look.

        greaterThan100_3 :: [Integer] -> [Integer]
        greaterThan100_3 xs = filter (>100) xs

An anonymous function (it has no name) is returned from a function with too-few arguments, which is a technique called "partial function application". If a function expects two arguments, we can pass just one to receive a function awaiting its final argument. (Note: Haskellers may also call this an "[operator section](http://www.haskell.org/haskellwiki/Section_of_an_infix_operator)".) Let's look at some examples:

        -- From section to function:
        (>100) == \x -> x > 100
        (?y) == \x -> x ? y

        -- Examples:
        > (>100) 102
        True
        > (100>) 102
        False
        > map (*6) [1..5]
        [6,12,18,24,30]

### Beauty of Haskell's Implementation of Functions

These partially-applied functions is a neat trick. How is this possible? The beauty of Haskell's functions is a single innovative idea which can be stated in one sentence:

        "All functions in Haskell take only one argument."

#### Order of Evaluation and Parentheses

To understand the implications of this beautiful idea, let's look again at the type signature of a Haskell function which has multiple arguments:

        -- Consider this function's type signature.
        -- f x y = 2*x + y
        f :: Int -> Int -> Int
        -- Use arrows like this for a two-arg function's type signature.
        -- Now, add some parentheses.
        f :: Int -> (Int -> Int)
        -- Pass one argument, receive one function!
        -- This is not intuitive if we look only at the function definition above.

In type signatures, we can add parentheses to the right-most groups and not change the meaning. In programming, parentheses are used to clarrify ambiguous evaluation orde, for the compiler or the program maintainer. So, when you see a complex type signature, try adding parentheses to the right-most elements to create a one-argument function on the right. This can be continued to create a series of one-argument functions: `W -> (X -> (Y -> Z))`

It is important to note, however, that we must use parentheses differently in type definitions than in function applications.

        -- Parentheses: Compare equivalency:
        -- Type definition
        W -> X -> Y -> Z
        W -> (X -> (Y -> Z))
        -- Function application
        f 3 2
        (f 3) 2

#### The Meaning of Associativity

To maintain equivalency when using parentheses depends on what we are grouping. Type definitions creates right-most groups while function applications creates left-most groups. Why? The concise answer: Type definitions are "right-associative" while function applications are "left-associative".

Let's quickly summarize what "associativity" means. If we have a list of things and we say they are "right-associative", our meaning is that right-er things are more important. That is, the value of a left-er thing depends on its right-er thing. In programming, this describes the evaluation order of equivalent elements of a statement.

### Partial Function Application in General

Why do we mention associativity? We wanted to explain how operator sections work. An operator is just a function. Likewise, an operator section is just a "partially applied function". That is, it is just one example of passing too-few arguments to a Haskell function. Let's use this idea in other, normal, functions.

In the function `f` above, we can pass only one argument and the compiler will still create something useful: a function. Until we satisfy every argument with a value, a Haskell function will not evaluate to a literal value.

Earlier, I mentioned that this idea is not intuitive. This is because a normal person's understanding of a function is different from Haskell's understanding of a function. Let's look at a mapping between these two views, in pseudocode:

        f x y z = <...>
        -- is semantically equivalent to
        f = \x -> (\y -> (\z -> <...>))

People see a three-argument function. Without three args, this function means nothing. Why? People want values. Without all arguments, a function can not produce a value.

Haskell, on the other hand, is a computer. Therefore, it has steps to evaluate a function, which include processing each argument. As a language which adheres to pure functional behavior, Haskell will return a useable bit after each step. This bit is a function!

#### Art of Choosing Argument Order

Haskell only allows us to easily partially apply a function's first argument. It *is* possible to partially apply an argument other than the first, but it isn't easy. (The one exception is [infix operators](http://en.wikibooks.org/wiki/Haskell/More_on_functions#Operators_and_sections) by using operator sections, as we've seen).

In practice this is not that big of a restriction. There is an art to deciding the order of arguments to a function. One pattern is to order arguments from from "least to greatest variation". That is, arguments which will often be the same should be listed first, and arguments which will often be different should come last.

### Function Composition

We mentioned that we can use parentheses in type definitions. Haskell only understands one-argument functions by default, but we can use parentheses to add more complex meaning. For example, we can use parentheses to denote a function which accepts a function argument.

We can see this in the `(.)` function's type definition.

        (.) :: (b -> c) -> (a -> b) -> a -> c

It says: pass two functions as arguments to produce a new, single function. This function sounds interesting, but is it useful? It must be useful, because it has a special name, "function composition". But how and where should we use it?

Using function composition, we can combine smaller functions into a single function. That is we can pass a value through one giant function, rather than multiple smaller functions. Why is this a Good Thing? Documentation and meaning. Compare the following two function implementations. The first is relatively simple, but the second shows this: the true value of this function is not the implementation, but the workflow it defines.

        myTest :: [Integer] -> Bool
        myTest xs = even (length (greaterThan100 xs))
        myTest' = even . length . greaterThan100

### Currying

Just for fun, let's look at a few ways to implement the `(.)` function:

        -- The interesting part: How to return a function?
        -- Could use a lambda
        (.) f g = \x -> f (g x)
        -- Or use partial function application
        (.) f g x = f (g x)

The key point here: we can write a function which returns a function. Actually, this idea is called "currying", named for the British mathematician and logician Haskell Curry (1900-1982).

Why do we mention currying? Earlier, we said that all Haskell functions accept only a single argument. But, what if we want to force a user to pass two arguments? We can use a tuple. Take a look:

        g :: (Int,Int) -> Int
        g (x,y) = 2*x + y

The important bit to understand here is that using tuples in arguments is a possibility. Actually, it may be useful in some cases. As a Haskell developer, you may want to use library functions which return tuples or accept tuple arguments. As tools in this tasks, Haskell's standard library has two interesting functions: `curry` and `uncurry`. These convert functions between a one-tuple argument function and a normal two argument function. Maybe their type signatures will help you:

        -- From tuple-accepting function,
        -- to a two-argument function.
        curry :: ((a, b) -> c) -> a -> b -> c
        -- From two-argument function,
        -- to a tuple-accepting argument.
        uncurry :: (a -> b -> c) -> (a, b) -> c
        -- Example
        > uncurry (+) (2,3)
        5

### Wholemeal Programming

Partial function application is a Big Deal. It may sound like just a neat trick at first, but when you see how a pro Haskeller uses it, you will want to practice the technique.

Consider the following function, which seems pretty straight-forward.

        foobar :: [Integer] -> Integer
        foobar []     = 0
        foobar (x:xs)
          | x > 3     = (7*x + 2) + foobar xs
          | otherwise = foobar xs

You like it? Well, this has bad Haskell style. Why? We're working at low level.

How can we improve this code in the eyes of a pro Haskeller? Instead of thinking about individual elements, we should try to think about transforming the entire input. Recursion patterns on lists is our knife and butter for this slice of bread.

Consider the refactored version of the `foobar` function above, written in idiomatic Haskell style:

        foobar' = sum . map (\x -> 7*x + 2) . filter (>3)

No arguments? Those are hidden by partial function application. This chain of functions has one function which is missing an argument, `filter`, which means that this entire chain will evaluate to a function awaiting a final argument. Therefore, the `foobar'` function's argument, an array, will become the final argument of `filter`.

A summary of this function chain: filter elements (greater than three) to keep, then transform them, then sum the array.

#### Pointfree Style

Interesting to note that the `foobar'` function above doesn't reference use or reference arguments. This style is considered a Good Thing. It defines a function which transforms, not a data-shuffling, complex function which spreads logic and case statements around (as if on a dissection table).

Not all functions can be cleanly written in pointfree style, however. Don't force it where it shouldn't be, as it will only muddy your code. Readable code is more maintainable, and maintainability is a software engineer's primary goal (in my book).

Why is it called pointfree? While it does have `(.)`, the "point" in the term refer to values, like points on a number line. Instead of writing functions from point to point, we are writing functions between number spaces/domains. More info on this in the [Pointfree page on the Haskell wiki](http://www.haskell.org/haskellwiki/Pointfree).

### Folds

While not related to anything we talked about, we must introduce folds. Suppose you have an array and you want to transform it into a single value. What functions do that? Sum, product, and length are examples of practical functions like this. If we look at their implementations, we can see that they use a function called "fold". What is a fold? Well, we can guess that, but how does it work?

        > :t foldr
        foldr :: (a -> b -> b) -> b -> [a] -> b
        -- Args: predicate function, initial value, array.
        -- Examples:
        sum     = fold 0 (+) -- Start at 0, add each element.
        product = fold 1 (*) -- Start at 1, multiply each element.
        length  = fold 0 (\_ s -> 1 + s) -- Add one for each element.

Psuedocode explanation:

        fold f z [a,b,c] == a `f` (b `f` (c `f` z))

English translation:

Start at z, apply f to last element, apply result to next element, apply result to next element, ..., apply result to first element.

That is `foldr`. There is also a `foldl`, which starts from the left, but you should use the `foldl'` version, which is more efficient.


