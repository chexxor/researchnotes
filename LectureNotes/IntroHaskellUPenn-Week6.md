
## Week 6 Notes

Source: [Lazy Evaluation](http://www.cis.upenn.edu/~cis194/lectures/06-laziness.html)

### Strict Evaluation

Before we can understand lazy evaluation, we must first look at the alternative strategy: strict evaluation. Basically, the difference lies in when expressions are evaluated. Strict evaluation will evaluate an expression _before_ passing it to a function. Let's use the following function to explain the difference.

    f x y  = x + 2

**Strictly evaluate** `f 5 (29^35792)`. First evaluate `5`, which is already a concrete value: `f 5 (29^35792)`. Then evaluate `(29^25792)`, which is...hundreds of numbers.

Why do people care about this? Well, look at the particular case in this example. We never use `y`! We could actually save many, many CPU cycles if we _don't_ evaluate `(29^25792)`. In this situation, if we wait until inside the `f` function, we can see that we don't need to evaluate it. Waiting until an expression's value is actually needed is called **lazy evaluation.**

So why does strict evaluation exist? Well, there is at least one situation in which strict evaluation is necessary. Consider the following Java snippet.

    `f (release_monkeys(), increment_counter())`.

In Java, the order in which these methods are evaluated is significant. Why? Java programmers rely on object state as much as functions. Suppose the `release_monkeys` method modifies a shared variable, which the `increment_counter` method uses to make a decision. Then, the order which these methods are executed is significant. In languages which allow shared variables like this, one of a programmer's essential duties is sequencing state modification. So, as we can see, if a language permits shared state, strict evaluation is implied.

### Side Effects and Purity

In it's early days, Haskell's designers really wanted a language with lazy evaluation, because it might be a great pair with function-based programming. As seen in the Java example above, side-effects implies strict evaluation. Likewise, lazy evaluation implies zero side-effects.

What's a side effect? Right, we should clarify that. By side effect, we actually mean anything that interacts with shared state in a time-sensitive manner. Examples include: modifying shared variables, printing to the screen, reading from a file or the network.

Wait, these abilities sound like essential programming facilities! Haskell doesn't allow these? Well, at first it didn't. But later, the designers finally created a way to enable these. It took so long, because they wanted the best of both worlds: side-effects *and* a lazy language. But, they finally found a solution. The solution was to place these interactions inside a thing called an `IO` monad. We'll talk about this in a few weeks.

### Lazy Evaluation

Now that we've seen strict evaluation, let's look at lazy evaluation. We already mentioned that it means to delay evaluating an expression until it's value is needed. So, in the above example, the compiler will package the second parameter, `(29^25792)`, as an _unevaluated expression_, called a "thunk", and immediately passes it to the function. Because this thunk is never used, it will eventually just be removed by the garbage collector.

### When to Evaluate an Expression?

The example above focused on when a function _used_ its arguments, because this is easy to understand for non-Haskell programmers. However, we must remember an important difference between Haskell functions and other languages' functions: pattern-matching. How does pattern-matching affect lazy evaluation? A great deal! Look at the following functions:

    f1 :: Maybe a -> [Maybe a]
    f1 m = [m,m]

    f2 :: Maybe a -> [a]
    f2 Nothing  = []
    f2 (Just x) = [x]

Unlike our first example, `f1` and `f2` both use their arguments. But, there is a difference between `f1` and `f2`. Can you see it? The first function doesn't need to know anything about its argument, but the second function must open its argument to look at its constructor. Because of this, `f2`'s argument will be evaluated before passing it to `f2`.

However, `f2`'s argument will not be "fully" evaluated. That is, it will only be evaluated as far as it must be pattern-matched. Suppose `f2` received `safeHead [3^500, 49]` as an argument. This expression would be evaluated to `Just (3^500)`, and leave the complicated `3^500` for another time.

### Consequences of Lazy Evaluation

#### Space Usage

Because Haskell creates and grows thunks, it can be a bit difficult to reason about space usage of a program. Let's illustrate this by looking at the following example:

    foldl (+) 0 [1,2,3]

When we evaluate this, it will move like this (skipping some steps):

    -- foldl (+) 0 [1,2,3]
    -- foldl (+) (((0+1)+2)+3) []
    -- (((0+1)+2)+3)
    -- (3+3)

This operation translates the `[1,2,3]` list into a `(((0+1)+2)+3)` list. If the input list is really long, this can become quite inefficient. A worse inefficiency: when `(((0+1)+2)+3)` is evaluated, the 3 and 2 must be pushed onto the stack before evaluating `0+1`.

This sounds terrible. What's the solution? An alternate implementation of `foldl` exists in the standard library, named `foldl'`, which evaluates like this (skipping some steps):

    foldl' (+) 0 [1,2,3]
    foldl' (+) (0+1) [2,3]
    foldl' (+) 1 [2,3]
    foldl' (+) (1+2) [3]
    foldl' (+) 3 [3]
    foldl' (+) (3+3) []

Can you see how this is better? Instead of building a list, the expression is evaluated at each step in the list. This *really* saves on space.

#### Short-circuiting Operators

In many languages, the `&&` and `||` operators are "optimized". That is, when evaluating `False && True` as an expression, the compiler will see the `False &&`, then stop evaluating the expression, and then return False. This optimization is an exception to the strict evaluation rule, so the compiler and language standard have special implementations for these.

In Haskell, these two operations are normal functions and will receive lazily-evaluated arguments. This is how the `(&&)` is implemented in the standard library:

    (&&) :: Bool -> Bool -> Bool
    True  && x = x
    False && _ = False

If you play with this, you will see it behaves exactly the same as most languages. Why? The second parameter is not used at all in pattern-matched.

However, if we implement this function differently, it will behave notably differently! Let's look. Let's implement it like this:

    (&&!) :: Bool -> Bool -> Bool
    True  &&! True  = True
    True  &&! False = False
    False &&! True  = False
    False &&! False = False

Now, let's compare the two implementations of Boolean AND:

    False &&  (34^9784346 > 34987345)
    -- Evaluates to False.
    False &&! (34^9784346 > 34987345)
    -- Evaluates to False, but takes a LONG time.

#### User-defined Control Structures

In most `if` control structures, only a single branch is executed, based on one condition. This can be easily replicated in Haskell, because it is a lazy language, which means its arguments are passed through, rather than evaluating them before entering the function.

    if' :: Bool -> a -> a -> a
    if' True  x _ = x
    if' False _ y = y

#### Infinite Data Structures

We've seen the `repeat 7` expression, which represents an infinite list. Actually, this just creates a thunk, or a seed, from which the data structure grows.

Compared to that, laziness is beneficial in "effectively infinite" data structures. Consider generating a tree of possible moves for a chess game, which could be *huge*. In Haskell, we can express this massive tree as data structures, then explore only particular parts of this effectively infinite tree. Keeping the tree as data constructors will consume only a fraction of the memory space.

#### Pipelining/Wholemeal Programming

Due to laziness, each stage of the pipeline can operate independently. Each stage generates a part of the result as the next stage in the pipeline demands it.

#### Dynamic Programming

http://en.wikipedia.org/wiki/Dynamic_programming

Dynamic programming is a method for solving complex problems by breaking them down into simpler subproblems. It is applicable to problems exhibiting the properties of overlapping subproblems and optimal substructure. When applicable, the method takes far less time than naive methods that don't take advantage of the subproblem overlap (like depth-first search).




