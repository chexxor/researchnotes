
## Week 3 Notes

Source: [Recursion Patterns, Polymorphism, and the Prelude](http://www.cis.upenn.edu/~cis194/lectures/03-rec-poly.html)

### Recursion Patterns

Much of the homework, involved writing recursive functions. Experienced Haskell programmers, however, don't write so many recursive functions.

Experienced Haskell programmers understand common patterns. These patterns have been abstracted into functions and placed into a common library. This way, to use the recursive functionality, they just pass the variable portion into a function. Simple.

Example: `IntList` is a simple recursive data type.

        data IntList = Empty | Cons Int IntList
            deriving Show

What would we want to do with an IntList? Here's some ideas: A) Do something to every element, B) Get a subset of the list, C) Summarize/collapse the list into a single value.

If we wanted to do these things, we would write a function then pass the list to the function. But we just said that experienced Haskell users don't write functions like this, but use a library instead. So, what are generic functions we can use on lists?

#### Map, Filter, Fold

To modify every element of a list, we can use the `map` function.

        exampleList = Cons (-1) (Cons 2 (Cons (-6) Empty))
        addOne x = x + 1
        square x = x * x
        mapIntList addOne exampleList
        mapIntList abs    exampleList
        mapIntList square exampleList

We only wrote `mapIntList` for `IntList` types. We may use many types of lists, however. Do we have to rewrite this same function for each type? No! Haskell supports polymorphism for both data types and functions. This means that we don't have to specify an exact type in type signatures. In its place, we can place a variable. Then, the Haskell compiler will allow many types as arguments to this function.

### Polymorphic Data Types

        -- Before
        data IntList = Empty | Cons Int IntList
        -- After
        data List t = E | C t (List t) -- List is parameterized by type t.

### Polymorphic Functions

        -- Before
        keepOnlyEven Empty = Empty
        keepOnlyEven (Cons x xs)
          | even x    = Cons x (keepOnlyEven xs)
          | otherwise = keepOnlyEven xs
        --------
        -- After
        filterList _ E = E
        filterList p (C x xs)
          | p x       = C x (filterList p xs)
          | otherwise = filterList p xs

If it isn't IntList, what is the type of our new function?

        > :t filterList
        filterList :: (t -> Bool) -> List t -> List t    

In English: “for any type t, filterList takes a A) function from t to Bool and B) a list of t’s, and returns a list of t’s.”

Let's make a more generic map function. We could say that it returns a list of the same type, but why not allow any type to be returned? Then we could transform a list of Ints into a list of String equivalents. To do that, we use `a` and `b` in the type signature.

        mapList :: (a -> b) -> List a -> List b
        mapList _ E        = E
        mapList f (C x xs) = C (f x) (mapList f xs)

If you are writing a polymorphic function, the most important difference: **The caller picks the type, not the function author.**

### The Prelude

The Prelude has a whole bunch of generic stuff, so it's useful to everyone.

The Prelude is a module that is implicitly imported into every Haskell program. It has a bunch of functions that will be useful to every Haskell function, presumably, so it effectively becomes the standard language of all Haskell programmers. The Prelude has a bunch polymorphic data types as well as polymorphic functions to work with them. You should skim the [Prelude documentation](http://www.haskell.org/ghc/docs/latest/html/libraries/base/Prelude.html) to see it's scope.

### Total and Partial Functions

Consider `head`, a polymorphic function which has this type.

        [a] -> a

If we give the `head` function an empty list, it crashes. Actually it was designed to do this. Why? The designers want this function to *always* return a value. Because this function is polymorphic, it doesn't know what type it will be used with. Therefore, it can't make a value of `a` type on edge cases like this, it can only return an existing value.

We have a name for functions like this, "partial function", which is a function that will crash for certain inputs. Opposite this is a "total function" which will return a well-defined value for *all* possible inputs.

Partial functions are bad, because they aren't safe, so don't use them. `head` is a mistake! It should not be in the Prelude.

Other partial Prelude functions you should avoid:
- tail
- init
- last
- (!!)

If you must write a partial function, use `Maybe` instead of crashing. In a sense, it will still be a partial function, but we have reflected this limitation in the type system, which means we can pattern-match around it, so we consider it safe. **The goal is to have its type tell us as much as possible about a function.**

If you know that a list is guaranteed to be non-empty, you can tell the compiler to enforce this fact, to ensure the list is never empty. This is still not recommended, but w/e.

        -- Declare in type constructor.
        data NonEmptyList a = NEL a [a]
        -- In pattern.
        nelToList :: NonEmptyList a -> [a]
        nelToList (NEL x xs) = x:xs
        -- In value.
        listToNel :: [a] -> Maybe (NonEmptyList a)
        listToNel []     = Nothing
        listToNel (x:xs) = Just $ NEL x xs



