
# 2012 Retrospective

## Last Year's Accomplishments

One of my priorities in life is to keep improving and growing. So, to see my progress, motivate me, and give me self-confidence, I like to review my accomplishments from the last year, both big and small.

I'll list everything I'm proud of, in no order. Then I will choose a few significant ones to elaborate:

- Spoke at a national software conference
- Travelled to Vietnam and Taiwan
- Fixed a falling garage door opener with my friend
- Replaced the retaining wall for my home's egress window with my dad
- Contributed to an open source software project
- Significantly improved my JavaScript skills, which is still only the first few steps down that road
- Learned more about my personality and moods
- Set up a mini home server computer
- Learned how to host a personal web server
- Started learning how to use vim to edit text files for blog posts and what not
- Dramatically improved my Chinese skills since really starting 12 months ago
- Watched every episode of Star Trek to build my nerd-cred
- Learned about early and middle history of Vietnam
- Started learning about early history of China
- Attained first rank (5-kyuu) in aikido after 6 months of training
- Built better posture and self-confidence after lifting weights for 6 months
- Improved straight-blade shaving skills by experimenting with variables of the art
- Made home-made yogurt and bread. Failed attempt at brewing kombucha, but I learned from it
- Bought many kinds of loose-leaf tea and became a fan of tea
- Attended an anime convention with friends, which was an interesting experience
- Was a member of the wedding party for a best college friend's wedding, which was an honor and humbling
- Authored a programming article that was published on a major site


## Spoke at National Conference

In the middle of summer last year I picked up an open source project that helps build HTML5 mobile apps on Salesforce to learn more about the practice. I believe I was the only one publicly making noise about the project by posting blog posts and tweets about it. The developer relations group in Salesforce announced a Call for Papers for Dreamforce 2012, which is the first year they included heavy support for both developers and the developer community at the huge conference. I thought I was qualified to talk about this open source project at Dreamforce, so I crafted an idea for a talk and an abstract to submit. A few weeks later, my talk was accepted. I was humbled for being chosen, excited for the opportunity, and scared of failure. I spent many hours in the next 2-3 months creating my talk, revising it, practicing it, and revising and practicing it more.

I finally found myself on the airplane to San Francisco. The night before my talk, I found myself in my hotel room practicing and still revising my talk. My talk was at noon on Wednesday, so I arrived at the conference hall an hour early to prepare my room. The conference employees told me that I must be in a room watching Salesforce's CEO give his keynote talk and ushered me into a room to watch it. His keynote talk was more than thirty minutes long, which meant that I was able to get into my room at 11:45am.

This proved to be not enough time since I found that my netbook was not compatible with the laptop presentation system in the room. I had to move my slide deck from my Linux laptop onto the Windows laptop which was provided with the room. I later found that several important images did not survive the file conversion to Windows' file format. I meant to create suspense before showing a slide with an image, but the audience laughed when I switched to blank slide and the suspense fell to the floor.

There was one other major issue with my presentation. My talk was about writing HTML5 applications, which are rendered in a browser. When I demonstrated how to use the software by opening the app in a browser, I was given a blank screen. Where was my app? I decided that the code must be wrong, and rather than debug the app, it would be best to continue. My second code demonstration fell to the same fate. What to do? Luckily, an audience member suggested I try a different browser. After spending a few empty minutes on stage loading my app into a new browser, I found it worked perfectly. Nice! This was hardly my fault, since I was forced to use a different laptop just minutes before starting my talk. An educational experience, to be sure.

In the end, it was a pretty great talk. At least five of the 150+ attendees stepped up after my talk to shake my hand and pay me compliments. One person later said that it was one of the most educational talks he attended at the four-day conference. How nice! 


## Visited Vietnam and Taiwan

From San Francisco, I skipped the last day of the conference and flew directly to Vietnam to attend a friend's wedding. I arrived on Saturday morning, and fought occasional punches of jet lag all day. The wedding was elaborate and I was humbled to be able to be there. I think is the most international wedding I will ever attend. The bride and grooms classmates from Japan, family from Vietnam and Scotland, and friends from even more countries were present to give their best wishes.

After the wedding, three of us travelled around Vietnam. One friend from Vietnam was an expert tour guide and served as a translator, a near-necessity when travelling to the smaller towns. With my other friend from Taiwan, the three of us saw remnants of the last war, climbed mountains, visiting a historic trading town, and saw dragons dancing for the autumn festival. And the food - the food was incredible. So many vegetables. Loved the variety.

After returning, I saw that my company had a two-day holiday for Thanksgiving, so I had to capitalize on the chance to take another extended vacation by spending a week in Taiwan to visit my friend there. Together, we again had the best vacation ever. We ate interesting food every day, climbed a mountain, saw the sun rise above the clouds, saw various sea creatures on a coral island, visited a town that was the site of the first trading settlement in the country. Awesome variety. We rode bullet trains, normal trains, subways, tour buses, city buses, taxis, and cars, not including the airplane home. My lust for adventure with friends was satisfied again. So awesome.


## Build Some Muscle and Learned Some Aikido

I joined a gym and an aikido dojo in July last year, both at about the same time. After spending nearly two years building my software engineering knowledge, I felt a growing desire to build myself physically. I was a thin and kinda awkward guy, so I made a conscious decision to commit to building upper-body muscle and to learn a martial art. I visited a few different martial art dojos a year or two before, and I decided at that time that aikido was the best fit for me.

I'm proud to say that I was successful with both commitments. The first few months were very difficult, mostly because I started a strict diet while lifting heavy weights 4-5 times a week and also spending 4-5 days a week training aikido. I arrived home at 8pm every day completely physically exhausted. After stopping the strict food diet, I learned that the persistant lack of energy I experienced was caused by a lack of proper nutrition. I added fat and more calories into my diet, and my mood and energy quickly returned. It was an educational experience, to be sure.

Several times during this six months I was very close to quitting aikido. However, I chose to obey my commitment and I continued my training. The dojo follows a very traditional Japanese style. In our dojo, this means that there is a deep respect for older students and a deeper respect for dojo rules. This respect manifests in the form of ceremony and behavioral corrections by older students. I felt very restricted by the rules. I wanted to ask questions about how to perform a technique, but I was discouraged from doing so. Instead, I was told that aikido is something that your body learns by doing and explanations will not help. I also felt restricted from learning by experimenting. When I did experiment, I was told "You're doing it wrong. The technique is supposed to go like this". This was frustrating because everyone did the same technique differently and had different opinions, but I couldn't create my own flavor. It was a restrictive environment indeed.

After six months of training, however, I took my first test and attained the first for adults (5-kyuu). The test consisted of only 5 techiques, so I wasn't too worried about it. I practiced the same damned techniques so many times in the previous six months that I think I would go crazy if I was forced to practice them again. Despite my low expectations for satisfaction from the test, I surprised myself by how happy I was to pass. After the test, sensei said, "Alex", and I stood up. He said "Pass!" with the same serious and straight face he always had, and then gave his comments on my test. "That was some good stuff." This was the third time in six months that sensei ever directly addressed me. The last time he did so he asked me how old I was. His only response was a deep, dark laugh and then he moved on to train with another student.

I derived zero fun from my time at the aikido dojo. I actually disliked class. I didn't dislike the ceremony, but rather, it was the zero-explanation behavioral fixes from older students that I grew to dislike. I really didn't like operating in the dojo under the constant fear of breaking "the code". This refers to the proper code of conduct, which only a few older students knew. They grew to understand sensei's true wishes because they trained with him the longest and they have a special relationship with him. It's very much like a religion in which a priest claims he communicates with God. Because the priest is closest to God, you have no choice but to believe and obey what he says. This situation bothered me quite deeply, and I really questioned my long-term compatibility with the dojo.

Having said all this, studying aikido at the dojo was still a very educational experience. I learned a lot about commitment and about situations that don't agree with me. This will be a good point of reference for later in life, to be sure.


## Focus for 2013

Having considered my progress in 2012, I would like to create areas of focus for my progress in 2013.

Last year, I made great progress in my career. I wanted to become an expert in my area, and I think I came close to attaining that. Am I satisfied with 2013 completely? No, not completely. One of my problems is that I push people away from me so that I can focus on satisfying my curiousity in software. However, I notice that one thing hasn't changed at all since last year: my friend count. While I have gained new friends in my career path, I haven't made new friends outside of work. Is this ok? I feel like I should diversify. Therefore, one of my areas of focus in 2013 is to spend less time learning about software and spending more time building friendships. This will be very challenging for me. I must recruit a friend to help me with this.

The second area of focus for me next year is to become even better at Chinese. I have made a lot of progress in the last year with my casual Chinese studies. I can recognize words when listening to songs, I can get a rough idea of a casual Chinese sentence on the Internet, I know many basic conversation words, and I have a pretty decent vocabulary about things I do. However, this opinion is all relative to my geographic location, which is Fargo, ND. If I want to get serious about learning Chinese, I would have to move to China. Anyways, I want to focus more on my Chinese studies next year.


This concludes my 2012 retrospective and 2013 goals. I'm all about encouragement in the right direction and setting attainable goals. I believe I've made my goals plenty general. Good luck to me and to you in 2013!

