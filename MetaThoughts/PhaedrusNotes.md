
## Plato: Phaedrus


Sources

[Phaedrus](http://www.gutenberg.org/dirs/1/6/3/1636/1636-h/1636-h.htm)


Notes:


- Phaedrus either introduces or follows the Symposium, and display Plato's philosophy on the nature of love.
- The dialogue is not strictly limited to love, but flows into and out of this theme as in a conversation.
- After completing the summary of the dialogue, it seems one of the main goals of Plato in The Phaedrus is to criticize the professors of rhetoric, rather than discuss love.
- Rather than reading the actual text, I read the author's introduction, which summarizes the dialogue and then analyzes it and Plato.
- My digest of the summary follows.

Scene

- Phaedrus has been spending the morning with Lysias, a rhetorician, and is going to take a walk, when he meets Socrates.
- Phaedrus and Socrates walk into the country and will read the speech of Lysias.
- Phaedrus asks Socrates his opinion on some ancient myths. Socrates replies, "The proper study of mankind is man." Since Socrates does not yet know himself, then how can he divert his attention to study these stories?
- The speech of Lysias consists of a paradox that the non-lover ought to be accepted rather than the lover, because he is more rational and enduring, less suspicious and engrossing, among other things.
- Phaedrus claims that nothing could ever be as beautiful or written better. Socrates disagrees with the quality of the speech and its values. He believes that he could make a better speech.
- Phaedrus is delighted to hear that he might gain another speech, so he convinces Socrates to write his own on the topic. Socrates is afraid he may never hear another speech of Lysias, so he begins.

Socrates Thoughts on Lovers

- Socrates asks, how is the non-lover distinguished from the lover?
- Socrates reasons, two principles are at war inside each of us, reason and desire. The result of the war is temperance and intemperance.
- Of all these irrational desires, the greatest is love. Love leads one away to the enjoyment of personal beauty by the desires of a kindred spirit.

- The non-lover has many advantages over the lover.
    - Encourages softness and exclusiveness.
    - Cannot endure superiority.
    - Train him in luxury and keep him out of society.
    - Deprive him of parents, friends, money, knowledge.
    - Through time, becomes the enemy of his beloved.
- "As wolves love lambs so lovers  love their loves."
- Socrates finishes his speech's demo and prepares to leave, but Phaedrus begs him to stay and continue.

- Socrates continues by glorifying madness, of which there are four types.
    - The art of divination.
    - The art of purification by mysteries.
    - Inspiration of the muses.
    - Love, which can't be explained without examining the nature of the soul.

- A soul is immortal, while the body is mortal.
- The gods lead the ideal lives in heaven.
- Humans spend their lives striving to reach heaven by following the path of the truth.
- After death comes the judgement. The bad are sent to correction houses, and the good are sent to joy in heaven.
- After 1000 years, souls choose the lives they will lead for the next 1000 years. If they choose to be philosophers three times in a row, they are given their wings. All others must spend another 10,000 years.
- The wisdom of philosophers is invisible to mortal eyes.

- The characters of lovers depend on the god whom they followed in the other world. They seek a love which is like their god.
- A charioteer and his two steeds are like the soul. One steed rushes on to enjoy, but the charioteer sees his beloved and falls back in adoration, and forces his steed into submission.
- When the evil steed is calmed, the same image of love dwells in the breast of either. If they have self-control, they pass their lives in the greatest happiness.

- Socrates concludes:
- These are the blessings of love, spoken in a language finer than before to please Phaedrus.

- They then take time to discuss the difference between good speaking and rhetoric. Rhetoric is the art of enchantment.

- They then take time to discuss the true use of writing.
- "Until a man knows the truth, and the manner of adapting the truth to the natures of other men, he cannot be a good orator."

- The author of this written version of Phaedrus notes two controversies - the subject and the date of the Dialogue.





