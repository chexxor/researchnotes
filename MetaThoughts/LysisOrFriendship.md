
## Platro: Lysis, or Friendship


Sources

[Lysis, or Friendship](http://onlinebooks.library.upenn.edu/webbin/gutbook/lookup?num=1579)
[Plato at Stanford](http://plato.stanford.edu/entries/plato-friendship/)


Notes:

- No answer is given in the Lysis to the question, 'What is Friendship?'
- The Dialogue consists of two scenes.
- Scene one.
- Socrates and Lysis. "Do your parents not love you very much?" Then moves to "You are not wise enough." and that people trust you if you are wise.
- Socrates then asks, "What is Friendship?"
- When one man loves another, which is the friend - he who loves, or he who is loved?
- Like is the friend of like.
- The bad are not friends, for they are not like each other.
- The good have no need for one another, and therefore do not care about one another.
- Others say that likeness is the cause for aversion, and unlikeness of love and friendship.
- What remains is should the indifferent be the friend of the good, or rather the beautiful?
- The indifferent may have evil attached to them, and with a desire to rid itself of evil befriends the good.
- Blah, blah, through false dichotomies such as this, the question remains unsolved.
- The subject is continued in the Phaedrus and the Symposium, and also referenced in Nichomachean Ethics of Aristotle.
- Socrates has two notions
    - Friendship arises out of human needs and wants
    - The higher form or ideal of friendship exists only for the sake of good.
-





