

### Cicero: On Friendship, or Laelius

- [Cicero: On Friendship, or Laelius](http://www.fordham.edu/halsall/ancient/cicero-friendship.asp://www.fordham.edu/halsall/ancient/cicero-friendship.asp)

- Marcus Tullius Cicero was a great Roman orator, born in 106 BCE,
- Cicero's father was upperclass, so Cicero received education in rhetoric, law, and philosophy with some of the most noted teachers of the time. He also spent few years traveling in Greece and Asia to continue his studies under various well-known masters.
- Worked in the judicial field and became known as a man of justice.
- Became popular after a certain judicial case, and was voted consul by popular demand.
- He unmasked and resolved another government plot, which made him a savior of his country.
- The political powers proposed laws that banished Cicero, motivated by his involvement in the recent government plot.
- While in exile, he received support from neither the country he saved nor his friends. He wrote letters to urge his supporters to request his recall and suffered extreme depression from his separation from his family and his ruined political ambitions.
- He was finally restored to received popular enthusiasm, but still barred from meaningful political positions. He delivered great defenses in law and became known as a great orator.
- He changed positions and locations and further studied philosophy and writing.
- When Caesar was assassinated, Cicero was included in the group of conspirators despite having zero involvement, and he was put to death.
- His 58 speeches, rather than his letters while in exile, best displayed his skill, wit, and passion. To fully appreciate his speeches, however, one must understand the history and politics of the time, as well as Cicero's personality.
- As a philosopher, his most important function was to enlighten his countryman with the Greek schools of philosophy.
- "On Friendship" gives a clear impression of a high-minded Roman's view on the main problems of human life.



Setting the Scene

- The lecture is about Quintus Mucius Scaevola, often remembered stories of his father-in-law, Gaius Laelius, who had the title "the wise"
- Cicero spent much time with Scaevola, and heard many stories. One story was a discussion between Scaevola and Laelius about friendship.
- Gaius Fannius, Laelius' son-in-law, was also present. This discussion occurs a few days after the death of Africanus.

Why is this story significant?

- Of all friendships, Gaius Laelius and Publius Scipio had the most remarkable, as Laelius was eminent for his friendship. Moreover, this anecdotal story gains weight from having the authority of ancient and distinguished men.


####Part I####

Gaius Fannius and Quintus Mucius Scaevola call on their father-in-law, Laelius after the death of Africanus to start a discussion.

Fannius

  - Yes, Laelius, Africanus was the most illustrius character. But now all eyes are on you, for everyone calls you "the wise".
  - Cato and Lucius Atilius before you were also called "the wise", but for different reasons.
  - Atilius had a reputation as a jurist. Cato had experience of extreme old age.
  - You, Laelius, received this title because of your character and your learning. The scholars give you this title, not the vulgars.
  - People say your wisdom is borne from your self-sufficing and protecting your virtues from the sways of life.
  - Therefore, people wonder about your thoughts on the death of Africanus. You missed our last meeting, which makes us more curious.

Scaevola

  - Yes, Laelius, I am also asked this question. I answer with what I observe: An illustrius man and dear friend has died, and you bear the grief in a reasonable manner.
  - You are of course affected, and you missed our last meeting. This is surely because you were ill, not melancholy, no?

Laelius

  - Thanks, Scaevola, you are quite right.
  - I have no right to miss my normal schedule if I am well.
  - Fannius, I do not agree that I am so wise, you must speak out of affection. If anyone was ever wise, of which I have doubts, it was Cato.
  - **Now, if I were to say that I am not affected by regret for Scipio**, I would be lying. I think there will never be a man like him again.
  - I need no medicine, I can find my own consolation. The cure lies in being free from the mistaken notion which causes pain at the departure of friends.
  - To Scipio, no evil has fallen. If anything, the problem is with me. To be distressed at one's own misfortunes does not show that you love your friend, but that you love yourself.
  - As for him, **who can say that all is not more than well?** He didn't wish for immortality, and what more can a man wish for that Scipio did not have?
  - He fulfilled the expectations of his peers. He was elected consul when he was not even a valid candidate. He put an end to the wars between two bitter enemy states. And finally, when he died his peers mourned such! What more could a man gain with the addition of a few more years?
  - We can conclude that, due to his fortune and glory, his life could not have been bettered.
  - Scipio, in his days, saw many days of triumph and exaltation. His most magnificant, however, was his last, in which he was escorted by the senators, people of Rome, allies, and Latins to his own door.

  - **I do not believe that the soul perishes with the body and that death ends all.**
  - Our ancestors have solemn observances of the dead, which is not rational if the person is totally annihilated. Our "most wise" philosopher always taught that "the souls of men are divine, and when they have quitted their body, a return to heaven is open to them, least difficult to those who have been most virtuous and just." Scipius himself, just days before his death, lectured on the immortality of the soul.
  - Who could have had an easier voyage to the heavens than Scipio?
  - Therefore, if one mourns his death, then it be a sign of envy rather than of friendship.
  - If the soul and body perish together and no sensation remains, then while there is nothing good in death, at least there is nothing bad.
  - Remove sensation, and a man is exactly as though he had never been born; and yet that this man was born is a joy to me.

  - Now, all is as well as is possible with him. This is not so with me, for it is not fair that he entered life after me and he must leave before me.
  - Yet this is the pleasure, that I look upon my life as a happy one, because I have spent it with Scipio.
  - With Scipio, I associated in business both publicly and privately, lived in Rome and abroad with him, and had complete harmony in our tastes, pursuits, and sentiments. This is the true secret of friendship.
  - Therefore, **it is not in his reputation for wisdom that I find my happiness, but in the hope that the memory of our friendship will be lasting**. I cherish a hope that the friendship of Scipio and Laelius by known even to posterity.

Fannius

  - Since you have mentioned friendship, Laelius, you would be doing me, and Scaevola, a great kindness to tell us your sentiments about friendship, its nature, and the rules to be observed in regard to it.

Laelius

  - I should not object if I have confidence in myself, and the theme is a noble one.
  - I am not a professional philosopher, however, so if you want a set discourse you must go to the professional lecturers.
  - **I can only urge onto you that friendship is the greatest thing in the world**, for there is nothing which so fits with our nature, or is so exactly what we want in both prosperity and diversity.

  - To begin, I must say this - **friendship can only exist between good men**. Note that I do not define 'good' so closely as philosophers.
  - **I do not mean that only the "wise" are "good"**. True wisdom is something no man can attain. No one understands what "wisdom" really means, so we will manage with just natural wit.
  - What I do mean by the "good" is those whose actions and lives leave no question as to their honor, purity, equity, and liberality. Those who are free from greed, lust, and violence and have the courage of their convictions. Those who follow nature as the most perfect guide to a good life.

  - This truth seems clear to me - that **nature has formed us so that a certain tie unites us all, and this tie becomes stronger with proximity.**
  - Friendship excels relationship in that whereas you may eliminate affection from relationship, you can not from friendship.

  - Now, **we may define friendship as so**: a complete accord on all subjects human and divine, joined by mutual good will and affection.
  - Some people believe the "chief good" of life is virtues. While noble, these virtues are the parent and preserver of friendship. Without virtues, friendship can not exist.
  - With virtuous men, friendship enhances prosperity and relieves adversity of its burden by halving and sharing it.
  - **In the face of a true friend, a man sees a second self.**
  - So that where his friend is he is; if his friend be rich, he is not poor; though he be weak, his friend's strength is his; and in his friend's life he enjoys a second life after his own is finished.
  - If you eliminate from nature the ties of affection, there will be an end of house and city.
  - I don't think I have any more to say about Friendship. If you want more, consult the professors.

Fannius

  - I would rather hear you, as you speak in a different manner.


####Part II####

Laelius

  - Now you are using force. No matter, I cannot refuse a wish of my sons-in-law.

  - I have often wondered, **is it weakness and want of means that make friendship desired?** That is, to give that in which one is strong to gain that in which one is weak? Or is this just the nature of existing friendship and the cause is more natural?
  - It is not uncommon for men to feign friendship for personal motives.
  - But this is not friendship, which requires no pretense and is genuine and spontaneous. Therefore, **I believe that friendship is a natural impulse** rather than a call for help.
  - Nothing inspires love like virtue.
  - **Affection is strengthened by received benefits.** When these are added to the natural feeling of the heart, an even warmer feeling springs up.
  - If someone believes that they should have someone to fill a personal weakness, they allow friendship an origin very far from noble. If this is the case, a man's friendship would be proportional to his own confidence and resources.
  - **For when a man's confidence in himself is greatest, when he is so fortified by virtue and wisdom as to want nothing and to feel absolutely self-dependent, it is then that he is most conspicuous for seeking out and keeping up friendships.**
  - We look on friendship as worth trying for, not because in expecting ulterior gain, but in the conviction that it gives us from first to last, included in the feeling itself.

  - There are those who, like brute beasts, refer everything to sensual pleasure.
  - **When once men feel the attraction of friendship, they of course try to attach themselves to the source of the affection and move nearer and nearer. Their aim is to be on the same level in regard to affection and want to do a good service rather than ask something in return.**
  - This proves both truths.  We get the most important material advantages from friendship, and its origin is a natural impulse rather than a sense of need.
  - **If friendship is borne of a sense of need, then it is cemented by material advantages. Likewise, any change in the advantages would dissolve the friendship.**
  - Nature, however, is incapable of change, so genuine friendships are eternal. Friendships that expect material gain, then, are not genuine friendships.
  - This is all I have to say on the origin of friendship. I believe you do not want to hear more.

Fannius

  - No! Let us have the rest!

Scaevola

  - Yes! Let us hear!

Laelius

  - Well then, let me tell you about the conversations between Scipio and myself.
  - Scipio used to say that the most difficult thing in the world was for a friendship to remain unimpaired to the end of life.
  - So many things might intervene: conflicting interests, differing opinions in politics, changes in character by misfortunes or aging.

  - **How far should personal feelings go in a friendship?**
  - For instance, suppose a man had friends, should they join him when he invades another country? Should his friends assist him to establish a tyranny?
  - A true instance shows this is possible. Regard for another person is so high that their wishes be considered as law.
  - If we decide to give our friends whatever they wish, and to ask whatever we wish, then perfect wisdom must be assumed on both sides. A perfectly wise man can not exist, so we mustn't grant our friends their every wish.
  - **So, we lay down this rule of friendship - neither ask nor consent to do what is wrong.**
  - Likewise, we should ask from friends, and do for friends, only what is good.
  - **But also, don't wait to be asked. Let us have the courage to give advice with candor.**

  - Some people teach that we should avoid close friendships, for fear they increase one's anxieties and responsibilities. They say the wisest friendship is a loose one, for the first pillar of a happy life is freedom from care. How can one be care-free if it has to consider other people in addition to oneself?
  - What is the value of "freedom from care"?
  - It is very tempting at first sight. There is nothing demanded of us that allows us to lay it aside once started to escape anxiety.
  - **If we wish to avoid anxiety, we must avoid virtue itself.** Virtue necessarily involves anxiety to stand apart from its opposite qualities, as kindness to ill-nature and courage for cowardice.

  - Therefore, we will encounter mental pain when upholding friendship just as when we uphold virtues.
  - **The clear indication of virtue is the beginning of a friendship**, since like minds are naturally attracted to the same virtues.
  - If we remove emotion from man, what difference is there between a man and a beast or a block of wood?
  - Compared to many objects, such as office and fame, friendship is endowed with virtue and is capable of returning affection.
  - The good love the good and naturally strive to be together.
  - **I think that friends ought to be in want for something.** How would Scipio have return my affections if he never wanted my advice or co-operation?

  - And what about the elite gentlemen? Those who have and strive for the greatest wealth and power?
  - For them, they have no fidelity, good will, nor affection. All is suspicion and anxiety.
  - Who can love one whom he fears? Or who can love one known to fear him?
  - Yet such men have many who create a show of friendship.

  - Now, let us consider the limits of friendship. **That is, what is the boundary beyond which our affections should not go?**
  - Three ideas, all of which I disagree.
      - Love our friends just as much as we love ourselves, and no more.
      - Our affection to friends should correspond their affection to us.
      - Value a friend at the same rate he values himself.
  - Here are the reasons I disagree.
      - There are many things we would do for a friend but not do for ourselves.
      - Such a view reduces friendship to figures and calculation, when **friendship should actually be a liberal thing**.
      - A man may have a low estimation. **A friend ought to raise his drooping spirits and lead him higher.**
  - The real limit in a friendship is this: the characters of the two friends must be stainless. There must be complete harmony of interesting, purposes, and aims.
  - **We must not concede the ground of our virtues to secure affection.**

  - Scipio used to complain - how could a person know exactly how many goats or sheep he had, but not how many friends?
  - While these men take great pains in procuring the former, they are utterly mindless when selecting friends. They possess no means of measuring a man's suitability for friendship.
  - **The qualities we ought to consider when selecting friends are: firmness, stability, and constancy.**
  - These qualities can often only be tested after a friendship begins, simply because friendship happens by chance and measurements are only possible later.
  - So, we should rein our impulse to affection on new friends as one might do to a chariot of horses.
  - **We should form, then, a kind of tentative friendship.** In this, we can watch for the potential friend's values, virtues, and priorities.
  - **Two common ways in which a man's values are tested.** If a man proves his firmness in both these ways, he is a member of the class rarest in the world.
      - Looking down on friends when they are themselves prosperous.
      - Deserting friends when they are in distress.


####Part III####

  - What is the quality to look for to ensure a stable and permanent friendship? Loyalty.
  - We should also look for simplicity, a social disposition, and a sympathetic nature similar to our own.
  - **A good friend will display two behaviors: not feign his feelings, and hold no suspicious of his friend, but rather defend their reputation.**

  - **Are there occasions on which we should prefer new friends to old friends?**
  - There should be no satiety in friendship. **Just as wines that keep well, we ought to discover new friends, as they may teach us new things, while we maintain and enjoy old Friendships.**
  - In many friendships, as they are equal in many ways, one side is often the superior. **The superior should wish that his friends be the better for his support.**
  - **If any of us have any advantage in personal character, intellect, or fortune, we should be ready to make our friends partners in it with ourselves.**
  - The converse must also be observed. As a friend becomes surpassed in genius, fortune, or rank, the surpassed must not become annoyed.

  - We must wait, therefore, to make up our mind about friendships until our character and years have developed.
  - **Difference of character leads to difference of aims and the result of such diversity is to estrange friends.** This is the reason that good men do not make friends with bad men.

  - **Do not let excessive affection hold your friends back, for the inability to endure separation is a weakness and a poor trait.**
  - There are limits to what you ought to expect from a friend and to what you should allow him to demand of you.

  - **There is such a disaster as having to break off friendship.**
  - There are times that an outbreak of disagreeable conduct affects a man's friends or strangers, and the discredit falls on the friends.
  - **The death of a friendship should occur gradually by an increasing intermissions of meetings. Unstitched rather than torn, so to speak.**
  - It is important to show a desire to resign friendship rather than display hostility. There is nothing more discreditable than to be at open war with a man with whom you were once intimate.
  - Rather than become hostile with a friend, honor the past friendship by allowing the party that inflicts the injury to be in the wrong.
  - There is one way to secure oneself against fault and inconveniences of friends - carefully choose our friends to exclude ones of bad character.

  - Those who are "worthy of friendship" are those who have qualities that attract affection.
  - Most people recognize nothing as good in this world unless it is profitable.
  - **Everyone loves himself. Unless this feeling is transferred to another, what a friend is will never be revealed to him. This is because a true friend should be considered as a second self.**

  - Most people want, unreasonably, such a friend as they are unable to be themselves, and expect from their friends what they do not themselves give.
  - **The fair course is to first be good yourself, and then to look out for another of like character.**
  - Men who know friendship know one must first rule those passions which enslave others, and then take delight in mutual attraction and affection.
  - One must satisfy their judgement before engaging one's affections.

  - No one is free from the want of friendship, for it penetrates into the lives of us all.
  - "If a man could ascend to heaven and get a clear view of the natural order of the universe, and the beauty of the heavenly bodies, that wonderful spectacle would give him small pleasure, though nothing could be conceived more delightful if he had but had some one to whom to tell what he had seen."

  - Friendship is varied and complex, and suspicion and offense may arise.
  - One kind of offense is when one chooses either the interests of a friend or one's own sincerity.
  - Compliance gets us friends, plain speaking hate.
      - Plain speaking is bad if it results in resentment, which is the poison of friendship.
      - Compliance, however, is more troublesome. **By allowing and encouraging a friend's faults, one allows a friend to plunge into ruin.**
      - The man to blame, however, is he who resents plain speaking and allows flattery to send him to ruin.
  - **We should, therefore, reprove a friend, but we must do it carefully, as to avoid bitterness and insult.**
  - "There are people who owe more to bitter enemies than to apparently pleasant friends: the former often speak the truth, the latter never."

  - **Genuine friends, then, should give advice freely and without bitterness and receive advice with patience and without irritation.**
  - Likewise, flattery, adulation, and base compliance is utterly subversive of Friendship.
  - However, flattery can hurt no one but the one who takes it in and likes it.

  - We have strayed from our discussion of friendships among wise men to discuss friendships among the vulgar. Let us return to discussing the former.

  - I repeat what I stated before. It is virtue which both creates and preserves friendship.
  - Virtue recognizes itself in others and gravitates towards it, and from this springs the flame of friendship.
  - Since the law of nature is that new generations are forever springing up, the most desirable thing is to reach the goal along with your contemporaries.
  - **In the view of perishableness of mortal things, we should be continually on the lookout for someone to love and to be loved by.**
  - **If we lose affection and kindliness from our life, we lose all that gives life its charm.**
  - Even though he was torn from me by sudden death, Scipio still lives and ever will live, for it is his virtue that lives on in me, and this can not suffer death.
  - If the memories of my time with Scipio had perished with the man, I could not possibly have endured the regret. But these things have not perished; they are fed and strengthened by memory.

  - That is all I had to say on friendship.
  - One piece of parting advice - Virtue is first, but next to it is Friendship.







