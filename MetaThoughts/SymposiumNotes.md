
Sources

[Symposium](http://classics.mit.edu/Plato/symposium.html)

By Plato, written in 360 BCE

Persons of the Dialogue:
- Apollodorus, who repeats to his companion the dialogue, which he heard from Aristodemus, who had narrated to Glaucon once before.
- Phaedrus
- Pausanias
- Eryximachus
- Aristophanes
- Agathon
- Socrates
- Alcibiades

Scene - The House of Agathon

Notes:

Apollodorus
- I was going to the city when an acquaintance, Apollodorus, yelled out to me.
- He wanted to hear the speeches in praise of love, which were delivered by Socrates, Alcibiades, and others at Agathon's supper.
- He heard them indirectly and in an indistinct manner, and wanted to hear the original, clear dialogue.
- He must have been an unreliable source, indeed, as Agathon has not resided in Athens for many years, and I only recently follow Socrates. It actually occurred in our boyhood.
- It wasn't Socrates who told me, but a little fellow at Agathon's supper, who was a devoted admirer of Socrates. However, I confirmed the story with Socrates.

Companion
- You are always speaking evil of yourself, and others.

Apollodorus
- Yes, the reason people say I am mad is just because I have these notions of myself and you, nothing else.

- To tell the story, I should begin at the beginning, and try to deliver the exact words of Aristodemus.

## Story between Aristodemus and Socrates

Aristodemus
- I met Socrates as he was leaving the bath. I asked him where he was going, to which he replied a banquet at Agathon's.
- He invited me, so I joined him.
- When I arrived, I turned around and found Socrates was missing. I learned he had stopped at a neighboring house and stayed to ponder his abstractions.

- They started dinner without seeing Socrates. When it was about half complete, Socrates finally appeared.
- Agathon begged him to sit near him, so he did and they ate and dined.
- After awhile, they agreed that the activity of the evening will be conversation, rather than hard drinking.

Phaedrus
- How strange it is that the great and glorious god of Love has not as many poems in its honor.

Eryximachus
- Quite right, For the sake of conversation, let us each, in turn, make a speech in honor of Love.

Socrates
- This isn't a bad topic. Phaedrus, you proposed the idea, so you must be the first.

## Phaedrus' Praise of the God of Love

Phaedrus
- First came Chaos, then the Earth was born, and then Love.
- The gods said that Love was the first, so it is. It is the eldest and the most beneficial.
- Love is the best principle to guide men to live a life of principle.
- One who is caught in a dishonorable or cowardly act is most pained when by their beloved, moreso than father or companions.
- Love will make men die for their beloved.

## Pausanias' Story of Love

Pausanias
- I think you framed the argument in the wrong manner.
- If there was only one Love, you would be well enough. However, there is more than one Love, so we must decide which is our topic.
- I will tell which Love is most deserving of our praise.
- There are two goddesses of Love, so there must be two Loves: Aphrodite's Love and common's Love.
- Aphrodite's Love is that of the body, rather than the soul, which is the worst kind.
- The best kind of love is that which is between two young men, as there is no wantonness. They love intelligent beings whose reason is being developed.
- However, the love of young boys should be forbidden by law, for their future is uncertain and the benefits of love may thrown away on them.
- In fact, this is very strange that most other countries do not have a law for this, probably because they don't want to be troubled.
- Love of youths share the same evil as tyranny. In both, the ruler requires that their subjects should be poor in spirit.
- Open loves are more honorable than secret ones, in our society.

- Consider also, how much the world encourages a lover. If he succeeds he is praise, and if he fails he is blamed.
- Also, in the pursuit of love, a man is allowed to do many strange things and his friends will not prevent him - beg, please, pray, and endure a kind of slavery.
- There is honor in this lover if he follows these practices honorably, and dishonor if dishonorably.

- Evil is the common lover who loves the body rather than the soul, for he loves a thing which is unstable. When the bloom of its youth is complete, he flies away, in spite of his promises.
- Our custom is to test the class of love to see to which of the two classes of love it belongs.
- Time is the true test of the class of love.
- This explains why a hasty attachment is viewed as dishonorable.
- This also explains the evils in being overcome by the love of money or power.
- The only honorable type of attachment is the way of virtue.

- We have a custom: Any one who does service to another in the belief that he will be improved by him in wisdom or virtue, should not be considered dishonorable or a flatterer.
- If one is gracious to his lover in the belief that he is rich, and is disappointed of his gains because his lover is found to be poor, then this beloved is disgraced and not honorable. This is because he has shown that he will gladly be used to gain money.
- If, however, one is gracious to his lover in the belief that he is a good man, and in the hope to be improved by his company, then he is virtuous.
- Thus, what is noble in every case is the acceptance of another for the sake of virtue.
- This is the love of the heavenly goddess, for it makes the lover and the beloved eager in the work of their own improvement.

- To you, Phaedrus, I offer this as my contribution in praise of love.

## Eryximachus's Story of Love

Eryximachus
- I see that Pausanias has rightly distinguished two kinds of love, but double love is not merely the affection of a man's soul towards the fair, but it can be found in the bodies of animals and all productions of the earth.
- The human body contains two kinds of love, each having different loves and desires; the desire of the healthy is one and the desire of the diseased is another. To indulge good men is honorable, and bad men is dishonorable.
- I am a physician, so I know that the good and healthy elements of the body should be indulged, and the bad elements should not be indulged, but discouraged. Medicines can be used to affect these, and it is the physicians job to have knowledge of these medicines.
- The best physician can separate fair love from foul, or convert one into the other, and he knows how to eradicate or implant love. He can also reconcile the hostile elements of the body, which are the most opposite, such as hot and cold, bitter and sweet, and make them loving friends.

- In music there is the same reconciliation of opposites. Harmony is a symphony, and symphony is an agreement.
- The seasons are also full of both these principles. When hot and cold, moist and dry, have harmonious love of one another, they bring health to man, animals, and plants.

- I believe I've omitted several things to praise Love, but maybe you, Aristophanes, can complete the thought or you have a different course to explain.

## Aristophanes' Story of Love

Aristophanes
- I believe that mankind has never fully understood the power of Love. If they did understand, they surely would have built temples and altars. This is not done, however, but it ought to be done, since Love is the best friend of mankind, the helper and healer of ills which are a great impediment to the happiness of the race.
- I will describe the power of Love, and you shall teach the rest of the world what I teach you.

- To begin, I must not that mankind has changed. The original human nature had three genders, not two: man, woman, and the union of the two, which once had a name.
- This early creature had might and strength, and the thoughts of their hearts were great. They made an attack upon the gods. The gods contemplated destroying this race of troublesome creatures, as they did the giants, but Zeus discovered an alternative answer.
- Zeus decided to show this creature humility by separating them into two to weaken them. Apollo then shaped the remaining halves appropriately.
- When one of them meets with their other half, they are lost in amazement of love and friendship and can not explain their desire for each other.

- This is my discourse on Love. Although it is different from yours Eryximachus, I ask that you do not laugh.

Socrates
- You played your part well, Eryximachus, but if you were as I am now, then you really are great.

Agathon
- You raise undue expectations in the audience.

## Agathon's Story of Love

Agathon
- I will first say how I will speak, then I will speak.
- The previous speakers, instead of praising the god Love, appear to have congratulated mankind on the benefits which Love provides to them. I will praise the god first, then speak of his gifts.
- Love is the fairest god, for he is the youngest for he is swift and flees away from age.
- Love's feet are tender, for she steps on the hearts and souls of men, not the ground. However, Love does not touch and  make home in every soul, but will leave where it finds hardness.

- Love is just and temperate.
- All men serve Love of their own free will.
- Love is exceedingly temperate, for Temperance is the ruler of pleasures and desires. No pleasure masters Love, for he is their master and they are his servants.

- Love is also wise.
- Love is a poet, and inspires poetry in others, even in those who had no prior music.

Socrates
- I was right when I said that you would make a wonderful oration.
- It was a rich and varied discourse, and had beautiful concluding words. Who could listen to them without amazement!
- How can I say I am a master of the art when I really had no idea how a thing ought to be praised!
- I presupposed that the topic of praise ought to be true, and the speaker ought to choose the best and present them.
- I felt proud, thinking that I knew the nature of praise, but now I see that the intention of praise is to attribute every kind of greatness and glory, whether it is true or not.
- You say to those who don't know him that "he is all this" and "the cause of all that", because it is impossible to offend when the audience does not know.
- So, I can not speak praise in this way, but if you like to hear the truth about love, then I am ready to speak in my own manner.

- Will you, Phaedrus, like to hear the truth of love as I understand it?

Phaedrus
- Of course, we want to hear you speak in any manner you think best.

Socrates
- Then I must ask Agathon a few more questions, because I will take his admissions as the basis for my discourse.

- Agathon, I think you were right to speak of the nature of Love first, then of his works.
- Is love the love of something or of nothing?

Agathon
- Of something, surely.

Socrates
- Then, does Love desire that of which love is?

Agathon
- Yes, surely.

Socrates
- And does Love possess that which he loves and desires, or not?

Agathon
- Probably not.

Socrates
- He who desires something is in want of something, and he who desires nothing is in want of nothing, is this necessarily and absolutely true?

Agathon
- I agree with you.

Socrates
- Very good. Then, would he who is great, desire to be great? Or would he who is strong, desire to be strong?

Agathon
- This is not consistent with our previous admissions.

Socrates
- True! He who is anything cannot want to be what he is?

Agathon
- Very true.

Socrates
- And yet, if a strong man desired to be strong, then he desires something which he already has. Who can desire that which is already has?
- If I say, "I desire what I have and nothing else." Is this the same as saying "I want what I now have and I want it in the future."?

Agathon
- Yes, it must be.

Socrates
- Then, if he desires that what he has may be preserved to him in the future, is this not the same as saying he desires something which is non-existent to him, and which he has not got?

Agathon
- Very true.

Socrates
- Then, one who desires, desires what he does not have, what he will have in the future, correct?

Agathon
- Very true.

Socrates
- Then, let us restate the argument. First, love is of something, and is of something which one wants, correct?

Agathon
- Correct.

Socrates
- I will remind you what you said in your speech, Agathon. You said that the love of the beautiful gave order to the empire of the gods, for deformed things have no love. Did you not say this?

Agathon
- Yes, and it is a just remark. Love is the love of beauty and not of deformity.

Socrates
- And we already understand that Love is of something which a man wants and does not have, correct?

Agathon
- True.

Socrates
- Then, Love wants beauty, but does not have it?

Agathon
- Certainly.

Socrates
- Would you call a thing beautiful if it wants beauty but does not possess it?

Agathon
- Certainly not.

Socrates
- Then would still say that love is beautiful?

Agathon
- Perhaps I did not understand what I was saying.

Socrates
- I must ask one more question - Is the good not also the beautiful?

Agathon
- Yes.

Socrates
- Then, in wanting the beautiful, love also wants the good, correct?

Agathon
- I cannot refute you, so let us assume this is true.

Socrates
- Now then, I will stop discussing with you, and I will tell a tale of love which I heard from Diotima of Mantineia, a woman wise in this and in many other things. She instructed me in the art of love, so I will repeat to you what she said to me.
- As you have, Agathon, I will speak first of the nature of Love, and then of his works.


## Socrates' Discussion with Diotima About Love

### Proving Love is Not a God

Diotima
- Love is neither fair nor good.

Socrates
- What do you mean? Is love, then, evil and foul?

Diotima
- Must the fair be foul?

Socrates
- Certainly.

Diotima
- Is the unwise not ignorant? There is a relation between wisdom and ignorance, you can't see?

Socrates
- And what is it?

Diotima
- Right opinion, which is incapable of giving a reason, is not knowledge. How can knowledge be devoid of reason?
- Right opinion, hereby, is clearly a mean between ignorance and wisdom.

Socrates
- Quite true.

Diotima
- Do not insist, then, that what is not fair is necessarily foul, or what is not good must be evil. And do not infer that because love is not fair and good, then he must be foul and evil, for he is a mean between them.

Socrates
- Well, Love is admitted by all to be a great god.

Diotima
- By those who know or by those who don't know?

Socrates
- By all.

Diotima
- And how can Love be acknowledged to be a great god by those who say he is not a god at all?

Socrates
- Who could that be?

Diotima
- You and I are two of them. You, yourself, would acknowledge that the gods are happy and fair, would you not?

Socrates
- Certainly not.

Diotima
- And by happy, we mean possessing good and fair things?

Socrates
- Yes.

Diotima
- And you admitted that Love, because he is in want, desires good and fair things which he wants?

Socrates
- Yes, I did.

Diotima
- But how can he be a god who has no portion in either good or fair?

Socrates
- Impossible.

Diotima
- then you see that you also deny that Love is a god.

### Decided the Essence of Love

Socrates
- What, then, is Love? Is he mortal?

Diotima
- No.

Socrates
- What then?

Diotima
- As we said before, he is neither mortal nor immortal, but in a mean between the two.

Socrates
- What is he, Diotima?

Diotima
- He is a great spirit, intermediate between the divine and the mortal.

Socrates
- And what is his power?

Diotima
- He interprets and conveys prayers and replies between gods and men.
- God mingles not with man, but through Love.
- The wisdom which understands this is spiritual, all other wisdom, such as arts and handicrafts is vulgar.

Socrates
- And who is Love's father and mother?

Diotima
- It is a long story, but I will tell you.
- The gods had a feast on Aphrodite's birthday. After the party, Penia (Poverty) came to the door to beg. Poros (Plenty) became sleepy after drinking at the party and went to the garden of Zues to sleep. Poverty plotted to have a child with Plenty, partly because he is naturally a lover of the beautiful. Love was conceived in this encounter.
- Like Poverty, Love is always poor and rough and is always in distress. Like Plenty, he is always plotting against the fair and good; bold, enterprising, strong, and keen for wisdom.
- Love is alive and flourishing at one moment when he is in plenty, and dead at another moment. What is flowing in is always flowing out, so he is never in want and never in wealth; always in a mean between ignorance and knowledge.
- No god is a philosopher, a seeker of wisdom, for a god is wise already.
- The ignorant do not seek Wisdom, for he is neither good nor wise but is nevertheless satisfied with himself. He has no desire because he feels no want.

Socrates
- Who, then, are the lovers of wisdom, if they are neither the wise nor the foolish?

Diotima
- Of course, it is those who are in a mean between the two. Love is one of these.
- Wisdom is the most beautiful thing, and Love is of the beautiful. Therefore, Love is a lover of wisdom, and being a lover of wisdom is in a mean between the ignorant and the wise.
- Your confusion lied in your understanding of love and the beloved, which made you believe that love is all beautiful.
- The beloved is the truly beautiful. It is delicate, perfect, and blessed.

Socrates
- You speak this quite well. However, if it is as you say, then what is the use of Love to men?

Diotima
- I will attempt to explain it to you.
- I have explained the nature and birth of Love. And you acknowledge that love is of the beautiful. But, some will say, of the beautiful in what?
- To put the question more clearly: When a man loves the beautiful, what does he desire?

Socrates
- That the beautiful may be his.

Diotima
- Ah, but what is given by the possession of beauty?

Socrates
- I have no answer.

Diotima
- Then let me use 'good' in the place of 'beautiful'. If a man loves good, what is it that he loves?

Socrates
- The possession of the good.

Diotima
- And what does he gain by possessing the good?

Socrates
- Happiness. This answer is easier.

Diotima
- The happy become happy by acquiring good things. Man desiring happiness is common sense.

Socrates
- You are right.

Diotima
- But is this wish, this desire, common to all? Do all men desire their own good, or only some men?

Socrates
- All men desire their own good.

Diotima
- Why then, is it said that only some men can love? You say that all men love the same things.

Socrates
- I, myself, wonder this.

Diotima
- There is nothing to wonder. The reason is that there are many kinds of love, but the same name is given to them all. The other parts of love have other names.

Socrates
- Please explain.

Diotima
- I will use 'poetry' as a metaphor.
- Poetry, which is complex and manifold. All passage from non-being into being is poetry. These makers are not always called poets.
- Poetry is the portion which is concerned with music and meter.

- You may say that all desire of happiness is only the power of love, and those who are drawn towards love by any other path, such as money-making, gymnastics, or philosophy, are not called lovers.
- The name of the whole is used for all because the affection of some people takes only one form.

Socrates
- I dare say that you are right.

Diotima
- Yes, and you may hear others say that lovers are seeking for their other half, but I say this is not correct as I don't believe that the whole is good.
- If one is evil, they will cut off their hands or feet and throw them away, for they do not love what they have. Unless, that is, you find someone who calls what they have the good and what they don't have the evil. This is because men love only the good, and they love the possession of the good, and they love the everlasting possession of the good.

Socrates
- Yes, men love the good.

Diotima
- So, love may be described as the everlasting possession of the good. Correct?

Socrates
- This must be true.

Diotima
- Then, if this is the nature of love, what is the manner of the pursuit? When one shows eagerness and the heat of love, what are they doing? And what is the object which they have in view?

Socrates
- If I knew, I would not be hungry for your wisdom.

Diotima
- Then I shall tell you.
- The object which a lover has in view is birth in beauty, either body or soul.

Socrates
- I don't understand you.

Diotima
- All men are bringing to the birth in their bodies and their souls.
- There is a certain age at which human nature desires to procreate. This procreation must be in beauty, rather than deformity.
- Procreation is the union of a man and woman. It is a divine thing, as conception and generation is an immortal principle in a mortal creature. Procreation can not be had in the inharmonious.
- The deformed is always inharmonious with the divine, unlike the beautiful.

- Beauty, then, is the destiny (goddess) which presides at birth.
- Therefore, when one approaches beauty, the conceiving power is auspicious, diffusive, and benign. When seeing ugliness, love frowns, shrivels up, and refrains from conception.
- This is why, in the hour of conception, there is an ecstasy about beauty, which brings an alleviation of labor and pain.
- Love is not love of the beautiful only, as you imagine.

Socrates
- Then what is it?

Diotima
- The love of generation and birth in beauty.

Socrates
- Yes.

Diotima
- Yes indeed. But why generation? Because to a mortal creature, generation is a sort of eternity and immortality.
- Love is the everlasting possession of the good, therefore, all men will necessarily desire immortality together with good. Love is of immortality.


Socrates
- These are all things she taught me about love at various times.
- One time she asked me:

Diotima
- What is the cause of love, and the desire it brings?
- You must see how all beasts are in agony when their desire for union begins. They, themselves, will suffer to maintain their young.
- Man acts from reason, but why must animals have these feelings?

Socrates
- I do not know.

Diotima
- If you want to become a master in the art of love, then you must know this.

Socrates
- As I have told you before, I am ignorant. Therefore I come to you as a student. Please tell me the answer.

Diotima
- We agree that love is of the immortal, and the mortal seek to be everlasting and immortal. This can only be attained by leaving behind offspring.
- All creatures experience loss and reparation, of the body and the soul. Knowledge and the sciences, also, are always changing. 'Recollection' is a word that explains the departure and renewal of knowledge, but, while it appears the same knowledge, in reality it is a substituted knowledge.
- In this way, our old, worn-out mortality is continually replaced by a new, similar existence. This is unlike the divine, which is always the same.
- In this way, anything that is mortal can partake in immortality.
- Knowing this, the love men have for their offspring is natural, for man is a mortal being, and this love and interesting is for the sake of immortality.

Socrates
- I am astonished! Is this really true?

Diotima
- You can be assured of this. Think of the ambition of men, and you will wonder at the senselessness of their ways, unless you consider how they are motivated by the love of an immortality of fame.
- For the immortality found in children, man is ready to run more risks, spend more money, and even die for their sake.
- I believe that all men do all things in hope of the glorious fame of immortal virtue.





