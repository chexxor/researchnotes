
# Modern Friendship

Sources


- [Cicero: On Friendship, or Laelius](http://www.fordham.edu/halsall/ancient/cicero-friendship.asp://www.fordham.edu/halsall/ancient/cicero-friendship.asp)
- [Lysis, or Friendship](http://classics.mit.edu/Plato/lysis.html)
- [Confucius and Socrates Compared](http://www.san.beck.org/C%26S-Compared.html)
- [The Analects of Confucius Summary](http://www.gradesaver.com/the-analects-of-confucius/study-guide/short-summary/)
- [The Analects of Confucius](http://classics.mit.edu/Confucius/analects.mb.txt)
- [FRIENDSHIP AND FILIAL PIETY: RELATIONAL ETHICS IN ARISTOTLE AND EARLY CONFUCIANISM](http://onlinelibrary.wiley.com/doi/10.1111/j.1540-6253.2012.01703.x/full)
- [Web page on friendship](http://www.infed.org/biblio/friendship.htm)
- [The Four Loves](http://www.amazon.com/The-Four-Loves-C-Lewis/dp/0156329301/ref=pd_sim_b_5)
- [The Economy of Cities](http://search.barnesandnoble.com/The-Economy-of-Cities/Jane-Jacobs/e/9780394705842/)


## Motivation

Recently, I've realized that the reason I am uncomfortable in some social situations is that I don't have personal policies and boundaries that guide me. I believe that I should develop this by studying the ideas of some famous philosophers. What philosophers am I referencing? Simply because they are popular and a convenient starting point, I am using Confucius, Socrates, and C.S. Lewis to start this project. I will list my sources above in the order I study them.


## Research Notes


