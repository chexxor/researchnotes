
# Nutrient Diet

## Foreword

I've always been annoyed with food.

By breaking a normal rhythm of meals with a variety of nutrition, mood and energy can too easily drop. These moods don't tell you why they appear, and the same mood may be a symptom of a different thing. Was the problem due to sleep choice, food choice, or something else in the body, such as hormones? A certain food will sometimes be very satisfying, and another time provide only a shadow of its satisfaction.

I spend so much time deciding what to eat. If you don't give your body what it needs, the feeling of hunger won't disappear. To cure this feeling, I spend too much time trying to interpret my body's messages of hunger to choose the right food. Something fatty? Salty? Meaty? Vegetabley? It's so hard to know.

The US FDA provides a list of nutrients that the average body requires in a day. It also requires most foods to have a label that lists the food's constituent ingredients and nutrients. This is all very good. I could read the nutrient labels, do some math, and choose foods that have nutrients I require for the day. This doesn't work so easily, however, since all food that I normally encounter in a day does not provide the FDA's recommended amount, nor the complete set, of nutrients. So, by eating normally, a person would have far too much of a few nutrients and little to none of several others.

Therefore, I want a diet that I don't have to think about, yet provides all necessary nutrients in a day. With this goal, I hope to have more consistent energy and focus and fewer occurrences of bad moods.

I have discovered a strategy presented by an internet blogger who has [found success](http://robrhinehart.com/?p=298). He has removed food from his diet completely to directly consume the nutrients they encase. With a calculated diet of pure vitamins and minerals, he has happily and energetically passed 30 days without consuming normal food and without craving it. I wish to try the same, so I'll be using [his ingredients](http://robrhinehart.com/?p=424) as a guide.


## Nutrient Source

Rather than buying each vitamin and mineral individually, measuring them, and mixing them, I chose a multi-vitamin that has the great majority of the FDA's recommended vitamins and minerals. A month's supply is less than ten dollars. This multi-vitamin lacks several nutrients, so I purchased them individually. These are potassium, chloride, phosphorus, magnesium, molybdenum, biotin, and folate. Because I'm buying a few individual ingredients, I decided to pick a few extras - ginseng and gingko bilboa.

# Daily Log

1) a) banana and half of multi-vitamin b) crackers, tea, and half of multi-vitamin c) spinach and some oatmeal as vehicle for olive oil, alcohol with friend. - Feeling: Energy levels high, just miss the activity of eating food. - Exercise: Walked 5+ miles.
2) a) half of multi-vitamin b) crackers, tea, and half of multi-vitamin c) frozen yogurt, and started rare minerals. - Feeling: Energy levels high after multi-vitamin. Energy lower after frozen yogurt, but still alert. Returned home, feel shaky, anxious, and desperate to feel settled. I've felt this way before, but never knew cause, always attributed it to lack of food or sugar. Tried to cure the feeling with protein.
3) a) half of mult-vitamin and rest of rare vitamins b) oatmeal, protein, and crackers c) half of multi-vitamin, protein and spinache. - Feeling: Still find myself thinking about food often. Maybe because I'm exercising a lot. - Exercise: Walked 6+ miles to work and back.
4) a) same as before b) chinese buffet with co-workers. had meat, vegetables, and some ice cream c) same evening supplements. - Feeling: Feel much better today after having healthy lunch. Pretty sure I need to increase my total nutrients per day. - Exercise: Walked 6+ miles to work and back.
5) a) same as before b) french fries for lunch to get sodium and fats for the day c) same as before. Went to hockey game with friend, had a beer. - Feeling: No cravings for sweets, which I really enjoy. - Exercise: Walked 6+ miles to work and around.




