"A Story of Vietnam"
by Truong Buu Lam

# Chapter 1: In the Beginning
==========

## Geography
- Vietnam is smaller than California, but Vietnam's population is three times greater with 84 million
- The Red River in its North runs from the Yunnan province in China. It commonly floods, so the Vietnamese
have been building dikes to contain it from very early on. In Hanoi, the river can reach 12.30, while some
areas of the city are lower than 4.
The Mekong River is very long, starting in the Tibetian plateau, flowing through Yunnan, and
linking all SE Asian countries.
- Vietnam does not have huge mountains, between 2000 and 5000 feet.
- Rain forests cover most of the mountainous areas and may fine exotic animals living there.
- Seasonal winds, monsoons, blow from north to east from October to April, bringing
cold and dry air to Northern Vietnam. May to September sees a hot and humid wind blowing
from the ocean around the equator, bringing heat and rain.

## Ethnic Groups
- Two general categories, by government policy: the plain (kinh) and the mountain people (dan toc),
although all are considered Vietnamese.
- The mountain people have around 60 ethic groups - a number of different languages.
- Some mountain people are related to the Thai people, some to Laos minorities, some
from southern China, and some from northern Myanmar.
- Some arrived one to two hundred years ago from China, others lived there since before history.
- The Muong speak close Vietnamese, thought to venture into mountains during Chinese rule of Vietnam.
- The Muong, the Cham, and a few other groups can claim to be indigenous.
- The plain, or kinh, constitute 90% of country's population and share territory with Chinese, Cham, and Khmers.
- Khmers, or Cambodians, reside in far southern Vietnam, which belonged to Cambodia not long ago.
- Cham reside in coastal plains of central Vietnam. The Champa kingdom controlled that area for
more than ten centuries, from 2nd to 15th centuries. Champa started to be overtaken by wars and
marriage alliances by a growing independent Vietnam, leaving only a few thousand people today and
a few ruins of brick temples. The Cham culture can be seen in Vietnamese music and dances.

## Immigrants
- Chinese community in Vietnam grows out of recent waves of immigration from 1870s to 1920s. During
the French colonial period, they were treated better than indigenous people. They boosted French
colony's economy by making industries and retail trade. One other wave of Chinese immigrants from
end of Chinese Ming dynasty in 1644.
- Tens of thousands of Indians immigrated to Vietnam during French colonial period. They were also
treated better than indigenous people, and helped in adminstration of the colony, retail trade,
or money lending and domestic jobs.


## Language
- The plain people in entire country speak same Vietnamese language, but with accents.
- Chinese loanwords due to long cohabitation by Chinese people, which is source
of the six tones.
- Many French loanwords from French occupation, which was only 80 years, mostly for new inventions.
- Also some English loanwords.

## Ethic Origins
- Nobody really knows origin of Vietnamese people. Two ideas: central Chinese tribe migrated south,
or Malayo-Polynesian tribe moved there and was influenced by Chinese culture.

## Social Values
- Trust family and close relatives above all else.
- Village attachment is valued second. "the law of the king yields to the customs of the village"
- Villages were mostly independent from central authority, receiving only taxes and conscription in quotas.
- Land and population for each village was recorded by village leaders, used when figuring quotas.
- Village communal house was important and used by village leaders.
- Each village honors historical contributors. Village holidays during birth or death days of these people.
- Third value was in national sovereign who presided over all villages. Acted as supreme authority on Earth.

## Human Evolutions in Vietnam
- Archealogy shows artifacts from Paleolithic age.
- Evidence of bronze age four thousand years ago.

## Legends
- Frogs bring rain, has a covenant with heavens.
- Legend of sea surging up against mountains. One of the Hung kings had a beautiful daughter. One day,
the mountain and water genies came asking for her hand at same time. The mountain won her hand,
and the water was jealous because the mountain cheated. Water is jealous of mountains and erodes
it over time.
- Legends of creation of Co Loa, ancient capital city. City had shape of conch shell due to circular
roads and river moats, which provided transportation. It was such a marvel that stories were created
to explain its creation. Legend has it that the king created the city with help from a local golden
turtle deity. The king was trying to build the city on the turtle's back, but it was destroyed every night.
The turtle asked the king to relocate the city, and the city was built in no time. The turtle thanked
the king and gave him once of its claws, which would multiple one arrow into thousands when used as
a trigger on a bow.

## Three Names for Vietnam, Given by Conquerers
- Van Lang (2800-258 BCE) during Hung dynasty. Ruled by 18 kings, all named Hung, Van Lang was the first name
of the Vietnamese territory. The first Hung descended from Lac the Dragon Lord and Au Co, the immortal.
Story of 100 eggs, split into two groups, one for plains and one for mountains. Vietnamese
still believe they are children of the dragon.
- Au Lac (258-189 BCE) during Thuc dynasty: Van Lang became named Au Lac when Thuc Phan declared himself king after invading the
weakening Van Lang in 258 BCE. Thuc Phan built awesome capital city, Co Loa, and defended against
numerous invasions from China's Qin dynasty.
- Nam Viet (207-137 BCE) during Qin and Han dynasties: Zhao Tuo created own kingdom north of Au Lac, called Nam Viet,
when the Qin dynasty was weakening. He tried to annex Au Lac numerous times, but failed, so he
tried other means. He sent his son, Trong Thuy, to wed the daughter of the Thuc king, My Chau.
After the marriage, the son found the magic turtle claw bow trigger of the Au Lac kingdom and brought
it back to his father, who used it to quickly conquer and annex Au Lac under his kingdom of Nam Viet.


## Significance of Nan Yue/Nan Viet
- Van Lang and Au Lac weren't unified kingdoms - kings were chiefs of powerful tribes, other groups pledged loyalty.
- Zhao Tuo merged four principalities with Vietnam, naming his newly extended territory 'Nan Yue/Nan Viet'.
- The Han replaced the Qin in 202 BCE, and the Han were eager to exact the boundaries of their territory.
They started a series of attempts to pull Nan Yue back into the Chinese kingdom, from persuasion to invasion.
- The Han dispatched an envoy to pull Nan Yue back into China in 196 BCE. Zhao Tuo finally decided to submit
to the emperor and surrendered his independence. Ten years later, the Han was forbidden from selling some goods
to Nan Yue, Zhao Tuo became angry and called himself emperor his own territories again, defeating Han reinforcements.
After the Han sent an embassy to persuade Zhao Tuo to rejoin, as before, he agreed, as before.
- The Nan Yue kingdom is important because it consisted of present day Guangdong and Guangxi as well as Vietnam.
Because this kingdom was annexed by the Han, it was all considered one territory, and for a very long time
the Chinese considered it their own territory to occupy, not a land to conquer. In fact, maps have been uncovered
that show the ROC's territorial extension included not only Tibet and Taiwan, but Vietnam as well.
- It has always been Vietnam's burden to prove that it is its own country, not a part of China. As there
is only one sun in the sky, there should be only one emperor on earth.

## Submitting to China's Power
- Question asked to surrounding countries: "To gain your territory makes us no richer, because it is so small,
but what would happen to you and to your innocent people, when we send a chastising army against you?"
- To revere the Chinese emperor keeps peace in own lands, and benefits from commerce with China's markets.
- Zhao Tuo would not submit to Han emperor if he lived in China. He wanted to be emperor of China instead
of king of Nan Yue. For Nan Yue to be considered separate from China, it must stay out of China's wars
and China's eye.

## Origin of Vietnam's Current Name
- Emperor Gia Long of Vietnam requested from China in the 19th century that his country be named
Nam Viet, China agreed to name it Vietnam, to avoid mix-up with Zhao Tuo's territory of Nan Yue/Nan Viet.
Nan Viet included the two Chinese provinces.


# Chapter 2: The Chinese Connection (111 BCE - 939)
==========

## Remote and Loosely Bound, Yet Affected by China
- The Han dynasty brought a long era of Chinese domination over Nan Yue.
- The Han divided Vietnam into seven prefectures: four located in China, and three in Vietnam.
Each prefecture was divided into districts.
- The northern prefectures of Nan Yue was under elaborate control from China. The southern prefectures
consisted of Chinese garrisons that simply oversaw the collection of taxes, trade, and law. Thus, Vietnam
retained control over its only people.
- The Chinese administrators, however, did not value being stationed in Vietnam. To satisfy their ego,
they attempted to assert Chinese ways onto Vietnam peoples: agriculture, marriage and clothing.
- Masses of Chinese immigrants also heavily affected Vietnam. Most immigrants were scholarly and escaping
dynastic change. Vietnam chieftains felt threatened by the incoming Chinese, who assumed a higher status.

## Rebellions against Chinese Occupancy
- Trung sisters led a successful revolt against Chinese governor in 40 CE. Significant because this is
earliest evidence that women held higher status in Vietnam that in Confucian China. The two sisters
proclaimed themselves queens over Vietnam, held reign for three years.
- The Han dispatched an able army, led by Ma Yuan, to quell the Trung sisters.
- Ma Yuan reorganized the administration of this southern territory, this time ensuring all positions
of authority are staffed by Chinese.
- The newly reorganized southern territory created two groups. One accepted the heavy Chinese influence,
the other group withdrew into the highlands, which was forbidden to the Chinese.

## Starting the Champa Kingdom
- In 192 CE, a local chieftan, Qu Lien, moved to and area south of the southern part of Nan Yue to
found his own, independent kingdom, which he named Lin Yi/Lam Ap, later as Huan Wang/Hoan Vuong, and
later as Champa/Chiem Thanh.
- The Champa people met the Khmer people, with whom they allied against Chinese dominance. They became
part of Indian trading routes, gladly welcoming the friendly Indian traders, and became a powerful kingdom.

## The Effect of the Three Kingdoms (222 - 265)
- Eastern Wu, Dong Ngo, inherited control of Nan Yue.
- In 226, the Wu split Nan Yue into two halves: northern portion was called Ghuangzhou, southern
was called Jiao Zhou/Giao Chao. This southern portion was completely inside present day northern
and central Vietnam.
- Under the name of Giao Chau, present day Vietnam finally received a separate geographical
identity, accelerated by the different language in GuangZhou to the north.

## Giao Chau - Rulers After the Wu
- 265 - 420 by the Jin/Tan
- 420 - 478 by the Song/Tong
- 479 - 501 by the Qi/Te
- 502 - 555 by the Liang/Luong
- 557 - 583 by the Chen/Tran

## Ly Bi
- Considered a native of Vietnam, as his ancestors emigrated from China seven generations earlier.
- In 544, grew a rebellion, which expelled Chinese administrators and troops from Giao Chao.
- Changed name of his country to Van Xuan, "Ten Thousand Springs".
- Dynasty carried on until 602, when they surrendered to the Sui/Tuy of China.
- Interesting to note that after 300 years of being a Chinese territorial base, cohabitation
made it impossible to distinguish the Chinese immigrants from the natives.

## Vietnam Becomes An Nam
- In 617 - 906, the Tang/Duong divided Vietnam into numerous provinces, which were placed under
control of two general commissioners. They reduced the number of provinces and assigned them
to two governors general.
- All frontier areas around China were were given a new administrative title called 'protectorates'.
- Pacified Protectorate of the West - Xinjiang
- Pacified Protectorate of the North - Mongolia
- Pacified Protectorate of the East - Korea
- Pacified Protectorate of the South - An Nam
- This name for Vietnam was kept for hundreds of years. When the French conquered Vietnam
in the 19th century, they resurrected the name An Nam, and the Kings of An Nam became puppets
to the French authorities.

## Challenging the Chinese Authority and Gaining Liberation
- Around 720, Mai Thuc Loan challenged the Chinese and became emperor of Vietnam, under the
title 'Mai Hac De', or 'Mai the Black Emperor'.
- In 782, Phung Hung and his brother had sheer physical strength and overwhelmed the Chinese
troops, proclaiming himself emperor and declaring Vietnam as independent. This didn't last long,
as they surrendered to the Tang emperor's troops who were sent to retake An Nam in 791.
- Around the 9th century, the Tang experienced uprisings both at home and in its colonies,
and could not hold on to all its colonies. Small states on China's periphery claimed autonomy.
- In 906, Khuc Thua Du fought the Tang administrators and claimed autonomy for An Nam. The Liang
dynasty started in 907, and they recognized his son as An Nam's military governor.
- In 923, Nan Han, the state directly north of An Nam, took control of An Nam from the Liang.
- In 938, the Nan Han king sent his son leading an army to take more direct control over An Nam.
Ngo Quyen became commander of An Nam's anti-Nan Han forces and dealt a humiliating defeat to
the Nan Han forces.
- Interesting story about this victory: Ngo Quyen anchored hundreds of wooden poles with sharpened
iron heads on both sides of the Bach Dang River. When attacked, the enemy retreated, being sNgolowed
or torn apart by the spears. This same strategy was used in the same river a few hundred years
later when the Mongols invaded. The same thing happened a hundred years later against the Ming.
- This was the last time the Chinese successfully held Vietnam as a colony. This date has been
accepted as the definitive date of Vietnamese liberation from >1000 years of Chinese domination.

## Retrospective - Influence of 1000 years of Chinese Control
- Even though Vietnam emerged as independent in the 10th century, it was only politically.
Culturally, it had become a 'little China'
- Arguable claims that the first Chinese governors introduced the use of clothes, shoes, and hats.
taught agriculture, wedding and funeral rituals, and the basics of morality.
- Some undeniable effects of China:
- Chinese became the official writing system of the government. Vietnam had no writing system
prior to Chinese occupation, so they adopted Chinese as their written language, regardless that
their pronunciations of Chinese words were unique from motherland China.
- Education system was a copy of China's.
- Confucianism, which taught to cultivate oneself, one's role in their family, and in their country.
It also taught that women must obey their father, husband, and first son. One's life for one's emperor.
- Daoism, which taught that human's are pure, but become corrupted by society. Therefore, it's best
to live as if on a mountain top, and to meditate to reach a state of no action.
- Buddhism, which taught karma, life and rebirth. The afterlife was not considered in Confucian teachings.
Four simple truths, which state that life is suffering, and we should eliminate cravings and desires.
Became very popular and widespread.
- Imperial palace in Hue is similar to China's.
- Literary works are similar to China's.
- The structure of Vietnam's central government is split into prefectures and districts, just as in China.
- This same culture path is in China, Korea, and Japan. Indian and Chinese civilizations emerged
early in time and were first to influence all countries around them, so this common sense of Asian
culture is to be expected.

## Uniquely Vietnamese
- The status of women was higher pre-China occupation. This, however, was mostly erased with
adoption of Confucianism.
- Grammar of language never adopted Chinese method of grammar.
- Village organization. Majority of Vietnamese lived in countryside, away from colonial
administration. They were somewhat insulated from governing bodies and immigrants. Family
dominance in geographical areas is related to this.


# Chapter 3: Independent Vietnam (939 - 1428)
==========

## Dinh Bo Linh Lays Foundations for Independent Vietnam
- Ngo Quyen finally pushed all Chinese forces out of Vietnam, claiming Vietnam's independence.
- Government and administration solution was to continue using the Chinese model, as Vietnamese
knew no other models.
- Quyen's public officials consisted of local lords of chieftans who helped him rebel
against China. These local lords claimed their own areas of the new territory. When Quyen
died in 944, his successors were unable to control these local lords. Fighting amongst
themselves from 954 to 968 in a civil war, they created 12 principalities.
- In 968, Dinh Bo Linh eliminated all 12 warlords, bringing unity to Vietnam. Dinh Bo Linh
was said to have two dragons that appeared out of nowhere and flew around him to protect him.
- Dinh Bo Linh changed the name of the country to Dai Co Viet, which means 'Great Grand Viet'.
- Dinh Bo Linh sent an embassy to China to ask their acceptance of him becoming ruler of
the newly named Dai Co Viet. The Song accepted, and granted him the title 'King of Prefecture
of Giao Chi'. This is significant because it starts a tradition in Vietnam-China relations
in which any new leader of Vietnam must report to China for blessings and to give tributes.
- Dinh Bo Linh set up plans to raise an army of 100,000 men to defend the newly independent
country. He was known as a harsh ruler, so he didn't have problems motivating men.
- He also set up plans for a civilian government, which would be similar to the Chinese model.
He needed to staff his government with literate and knowledgable people, not only military.
The only Vietnamese people who qualified where Buddhist monks, who spent most of their time
studying and meditating.

## Le Hoan Rules and Expands (980-1009)
- Dinh Bo Linh was assassinated in 980. The remaining heir was six years old and became king,
however all actual power was held by the Commander-in-Chief of the army, Le Hoan.
- Le Hoan was born to a mother who had a vision of a lotus flower growing in her belly. The
baby had an extraordinary physique. His parents both died, and he was raised by a lowly official.
He was known to have two dragons flying around his head, generally when he was sleeping.
- Le Hoan remained at Hoa Lu, calling himself Le Dai Hang Hoang De, which means Emperor
with Great Behavior. His dynasty lasted 25 years, followed by Le Long Dinh's rule for 4 years,
who killed his brothers to take the throne.
- The Song, from China, attacked the Vietnamese from land and sea. They were defeated at a mountain
pass and two commanders were taken prisoner. The sea fleet heard this and gave up their fight.
- He sent the Chinese prisoners of war back to China as a tribute. He knew his country had no
fighting chance against China, so he chose to uphold the tributary relationship with China. The
Song emperor granted him the title of "Military Governor of Giao Chi", followed by "King of Giao
Chi prefecture", and again followed by "King Who Pacifies the South" (Nam Bing Vuong).

## South of Vietnam: The Champa Kingdom
- Now that he had peace with the North, Le Hoan looked towards the South to expand his kingdom.
The Champa controlled the area from Da Nang to Nha Trang.
- The Champa spoke a different language from the rest of the area, a type of Malayo-Polynesian.
Not long after they settled, the Champa saw influences from India. This made the Champa one of
the first Indianized states of Southeast Asia, which made it the point of trade between India
and Southeast Asian civilizations.
- Champa was never a unitary state, but rather a confederation of groups of people, all located
along the shore of central Vietnam. Their main cities were located on the coast along the
India-China trade route.

## Relations with Champa to the South
- The Champa often raided and plundered their Indochinese neighbors, which made the South
China sea unsafe for trade, unless the Champa were involved in the dealings.
- The Champa rulers practices Hinduism until around the 13th or 14th century, when Islam
spread from India to Southeast Asia.
- China was often unwilling to protect Vietnam from Cham's frequent incursion because they
were busy managing internal conflict.
- Le Hoan was the second Vietnamese ruler, after Ly Bi in 6th century, to assault Champa. He
seized its capital city in 982, looted it, and brought back many prisoners, mainly craftsman.
The Champa moved their capital farther south, from present-day Quang Nam to Binh Dinh.
- After this battle, Le Hoan demanded a regular tributary to be paid, similar to what Vietnam
paid to China.

## Ly Cong Uan, Vietnam's First Monk King
- Mother of Ly Cong Uan dream she had relations with a spirit. Gave her son to a monastery of
monks, under a person by the name of Ly Khanh Van, from which he got his name Ly. From this monk,
he was introduced to the Court and had many accomplishments, both administrative and militaristic.
- Transferred the capital to Hanoi, which he called Soaring Dragon City (Thang Long Thanh) because
he saw a dragon taking flight from the site. He also changed the country's name back to Dai Co Viet.
- To protect his country, he encouraged a strong army. To ensure the army didn't turn against their
rulers, he assigned royal family members to important military positions.
- Ly Thuong Kiet was one such royal military leader. He lead several successful campaigns against
the Cham. He also preemptively attacked the Song in 1075 by launching a two-prong attack against China's
southern provinces. He sent an embassy to the Chinese capital to apologize and resume tributaries. China
bestowed the Vietnamese ruler with the title King Who Pacifies the South (Nam Bing Vuong), same as
Le Hoan hundreds of years earlier.
- The Ly dynasty looked south to expand their territory. They attacked the Champa and captured
the Cham king in 1044. The king gave up 3 districts to the Ly dynasty. Rather than occupy the
newly gained territoy, they took 4-5 thousand Cham people and settled them near the capital city.
30 years later, Ly Thuong Kiet sent his armies to these territories to pacify their conflicts, but
he also settled Vietnamese peasants here. This is the first annexation of Champa territory, an
expansion that will eventually consume all Champa lands.

## The Ly Empire's Governmental Changes
- Until now, the Buddhist community were the sole administrators of government. The Ly dynasty changed
this in 1075 by setting up mandarinal examinations. No official could be appointed to any position without
having passed these competitive Court examinations. This is the first example of Chinese organization
taking root in Vietnam. This exam is significant because it leaks power away from aristocratic families
by allowing any exam-taker to assume official government positions.
- They constructed a university in Hanoi in 1076, called School for the Children of the Nation (Quoc
Tu Giam). It's students were largely children of government families, and was meant to train them
for regional and metropolitan examinations. It taught Buddhist and Daoist texts in the beginning, but
shifted to exclusively focus on Confucian writings.

## Path to Government Official
- From 1075 to 1917, the road becoming a scholar and official was a long one, and it remained unchanged.
- Women, actors, and convicts unable to take examinations. Actors are trained to play roles, can't be trusted.
- The training of a mandarin begins in his village.
  - Most kids in rural areas attended private school in the village. Education was left to private sector.
  - Village schools taught three standard texts: Three Thousand Words (Tam Thien Tu), Three Words Classic
  (Tam Tu Kinh), and Mirror of Clear Mind (Minh Tam Tu Kinh), all written in Chinese. Heavy memorization.
- Next, the student moves to a provincial capital city, where a government institution held examination
review sessions part-time.
  - While having taken oral and qualifying tests, this is first time they are in direct competition with
  students from the entire province.
  - Aim of examination was to test a) classical Chinese, b) ability to comment on Confucian excerpts.,
  and c) write one dissertation in each literary genre chosen by examiners.
  - Results of provincial examinations ranked students from 9 (bottom) to 1 (top) and subdivided into A or B.
  These ranks decided what position they may fill in government, such as scribe, translator, or liaison.
- Students can aim for higher levels by preparing for the Regional Examinations (thi huong).
  - Provincial Examinations were once per year, while Regional Examinations were once every three years.
  - Four stages: a) comment on excerpt from Confucius' writings and his Four Books and Five Classics,
  b) compose poetry and prose following rigid rules of style, c) writing of official documents, d) compose
  literary dissertation that proposed a strategic plan to solve problems posed by examiners.
- Final examination was the Metropolitan and Palace Examinations, offered every three years at the Royal Palace.
  - Same four tasks as at Regional Examinations. If the metropolitan examiners were satisfied, you could move
  on to the ultimate test: the Palace Examination, which were held on royal grounds. The king presided over
  the test, chose the topic of the dissertation, and graded them himself the next day.
  - Successful candidates received the degree of Advanced Scholars (Tien Si) and had their names inscribed
  on stone stelae in the courtyard of the Temple of Literature. They started a journey to their hometown,
  accompanied by capital guards and carried on a palanquin, stopping at various receptions along the way.

















