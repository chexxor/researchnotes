
# MDCC TechTalk - Classes, Jim, But Not as We Know Them

## Speaker

Simon Peyton Jones, MA, MBCS, CEng and key contributor of Haskell

## Source

[MDCC TechTalk - Classes, Jim, but not as we know them](http://channel9.msdn.com/posts/MDCC-TechTalk-Classes-Jim-but-not-as-we-know-them)

## Location

Microsoft Development Center Copenhagen

November 9, 2011

## Notes

### Typical Lifecycle of Programming Language

Most new programming languages

- 0 year, 1 user
- 3 year, 1 user
- 5 year, 0 user

Successful research languages

- 0 year, 1 user
- 5 year, 100 users
- 7 year, 0 user

C++, Java, Perl, Ruby

- 0 year, 1 user
- 1 year, 80 users
- 5 year, 10k users
- 10 year, 1000k users

Committee languages

- 0 year, 0 user
- 1 year, 0 user
- 10 year, 0 user

Haskell

- 0 year, 1 user
- 3 years, 100 users
- 5 years, 1k users
- 10 years, 1k users
- 15 years, 1k users
- 17 years, 20k users

#### Typical Lifecycle Summary

Haskell is unusual because it is designed by a committee, but it is experiencing a second life. Industry experts cite Haskell as a language which will teach good programming practices and ideas.

Supporting this, statistics show that, while a few projects *are* written in Haskell, many people are talking about it on the Internet.

Unique ideas in Haskell which people want to see:

- Purely functional (immutable values)
- Controlling side-effects (monads)
- Laziness
- Concurrency and parallelism
- Sexy types
- Type classes

This talk will discuss the "type classes" aspect which is implemented by Haskell.

### Type Classes

#### Number One Motivating Problem

Type classes are not like classes in the the Object-oriented world, despite both using the word "class".

Start with a problem. The type signature of the following function has a type variable, which means it works for any type. Look closer, can it really work for **any type**?:

        member :: a -> [a] -> Bool
        member x []                 = False
        member x (y:ys) | x==y      = True
                        | otherwise = member x ys

Look at `x==y`. Can you think of a type which can't be tested for equality? Functions. How can we say two functions are equal? Other languages might look at their address in memory, but this disagrees with Haskell's conception of pure functions.

Instead, consider this idea. Let's constrain this function's inputs to allow only *comparable* types.

#### Similar Motivating Problems

These functions *claim* to work for any type, but how can they possibly work for every type? They *must* behave differently for different types.

- `sort :: [a] -> [a]` -- Equality is type-dependent
- `(+) :: a -> a -> a` -- Float, Integers
- `show :: a -> String`
- `serialize :: a -> BitString` -- Serialize a type is type-dependent
- `hash :: a -> Int`

#### Unsatisfactory Solutions

How can we change a function's behavior when certain types are passed? Here are some solutions, but rather poor solutions:

- Local Choice
   - Replace `a + b` with `a 'plusfloat' b` or `a 'plusInt' b`, depending on types.
   - Why not great? Loss of abstraction. Becomes monomorphic.

- Special Cases
    - To solve equality, provide default equality for everything, but restrict incompatible types. (i.e. functions)
    - To solve serialization, provide default serialization for everything, but restrict incompatible types.
    - Why not great? Run-time errors.
    - Can enable a fixed set of operations, but what about enabling everything a programmer wants to do?

#### Haskell's Type Classes

Haskell enables a function to change behavior with an idea called "type classes".

Consider the following function:

        square :: Num a => a -> a
        square x = x*x

This type signature of this function says it works for a parameter **any** type. However, this function only makes for functions which can be multiplied, that is, it only makes sense for *numbers*. So, we say this function will work for a parameter of **any** type *which has numeric properties*. This is what Haskell calls a class, which is similar to what Java calls an interface. The class of types allowed for parameters to the `square` function is called the "Num" class.

The `Num` type class defines the properties of a type. Then, a type can declare it is an instance of `Num` by providing definitions for these properties.

Similar functions which must restrict parameter types:

        sort      :: Ord a  => [a] -> [a]
        serialize :: Show a => a -> String
        member    :: Eq a   => a -> [a] -> Bool

### Defining a Haskell Class

Looking at code helps one to understand, so the following code shows how to make a class in Haskell.

        class Num a where
          (+)    :: a -> a -> a
          (*)    :: a -> a -> a
          negate :: a -> a
          -- ...etc.

And here is how the `Int` type declares it is a member of the `Num` class. It must provide definitions for each property that the `Num` defined. Note that the `plusInt`, `mulInt`, and `negInt` functions are defined elsewhere. Their type signatures look like this: `plusInt :: Int -> Int -> Int`.

        instance Num Int where
          a + b    = plusInt a b
          a * b    = mulInt a b
          negate a = negInt a
          -- ...etc.

### Implementation of Type Classes

Type classes are a new thing, a difficult-to-understand new thing. How do we understand new things? We relate them to familiar things! Conveniently, type classes can be viewed as syntactic sugar for familiar things.

I know you're curious about how they work. So, for full understanding, let's look at how type classes are implemented. Let's do this by looking at type classes from two points of view: the programmer and the compiler.

#### Implementation of Type-classed Parameter

        -- Programmer --
        square :: Num a => a -> a
        square x = x*x
        -- Compiler --
        square :: Num n -> n -> n
        square d x = (*) d x x

The programmer declares a function with a type-classed parameter. What does the compiler do? It adds one extra parameter to the function.

Why add an extra parameter if a function has a type-classed parameter? Think of the meaning of a class in Haskell. `Num a =>` means: this function depends on `a` having special properties, that `a` implements certain functions. The original function provides half of the functionality and, in turn, expects `a`'s type to provide the other half which is type-dependent. When the programmer references a class property, such as `(*)`, he assumes the compiler will find the correct implementation from the type. The compiler does this by "swapping in" the correct implementation, which is found on `a`'s type.

Let's see how the "swap in" works. Inside the function, the compiler sees that `a`, named `x`, is used with `(*)`. `(*)` is a class property, so it needs to execute the type's implementation. How can we find that implementation? Use normal function matching. Before we can match it, the compiler needs to set up the function. This comes from the class declaration, as we'll see next.

Before we continue, look at the compiler's version of the `square` function at the `(*)` function. It has one extra parameter. As you can see from the function type signature, this parameter's type is `Num`. Where is this type defined? This comes from the class declaration. Let's look at that now.

#### Implementation of Type Class

        -- Programmer --
        class Num a where
          (+)    :: a -> a -> a
          (*)    :: a -> a -> a
          negate :: a -> a
          -- ...etc.
        -- Compiler --
        data Num a
          = MkNum (a->a->a)
                  (a->a->a)
                  (a->a)
                  ...etc...
        -- Get functions defined on the type
        (*) :: Num a -> a -> a -> a
        (*) (MkNum _ m _ _) = m

The programmer declares class properties in the form of function names and type signatures. The compiler sees this as two things: a) a new type and b) unimplemented functions.

How will an unimplemented functions receive its implementation? Pass it as a parameter, of course. It only consumes the first parameter, leaving the remaining parameters for the real implementation. The first parameter, `Num` type, should contain functions. Let's look at this value type.

#### Implementation of Instance of Type Class

        -- Programmer --
        instance Num Int where
          a + b    = plusInt a b
          a * b    = mulInt a b
          negate a = negInt a
          -- ...etc.
        -- Compiler --
        dNumInt :: Num Int
        dNumInt = MkNum plusInt mulInt negInt ...

The programmer adds the `Int` type to the `Num` class. The compiler sees this as the declaration of a value type.

This value type contains implementations of the class properties. Why a value type? The value of an unimplemented function in a function, no? This value type is intended to be passed to the unimplemented methods which were generated from the type class declaration. These unimplemented methods use a mask to choose the value which corresponds to the method's name. As you can see, the values in this value type must be placed to match the masks in the unimplemented functions. How to manage this? It's not difficult, as they can simply use the same sort order, such as alphabetical ordering.

#### Summary of Implementation

So, what really happens when you use type classes? When you define a new class, you are effectively declaring a new data type with empty implementations. When you define a new instance of that class, you are effectively injecting your type-specific behavior by using it to constructing the type.

How does the compiler help? It reorganizes the function's type signatures, the functions which are properties of the class, and sets up the new type's empty functions and value type's implementations to correctly thread together.

Let's step through it. The compiler interpreted the class and instance as a new type and value type. Then, the compiler rewrites functions which use the type class to pass the value type through all type-classed functions. The intended `(*)` function will be found when it is passed this value type.

        -- Parsed Type-classed Parameter --
        square :: Num n -> n -> n
        square d x = (*) d x x
        -- Modified (*) Type Signature --
        (*) :: Num a -> a -> a -> a

The type of `d` will be `Num`, which can be constructed using `MkNum`, which comes from the class instance.

        -- Class --
        (*) (MkNum _ m _ ...) = m
        -- Instance --
        dNumInt :: Num Int
        dNumInt = MkNum plusInt mulInt negInt ...


#### All This Scales Up Nicely

We can build big overloaded *functions* by calling smaller overloaded *functions*.

        -- Programmer --
        sumSq :: Num n => n -> n -> n
        sumSq x y = square x + square y
        -- Compiler --
        numSq :: Num n -> n -> n -> n
        numSq d x y = (+) d (square d x) (square d y)

Here, we get get the right `(+)` operator from `d` in the same way as above. What do we do with the `square` function? It's type signature says it's parameters must also be instances of the `Num` class. We pass the very same `d` into square for it to use.

We can build big *instances* by building on smaller *instances*.

        -- Programmer --
        class Eq a where
          (==) :: a -> a -> Bool
        instance Eq a => Eq [a] where
          (==) []     []     = True
          (==) (x:xs) (y:yx) = x==y && xs == ys
          (==) _      _      = False
        -- Compiler --
        data Eq = MkEq (a->a->Bool)
          (==) (MkEq eq) = eq
        dEqList :: Eq a -> Eq [a]
          where 
            eql []     []     = True
            eql (x:xs) (y:ys) = (==) d x y && eql xs ys
            eql _      _      = False

When we test equality on the heads `x==y`, we need `d` to choose the correct `(==)` function.

## Examples of Type Classes

### Overloaded Constants

Constants, such as 1 and 2, are what type? Are they `Int`? `Float`? Well, we should be able to add a literal 1 to either a `Float` or an `Float`. So, literal values like these should be "type polymorphic", right? This is the perfect place to use type classes.

        class Num a where
          ...
          fromInteger :: Integer -> a
          ...
        -- Programmer --
        inc :: Num a => a -> a
        inc x = x + 1
        -- Compiler --
        inc :: Num a -> a -> a
        inc d x = (+) d x (fromInteger d 1)

When we use a literal `1`, we actually mean something like `fromInteger 1`. This allows us to change the type of `1` into an `Int` or `Float` or any other type of the `Num` class.

### Quickcheck

Until now, each use-case of type classes could have been instead implemented by special treatment from the compiler. The important question is, can type classes have any practical use? Can they be used to create currently unknown tools?

John Hughes used type classes to make a testing tool called Quickcheck, which is incredibly awesome. Quickcheck works out how many arguments, generates test data, then runs tests. If the function returns True 100 times, we can be confident it is correct. Let's look at how to use Quickcheck.

        -- Write assertions about your functions
        propRev :: [Int] -> Bool
        propRev xs = reverse (reverse xs) == xs
        propRevApp :: [Int] -> [Int] -> Bool
        propRevApp xs ys = reverse (xs++ys) == reverse ys ++ reverse xs
        -- Pass to quickCheck to generate random parameters.
        > quickCheck propRev
        OK: passed 100 tests
        > quickCheck propRevApp
        OK: passed 100 tests

Think about the type of the `quickCheck` function. What could it be? If it was `quickCheck :: a -> Bool`, it wouldn't work with the `propRevApp` function, which has two parameters. So, what *is* its type signature?

Look below to see that `quickCheck` uses only one type-classed parameter. How does it work with a two-parameter function? Let's look.

        quickCheck :: Testable a => a -> IO()
        class Testable a where
          test :: a -> RandSupply -> Bool
        class Arbitrary a where
          arby :: RandSupply -> a
        instance Testable Bool where
          test b r = b
        -- Support testing one-argument functions
        instance (Arbitrary a, Testable b)
            => Testable (a->b) where
          test f r = test (f (arby r1)) r2
              where (r1,r2) = split r -- Make random data for param1 and param2
        split :: RandSupply -> (RandSupply, RandSupply)

`quickCheck` takes *any* type `a` which has implemented `Testable`s `test` function. This function accepts two parameters, the function to test and the random numbers to give it.

So, our tests take `Int` typed parameters. Let's assume that Quickcheck already defined this common type as a `Testable` instance, which means that Quickcheck already implemented a random `Int` value generator.

We want to test any function, no matter the number of parameters, right? Remember the awesome thing about Haskell's functions? They are all one-argument functions. This means that a three-argument function is the same as three one-argument functions. Therefore, to test a three-argument function, we only need to support testing one-argument functions, right? The key thing to note is that each of these one-argument functions returns another function, until we get to the last argument, which should be the resulting value. Sounds like we need a recursive solution. `quickCheck` passes the assertion function through its `test` function 100 times, which means it is simply a loop over the real work, which is `test`. So `test` must do the recursion.

Let's start with the base case, which is the desired result of `test`, which is `True` or `False`. If we `test True`, we want `True`, and likewise `test False` should return `False`. That's the stopping condition.

Now, the next case, which is a one-argument function. We are thinking of `test f`, where `f` is a testable assertion. A testable assertion is anything which returns True or False, that is `f :: a -> Bool`. So, we need two things: 1) generate a random value a type `a` and 2) test the result. How do we do this? It's pretty clear if we look at the correct implementation: `test f r = test (f (arby r1)) r2`. 1) Pass a random value to the function, `f (arby r1)`, then 2) test the result, `test <that> r2`.

Now, the next case, which is a two-argument function. Actually, we don't need to do anything, becase the above case handles this. Think about it: what if `f` is a two-argument function? Then `f (arby r1)` returns another function, which is passed to `test.` `test` is the root function, so, through recursion, we eventually receive a value.

Here's a question. How do we make random data for a yet-unknown type? We let the new type decide! The new type makes an instance of `Arbitrary` and implements an `arby` function. For example, how do we make an instance of `Arbitrary` for a `(Int, Int)`? In the `arby` function, we ask for an arbitrary first element and an arbitrary second element, then put them together. Simple!

The amazing bit is, these dozen lines are the *entire core* of Quickcheck! This piece of technology, Quickcheck, has become so successful that it has moved from Haskell to a few other languages.

### Type Classes in Other places

- Equality, ordering, serialization
- Numerical operations
- Monads
- Time-varying values
- Pretty-printing
- Collections
- Reflection
- Generic programming
- Marshalling
- Monad transformers

## Related

Implementing type classes took years and many headaches to implement it correctly and efficiently.

The idea of type classes was the root of so many other great ideas.

- Higher-kinded type variables
- Multi-parameter type classes
- Variations
    - Overlapping instances
    - "newtype deriving"
    - Derivable type classes
- Implicit parameters
- Extensible records
- Functional dependencies
- Associated types
- Applications
    - Computation at the type level
    - Generic programming
    - Testing

## Type Classes and Object-oriented Programming

### Type-based dispatch, not value-based dispatch.

Where is the functionality of a class defined? In type classes, it's stored on the type. In OOP, it's stored on the values.

### Haskell "class" ~ Java "interface"

Define which operations a type must support.

        -- Haskell --
        class Show a where
          show :: a -> String
        f :: Show a => a -> ...
        -- Java --
        interface Showable {
          String show();
        }
        class Blah {
          f (Showable x) {
            ... x.show() ...
          }
        }

But still different, and here's how.

Type classes enable multiple constraints. Suppose we want a type to be both Showable and Numeric. Java doesn't allow that.

        -- Haskell --
        f :: (Num a, Show a) => a -> ...
        -- Java --
        class Blah {
          f ( ??? x) {
            ... x.show() ...
          }
        }

*Existing* types can *retroactively* be made instances of *new* type classes.

        -- Haskell --
        class Wibble a where
          wib :: a -> Bool
        instance Wibble Int where
          wib n = n+1
        -- Java --
        interface Wibble {
          bool wib()
        }
        ... does Int support Wibble? ...
        ... how to make Int support Wibble? subclass? ...

## Two Approaches to Polymorphism

We want code which works on a variety of different argument types.

OO culture adds polymorphism by subtyping/subclassing.
ML culture (Haskell's) adds polymorphism by generics.

OO's polymorphism provides functionality that can't be had in ML's culture, so eventually ML's culture added similar ideas, such as type classes, type families, and existentials. The reverse is also true, OO added features to support cool things ML's style enables, such as interfaces, generics, and constrained generics.

### Generics, not Subtyping

Haskell can not do sub-types. This is inconvenient, because if I want to write a function that works with Trees, I must anticipate this need by creating a type class. OO, on the other hand, can simply subclass tree without bothering the function author.

#### Subtyping's Effect on Type Inference 

However, because Haskell has no subtypes it has great type inference.A Haskell programmer doesn't need to declare a type *every* time a new name is introduced. Java, on the other hand, has subtypes, so every new name must have a type declaration to specify from its type options.

Is type inference a cultural heritage? That is, Java doesn't have great type inference because they simply aren't used to the idea? No, its type engine generates subtyping constraints, which means types can not be deterministically matched. Haskell's type system, on the other hand, generates equality constraints, which is one type can only be equal to that same type. This means type matching can be uniquely solved for a program.

#### Subtyping Loses Type Information

In the following example, we want to increment a numeric type. We use `INum` because we want this function to be reuseable. Suppose `Float` implements the `INum` interface. After we do `x.inc`, is `x` still a `Float`? We don't know! It could be any subtype of `INum`.

        -- Java --
        INum inc( INum x )
        x :: Float
        ... (x.inc) ...

This seems like a big deal. Why is it accepted? Perhaps because a Java-like language doesn't often deal with types. That is, it creates an instance of a class, then a programmer spends most of their time mutating state inside that class. In Haskell, on the other hand, a programmer spends much more type changing between types.

        -- Suppose x is a Button type.
        x.setColour(Blue);
        x.setPosition(3,4);
        -- Nothing changes x's type.

If the above example was Haskell, we would use functions which accept a Button type and return an entirely new Button type, perhaps a different type of Button. We can see that type information is very important in Haskell, but it's used sparingly in Java.

Actually, Java and C# designers must have seen this issue, because both have added constrained generics. However, (I believe) it's little-used in practice.

        -- Java --
        A inc<A>(A x)
          where A:INum {
            ...blah...
        }
        -- Haskell --
        inc :: Num a => a -> a

#### Most Interesting Question

If a language has generics and constrained polymorphism, does it really need subtyping?




