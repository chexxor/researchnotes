
# GNU Hackers Meeting 2009: The Nix Package Manager

## Speaker

Eelco Dolstra, postdoc at the Software Engineering Research Group in the Department of Software Technology, Delft University of Technology.

[Personal Website](http://nixos.org/~eelco/index.html)

## Source

[GNU Hackers Meeting 2009: The Nix Package Manager](http://audio-video.gnu.org/video/ghm2009/ghm2009-dolstra-large.ogg)

[Event Page](http://audio-video.gnu.org/video/ghm2009/)

[Slides](http://www.gnu.org/ghm/2009/dolstra-nix-slides.pdf)

## Location

Gothenburg, Sweden

November 11-13, 2009

## Notes

### Problems With Package Managers

- Upgrading a package is dangerous.
    - Library version two isn't backwards compatible.
- Hard to have multiple versions of a package installed at the
same time.
    - Multiple library versions override each other if installed at same time.
- Upgrades are not atomic.
    - Upgrade overwrites files. If interrupted, package state is corrupt.
- No rollbacks.
- Incomplete dependency info.
    - If forget dependency, may still work on your system but not others.
- Only root can install packages.
    - Not nice for normal users.

### Nix: Purely Functional Package Manager

A more comprehensive solution for package management.

- Package specification file uses pure functions, like Haskell.
- Package contents are stored similar to named entities in Haskell-like language: Values, not variables.
- Once a package is built, never modify it or overwrite it with new version. New version is placed in different directory so they don't conflict.

### Nix Store

- Packages are stored in `/nix/store` directory.
- Every package has own directory, name is combination of package hash and name. (e.g. `/nix/store/rpdqxnilb0cg...-firefox-3.5.4`)
- Hash of *all* inputs used to build package: sources, libraries, compilers, build scripts, etc.
- Package library is part of input. This means that if it changes, the package is considered different, which will produce a different package on the file system.

### Nix Expressions

Dealing with hash codes is not human-friendly, so it's mostly done automatically.

The following package defines how to build the OpenSSH package. This is actually a function, with `{ stdend, fetchurl... };` as its arguments.

        /* openssh.nix */
            # Package parameters.
        { stdenv, fetchurl, openssl, zlib }:
            # This function makes a thing in the Nix store.
            # Three parameters to make a package:
            # 1) Package name, 2) Package contents, 3) How to build
        stdenv.mkDerivation {
          name = "openssh-4.6p1"
          src = fetchurl {
            url = http://.../openssh-4.6p1.tar.gz;
            sha256 = "0fpjlr3bfind0y94bk442x2p...";
          };
          buildCommand = ''
            tar xjf $src
                # Note:
                  # ${openssl} will be replaced with path to openssl package in Nix store.
                  # $out will be replaced with intended location in Nix store.
            ./configure --prefix=$out --with-openssl=${openssl}
            make; make install
          '';
        }

The OS environment is initially empty except for what this function adds to it. The `mkDerivation` function is an easy-to-use high-level function, but there is a low-level alternative. The `stdenv` argument contains an empty Unix-like environment which has essential tools, such as `make` and `gcc`. To specify a different `gcc`, pass a different `stdenv` argument to nix which contains a different `gcc`.

The previous thing is a function, so we need to call it with appropriate arguments. Another nix expression composes these arguments.

        /* all-packages.nix */
        openssh = import ../tools/networking/openssh {
               # "inherit" means copy from surrounding scope,
               #   which is the value definition below.
               # This says what openssh needs: openssl.
          inherit fetchurl stdenv openssl zlib;
        };
            # This is a concrete instance of openssl.
        openssl = import ../development/libraries/openssl {
              # openssl also has dependencies.
          inherit fetchurl stdenv perl;
        };
        stdenv = ...;
        openssl = ...;
        zlib = ...;
        perl = ...;

Nix will use this `all-packages.nix` file to recursively build a dependency graph, then actually build them. Evaluating the `openssh` variable will produce an OpenSSH package in the Nix store, which looks like this.

        /nix/store
        - l9w6773m1msy...-openssh-4.6p1
            - bin
                - ssh
            - sbin
                - sshd
        - ...

### Nix User Operations

            # Build and install OpenSSH.
        $ nix-env -f all-packages.nix -i openssh
            # Upgrade to new version of OpenSSH.
        $ nix-env -f all-packages.nix -u openssh
            # Rollback to previous version if problems.
        $ nix-env --rollback
            # Delete unused packages.
        $ nix-collect-garbage

### User Environments

This explains how a user doesn't need to think about the package hashes.

1) `PATH` points to one user profile. Each represents a state of system packages.
2) A user profile points to a directory in the Nix store which defines a user environment.
3) A user environment defines a `bin` directory which has packages. These packages are pointers to specific versions in the Nix store.

When a user installs a new package, Nix will create a new user-profile, which creates a new user environment, which defines a new collection of packages. This new collection includes the newly installed package.

### How Does Nix Use /user or /userprofile?

TODO: 0:30 min mark


### Deployment Using Nix

Source deployment model is horrible for users because they must wait a long time for packages to compile. But, source deployment is as useful as binary deployment.

Nix is a source deployment model, but it also has binary deployment for pre-built components.

How to do binary deployment? For Nix's pacakge collection, they pre-build them and host them on an HTTP server.

        nix-push $(nix-instantiate all-packages.nix) \
          http://server/cache

A client can then choose to use these pre-built packages by using the `nix-pull` command to define a cache of pre-built components. When the client uses nix to install a package, nix will first compute its hash code, then check the pre-built components for a match. If, and only if, the pre-built component is exactly the same, the client's build process will use it instead of building the package from source. The commands look like this:

        $ nix-pull http://server/cache
        $ nix-env -f all-packages.nix -i openssh

### Finding Run-time Dependencies

How to find run-time libraries? They are usually hard-coded in binaries. This is also how the Nix packaging model finds run-time dependencies.

The Nix packaging model only cares about build-time dependencies. If a package definition doesn't properly define a build-time dependency, they just won't be found by the compiler or linker, and the build will fail.

Run-time dependencies are usually hard-coded into package binaries. They are found automatically by the system by scanning for hash codes in all files in binaries.

This sounds like a terrible idea. However, this is the same technique used by conservative garbage collectors, which scan working memory for things which look like a pointer and assume it *is* a pointer.

Nix uses a similar technique to add dependencies, for run-time dependencies to a package. Nix scans for hash codes in all files in a package. If it finds a match, Nix will assume this package is still used, so it won't garbage-collect the package. For example, if Nix scans the OpenSSH package and finds a reference to a hash, which matches the OpenSSL package hash, Nix will not garbage-collect that OpenSSL package.

The list produced by this technique is used for Nix garbage collection, package installation, and deployment.

### Nix Packages Collection

Nix has created a thing called the Nix package collection. It is a set of Nix expressions for existing packages, which now lists more than 7000 packages.

### NixOS

To test the completeness of this model, a Linux distribution was created with Nix as the sole package manager, which is named NixOS. After completing that, the creators decided to build not only packages, but *all* parameters of a system configuration. So, they used Nix expressions to generate all static parts of a system, including configuration files, boot scripts, startup scripts, initial ramdisk, etc. This enables using Nix's upgrade and rollback mechanism for upgrading and maintaining *all* parts of a system.

What are the consequences of this? How does NixOS manage a system's static configuration? In NixOS, there is very few conventional Linux directories, such as `/lib`, `/bin`, and `/usr`. These directories are instead places under `/nix/store`, which is versioned. Also, upgrades are non-destructive, which allows rollback. The system is stateless, which is equivalent to reinstalling the OS from scratch. Also, the system creation is deterministic, which means the entire OS configuration can be recreated on another machine.

### Example Nix Expression for System Config File

        /* ssh_config.nix */
        { config, pkgs }
        pkgs.writeText "ssh_config" ''
          SendEnv LANG LC_ALL ...
          ${if config.services.sshd.forwardX11 then ''
            ForwardX11 yes
            XAuthLocation ${pkgs.xorg.xauth}/bin/xauth
          '' else ''
            ForwardX11 no
          ''}

Note: in the above expression, if the `config.services.sshd.forwardX11` is false, then the `pkgs.xorg.xauth` package will not be listed as a dependency of the `ssh_config` file. This shows Nix's property of laziness, which is also a property of Haskell.

What is the result of executing this Nix expression?

        /nix/store
        - 33lcnh62yll3...-ssh_config
        - kyv6n69a40q6...-xauth-1.0.2
            - bin
                - xauth

This `kyv6n69a40q6...-xauth` package won't be garbage-collected when the `ssh_config` file is referencing it. Other package managers, such as `rpm`, don't allow dependencies like this, which are between files or config files.

### NixOS System Configuration File

We don't want end-users hacking random Nix expressions, because maybe they don't understand how to do this. So, there is one file that defines all the "options" for the system. Modify this one file to modify system functionality.

        /* /etc/nixos/configuration.nix */
        {
          boot.loader.grub.bootDevice = "/dev/sda"
          fileSystems = singleton
            { mountPoint = "/";
              device = "/dev/sda1";
            }
          swapDevices = [ { device = "/dev/sdb1"; } ]
          services.ssh.enable = true;
          services.sshd.forwardX11 = true;
        }

So, from an end-user perspective:

- Edit `configuration.nix`
- Run `nixos-rebuild`

This builds `system.nix` and runs its activation script. The activation script manages the imperative part of system setup, such as starting services. This operation, like any Nix operation, operates under the Nix store. As a result, system configuration using Nix is non-destructive.

Each time a NixOS user modifies the `configuration.nix` script and updates the system, a new entry is added to the GRUB loader. If the new configuration has problems, it is easy to boot into the last version of the system. When a user selects a GRUB entry, the activation script is run, which makes sure the kernel loads the right modules and loads system files.

Another great feature is that Nix can build a VM for a system configuration file, which is great for testing.

### Hydra: Nix Continuous Build System

Uses Nix to describe how to perform the build, which is quite comprehensive. Checks out projects from repos and builds them. Produces releases for various Linux distributions. This makes system-level builds consistent and reproducible.

As an example, NixOS project has a project called "patchelf" which is built using Hydra. Nix expressions perform many builds on many platforms and OS's, and hosts the resulting packages or ISO images for each build target for download.

### Conclusion

- Nix: Safe package management, atomic upgrades, rollbacks, multi-user, portable
- NixOS: Safe upgrades, atomic upgrades, rollbacks, reproducibility
- Hydra: Builds dependencies of a continuous build job automatically

More information: http://nixos.org

### Q&A

Q: If multiple versions of a package, are there multiple Nix expressions for each?

A: Yes, that's one way. However, Nix is a functional language, so you can use abstractions. If most build steps between versions are the same, they can be placed into a function. Then, call this function from each version-specific Nix expression.

Something like "Bison". Building a package with different versions of Bison, for instance, is quite easy to express. Or do something like build "all Nix packages" with "most recent version of Bison" to test for feature regression. This strategy is currently used for "Guile", which builds Nix packages trunk with a different version of Guile. If any package which depends on Guile breaks, this is useful information for Guile maintainers.

Q: So, we get to see which applications are breaking the new unstable version? So in terms of compatibility and such? And around user permissions?

A: Yes, if you want to set up Nix on a non-NixOS system, you can set it up for single-user, which sets up multi-user builds.

Q: Does Nix support Solaris?

A: Yes, actually someone recently fixed OpenSolaris support. Right now, we have Linux, Cygwin, FreeBSD, OpenBSD, perhaps we have a Solaris machine. Nix, the package manager, should be very portable. If you want to build the Nix Package Collection for a new platform, this would be more work.

Q: How do you handle dependency conflict, such that you can't build something anymore? If you depend on libraries and they have dependencies... For example, LDAP and UTFS and gcrypt. Both LDAP and UTFS require gcrypt, but LDAP requires an older version than UTFS. This will produce an error?

A: If there is a conflicting dependency in one package, so it probably would never work. At build time, you could end up linking against two different versions... then at compile time...

Q: The question is, does the Nix package manager detect that situation, that you are linking against two different versions of the same package.

A: No, no. Nix doesn't know what you *do* with packages and their builders. It is completely agnostic about what builder do. If you want to pass two different versions of lib-gcrypt to a package, that's fine. Perhaps there is an application to this. It is up to the linker to complain about it or do something sensible when the package is built.

Q: Does Nix allow dependency cycles?

A: No, you can't have cycles. It will very quickly give you an infinite recursion error.

However, there is the conceptual cycle of building gcc, which requires gcc to build. The solution to this is to have a statically-linked binary of gcc at the bottom of the dependency graph. If you want to compile a compiler, you need the binaries of the compiler. There are two things to break a cycle like this, a) Depend on something outside the Nix store (i.e. `usr/bin/gcc`), or b) provide a binary. The latter is reproducible, while the former isn't.

Q: On NixOS, how would a user modify a configuration file, such as for X11?

A: Open the configuration file, scroll through the array of configuration options. Can find a setting, such as Apache, and change it to "false", for example.

Q: But this is in the Nix language, correct? This is not the actual contents of the Apache configuration file.

A: Correct. Change the OS-level Nix expression to change any system configuration. It manages each app's configuration file. No config files are written or managed by hand, they are all generated. When I run `nix-os-rebuild`, these config files are all regenerated.

Q: But, if I install a package, it might want to go into the X directory and add or modify a file. If I get gb, this program has some configuration files, including X11 resource definitions. The package takes this file and puts it in `usr/blah`. This means that in the gb package in Nix Packages, there is a Nix expression which generates the X11 resource definitions which are made by gb? This may be something the user wants to customize system-wide.

A: No, the gb package should just install these things in its own prefix. A Nix expression could combine these resources in some way, but this may be a bit problematic. Anything that exists in the `home` directory are not touched by Nix at all. User configuration files are just under your `home` directory, not managed by Nix.

Q: How do you handle security upgrades? You must rebuild the entire OS?

A: Uh yeah, so security upgrades... <video end>



