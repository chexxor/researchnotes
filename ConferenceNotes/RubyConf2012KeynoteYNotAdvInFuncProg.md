
## Ruby Conf 2012 Keynote - Y-Not Adventures in Functional Programming

Source: [Youtube](https://www.youtube.com/watch?v=FITJMJjASUs)

## Intro

### About this talk
- Highly technical
- Extremely pointless
- Very bad
- Only entertaining

### Audience Participation

Calculate the cosine of 0, many, many times.

### Talk Name: Effectively Computable

Universal Turing Machine
- Tape
- Read/write head
- Move left and right
- Act on command in box

Turing Thesis

        Anything that is "effectively computable" can be computed by a Universal Turing Machine

(Can write anything in an algorithm for a Turing machine, and the machine can calculate it for you)

Alonzo Church
- Designed Lambda Calculus

yx.x
- punctuation
- argument (variable)
- punctuation
- body (y expression)

### Lambda Calculus

No Strings, Integers, or types at all
Can still express any programming concept

Sample Lambda expressions
- Zero = yf.yx.x
- One = yf.yx.f x
- Two = yf.yx.f (f x)
- True = yx.yy.x
- False = yx.yy.y

Lambda calculus rules
- Functions are the only data type
- Lambda binding is the only way to associate values to variables (function argument)
- Calculation happens via Beta reduction (or Alpha, but less important right now)

### Beta Reduction

1) take function
2) find first argument (from left)
3) expand body of function out
4) substitute anywhere in body with argument

Let's Calculate True with two arguments, 1 and 0

        (True  One  Zero)
        ((yx.yy.x)(yf.yx.f x)(yf.yx.x))
        -> ((yy.(yf.yx.f x))...)
b/c... `(yx.yy.x)` is first body and `(yf.yx.f x)` is first argument. So, remove first `yx`, replace all `x` in body with `(yf.yx.f x)`.

        (One  Zero)
        (yy.(yf.yx.f x))(yf.yx.x)
        -> (yf.yx.f x)
b/c... `(yf.yx.f x)` has no `y`, so substitute nothing and just drop the argument.

        (One)
        (yf.yx.f x)
        -> 1
b/c... We showed that `yf.yx.f x` equals 1 earlier.

This makes sense, true should return first argument if true.
This is just simple macros.

### Church Thesis

        Anything that is "effectively computable" can be computed via lambda calculus. (1933, 1935)

### Lambda Calculus in Programming Today

Languages with Lambda
- Lisp, Clojure, Ruby, Coffeescript, JavaScript

### Fixpoints

A value passed to a function that produces the same value.
- 0.739... = cos (0.739...)
- 0 = sin(0)
- 1 = sqrt(1)
- 0 = sqrt(0)
- identity function has infinite number of fixpoints

Will return to fixpoints later.

### Core Question with Lambda Calculus

Factorial function using lambda calculus

        ->(n) { n.zero? ? 1 : n * fact(n-1) }.(5)

Problem: In lambda calculus, every function is anonymous, so how to do recursion like this?

Will return to this problem later.

### Higher order functions

Using functions like values; using function as argument or return value.

        make_adder = ->(x) {
          ->(n) { n + x }
        }
        add1 = make_adder.(1)
        mul3 = ->(n) { n * 3 }
        compose = ->(f, g) {
            ->(n) { f.(g.(n)) }
        }

### Functional Refactorings

#### Tennent Correspondence Principle

Wrapping an element in a lambda which has no arguments should not change the element.
- That is, this should do nothing to x: `->() { x }.()`

        mul3 = ->(n) { n * 3 }
        # these are the same
        mul3 = ->(n) { ->() { n * 3 }.() }

#### Introduce a binding

Introducing an argument which is not used in the lambda function should not change the result.

        mul3 = ->(n) { ->() { n * 3 }.() }
        # these are the same
        mul3 = ->(n) { ->(z) { n * 3 }.(1234) }

#### Wrap Function

If we have an expression that is inside a function of one argument, we can pass the argument down to the call site. (?)

        make_adder = ->(x) {
          ->(n) { n + x }
        }
        # these are the same
        make_adder = ->(x) {
          ->(v) { ->(n) { n + x }.(v) }
        }

#### Inline Definition

Replace the name of a function with the definition.

This removes all function names from a program


### Return to Core Question

How can we refactor this to enable recursion?

        ->(n) { n.zero? ? 1 : n * fact(n-1) }.(5)

`fact` should be a function that calculates the next smaller value...
Can we pass this function definition as an argument?

        make_fact = ->(fact) {
          ->(n) { n.zero? ? 1 : n * fact.(n-1) }.(5)
        }
        fact = make_fact.( <fact def here> )

But our problem is still the same. We still need a function name so it can refer to itself. This seems to be the wrong path.

### Bottom-up Approach?

Instead of "make_fact", which counts down, can we make a function with counts up? We can call it a "fact_improver"

        error = ->(n) { throw "Should never be called" }
        fact_improver = ->(partial) {
          ->(n) { n.zero? ? 1 : n * partial .(n-1) }
        }
        f0 = fact_improver.(error) => 1
        f1 = fact_improver(f0) => 1
        f2 = fact_improver(f1) => 2
        f3 = fact_improver(f1) => 6

But with this method, we have so many functions. We can do use function inlining to refactor this, but still need to copy-paste the code logic many times to nest it deeper. This is not a practical solution.

Let's refactor the name and see what happens:

        fx = ->(improver) { // Body
          improver.(improver.(error)) // if this, fx(1) works
          improver.(improver) // if this, fx(0) works
        }.(
            ->(partial) { // Argument
              ->(n) { n.zero? ? 1 : n * partial.(n-1) }
            }
          )
        fx.(2)

If Body is 'a', fx(1) works. This is the refactored function after we inlined the f0, f1, f2, etc functions. So, this should still work for 1 if we only inline the function once. (I believe I interpreted correctly) We do not need the 'error' thing, so remove it and try.

If Body is 'b', fx(0) works. However, fx(1) produces a strange error: "Proc can't be coerced to a Fixnum". This error is thrown here `n * partial.(n-1)`. The argument of the `partial` function should be a function, but we gave it number, `2`.

### Wrap in Function to Delay Execution

We can fix this. Change the name of 'partial' to 'improver', because it sounds more accurate. Now, to solve the argument problem, the `partial` function should receive a function. The only function we have available is the 'improver' function, so use that.

        fx = ->(improver) { // Body
          improver.(improver) // if this, fx(0) works
        }.(
            ->(improver) { // Argument
              ->(n) { n.zero? ? 1 : n * improver.improver.(n-1) }
            }
          )
        fx.(2)

This function now correctly calculates the factorial, recursively. We do not need to have the function name 'fx', so we can remove it to make this a pure lambda calulus function.

This is one example of how we can use lambda calculus to calculate arbitrary mathematical expressions. If recursion is involved, we can use this little trick to pass itself to itself through arguments.

### Organize the Lambda Calculus Factorial

The function we created works, but it is hard to read and understand. Can we improve it?

First, 'improver' is semantically difficult to understand. In essence, we pass a function to it, and it generates a recursive version. So, let's change the name to 'gen'.

Also, we want to extract the core logic of this expression, which is `n * gen.gen(n-1)`. To do this, we can wrap it in a function and put this partial piece of the algorithm into the argument.

        ->(gen) { // Body
          gen.(gen)
        }.(
            ->(gen) { // Argument
              ->(partial) {
                ->(n) { n.zero? ? 1 : n * partial.(n-1) }
              }.(->(v) { gen.(gen).(v) })
            }
          ).(2)

Let's move this partial piece of the algorithm further out by using another argument from a wrapper function.

        ->(partial) { // Recursive solution
        　　->(gen) {
          　　gen.(gen)
        　　}.(
           　->(gen) {
            　 partial.(->(v) { gen.(gen).(v) })
           　}
          　　)
        }.( // Factorial algorithm
          ->(partial) {
            ->(n) { n.zero? ? 1 : n * partial.(n-1) }
          }
          ).(2)

### Name the Pieces of the Recursive Lambda Algorithm

Now, let's pull this apart and give names to these.

        fact_improver = ->(partial) { // Factorial algorithm
          ->(n) { n.zero? ? 1 : n * partial.(n-1) }
        }
        y = ->(partial) {  // Recursive solution
          ->(gen) {
          　　gen.(gen)
        　　}.(
           　->(gen) {
            　 partial.(->(v) { gen.(gen).(v) })
           　}
            )
        }
        fact = y.(fact_improver) // Add recursion to Factorial
        fact.(2)

As a sidenote, `fact` is a fixpoint of `fact_improver`, so these two are equivalent

        fact = y.(fact_improver)
        fact2 = fact_improver.(fact)

Another sidenote, `y` calculates the fixpoint of an improver function. We can write any recursive function, and `y` will calculate the fixpoint of that recursive function (if it is written in an improver style).

### The Y-Combinator

`y` is the famous Y-Combinator.

This version is the Applicative Order Y-Combinator, which is necessary for the Ruby implementation because Ruby is an applicative language. That is, Ruby evaluates a function's arguments before calling the function.

Other names for this is [Z-Combinator]() or the [Fixpoint Combinator](https://en.wikipedia.org/wiki/Fixed-point_combinator).

Related reading:
- [Typed Lambda Calculus](https://en.wikipedia.org/wiki/Typed_lambda_calculus)
- [Simply Typed Lambda Calculus](https://en.wikipedia.org/wiki/Simply_typed_lambda_calculus)
- [(Untyped) Lambda Calculus](https://en.wikipedia.org/wiki/Untyped_lambda_calculus)
- [Recursive (Data) Type](https://en.wikipedia.org/wiki/Recursive_type)


### Clean up the Recursive Function

If you look up this Y-Combinator in Wikipedia, you will probably see this version, which is just a slight refactoring.

        y = ->(f) {
          ->(x) { x.(x) }.(
            ->(x) { f.(->(v) { x.(x).(v) }) }.(
          )
        }

or adding a wrapper function:

        y = ->(f) {
          ->(x) { f.(->(v) { x.(x).(v) }) }.(
            ->(x) { f.(->(v) { x.(x).(v) }) }.(
          )
        }

from this cleaned-up version of the `y` function above:

        y = ->(partial) {
          ->(gen) { gen.(gen) }.(
           　->(gen) { partial.(->(v) { gen.(gen).(v) })　}
          )
        }

or adding a wrapper function:

        y = ->(partial) {
          ->(gen) { partial.(->(v) { gen.(gen) }) }.(
           　->(gen) { partial.(->(v) { gen.(gen).(v) })　}
          )
        }





