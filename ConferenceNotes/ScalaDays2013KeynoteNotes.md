
# Scala Days 2013 Keynote - Scala with Style

## Speaker

Martin Odersky, PhD and creator of Scala

## Source

[http://www.parleys.com/play/51c1994ae4b0d38b54f4621b/](ScalaDays 2013 Keynote - Scala with Style)

## Notes

(Starts from 21:30)

At the point of two paradigms, OOP and Functional, and Scala is right in the middle. Because these are orthogonal ideas, a programming language that follows both of these will have many problems around programming style.

### Why did OOP become popular?

To gain insight at the current transition from OOP to functional, we can look at what happened when OOP became popular. The first popular OOP language was Simula 67, and the second was Smalltalk.

Encapsulation? No
Code Re-use? No
Dynamic Binding? Indirectly
Dependency Inversion? Came much later
Open-close principle? No way

Not OOP principles. It's what you can *do* with OOP.

Traditional Linked List (C style)
- Cases: Two (Empty, Non-Empty)
- Unbounded: Operations (map, reverse, print, insert)

Simulation (Simula 67 style)
- Operations: Fixed (nextStep, toString, aggregate)
- Unbounded: Implementations (Car, Road, Molecule)

GUI Widgets (Smalltalk)
- Operations: Fixed (redraw, boundingRectangle, move)
- Unbounded: Implementations (Window, Menu, Letter, Shape, Curve)

Simulation and GUI Widgets are similar. What do they have in common? Both need to execute a *fixed API* with an *unknown* implementation. This is possible in C, but cumbersome.

## How is this related to Functional Programming?

FP has some methodological advantages
- Fewer errors
- Better modularity
- Higher-level abstractions
- Shorter code
- Increased developer productivity

*Me: show facts, not opinions, please*

## Catalyst for mainstream adoption

These are not enough for mainstream adoption. Need a catalyst that shows everyone these advantages. Current trend is **multicore** and **cloud computing**. 

Challenges
- Parallel 
- Async
- Distributed

Mutable state is liability in all three (cache coherence, races, versioning)

## Essence of Functional Programming

Concentrate on transformations of immutable values,
not stepwise modifications of mutable state.

## What about objects?

What goes where?
- Object-oriented decomposition in design is good
- OOP is good for organizing things
- Need namespaces
- Whether you call it OOP or a module system doesn't matter

New role for objects
- Old: "Objects are characterized by state, identity, and behavior"
- New:
    - Eliminate or reduce mutable state
    - Structural equality instead of reference identity
    - Concentrate on functionality (behavior)

## Bridging OOP and FP

To bridge the gap, must be inclusive and allow many styles
- Orthoganal
- Expressive
- Un-opinionated

Scala is not a better Java
Scala is not Haskell on JVM
Scala will create a unique programming style

## 6 Guidelines for Style in Scala

1) Keep it simple. Ease is familiarity, simple is not complex.

2) Don't pack too much in one expression
- If an intermediate value is important, give it a name

3) Prefer functional
- Use vals, not vars
- Use recursions or combinators, not loops
- Use immutable collections
- Concentrate on transformations, not CRUD
- But, sometimes mutable gives better performance
- But, sometimes (not often) mutable adds convenience

4) Don't diabolize local state
- Local state is less harmful than global state
- If deciding between mutable and immutable, choose the one that uses clearest names

Another situation: You have a shopping card with items, and you want to find the total price and discount of all items. This is how we would do it.

        var totalPrice = items.map(_.price).sum
        var totalDiscount = items.map(_.discount).sum

However, suppose we say that we don't want to traverse `items` twice, maybe because it is a really big list. How can we do this? 

The canonical functional method might be to use a fold:

        val (totalPrice, totalDiscount) =
          items.foldLeft((0.0, 0.0)) {
          case ((tprice, tdiscount), item) =>
            (tprice + item.price,
            tdiscount + item.discount)
        }

An alternative solution, using imperative style:

        var totalPrice, totalDiscount = 0.0
        for (item <- items) {
            totalPrice += item.price
            totalDiscount += item.discount
        }

The imperative style is fine here because information flow is much more clear.

5) Careful with Mutable Objects

Mutable objects tend to encapsulate global state. "Encapsulate" sounds good, but it doesn't make the global state go away. This could create complex entanglements.

When is an object mutable? When it contains vars?

        class Memo[T, U](fn: T => U) {
            val memo = new mutable.WeakHashMap[T, U]
            def apply(x: T) = memo.getOrElseUpdate(x, fn(x))
        }

This doesn't contain 'var', but it is still mutable because it is using a simple array, which is mutable.

An object is mutable if its (functional) behavior depends on its history. That is, it depends how we've used this object before.

In the example above, if we pass a pure function, then the object is immutable. 

        new Memo { i: Int => i + 1} // immutable

However, if we pass a mutable entity, then it is mutable. That is, now this object depends on how we use ctr.

        var ctr = 0
        new Memo { i: Int => ctr += i; ctr } // mutable

6) Don't stop improving too early

The cleanest and most elegant solutions do not always come to mind immediately.

## 6 Choices for Style in Scala

Here are proposed opinions on controversial style questions.

1) Infix vs "."

`items + 10`  or  `items.+(10)`
`xs map f`    or  `xs.map(f)`

- If the method name is symbolic, always use infix.
- For alphanumeric method names, one can use infix if there is only one operator in the expression. But for chained operators, use "." - `mapping.add(filter).map(second).flatMap(third)`

2) Alphabetic vs Symbolic

Scala unifies operators and methods. How do you choose which to use?

`xs map f`             or `xs *|> f`
`vector + mx`          or `vector add mx`
`(xs.foldLeft(z))(op)` or `(z /: xs)(op)`
`UNDEFINED`              or `???`

Use symbolic only if:
- Meaning is understood by your target audience
- Operator is standard in application domain
- You would like to draw attention to the operator (it visually sticks out in source code)

3) Loop, Recursion, or Combinators

The following functionality is the same, but we can use:

a loop

        var i = 0
        while (i < limit && !qualifies(i)) i += 1
        i

or recursion

        def recur(i: Int): Int =
          if (i >= limit || qualifies(i)) i else recur(i + 1)
        recur(0)

or combinators

        (0 until length).find(qualifies).getOrElse(length)

How do we choose?

Another example:

Loop:

        var buf = new ListBuffer[Int]
        var ys = xs
        while (ys.nonEmpty) {
            buf += ys(0) * ys(1)
            ys = ys.tail.tail
        }

This loop is clearly not optimal. Not easy to read, not maintainable, and not safe.

Combinators

        xs.grouped(2).toList.map {
            case List(x, y) => x * y
        }

That is great if you know combinators, and everybody else does as well.

Recursion

        def recur(xs: List[Int]): List[Int] = xs match {
            case x1 :: x2 :: xs1 => (x1 * x2) :: recur(xs1)
            case Nil             => Nil
        }
        recur(xs)

This is easy to grasp for newcomers, and it's recursion so it tends to be more efficient.

Why does Scala have all three?

- Combinators: Easiest to use, are done in a library
- Recursive Functions: Bedrock of FP
- Pattern Matching: Clearer and safer than tests and selections
- Loops: Familiar, sometimes the simplest solution

Recommendation
- Use combinators first
- If must, use tail-recursive functions
- Loops should be used last. Only in simple cases or when computation inherently modifies state

4) Procedures or "="

A 'procedure' in Scala doesn't return a value. That is, a procedure is distinct from a function in that it is created to perform side effects, rather than calculate and return a value.

Here, we have a procedure, marked with no return type.

        def swap[T](arr: Array[T], i: Int, j: Int) {
            val t = arr(i); arr(i) = arr(j); arr(j) = t
        }

or, we can explicitly show this function's return type is nothing (Unit type).

        def swap[T](arr: Array[T], i: Int, j: Int): Unit = {
            val t = arr(i); arr(i) = arr(j); arr(j) = t
        }        

Recommendation: Don't use procedure syntax. That is, don't use `def swap[T](...): Unit = {}`, rather use `def swap[T](...) {}`.

Why have "="?
- A historical accident.
- Concerned about how to explain `Unit` to Java programmers.
- Mistake: Trades simplicity for familiarity
- Also, opens up a bad trap:

        def swap(arr: Array[T], i: Int, j: Int): Unit {
            val t = arr(i); arr(i) = arr(j); arr(j) = t
        }

It's missing an '=' after the `: Unit` return type declaration. The parser thinks you are writing a "refinement type", but it's missing an '='. In the past, would give error, "This is not a legal beginning of a type.", which is scary for new programmers. Now, however, the compiler is must more helpful, so it's better to use the short-hand syntax.

5) Private vs Nested

        private def isJava(sym: Symbol): Boolean =
          sym hasFlag JAVA

        def outer() = {
          if (symbols exists isJava) ...
          ...
        }

Definitely nest if you can avoid passing parameters

        def outer(owner: Symbol) = {
          def isJava(sym: Symbol): Boolean =
            (sym hasFlag JAVA) || owner.isJava

          if (symbols exists isJava) ...
          ...
        }

Recommendation
- Prefer nesting if you can save on parameters.
- Prefer nesting for small functions, even if nothing is captured.
- Don't nest too many levels deep.

6) Pattern Matching vs Dynamic Dispatch

        class Shape
        case class Circle(center: Point, radius: Double)
          extends Shape
        case class Rectangle(ll: Point, ur: Point)
          extends Shape
        case class Point(x: Double, y: Double)
          extends Shape

Could write a single method `area` and match every possible shape.

        def area(s: Shape): Double = s match {
          case Circle(_, r) =>
          math.pi * r * r
          case Rectangle(ll, ur) =>
          (ur.x - ll.x) * (ur.y - ll.y)
          case Point(_, _) =>
          0
        }

Or, could add an area method to each Shape class.

        class Shape {
          def area: Double
        }
        case class Circle(center: Point, radius: Double)
          extends Shape {
          def area = pi * radius * radius
        }
        case class Rectangle(ll: Point, ur: Point)
          extends Shape {
          def area = (ur.x - ll.x) * (ur.y - ll.y)
        }

Which do you choose?

Why does Scala have both?
- Pattern Matching: functional, convenient
- Dynamic Dispatch: core mechanism for extensible systems

Recommendation
- It depends whether your system should be extensible or not
- If you foresee extensions with new data alternatives, choose dynamic dispatch
- If you foresee adding new methods later, choose pattern matching
- If the system is complex and closed, also choose pattern matching



