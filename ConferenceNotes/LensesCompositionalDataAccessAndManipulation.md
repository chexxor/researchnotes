
# Haskell eXchange 2013: Simon Peyton Jones on Lenses: Compositional Data Access and Manipulation

## Speaker

Simon Peyton Jones, MA, MBCS, CEng and key contributor of Haskell

## Source

[Haskell eXchange 2013: Simon Peyton Jones on Lenses: compositional data access and manipulation](http://skillsmatter.com/podcast/scala/lenses-compositional-data-access-and-manipulation/te-8510)

## Location

London

October 9, 2013

## Notes

### Lens: The Basic Idea

A lens is a first-class value that gives you access into a data structure. Lens allows you to read, write, modify, fold, traverse, etc. a data structure. Lens' type is `Lens' s a`, where `s` is the container's type and `a` is the target's type inside the container.

    -- Example Lens Type
    Lens DateTime Mins
    Lens DateTime Hours

The powerful thing about lens is that you can compose them. This allows access to a container inside a container. This is shown much later in the talk.

    -- Go from type s1 to s2
    composeL :: Lens' s1 s2 -> Lens' s2 a -> Lens' s1 a

### Why Better Than Existing Techniques?

#### Existing Technique: Record Syntax

Consider nested records. The following example shows a `Person` record which has a nested value of `Address` type.

    -- Two types: Person, which contains Address
    data Person = P {
        name :: String
      , addr :: Address
      , salary :: Int}
    data Address = A {
        road :: String
      , city :: String
      , postcode :: String }

How do we update a person's name? We write a function which uses "record update syntax". This isn't too bad.

    setName :: String -> Person -> Person
    setName n p = p { name = n }

How do we update a person's address' postcode? The "record update syntax" becomes a bit more complex. As depth grows, complexity also grows. This is not a scalable solution.

    setPostcode :: String -> Person -> Person
    setPostcode pc p
      = p { addr = addr p { postcode = pc } }

#### Better Technique: Lens

Here's what we want. We want an alternative syntax to record syntax which uses functions. Functions are better because they are scalable composable. Look how to use lenses to do the same thing. Note this is only a step towards lenses, this is not *the* lenses yet.

    -- How to view each field? Make a lens for each.
    lname   :: Lens' Person String
    laddr   :: Lens' Person Address
    lsalary :: Lens' Person Int
    -- How to modify each field? Use its lens.
    view :: Lens' s a -> s -> a
    set  :: Lens' s a -> a -> s -> s
    -- How to modify a nested field? Compose lens.
    setPostcode :: String -> Person -> Person
    setPostcode pc p
      = set (laddr `composeL` lpostcode) pc p

To access a record's fields, we still need to make a function for each one. However, if these functions use a lens instead of record syntax, we can compose them. Why is composition useful in this case? To access a record's deeper fields, we can compose lens functions at each level, as seen in the `setPostcode` function.

### Obvious First Attempt

That was just the basic notion of lenses. There is aspects of those lenses which we can improve. How about we make a lens which has a getter and setter? That is, why not put the `view` and `set` functions inside the `lname` function? To view the field, we select the `view` field from the lens; to set it, we select the `set` field from the lens. See the following pseudocode to understand the idea:

    data LensR s a = L {
        viewR :: s -> a
      , setR  :: a -> s -> s
    }
    composeL (L v1 u1) (L v2 u2)
      = L (\s -> v2 (v1 s))
          (\a s -> u1 (u a (v1 s)) s)

This works, but looking at it now, it's a bit inflexible. What if we want to do things besides getting and setting? How about modifying fields? Isn't modify the same as set? No, setting just requires the new value, but modifying will use a function to define *how to transform* the value into its new value.

    -- How to modify using a lens? Pass a function.
    over :: LensR s a -> (a->a) -> s -> s
    over ln f s = setR l (f (viewR l s)) s

This is a bit inefficient because it depends on a type's structure. We must pattern-match into the structure to modify, then we must use constructors to rebuild the structure.

### Add More Fields to Lens Structure

How to build a `modify` function? Rather than building a view-then-update function, let's add a new field to the lens record. But, what if there are other things we want to do to nested values in a data structure, such as use `Maybe` or use `IO` to print the nested values? Following our previous plan, we would add new functions and fields in our lens structure.

    -- New functions for each new functionality.
    modifyM :: LensR s a -> (a -> Maybe a) -> s -> Maybe s
    modifyIO :: LensR s a -> (a -> IO a) -> s -> IO s
    -- Support these new functions in our lens structure.
    data LensR s a
      = L {
          viewR :: s -> a
        , setR  :: a -> s -> s
        , mod   :: (a->a) -> s -> s
        , modM  :: (a->Maybe a) -> s -> Maybe s
        , modIO :: (a->IO a) -> s -> IO s }

Wait a minute, this doesn't look scalable. Each new functionality requires new functions and fields. This needs to be generalized. How can we do that? The `Maybe` and `IO` types look similar. Actually, we can generalize them as `Functor`s. We can make a `modF` field in Lens which can do both of those things.

    data LensR s a
      = L {
        ...
        , modF  :: Functor f => (a->f) -> s -> f s}

Now, this is much more flexible. How flexible? If we are creative, we can replace *every* field with this function. Yes, the `viewR`, `setR`, and `mod` fields can be implemented using Functors, amazingly!

### Changing Lens to a Functor

How do we modify the `view` and `set` functions to use `modF`? Their type signatures look unrelated to this idea of Functors.

First, let's look at how to write the `set` function. Remember that `s` is the container's type and `a` is the target's type inside the container.

    -- Need a function.
    -- Should set the focal point and return its container.
    set :: Lens' s a -> (a -> s -> s)
    set ln a s = ???

    -- Not sure why we split Lens into this structure.
    type Lens' s a
      = forall f. Functor f
        => (a -> f a) -> s -> f s
    data LensR s a
      = L {
          viewR :: s -> a
        , setR  :: s -> s -> s}

So, after putting our thoughts into pseudocode and looking at types, we can see the problem. Lens is a Functor, which returns type `f a`, but the `set` function must return type `a`. How do we change a type from Functor to just its value? If we choose the right `f`, this becomes a no-brainer. Identity! Here's its type. Note that `Identity` is an instance of `Functor`. Now we can implement `set`.

    set :: forall s a.
           Lens' s a -> (a -> s -> s)
    set ln x s
      = runIdentity (ln set_fld s)
      where
        -- Forget old value, use Identity for type conversion.
        set_fld :: a -> Identity a
        set_fld _ = Identity x
        runIdentity :: Identity s -> s
        runIdentity (Identity x) = x

So, to implement `set` as a Functor, we use `Identity`. How, then, do we implement `over` (means overwrite/modify)? Note that in `set`, we forget the old value. In `over`, we can use that value by applying the modification function to it.

    -- Set, written here in point-free style...
    set :: Lens' s a -> a -> s -> s
    set ln x = runIdentity . ln (Identity . const x)
    -- ...is very similar to over.
    over :: Lens' s a -> (a -> a) -> s -> s
    over ln f = runIdentity . ln (Identity . f)

Next, how do we implement `view`? Let's try to implement it.

    view :: Lens' s a -> (s -> a)
    view ln s = ???
    -- `ln` returns type `f s`, but we must return type `a`.

Here, the problem looks similar to `set`, but instead of returning `s`, we must return `a`. This looks a bit more difficult. Where can we get `a` in that context? The trick is, we must pack the `a` inside the `f`.

    -- Make (Const v), which is an unusual functor.
    newtype Const v a = Const v
    getConst :: Const v a -> v
    getConst (Const x) = x
    -- When fmap is applied to this Const, it ignores f.
    instance Functor (Const v) where
      fmap f (Const x) = Const x

So, let's use this to build our `view` function:

    view :: Lens' s a -> (s -> a)
    view ln s
      = getConst (ln Const s)
    -- Magically, Const evaluates to a
    -- Const == a -> Const a a

### Making Lenses

So far, we've discussed how to use lens, and how lens is implemented. Now, let's look at how to make a lens which we can use.

Consider the `Person` record again. For now, we'll add an underscore to field names, because the lens we make will have the same name.

    data Person = P { _name :: String, _salary :: Int }

Now, let's make a lens for this `Person` container. How do we make a lens? Well, what is a lens? A lens needs a function and a `Person`. Lens is a Functor thing, `String -> f String`. But, the `name` function needs to return a `Person`. How do we convert from `f String` to `f Person`? We use `fmap`, and place the conversion in its first argument.

    -- Expanded function type signature:
    -- name :: (String -> f String) -> Person -> f Person
    name :: Lens' Person String
    name elt_fn (P n s) = fmap (\n' -> P n' s) (elt_fn n)
    -- Type flow:
    -- = fmap (String -> Person) (f String)
    -- = f Person

We can use the general pattern in `fmap`s first argument when writing a lens. The `(\n' -> P n' s)` function is like our target data structure with a hole in it. In this case, we modify the the `_name` field. We can write a very similar function for the `_salary` field.

### Using Lens

After we make the lens above to interact with the `name` field, we can use it in various ways like this:

    > let fred = P { _name = "Fred", _salary = 100 }
    > view name fred
    "Fred"
    > set name "Bill" fred
    P { _name = "Bill", _salary = 100 }

### How on Earth Does This Work?

Here's a play-by-play when evaluating the `view` function with lens. Reference the functions we wrote by looking above.

    > view name (P {_name="Fred", _salary=100})
      -- After inlining `view`...
    = getConst (name Const (P {_name="Fred", _salary=100}))
      -- After inlining `name`...
    = getConst (fmap (\n' -> P n' 100) (Const "Fred"))
    = getConst (Const "Fred")
    = "Fred"

Draw attention to one interesting part. How do we evaluate `fmap <something> (Const x)`? We defined this relationship when we made `(Const x)` an instance of Functor type class. It will drop the `<something>` and only return the `x` as a `Const x`.

### Removing Boilerplate

    name :: Lens' Person String
    name elt_fn (P n s) = fmap (\n' -> P n' s) (elt_fn n)    

This is all boilerplate. This can be generated by using this code:

    import Control.Lens.TH
    data Person = P { _name :: String, _salary :: Int }
    $(makeLenses ''Person)

### Composing Lenses

    $(makeLenses ''Person)
    $(makeLenses ''Address)

    setPostcode :: String -> Person -> Person
    setPostcode pc p = set (addr . postcode) pc p

The `(addr . postcode` bit is lens composition. From a person `p`, we can go to the `postcode` by first going to `addr`.

### Further Lens Applications

#### Special Symbols Used by Lens

    setPostcode pc p = addr.postcode .~ pc $ p
    -- (.~) = set
    -- f $ x = f x

#### Virtual Fields

Any uses beyond just nested records?

A data structure sometimes has things that can be considered one-off fields. In this example, the `Temp` data structure doesn't have a centigrade field, but we can compute it from its `fahrenheit` field.

    data Temp = T { _fahrenheit :: Float }
    $(makeLenses ''Temp)
    --That gave us: fahrenheit :: Lens Temp Float
    centigrade :: Lens Temp Float
    centigrade centi_fn (T faren)
      = (\centi' -> T (cToF centi'))
        <$> (centi_fn (fToC faren))

#### Maintaining Invariants

    data Time = T { _hours :: Int, _mins :: Int }

If we add 4 minutes to the time, we don't want to get 62 minutes. Instead, we want to roll it around 0 to become 2, like this:

    > let now = T { _hours = 3, _mins = 58 }
    > over mins (+ 4) now
    T { _hours = 4, _mins = 2 }

How can we implement this rule in a lens?

    mins :: Lens Time Int
    mins min_fn (T h m)
      = wrap <$> (min_fn m)
      where
        wrap :: Int -> Time
        wrap m' | m' >= 60  = T (h+1) (m'-60)
                | m' < 0    = T (h-1) (m'+60)
                | otherwise = T h m'

#### Non-record Structures

Consider using Lens to interact with a key-value map.

That is, this allows us to say `at "b" (+ 5) <some map>`. We can create a lens named `at`.

    at :: Ord k => k -> Lens' (Map k b) (Maybe v)
    at k mb_fun m
      = wrap <$> (mb_fn mv)
      where
        mv = Map.lookup k m
        wrap :: Maybe v -> Map k v
        wrap (Just v') = Map.insert k v' m
        wrap Nothing   = case mv of
            Nothing -> m
            Just _  -> Map.delete k m

#### Bit Fields

    bitAt :: Int -> Lens' Int Bool

    > view (bitAt 1) 3
    True
    > view (bitAt 1) 2
    True
    > view (bitAt 1) 5
    False

#### Deep Data Structures

Web scraper.

    p ^.. _HTML' . to allNodes
                 . traverse . named "a"
                 . traverse . ix "href"
                 . filtered isLocal
                 . to trimSpaces

### Second Amazing Insight

What if we changed our Functor from Control to Applicative. We get multiple focal points.

    road :: Lens' Address String
    road elt_fn (A r c p) = (\r')

This is really complex, and I don't understand it, so I won't take notes on this. Basically, it doesn't point to one focus, but to multiple focii. If you're interested, it's at time mark "0:52" in the recording.


