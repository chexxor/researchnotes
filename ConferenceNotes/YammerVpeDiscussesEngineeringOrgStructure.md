
## Yammer VPE Discusses Engineering Org Structure

Source: [Why Yammer believes the traditional engineering organizational structure is dead wrong, on YouTube](http://www.youtube.com/watch?v=RsWZNaaic1k)

Speaker: Kris Gale, VP Engineering at Yammer

### Talking Points

- Talking at CTO level.
- Transition from small startup to larger company.
- Being intentional about organizational design, to keep the unique feeling of a small company.

### Yammer Background

- Yammer wanted to build a better product, faster, so they were hiring new engineers. However, they saw small, marginal increases in productivity.
- What about adding engineers is not fast?
- Communication overhead. Blocking them on external things.

#### Early project management
- Small group of people, each person had an area of expertise.
- Teams of specialized skills, siloed skills.
- Still not 10x faster at development. Why not faster?

#### Amdahl's Law
- [Amdahl's Law](http://en.wikipedia.org/wiki/Amdahl's_law) is a math model, says speedup from parallelizing parts of a serial computation is constrained by the amount of the computation that remains serial.
- If you can only parallelize 50% of a project, you can only gain 2x speedup even as you add more and more processors.

#### Software Engineering
- Everyone assumes SE is writing code to a spec.
- An engineer's hard part is actually the everyday decisions about engineering. Nobody designs an engineering organization with this in mind.
- Everyone thinks the responsibility of making engineering decisions lies with engineering management or level-upped engineers. This is a very expensive mistake.

#### Average SE Org
- Front-end team, back-end team, middle-ware team.
- Each team has a manager, each highest-level manager must meet to divide project work.
- When engineers are waiting for this serial part of the process, they will spend their autonomy on personal time instead of on moving forward the business.

#### Blocking Management
- In "Average SE Org", if a implementor wants to make a change, they have to send a flag up the change of managers, then wait for a response to filter back down. This is a blocking process.
- The management is not in context with the engineer, so they can not understand the rationale for the change. The engineer will believe that management "doesn't get it" and get frustrated.

#### Yammer's Trust in Engineering
- If you hire smarter-than-yourself engineers, you can trust them to make the decisions that would have blocked on you.
- As a CTO, your job is to build and nurture the organization. If you are currently small and growing, this change must happen very early.
- As a CTO, you should not be building a product, but be building an organization which builds a product.
- Engineering decisions should be made by individual contributors. If management is making decisions, view this as a red flag.

#### Yammer's Management
- As you want to iterate on SE process to improve it, you should also iterate on management process to improve your organization.

#### How Yammer Builds Features
- All features are intended to move three core metrics: Attract, Retain, and Sell to customers. If you're not moving these core metrics, you're just changing the product, not improving it.
- Core metrics enables you to measure and validate changes by A-B testing. It also gives transparency to engineers to help them make decisions, because they have the right end-goal in mind.
- What does management do? A manager creates an idea and writes a spec. This isn't a rigid spec, just a place for a cross-functional team to start discussing.
- All engineers should understand the decision-making behind the feature. This helps them feel ownership and make later decisions when implementing.

#### 2-10 People, 2-10 Weeks
- Any project bigger than 10 people or 10 weeks has a painful crunch time at the end, which is disproportionate to smaller projects.
- Smaller projects force you to release often and not over-invest in mistakes.
- Gives urgency. Engineers lose sight of goal if they can't see the finish line. With a finish line in sight, you feel the positive motivation of moving closer to it every day.

#### Yammer's Org Chart
- Broken into areas of expertise, not code ownership.
- Organizing around code-bases creates perverse incentives. Code becomes more important to engineers than moving the business forward.
- Engineers are smart and motivated. Great things happen if you align them with your business goals.
- Teams are domain experts. e.g. Rails team, mobile team, front-end team.
- However, work does not happen in these teams. When starting a project, must estimate amount of work by speciality. Wait for the resources to free up, then assign them to the project.

#### The Big Board
- Big, physical whiteboard, called The Big Board, which has a grid and a magnet for each engineer who works on features.
- An engineer can only be assigned to one feature at a time. 100% dedication has been key to Yammer's velocity, because context-switching is expensive. Build this constraint into your organization.
- Transparency around priorities. CEO can walk by and see what is high-priority. Easy to see what is important to the org, and can see if org priority is moving in wrong direction.

#### Bugs and Maintenance
- A cross-functional support team is built in the same way as project teams.
- Engineers rotate onto and off of this team, so it doesn't block feature development.
- Senior engineers are important to have on this team. When solving bugs, it is important to get to the root of the problem, not just do enough to make the bug ticket go away. Senior engineers may know more and can do this, or they may be the original designer of the bug's environment.
- If engineers see the type of bugs and frequency of bugs, it affects the decisions they make in feature development later.

#### Project Development
- Each team has a single engineer named as tech lead for a project. Management should be in this position during project development.
- Smart people make better decisions with more context. If an engineer only writes code, their code will suffer because their mind/viewpoint is constrained. Therefore, any engineer on the team can be chosen as tech lead.
- Each team can choose any technology, framework, language, etc. because allowing autonomy is important.
- Teams work closely with the product manager, who becomes a member of that team. This person cuts scope from the spec when it's not critical to the company's core metrics.
- Allowing an engineer to say "Maybe we don't need this" and discuss with the project owner is very important. Many organizations probably build lots of things they don't need because there was no room for conversation.

#### Summary
- Autonomous, cross-functional teams, 2-10 people, 2-10 weeks. Each has an assigned tech lead and a dedicated product manager working closely together. Never more than one project per person.
- Engineering exists to iteratively improve the product.
- Engineering grows in order to do this faster.
- Can't have focus on velocity without decentralizing decision-making.
- If you can't trust your engineers to make important decisions, you should not have them on your team.
- Smart people do smart things if you empower them.
- Grow your company, not doing work or managing work, but by managing the environment in which the work is done.
- Constancy of purpose. Evaluate all decisions based on some higher goal. Each business may have a different goal.

