
## Actor Model versus Turing's Model

Author: [Carl Hewitt](carlhewitt.info)
Source: http://what-is-computation.carlhewitt.info

## Paper Notes

### Importance of Concurrency

Concurrency is important partly because of the rise of the Internet and multi-core computer architectures.

However, concurrent design enables a program to do things that it can not in a basic Turing Machine.

### How Actor Model Relates to Turing's Model

Turing's Model is a special case of the Actor Model.

A Turing Machine has a well-defined state, both in the machine, and on the memory tape.

The Actor Model also has this, but only within a single Actor. 

However, the Actor Model consists of many independent Actors. Within a system of Actors, computation is asynchronous and timing can not be determined, therefore the computation is not in any well-defined state.

### Bounded vs Unbounded Non-determinism


        A non-deterministic Turing Machine has bounded non-determinism.

To explain this, let's first explain the concepts of the Actor Model. Then, we can see that a core component of the Actor Model, the Arbiter, is similar to a non-deterministic Turing Machine, which has been previously analyzed.

To reimagine computation in a world of many-core and distributed computing architectures, the Actor Model forgets the Turing model of computation, which consists of processes composed of sequential steps to modify the program's state. The global state of a Turing computer is always well-defined, that is, we can find the path used to reach that state. To implement proper concurrency in a Turing machine, which uses sequential processes, algorithms must be used by the programmer to orchestrate the processes. Orchestrating processes is a very complex field of study, and the correct implementation of a concurrent program written in the Turing model is a long detour from common practices. However, if program development is managed properly, it is possible to apply and enforce using concurrent tools and patterns. Such patterns are detailed in Tony Hoare's Communicating Sequential Processes (CSP) paper.

The Actor Model can't use the concurrency strategies of the CSP paper, so what strategy *does* it use? I believe the key to the Actor Model's concurrency is by fairly dividing execution time among its system of Actors. It does this by using a hardware construct called an Arbiter. An Arbiter will remain in a meta-stable state for an unbounded period of time, then choose an undeterminable one of its two outputs. Because this construct, which is the foundation of the Actor Model, has this property of unbounded non-determinism, the entire Actor Model has this property. This property is new to the computer science community, so a proof must be written to show that a computable process can be described in a system based on chaos.

The Arbiter has similar behavior to a Non-deterministic Turing Machine (NTM), so we can reference an NTM for behavior analysis. By its definition, a NTM has *bounded* non-determinism. That is, becase it guarantees a result, it must have finite (bounded) number of steps.

        Proving that a server will actually provide service to its clients requires unbounded non-determinism.

To explain this, we must understand the meaning of "unbounded non-determinism". In the context of concurrent processes, unbounded non-determinism means: A request is guaranteed to be serviced, even if the amount of delay in servicing a request is unbounded.

Suppose I am a shared resource server. I begin to service your request, then stop to begin servicing a different request. If you are continually interrupted, you can not calculate the time you must wait before you receive a response; your wait time is non-deterministic. If other requests continue to steal your processing time, it appears you could wait forever, so how can we guarantee a response? We can guarantee fair processing time to all requests by designing an arbitration algorithm that distributes response servicing time. In effect, this only ensures that handling of one request will not stop the handling of another request, but this is enough to prove that unbounded non-determinism can guarantee service for infinitely many requesters.

        In the semantics of bounded nondeterminism, a request to a shared resource might never receive service because a nondeterministic transition is always made to service another request instead.

This is the author's explanation of his previous statement, but my explanation above is more clear and complete

        That's why the semantics of CSP were reversed from bounded non-determinism to unbounded non-determinism.

The semantics of the CSP paper was originally in the context of *bounded* non-determinism, but only because the author believed unbounded non-determinism was impossible to implement. Since the author wanted to prove that a useful implemention of his ideas was possible, he had to prove it could actually service infinitely many clients. This demanded that he change the semantics of the CSP paper, in a later version, from *bounded* to *unbounded* non-determinism. I explained the nature of these two concepts above, if you don't understand this.

        However, bounded non-determinism was but a symptom of deeper underlying issues with communicating sequential processes as a foundation for concurrency.

That is, if your default method of programming is writing individual processes, adding concurrency later is difficult unless you study the problem. Then, you must use and adhere to a pattern when designing cooperative processes.

        The Computational Representation Theorem characterizes the semantics of Actor Systems without making use of sequential processes.

The Actor Model doesn't use such sequential processes, so it need not use such complex CSP patterns. The [Computational Representation Theorem](http://en.wikipedia.org/wiki/Actor_model#Computational_Representation_Theorem), mathematically explains the semantics of unbounded non-deterministic processes, which proves that the basis of the Actor Model can use infinitely many, highly responsive Actors.

### Comparison with Process Calculi

Process Calculi is an idea which competes with the Actor Model.

Conceptual differences:
- Process Calculi: Communication via channels, algebraic equivalence, bi-simulation
- Actor Model: Communication via Actor addresses, futures, Swiss cheese, garbage collection


## Systems of Computation in History

### Social Computation

First computers were humans.

An example
- Harvard Observatory collected astronomical data
- Amount was too great to process
- Hired a team of women to process it
- Observatory published a catalog of 10,000+ stars, classified according to their spectrum

### Turing's Psychological Model of Computation

Turing's Model
- Intensely individual, psychological, and sequential
- Computer behavior determined by current symbol and its "state of mind" at that time
- Limit to number of symbols can use at once
- Moves from one well-defined state to the next


## Actor Model

### Summary

Turing's model needs to be updated, because concurrency is an important concern in modern computing, which uses client-cloud design and many-core architectures.

Computational devices called Actors communicate asynchronously with each other using Actor's addresses, which are individualistic elements of a larger, complex, computationally useful system.

Axioms:
- Actor's known addresses can be
    - provided on creation
    - received via messages
    - of actors created "here"
- Actor's responses to messages can
    - create more Actors
    - send messages to addresses
    - specify how to process another message

### Differences from Previous Models of Computation

Actor Model has assumptions:
- Concurrent execution in processing a message
- Message passing has same overhead as looping and calling a procedure

*Not* required by an Actor:
- thread
- mailbox
- message queue
- own OS process

Acceptable implementions of the Actor Model:
- Axiomatically
- Theoretically, using the Computation Representation Theorem
- Operationally, using an Actor programming language


### Lambda Expressions

Lambda expressions a special case Actor - one which doesn't change.
- Lambda identifier is bound to an Actor's address.
- Beta reduction corresponds to an Actor receiving addresses of its arguments in a message.

Deeper comparison between Lambda calculus and Actor Model later.


### Turing's Model is a Special Case of the Actor Model

The Actor Model can implement systems which are impossible in Turing's model. This is proven by looking at the property of "unbounded non-determinism".

If we assume a Turing Machine will stop, there is a bound to the size of integer it can compute. A proof using König's lemma can be found [here](http://en.wikipedia.org/wiki/Unbounded_nondeterminism#Nondeterministic_automata).

We can make an Actor system will also halt, like a Turing Machine, but it will create an integer of unbounded size. A description of this system is as follows. Then a similar implementation is created in a non-deterministic Turing machine and discussed.

### Calculating an integer of Unbounded Size

#### Actor Model

Create an Actor which has a `currentCount` variable initialized to 0 and a `continue` variable initialized to True. When this Actor receives a `Stop` message, set `continue` to False and return `currentCount`. When it receives a `Go` message, increment `currentCount` by one and send itself a `Go` message, but if `continue` is False in this case, do nothing. Then, start this Actor by concurrently sending it a `Go` and `Stop` message.

#### Nondetermistic Turing Machine

According to the definition of a NTM, the program to generate an unbounded integer might never halt. However, this is still a bounded non-deterministic machine.

Step 1: Either print 1 on the next square of tape or execute Step 3.
Step 2: Execute Step 1.
Step 3: Halt

#### Different Method of Decision Making

The NTM implementation is different from the equivalent implementation in the Actor Model due do the decision making.
- The decision making of the NTM is *internal*.
- The decision making of the Actor Model is *partly external*. This

The program behaves the same, so unbounded non-determinism seems a little esoteric, no? The author says it is crucial to showing that a server does not accidentally starve a client. Not only a web server, but this is also practical for an operating system.

### Configurations vs Global States

Computation can be represented in:
- State Machine: as a global state which determines all information about the computation. Next global state is nondeterministic (e.g. using a external clock).
- Actors: as a configuration, which can be indeterminate (e.g. messages in transit).


### Unbounded Nondeterminism Controversy

Mostly explained near the beginning of my notes.

- Dijkstra believed unbounded nondeterminism was impossible to implement.
- Semantics of the CSP paper was bounded nondeterminism.

Definition of Unbounded Nondeterminism
- When started, system will halt.
- For every integer n, it is possible for the system to halt with output greater than n.

Summary of controversy:
- Semantics of unbounded nondeterminism are required to prove that a server provides service to every client.
- A Nondeterministic Turing Machine cannot implement unbounded nondeterminism.
- Dijkstra believed that unbounded nondeterminism cannot be implemented.
- An Actor system [Hewitt, et. al. 1973] can implement servers which provide service to every client and consequently unbounded nondeterminism.
- Since Hoare wanted to be able to prove that a server provided service to clients, the semantics of a subsequent version of CSP were switched from bounded to unbounded nondeterminism. 
- Unbounded nondeterminism was but a symptom of deeper underlying issues with communicating sequential processes as a foundation for concurrency.

## Process Calculi

The process calculi researchers were inspired by the Actor Model and lambda calculus to create a calculus for processes which uses bare minimum number of terms/things.

Differences between process calculi and Actor Model
- One Actor Model, many types of process calculi.
- Actor Model inspired by laws of physics, process calculi inspired by algebra.
- The sender is an important piece of process calculi, but not in Actor Model.
- Process calculi processes communicate via named channels or ambients, but Actors only use messages.

## Computational Undecidability

## Completeness vs Inferential Undecidability




