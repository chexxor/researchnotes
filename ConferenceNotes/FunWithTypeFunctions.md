
# MDCC TechTalk - Fun with Type Functions

## Speaker

Simon Peyton Jones, MA, MBCS, CEng and key contributor of Haskell

## Source

[MDCC TechTalk - Fun with type functions](http://channel9.msdn.com/posts/MDCC-TechTalk-Fun-with-type-functions)

## Location

Microsoft Development Center Copenhagen

November 9, 2011

## Notes

### Getting it Right

From "The ideal of program correctness", by Tony Hoare, BCS lecture and debate, Oct 2006:

> "Program correctness is a basic scientific ideal for Computer Science"

> "The most widely used tools [in pursuit of correctness] concentrate on the detection of programming errors, widely known as bugs. Foremost among these [tools] are modern compilers for strongly typed languages"

> "Like insects that carry disease, the least efficient way of eradicating program bugs is by squashing them one by one. The only sure safeguard against attack is to pursue the ideal of not making the errors in the first place."

However, Tony was being a bit cruel.

#### Importance of Program Specification

Static typing eradicates a large subset of possible bugs.

> As specification detail goes up, confidence in having intended program also goes up.

The static type of a function is a specification, albeit only a partial one. If we wrote the *entire* specification in the code, it would be hard to read the code. Static typing is, by far, the most widely-used program-verification tool in use today.

Why is static typing so widely use?
- Relatively lightweight addition to workflow
- Machine-checked, fully automated
- Forced-use in many languages

#### Balance of Static Typing

Spectrum of static typing, from very weak to very strong.

Weak static typing, like a hammer, is easy-to-use but has limited effectiveness. Strong static typing, like a tactical weapon, needs trained user but very effective.

If static typing is too strong, there is collateral damage. Need a highly trained programmer, which means few people can read or write it.

If static typing is too weak, it adds no benefit to a program.

It's best to start closer to well-designed weak static typing, then add features to express more detailed type specification.

A type system designer strives to ensure that types stay easy to use while adding more expressive features. If a program passes the type checker but fails in practice, this is a type system failure. If a program fails the type checker but would work in practice, this is a type system failure because it gets in the way.

One way to make a type system stronger is to use "indexed type families".

### Indexed Type Families

#### Basic Type Class

A type class allows us to declare different types as alternatives in a function. In the following example, we declare how to use the `(+)` function with two `Int` types.

        -- Type Class declares type signature
        --   of required methods.
        class Num a where
          (+), (*) :: a -> a -> a
          negate   :: a -> a
        -- Function with type-classed parameter.
        square :: Num a => a -> a
        square x = x*x
        -- Declare Int is a type of Num
        --   by implementing required methods.
        instance Num Int where
          (+)    = plusInt
          (*)    = mulInt
          negate = negInt
        -- Test the expression we want to write.
        test = square 4 + 5 :: Int 

This allows us to say `4 + 5`, which should return an `Int`, and the compiler will interpret the argument's types of this function as `Int + Int`.

Type classes allow us to add two `Num` types. This works great. However, there is a hidden limitation: the resulting type must be known. Haskell wants a Good type system, so the type of the expression must be determinable at compile-time. If we add one `Int` to another `Int`, we assume the result should also be an `Int`, so this isn't a problem in the above example. 

#### General Type Class

As a programmer, we might also want to add _different types_ of `Nums`, such as an `Int` and a `Float`, like this: `(4::Int) + (5::Float)`. For a human, this is no problem; of course the resulting type should be `Float`. But how do we specify this to the type checker?

In a basic type class, as above, we assumed an operation is performed using two arguments of the same type. In a "general" type class, we specify expected types when arguments are two **different** types.

        -- General type class, using two different types.
        class GNum a b where
          (+) :: a -> b -> ???
        -- Declare behavior when two Ints.
        instance GNum Int Int where
          (+) x y = plusInt x y
        -- Declare behavior when Int and Float.
        instance GNum Int Float where
          (+) x y = plusFloat (intToFloat x) y
        -- Test the expression we want to write.
        test1 = (4::Int) + (5::Int)
        test2 = (4::Int) + (5::Float)

Look at the type class declaration, here `(+) :: a -> b -> ??`. This specifies that, if `(+)` is given arguments of two different types, well then we don't know the resulting type. Should it be `a`'s type or `b`'s type? Or some new type which combines them? Well, we can't be certain because it depends on the types. Who knows the right way to combine the types? The creator of the types. So, the type class leaves it as `???` for the programmer to decide, which is declared in the `instance` statements.

This looks like what we want, but the `???` is a problem. Why? This must be known at compile-time, not just-in-time decided. We can't leave it as a blank box. So, we must tell the type-checker exactly which type to expect.

#### Type-level Functions

If a type class accepts two arguments of different types, how can we decide the output type? Well, if we assume the output type can be decided, it will be dependent on the two input types. This sounds like a function, a function of two types. Let's call this a "type-level function", because it will exist entirely in the type system and will calculate types, not data.

        -- Same declaration from above.
        class GNum a b where
          (+) :: a -> b -> ???
        -- Replace ??? with a type signature function,
        --   and expect instances to implement it.
        class GNum a b where
          type SumTy a b :: *
          (+) :: a -> b -> SumTy a b
        -- Same instance declarations from above,
        --   with added type functions.
        instance GNum Int Int where
          type SumTy Int Int = Int
          (+) x y = plusInt x y
        instance GNum Int Float where
          type SumTy Int Float = Float
          (+) x y = plusFloat (intToFloat x) y

The `*` is called "kind". A Kind groups Types in the same way that a Class groups Types. ?


These are not just macros, they can't be decided on-the-fly.

#### Eliminate Bad Programs

Consider the following expression, which tries to add `Dollars` and `Int`.

        newtype Dollars = MkD Int
        test = (MkD 3) + (4::Int)

This should not be allowed. Why? Well, should the result be 7 Dollars? Or 7.03 Dollars? Or 7 Int? It's not clear.

However, two `Dollars` types _should_ be addable. How can we do this? Add `Dollars` type to the `Num` class so we can use the `(+)` function. It looks like this:

        instance GNum Dollars Dollars where
          type SumTy Dollars Dollars = Dollars
          (+) (MkD d1) (MkD d2) = MkD (d1+d2)
        -- Test the expression we want to write.
        test = (MkD 3) + (MkD 4)

We can see that we can disallow incompatible types by simply not declaring instances for them. Strictness by default.

### Type Functions in Practice

#### Flexibly-Typed Map

Consider a finite map which maps keys to values. In this map, we want to allow keys and values of any type.

- If key is Bool, value is any two things.
- If key is Int, value is balanced tree.
- If key is a pair, value is a stack of maps. Value is a map from x to a map of y, to a value.

As you can see, we want a flexible map which can be used with very complicated types. Without type functions, this is impossible in Haskell.

How can we write such a complex data structure in Haskell? Take a look.

        class Key k where
          data Map k :: * -> *
          empty :: Map k v
          lookup :: k -> Map k v -> Maybe v
          -- Other methods, such as insert, union, etc. ...

How can we use an instance of `Key`? We can use `lookup` to get its associated value, or other functions, such as insert, union, etc.

Notice this class has an associated class, named `Map`. It's data type will vary, depending on the key's type, denoted by `k`.

Let's make `Bool` an instance of `Key`.

        instance Key Bool where
          -- A Value of this Key will be either True or False.
          data Map Bool v = MB (Maybe v) (Maybe v)
          empty = MB Nothing Nothing
          lookup True  (MB _ mt) = mt
          lookup False (MB mf _) = mt

And let's make a pair an instance of `Key`.

        instance (Key a, Key b) => Key (a,b) where
          -- Value's type is two level Map.
          data Map (a,b) v = MP (Map a (Map b v))
          empty = MP empty
          -- Two level lookup.
          lookup (ka,kb) (MP m) =
                case lookup ka m of
                    Nothing -> Nothing
                    Just m2 -> lookup kb m2

#### Data Parallel Haskell

Data parallel was the main motivating use-case which forced the creation and implementation of type functions.

        -- [::] means "data parallel vector"
        [:Double:] -- Vector of Doubles
        [:(a,b):]  -- Vector of pairs

When one wants to do fast computation, such as in GPUs, pointers to values are an unwanted overhead. In most data structures in Haskell, values are "boxed", which basically means "behind a pointer". To be suitable for such use-cases, we want to keep the representation unboxed. These vectors are a way to accomplish this.

But, the most efficient way to represent unboxed numbers is not the most efficient way to represent pairs.

        class Elem a where
          data [:a:]
          index :: [:a:] -> Int -> a
        -- Most efficient representation is ByteArray.
        instance Elem Double where
          data [:Double:] = AD ByteArray
          index (AD ba) i = ... -- Fetch using memory addresses.
        -- Most efficient representation of
        --   an array of pairs
        --   is a *pair of arrays*.
        instance (Elem a, Elem b) => Elem (a,b) where
          data [:(a,b):] = AP [:a:] [:b:]
          index (AP a b) i = (index a i, index b i)

"Vectorization" is a transformation which compiles programs which use these vectors as if they were using vector-optimized hardware instructions. The `fst^` method, for example, is nearly constant time: `fst^ (AP as bs) = as`

#### Nested Arrays

How to make arrays of arrays more efficient using parallelism? If the length of the top-level array is greater, then it's better to distribute processing across keys. If the depth of the array is greater, then it's better to distribute processing across values. Ideally, we can balance the workloads.

A mathematician found an algorithm to do this. First, flatten it into a single array, then segment it into parts to process. However, managing the segmentation and status of these is a little challenging.

There isn't much to learn here, and the speaker rushed through this, so I'll skip it in notes.

#### Client-server Programs

Goal: Write a `run` function with a type signature which allows the type-checker to verify they communicate using the same protocol.

        run addServer addClient -- Good.
        run addServer addServer -- Should fail.

The `run` function should express the relatively complex relationship between a client and a server. What type signature should `run` have?

        run:: ??? -> ??? -> End

Well, the goal is to use generics to write *any* pair of client-server processes. So, client's type should be a `Process`, and the server's type should be a `Process`. Like this class:

        -- A process has a complement.
        -- If running two processes they must be complements.
        class Process p where
          type Co p              -- Type function.
          run :: p -> Co p -> End

The important part of this example is the type function `Co` to match two custom functions together. This can be expressed in the type signature of the class.

If you're curious to see more, here's how to implement `Co` when we make `In` and `Out` instances of `Process` class.

        -- Make a In an instance of Process
        --   In's complement will be called Out.
        instance Process p => Process (In v p) where
          type Co (In v p)       = Out v (Co p)
          run (In v p) (Out v p) = run (vp v) p
        -- Make a Out an instance of Process
        --   Out's complement will be called In.
        instance Process p => Process (Out v p) where
          type Co (Out v p)     = In v (Co p)
          run (Out v p) (In vp) = run p (vp v)

And here's some type signatures, if you're curious.

        addServer :: In Int (In Int (Out Int End))
        addClient :: Out Int (Out Int (In Int End))
        addServer = In (\x -> In (\y -> Out (x + y) End))
        addClent = Out (\x -> Out (\y -> In (x + y) End))
        --
        data In v p  = In (v -> p)
        data Out v p = Out v p
        data End     = End


### Conclusion

If you are still hungry for more, you can read the paper titled "Fun with type functions", which is full of examples of type functions in practice.

See Simon's paper on the Microsoft Research site here: [Fun with type functions](http://research.microsoft.com/en-us/um/people/simonpj/papers/assoc-types/fun-with-type-funs/typefun.pdf)
Or, see Simon's profile page on Microsoft Research here: [Simon Peyton Jones](http://research.microsoft.com/en-us/people/simonpj/).





