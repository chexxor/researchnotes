
# Using NixOS for Declarative Deployment and Testing

## Speaker

Sander van der Burg, PhD student at the Software Engineering Research Group, Delft University of Technology, The Netherlands, working on the Pull Deployment of Services project and NixOS contributor

## Source

[Video Recording](https://www.youtube.com/watch?v=uYZrbb78YzA)

[Slides](http://www.st.ewi.tudelft.nl/~sander/pdf/talks/vanderburg11-fosdem.pdf)

[Session page](https://archive.fosdem.org/2011/schedule/event/distro_nixos.html)

## Location

Universite Libre de Bruxelles, Brussels, Belgium

February 5, 2011

## Notes

### Intro

Not going to explain general, just particular application of NixOS.

### Many Linux Distributions Available

Each distribution has different goals and solutions.

Different package manager
Fedora - RPM
Debian - yet different
Slackware - Shell script

Different Goals
DSL - Small disk space
NixOS

### Software Deployment

Software deployment is all activities that make a software system available for use.

Typical software deployment activities for Linux:
- Install OS distribution with standard software
- Modify configuration files
- Install custom software
- Upgrade system and custom software

### Deployment Scenario

Single installation
- Workstation OS
- Mobile device
- OS, desktop, applications, modify config files

Multiple installations
- Data-center
- Dependent machines
- Simultaneously manage machines
- Load balancers

Virtual Environments
- Try new software

### Challenges

Deploying single machine is difficult. Upgrading is dangerous.

Deploying distributed environment more difficult. Machines dependent on each other, such as web app and database. Downtimes may occur during upgrades.

Deploying VMs equally difficult. A bit easier because no hardware to manage.

### NixOS Introduction

NixOS has a tool which can deal with these issues. It is a Linux distribution which exclusively uses the Nix package manager.

### Nix Package Manager

Each package is stored in isolation from others, each gets a unique pathname. Each package name includes a hash, which is a function of every build-time variable: source, libraries, compiler, build scripts.

The Linux kernel, system configuration files, and all packages are stored in the Nix store by hash. This includes Apache configuration and SSH configuration.

### Nix Expressions

Consider the package for OpenSSH. It lists dependencies at top of package file: `stdenv`, `fetchurl`, `openssl`, `zlib`. Package includes shell command to build the package. Nix manages dependencies, not how to build a package's actual dependencies.

    ### openssh.nix ###
    { stdenv, fetchurl, openssl, zlib }:
    stdenv.mkDerivation {
        name = "openssh-4.6p1";
        src = fetchurl {
            url = http://.../openssh-4.6p1.tar.gz;
            sha256 = "0fpjlr3bfind0y94bk442x2p...";
        };
        buildCommand = ’’
            tar xjf $src
            ./configure --prefix=$out --with-openssl=${openssl}
            make; make install
        ’’;
    }


#### NixOS Implications

No `/lib` or `/usr` - These are managed by Nix
Minimal `/bin` and `/etc` - Symlink from `/bin` to Nix store, it is only used to only link to Bash shell. `/etc` stores some shared configuration files, such as `/etc/host`. MySQL stored in Nix store.

Instead of editing packages, everything is controlled by a single `configuration.nix` file. It declares how to use Grub, which file systems to use, which desktop to use, and which packages with which to initialize the system.

`nixos-rebuild` is a command which builds a complete OS configuration from a single declarative file. Generated components are stored next to each other, namespaced by their hashes. Does not remove or overwrite any files. Old configurations are saved, which means can easily rollback to any old configuration, at boot-time or run-time. Unused components are garbage-collected later.

### Distributed Deployment Case Study

NixOS has good properties for deployment for single systems. Can we extend this to distributed systems?

Let's try to automate the deployment of a relatively complicated software application. "Trac" is web-based management system of software projects. It must access subversion repo and create bug tickets. Trac can be installed on a single machine, but we can also distribute a Trac installation across machines. We can tell it to use a separate Subversion server, Database server, and Web server.

#### Deploy Network of Machines

    ### network.nix ###
    { storage = {pkgs, ...}:
        {
            services.nfsKernel.server.enable = true; ...
        };
        postgresql = {pkgs, ...}:
        {
            services.postgresql.enable = true; ...
        };
        webserver = {pkgs, ...}:
        {
            fileSystems = [
                { mountPoint = "/repos"; device = "storage:/repos"; }
            ];
            services.httpd.enable = true;
            services.httpd.extraSubservices = [
                { serviceType = "trac"; }
            ];
            ...
        };
        ...
    }

This Nix expression uses the same format as the NixOS `configuration.nix` file. Each attribute specifies a particular target in the Trac network. One for storage server, one for database server, and one for web server. Can deploy a network of machines by capturing their properties.

If we compile the Nix expression with a different tool, is should have all the information it needs to deploy and connect each machine. The `nixos-deploy-network` command, as follows.

    nixos-deploy-network network.nix

This command first builds the system configuration of each machine locally and saves them in the local Nix store. Won't they collide? They are hashed by their content and parameters, so they are isolated from each other.

#### How Does Network Deploy Work?

After all configurations are built, the complete system and all dependencies are transferred to target machines in the network. If the target machine already has a component with that hash, it will not be transferred. So, this is reasonably efficient.

While upgrading, the existing conﬁguration is not affected. Components in the Nix store are immutable, and an upgrade only adds components not present. So, if an upgrade fails, we can roll back to the old, working configuration.

After all components are transferred, the new system configuration must be activated. Failures may occur, but the system can be easily rolled back.

#### Deploy Network of VMs

This Nix expression for a network of machines can also be used to deploy a network of *virtual machines*. The following command with build a a network of QEMU-KVM virtual machines which closely resemble the physical network.

    nixos-build-vms network.nix; ./result/bin/nixos-run-vms

This does *not* create disc images for each VM. Instead, the Nix store of the *host* system is mounted across the network. This makes VM deployment quick, because the components don't need to be copied.

This is difficult to do with `/etc`, `/usr`, and `/lib` because we have no guarantees of their contents and versions.

### Testing Network Deploys

We can deploy the distributed application to VMs. Wouldn't it be nice to be able to write tests to verify a successful deployment?

A Perl test driver was created to write test cases, like unit tests, for a network of virtual machines.

The following Nix expression will issue shell commands to use the Perl deployment testing tool.

    testScript = ’’
        # 1) Create Postgres database.
        $postgresql→waitForJob("postgresql");
        $postgresql→mustSucceed("createdb trac");
        # 2) Create Subversion repository.
        $webserver→mustSucceed("mkdir -p /repos/trac");
        $webserver→mustSucceed("svnadmin create /repos/trac");
        $webserver→waitForFile("/var/trac");
        # 3) Create a Trac project.
        $webserver→mustSucceed("mkdir -p /var/trac/projects/test");
        $webserver→mustSucceed("trac-admin /var/trac/projects/test initenv ".
            "Test postgres://root\@postgresql/trac svn /repos/trac");
        # 4) Start X, launch a web browser to view Trac web app.
        $client→waitForX;
        $client→execute("konqueror http://webserver/projects/test &");
        $client→waitForWindow(qr/Test.*Konqueror/);
        # 5) Take screenshot to see result.
        $client→screenshot("screen");
    ’’;

Will this produce and run several VM windows? Not necessarily. These test cases can be executed in a non-interactive manner. Use the following command to run the Perl tests.

    nix-build tests.nix -A trac

No windows will be created, but everything described in the test suite will be executed. Results of the test will be output as text.

### Experience

The distributed deployment and testing features of Nix is currently used in a few places.

Hydra build machines for continuous integration and testing.
- Nix packages
- NixOS installer
    - On every commit, deploy installation target machine and machine to host nixpkgs.
- OpenSSH
    - Deploy machine with SSH server and machine to test SSH connection.
- Trac
- NFS server
- Various GNU projects
    - E.g. On every new version of `glibc`, deploy NixOS, install `glibc` and run some test cases.

### Related Work

These projects use "convergent" models:

- Cfengine: Imperative. Shared files are overwritten, instead of using immutable packages.
- Stork

NixOS uses "congruent" models.

### Q & A

Q: How do you deal with user data in a service or program. For instance, update Postgres to a new version.

A: Nix doesn't deal with mutable state. Another Nix project is in the works.

Q: The Nix package manager will overwrite many directories?

A: Everything is stored in `/nix/store`, so nothing else on the system is touched. In order to use those components, the `PATH` environment variable must be modified to point to the nix store.

Q: How to deal with scripts or programs which refer to absolute paths?

A: The developer must adapt these scripts. In practice, this problem does not often occur. For Python, a wrapper script was created. The wrapper must specify the environment variables used to enable Python to find its libraries.

Q: Does NixOS deal with user configuration files as well, such as `~/.*`? Or only system config files.

A: The home directory is not touched by NixOS.

Q: Is anybody using this in production? The whole NixOS.

A: Currently not many users, but some universities and smaller companies are using it. But the userbase has been linearly growing for over 6 years. Debian will not use NixOS because it does not comply with the FHS standard, which dictates which files live where.

Q: Can the Nix store be moved to different architectures?

A: Yes, Nix expressions have a parameter named `stdenv`, which stored many things, included the system architecture. Because the architecture is a parameter in a Nix expression, if a component is built for a different architecture, it will receive a different hash.

Q: Can the Nix store be moved to different places, machines, architectures?

A: Yes, we do this in the Hydra build cluster. The cluster creates builds for many architectures, but the built binaries and images are all stored in a single Nix store. They each have a different hash, so they can live in the same store.

Q: Is there a host, like for Debian packages, that you can just load. I don't want to spend like three days waiting for my machine to build.

A: Nix mainly builds from source to produce binaries. These binaries are cached, and the Nix builder can use a component's hash to ask the binary cache for a pre-built component.

Q: Can we use other tools, such as Chef, to build a Nix system? Would you reject an integration with a reason that it's against the spirit of the project?

A: It wouldn't be impossible to integrate other tools, but it should be possible. I wouldn't be against it.

Q: There is no real overhead, the same files aren't duplicated on different boxes? For every binary file on your system, there is no duplicate?

A: It all depends on hash codes. Each package is guaranteed unique, not binary files.

Q: Can individual users use Nix to customize a machine?

A: Yes, it's already possible with Nix packages.

Q: If two users install the same package, will there be only one component in the Nix store?

A: Yes, on a single system all users share a Nix store.

Q: Is there a way to force upgrades due to security issues?

A: There is one implementation which deals with this, but it is not currently in this release (2011).

Q: If I install two different versions of Firefox, many files are the same. Will they be shared?

A: There is an optimization procedure which compares files and uses sym-links. But this depends on the way the application is packaged.




