#!/usr/bin/env ruby
require 'Nokogiri'
require 'pathname'
require 'pp'

Metadata_Src_Dir = "C:/Users/Alex/Documents/Fiti/SFDC_Component_fetch/MXIC\ UAT/src"

module Globals
	Approval_Process_Folder_Name  = "approvalProcesses"
	Approval_Process_Extension    = ".approvalProcess"
	Approval_Process_File_Pattern = "*" + Approval_Process_Extension

	Assignment_Rule_Folder_Name  = "assignmentRules"
	Assignment_Rule_Extension    = ".assignmentRules"
	Assignment_Rule_File_Pattern = "*" + Assignment_Rule_Extension

	Autoresponse_Rule_Folder_Name  = "autoResponseRules"
	Autoresponse_Rule_Extension    = ".autoResponseRules"
	Autoresponse_Rule_File_Pattern = "*" + Autoresponse_Rule_Extension

	Class_Folder_Name  = "classes"
	Class_Extension    = ".cls"
	Class_File_Pattern = "*" + Class_Extension

	Component_Folder_Name  = "components"
	Component_Extension    = ".component"
	Component_File_Pattern = "*" + Component_Extension

	Document_Folder_Name  = "documents"
	Document_Extension    = ".*"
	Document_File_Pattern = "*" + Document_Extension
	Document_Extension_Ignore = "-meta.xml"
	Document_File_Ignore_Pattern = "*" + Document_Extension_Ignore

	Email_Folder_Name  = "email"
	Email_Extension    = ".email"
	Email_File_Pattern = "*" + Document_Extension
	Email_Extension_Ignore = "-meta.xml"
	Email_File_Ignore_Pattern = "*" + Document_Extension_Ignore

	Escalation_Rule_Folder_Name  = "escalationRules"
	Escalation_Rule_Extension    = ".escalationRules"
	Escalation_Rule_File_Pattern = "*" + Escalation_Rule_Extension

	Homepage_Component_Folder_Name  = "homePageComponents"
	Homepage_Component_Extension    = ".homePageComponent"
	Homepage_Component_File_Pattern = "*" + Homepage_Component_Extension

	Homepage_Layout_Folder_Name  = "homePageLayouts"
	Homepage_Layout_Extension    = ".homePageLayout"
	Homepage_Layout_File_Pattern = "*" + Homepage_Layout_Extension

	Layout_Folder_Name  = "layouts"
	Layout_Extension    = ".layout"
	Layout_File_Pattern = "*" + Layout_Extension

	Object_Folder_Name  = "objects"
	Object_Extension    = ".object"
	Object_File_Pattern = "*" + Object_Extension

	Object_Translation_Folder_Name  = "objectTranslations"
	Object_Translation_Extension    = ".objectTranslation"
	Object_Translation_File_Pattern = "*" + Object_Translation_Extension

	Page_Folder_Name  = "pages"
	Page_Extension    = ".page"
	Page_File_Pattern = "*" + Page_Extension

	Portal_Folder_Name  = "portals"
	Portal_Extension    = ".portal"
	Portal_File_Pattern = "*" + Portal_Extension

	Profile_Folder_Name  = "profiles"
	Profile_Extension    = ".profile"
	Profile_File_Pattern = "*" + Profile_Extension

	Queue_Folder_Name  = "queues"
	Queue_Extension    = ".queue"
	Queue_File_Pattern = "*" + Queue_Extension

	Report_Folder_Name  = "reports"
	Report_Extension    = ".report"
	Report_File_Pattern = "*" + Report_Extension
	Report_Extension_Ignore = "-meta.xml"
	Report_File_Ignore_Pattern = "*" + Report_Extension_Ignore

	Site_Folder_Name  = "sites"
	Site_Extension    = ".site"
	Site_File_Pattern = "*" + Site_Extension

	Static_Resource_Folder_Name  = "staticResources"
	Static_Resource_Extension    = ".resource"
	Static_Resource_File_Pattern = "*" + Static_Resource_Extension

	Tab_Folder_Name  = "tabs"
	Tab_Extension    = ".tab"
	Tab_File_Pattern = "*" + Tab_Extension

	Trigger_Folder_Name  = "triggers"
	Trigger_Extension    = ".trigger"
	Trigger_File_Pattern = "*" + Trigger_Extension

	Weblink_Folder_Name  = "weblinks"
	Weblink_Extension    = ".weblink"
	Weblink_File_Pattern = "*" + Weblink_Extension

	Workflow_Folder_Name  = "workflows"
	Workflow_Extension    = ".workflow"
	Workflow_File_Pattern = "*" + Workflow_Extension
end




Approval_Process = Struct.new(:object, :name, :actions)

def approval_process_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Approval_Process_Folder_Name, Globals::Approval_Process_File_Pattern)
	Dir.glob(metadata_dir) do |file|
		file_name = File.basename(file, Globals::Approval_Process_Extension)
		
		parts = file_name.split('.')
		object = parts.at 0
		name = parts.at 1

		xml_doc = Nokogiri::XML(File.open(file))

		actions = xml_doc.xpath('//xmlns:ApprovalProcess//xmlns:action').map do |entity|
			{
				:name => (entity.at_xpath("xmlns:name").content rescue nil),
				:type => (entity.at_xpath("xmlns:type").content rescue nil)
			}
		end

		results << Approval_Process.new(object, name, actions)
	end

	results
end



Assignment_Rule = Struct.new(:object)

def assignment_rule_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Assignment_Rule_Folder_Name, Globals::Assignment_Rule_File_Pattern)
	Dir.glob(metadata_dir) do |file|
		file_name = File.basename(file, Globals::Assignment_Rule_Extension)
		
		object = file_name
		results << Assignment_Rule.new(object)
	end

	results
end



Autoresponse_Rule = Struct.new(:object)

def autoresponse_rule_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Autoresponse_Rule_Folder_Name, Globals::Autoresponse_Rule_File_Pattern)
	Dir.glob(metadata_dir) do |file|
		file_name = File.basename(file, Globals::Autoresponse_Rule_Extension)		# file_name = Case.autoResponseRules
		
		object = file_name
		results << Autoresponse_Rule.new(object)
	end

	results
end



Class_ = Struct.new(:name)

def class_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Class_Folder_Name, Globals::Class_File_Pattern)
	Dir.glob(metadata_dir) do |file|
		file_name = File.basename(file, Globals::Class_Extension)
		
		name = file_name

		results << Class_.new(name)
	end

	results
end



Component = Struct.new(:name, :controller)

def component_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Component_Folder_Name, Globals::Component_File_Pattern)
	Dir.glob(metadata_dir) do |file|

		name = File.basename(file, Globals::Component_Extension)

		xml_doc = Nokogiri::XML(File.open(file))
		controller_name = xml_doc.children.first['controller']

		results << Component.new(name, controller_name)
	end

	results
end



Document = Struct.new(:name)

def document_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Document_Folder_Name)

	Dir.foreach(metadata_dir) { |file|
		next if file == '.' or file == '..' or file.end_with?(Globals::Document_Extension_Ignore)

		file_path = File.join(metadata_dir, file)
		if File.directory? file_path
			results << document_folder_info(file_path)
		else
			parent_and_file = File.join("/", File.basename(file_path))
			results << Document.new(parent_and_file)
		end
	}

	results
end

def document_folder_info(document_folder)

	results = []

	Dir.glob(File.join(document_folder, "*.*")) do |file|
		next if file.end_with?(Globals::Document_Extension_Ignore)

		file_name = File.basename(file)
		parent_and_file = File.join(File.basename(File.expand_path("..", file)), file_name)
		results << Document.new(parent_and_file)
	end

	results
end




Email = Struct.new(:name)

def email_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Email_Folder_Name)

	Dir.foreach(metadata_dir) { |file|
		next if file == '.' or file == '..' or file.end_with?(Globals::Email_Extension_Ignore)

		file_path = File.join(metadata_dir, file)
		if File.directory? file_path
			results << email_folder_info(file_path)
		else
			parent_and_file = File.join("/", File.basename(file_path))
			results << Document.new(parent_and_file)
		end
	}

	results
end

def email_folder_info(email_folder)

	results = []

	Dir.glob(File.join(email_folder, "*.*")) do |file|
		next if file.end_with?(Globals::Document_Extension_Ignore)

		file_name = File.basename(file)
		parent_and_file = File.join(File.basename(File.expand_path("..", file)), file_name)
		results << Email.new(parent_and_file)
	end

	results
end



Escalation_Rule = Struct.new(:name)

def escalation_rule_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Escalation_Rule_Folder_Name, Globals::Escalation_Rule_File_Pattern)
	Dir.glob(metadata_dir) do |file|
		file_name = File.basename(file, Globals::Escalation_Rule_Extension)
		
		name = file_name

		results << Escalation_Rule.new(name)
	end

	results
end



Homepage_Component = Struct.new(:name)

def homepage_component_rule_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Homepage_Component_Folder_Name, Globals::Homepage_Component_File_Pattern)
	Dir.glob(metadata_dir) do |file|
		file_name = File.basename(file, Globals::Homepage_Component_Extension)
		
		name = file_name

		results << Homepage_Component.new(name)
	end

	results
end



Homepage_Layout = Struct.new(:name)

def homepage_layout_rule_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Homepage_Layout_Folder_Name, Globals::Homepage_Layout_File_Pattern)
	Dir.glob(metadata_dir) do |file|
		file_name = File.basename(file, Globals::Homepage_Layout_Extension)
		
		name = file_name

		results << Homepage_Layout.new(name)
	end

	results
end




Layout = Struct.new(:object, :record_type)

def layout_rule_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Layout_Folder_Name, Globals::Layout_File_Pattern)
	Dir.glob(metadata_dir) do |file|
		file_name = File.basename(file, Globals::Layout_Extension)
		
		parts = file_name.split('-')
		object = parts.at 0
		record_type = parts.at 1

		results << Layout.new(object, record_type)
	end

	results
end



Object_ = Struct.new(:name)

def object_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Object_Folder_Name, Globals::Object_File_Pattern)
	Dir.glob(metadata_dir) do |file|
		file_name = File.basename(file, Globals::Object_Extension)
		
		name = file_name
		results << Object_.new(name)
	end

	results
end



Object_Translation = Struct.new(:name)

def object_translations_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Object_Translation_Folder_Name, Globals::Object_Translation_File_Pattern)
	Dir.glob(metadata_dir) do |file|
		file_name = File.basename(file, Globals::Object_Translation_Extension)
		
		name = file_name

		results << Object_Translation.new(name)
	end

	results
end



Page = Struct.new(:name, :controller, :extensions, :components)

def page_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Page_Folder_Name, Globals::Page_File_Pattern)
	Dir.glob(metadata_dir) do |file|
		file_name = File.basename(file, Globals::Page_Extension)

		xml_doc = Nokogiri::XML(File.open(file))
		controller = xml_doc.children.first['controller']
		extensions = xml_doc.children.first['extensions']
		components = []

		xml_doc.root.traverse do |node|
			if node.name.start_with? "c:"
				components << node.name
			end
		end
		
		name = file_name

		results << Page.new(name, controller, extensions, components)
	end

	results
end




Portal = Struct.new(:name)

def portal_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Portal_Folder_Name, Globals::Portal_File_Pattern)
	Dir.glob(metadata_dir) do |file|
		file_name = File.basename(file, Globals::Portal_Extension)
		
		name = file_name

		results << Portal.new(name)
	end

	results
end





Profile = Struct.new(:name)

def profile_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Profile_Folder_Name, Globals::Profile_File_Pattern)
	Dir.glob(metadata_dir) do |file|
		file_name = File.basename(file, Globals::Profile_Extension)
		
		name = file_name

		results << Profile.new(name)
	end

	results
end



Queue_ = Struct.new(:name)

def queue_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Queue_Folder_Name, Globals::Queue_File_Pattern)
	Dir.glob(metadata_dir) do |file|
		file_name = File.basename(file, Globals::Queue_Extension)
		
		name = file_name

		results << Queue_.new(name)
	end

	results
end




Report = Struct.new(:name)

def report_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Report_Folder_Name)

	Dir.foreach(metadata_dir) { |file|
		next if file == '.' or file == '..' or file.end_with?(Globals::Report_Extension_Ignore)

		file_path = File.join(metadata_dir, file)
		if File.directory? file_path
			results << report_folder_info(file_path)
		else
			parent_and_file = File.join("/", File.basename(file_path))
			results << Report.new(parent_and_file)
		end
	}

	results
end

def report_folder_info(report_folder)

	results = []

	Dir.glob(File.join(report_folder, "*.*")) do |file|
		next if file.end_with?(Globals::Report_Extension_Ignore)

		file_name = File.basename(file)
		parent_and_file = File.join(File.basename(File.expand_path("..", file)), file_name)
		results << Report.new(parent_and_file)
	end

	results
end



Site = Struct.new(:name)

def site_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Site_Folder_Name, Globals::Site_File_Pattern)
	Dir.glob(metadata_dir) do |file|
		file_name = File.basename(file, Globals::Site_Extension)
		
		name = file_name

		results << Site.new(name)
	end

	results
end



Static_Resource = Struct.new(:name)

def staticresources_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Static_Resource_Folder_Name, Globals::Static_Resource_File_Pattern)
	Dir.glob(metadata_dir) do |file|
		file_name = File.basename(file, Globals::Static_Resource_Extension)
		
		name = file_name

		results << Static_Resource.new(name)
	end

	results
end



Tab = Struct.new(:name)

def tabs_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Tab_Folder_Name, Globals::Tab_File_Pattern)
	Dir.glob(metadata_dir) do |file|
		file_name = File.basename(file, Globals::Tab_Extension)
		
		name = file_name

		results << Tab.new(name)
	end

	results
end




Trigger = Struct.new(:name)

def trigger_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Trigger_Folder_Name, Globals::Trigger_File_Pattern)
	Dir.glob(metadata_dir) do |file|
		file_name = File.basename(file, Globals::Trigger_Extension)
		
		name = file_name

		results << Trigger.new(name)
	end

	results
end



Weblink = Struct.new(:name)

def weblink_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Weblink_Folder_Name, Globals::Weblink_File_Pattern)
	Dir.glob(metadata_dir) do |file|
		file_name = File.basename(file, Globals::Weblink_Extension)
		
		name = file_name

		results << Weblink.new(name)
	end

	results
end





Workflow = Struct.new(:name)

def workflow_info(metadata_src_dir)

	results = []

	metadata_dir = File.join(metadata_src_dir, Globals::Workflow_Folder_Name, Globals::Workflow_File_Pattern)
	Dir.glob(metadata_dir) do |file|
		file_name = File.basename(file, Globals::Workflow_Extension)
		
		name = file_name
		results << Workflow.new(name)
	end

	results
end



# ap_info = approval_process_info(Metadata_Src_Dir)
# # puts ap_info
# PP.pp ap_info

# ar_info = assignment_rule_info(Metadata_Src_Dir)
# puts ar_info

# arr_info = autoresponse_rule_info(Metadata_Src_Dir)
# puts arr_info

# class_info = class_info(Metadata_Src_Dir)
# puts class_info

# comp_info = component_info(Metadata_Src_Dir)
# puts comp_info

# doc_info = document_info(Metadata_Src_Dir)
# puts doc_info

# email_info = email_info(Metadata_Src_Dir)
# puts email_info

# er_info = escalation_rule_info(Metadata_Src_Dir)
# puts er_info

# hpc_info = homepage_component_rule_info(Metadata_Src_Dir)
# puts hpc_info

# hpl_info = homepage_layout_rule_info(Metadata_Src_Dir)
# puts hpl_info

# layout_info = layout_rule_info(Metadata_Src_Dir)
# puts layout_info

# obj_info = object_info(Metadata_Src_Dir)
# puts obj_info

# objTrans_info = object_translations_info(Metadata_Src_Dir)
# puts objTrans_info

# p_info = page_info(Metadata_Src_Dir)
# # puts p_info
# PP.pp p_info

# port_info = portal_info(Metadata_Src_Dir)
# puts port_info

# prof_info = profile_info(Metadata_Src_Dir)
# puts prof_info

# q_info = queue_info(Metadata_Src_Dir)
# puts q_info

# r_info = report_info(Metadata_Src_Dir)
# puts r_info

# s_info = site_info(Metadata_Src_Dir)
# puts s_info

# sr_info = staticresources_info(Metadata_Src_Dir)
# puts sr_info

# t_info = tabs_info(Metadata_Src_Dir)
# puts t_info

# tr_info = trigger_info(Metadata_Src_Dir)
# puts tr_info

# w_info = weblink_info(Metadata_Src_Dir)
# puts w_info

# wf_info = workflow_info(Metadata_Src_Dir)
# puts wf_info





