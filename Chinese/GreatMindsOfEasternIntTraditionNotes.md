# Great Minds of the Eastern Intellectual Tradition

## Preface

Notes taken when consuming the The Great Courses course called "Great Minds of the Eastern Intellectual Tradition".

Professor Grant Hardy - University of North Carolina. BA in Ancient Greek, PhD in Chinese language and literature. Written many books and livedin Taiwan for two years, teaching in Hawaii and Brigham Young.

Lecture 1 - Life's Great Questions, Asian Perspectives
-------

Inspired by "Great Minds of the Western Intellectual Tradition" series.

Where: 36 lectures about Asia, including India, China, Japan, and a bit about Persia, Tibet, and Korea

What: In the west, Philosophy is a critical examination of fundamental questions of existence, values, knowledge, and language based on systematic, rational argument. Asian philosophy does not distinguish between philosophy, religion, politics, literature, history, psychology, and science. This course will use the Greek definition of philosophy, which is "the love of wisdom".

Who: Influencial figures in the West include: Moses, Aristotle, Jesus, Aquinas, Shakespeare, Newton, Marx, Einstein, Freud. This course will cover similarly influencial figures, but Asia's figures.

When: A long timespan, beginning with anonymous writers of Indian vedas, about 1200 BC, then Buddha and Confucius about 500 BC. Eight lectures on origins of Asian intellectual history, ten lectures on development of major intellectual traditions, ten lectures of figures in medieval period. Finish the course with modern thinkers, mixing Western and Asian ideas.

This course will treat Islam as a Western religion, even though three times as many Muslims live in Asia than in the West.

It's important we talk about the leading figures along the way. This knowledge is cumulative and builds on prior ideas.

Our cultural conventions and ideas come from somewhere. They come from a Great Conversation, which is what books and courses like this are. If we can discover the origins of our cultural ideas, we can choose what to keep, throw out, and believe.

European philosophy consists of a series of footnotes to Plato. This can't be said about Asian thought, as there are a few different schools of thought. We will be organizing them into a analogy of three hotels, or conferences of thinkers:
- Dhartiana Hotel: Translated from Sanskrit as To See, or To View, but came to mean Understanding or Insight. This is set in India, began with The Vedas and Upanishads and ideas about Samsara (reincarnation), Karma (justice), and Moksha (liberation). Two groups reject the authority of the Vedas.
- Dao Hotel: Translated as The Way or The Path, in China. All schools of Chinese thought claim to teach about The Way. Different context than in India; no caste system, more concerned with government, and more focused with problems of this world rather than next world. Many people all arguing about The Way. More concerned with providing guidelines on how to live.
- Do Hotel: Translated as The Way, in Japan. Same character and meaning as Chinese Dao. Most concerned with themselves rather than arguing with those at the Dao Hotel.

Over time, Buddhism begins in India, then travels to the Dao Hotel and then to the Do Hotel. These hotels adapt their ideas to respond to Buddhist ideas, then dies out in India.

Is there a single line of Asian intellectual tradition? Not really. More accurately, there are these three schools of intellectual tradition and ideas.
Why study Asian intellectual tradition? It is interesting. Also, it's useful to understand Asian thought because the world is becoming a smaller place. An important unseen part of being human is contained in these questions. If you don't consider these questions, you are missing a large part of being human.

Five questions that are pondered in Asian schools of thought:
1) What is the nature of reality?
2) How do we know what we know?
3) How should society be organized?
4) Why do people suffer?
5) How can we find happiness?

The professor does not subscribe to any of these religions or schools of Asian thought, but still gains value from understanding them.


Lecture 2: The Vedas and Upanishads - The Beginning 
-------

Two anonymous pieces of writings - the Vedas and the Upanishads. We don't know much about the writers, but we do know a little. In the Egypt, civilization began along the Nile river. Like the Mesopotamian civilzation in Egypt, another civilization began along the Indus river around 2500 BC in Pakistan.

The Indus river valley civilization was called Harapa. They flourished from 2500 BC to 1500 BC, and they were a rather impressive people, controlling a vast area with extensive trade and indoor plumbing. Their writing system has never been deciphered. About the time this group fell into decline, another group moved in from the West, called the Aryans or Indo-Aryans (unrelated to Hitler's Aryans). The Aryans, meaning 'noble people', were nomadic cattle herders, and eventually took up agriculture. We know more about the Aryans because they wrote down their stories. These stories were passed through generations orally for nearly a millenia before they were written down.

These oral generational stories from the Aryan civilization are called the Vedas, which means 'wisdom'. Through these, we can learn about the Aryan culture, food, clothing, etc. Most Vedas talk about nature, wind, storms, and gods. Their gods were ordered into a family-like hierarchy, like the Greek gods. The Aryans were an Indo-European people, which describes their linguistic heritage, so their language, Sanskrit, is related to other languages in Europe and some of their words are similar to some Latin words. While they existed orally from 1500 BC to 1000 BC, they weren't written down until around 500 AD, which is 1000 years later.

The Vedas describe a creation story. Humans were created from the body of a god named Pirusha. His mind became the moon, his eye became the sun, the sky from his head, and the earth from his feet. This same creation story shows a very early description of the caste system the Indo-Europeans introduced to India. The top were the Braman priests and came from Pirusha's mouth, the Kashatrias were the warrior-rulers came from his arms, the Vaishas were the commoners/land-owners came from his thighs, and the Shudras were the laborers came from his feet. This lowest caste inhabited the land before the Aryans arrived and weren't included from the Vedic religion. Outcasts, those with mother from one caste and father from a different caste, were also out of the system.

The priests memorized the Vedas and they performed the appropriate sacrifices, and they had an interest in upholding their social status. Actually, this Vedic religion is sometimes called Bramanism, which is distinct from Hinduism.

Ritual maintains the order of the cosmos, which implies deal-making. When things when poorly, the kings blamed the Bramans, who blamed improper training. The Bramans developed a set of ritual texts which defined exactly how rituals should be performed to solve this problem.

Does this really work? Is religion nothing more than rituals and specialized rule-books? Some Indians questioned this idea of castes and sacrifices and devoted their lives to discovering the truth of reality. These pragmatic thinkers chose an ascetic life. They argue that the most important thing is not the ritual, but the spiritual reality behind the ritual. These teachings are called the Upanishads, meaning 'close to' to describe their underground origins, and were eventually included in the Vedas. Thirteen major Upanishads, most taking the form of dialog or debates and are rather complicated. They were composed over several centuries by the hands of different authors, so they aren't consistent.

The most important concepts in the Upanishads are Samsara, Karma, Darma, and Moksha.
1) Samsara is reincarnation. When we die, we will be reborn in a different body, perhaps an animal. This is appealing because we each have many chances. This explains deja-vu, geniuses, and immediately good or bad relationships.
2) Karma is action. All actions have consequences, either good or bad. This determines your future and your reincarnation. There is no judge, it's just a universal system of law. Snakes and ladders is a game that comes from India and is related to reincarnation - up with a ladder, down on a snake's back.
3) Darma is right behavior or duty or truth. If you're born a servant, you should be the best servant you can be.
4) Moksha is the extraordinary goal. It is the release or liberation from this life. The same life repeatedly needs an escape. Liberation through knowledge. Braman and Atama. Braman is the underlying reality that sustains reality. Atama is the unchanging internal self, which can only be perceived through meditation. Atman = Braman is a piece of miracle insight in the Upanishads; what composes you is identical to what composes everything else. A great consciousness. Monism is a western work that means everyone and everything is separate but will be reabsorbed into Braman.

Udalica - Over 100 sages in the Upanishads. Udalica lives in about 800 BC. Shvitakat goes to study, then returns to his father, Udalica, after 12 years. He is rather arrogant with his knowledge. His father asks him if he "has the wisdom that allows him to hear the unheard, to perceive the unperceivable, to know the unknowable". His son has no idea, so his father enlightens him with an anology about clay. If you understand clay, you understand all clay things. Also gives a teaching about a banyan seed. What's inside a seed? The essence of the entire banyan tree. You, Shvitakat, are that.

Caste system focuses on ritual rather than politics. In India, history isn't as essential as other religions which are concerned with relationship between god and human. Gods, humans, and animals are all on the same level, in a way. It is polytheist and monotheist at the same time. Or maybe atheist, because everything is an illusion.

These concepts, Samsara, Karma, Darma, and Moksha, are at the heart of the three great Asian religions, but there are some subtle differences in how these ideas are understood in these religions.


Lecture 3 - Mahavira and Jainism: Extreme Non-Violence
-------

Two most significant heretical religions in India in that they reject the ideas of the Vedas and the teachings of the Darman priests: Mahavira and Jainism. Mahavira and Jainism aren't the most popular religions.

Charvaka disagreed with the Darman priests. If a thing couldn't be seen, heard, or touched, then it didn't exist. Claim that religion is a trick to take advantage of man. What's the best way to live? Just find pleasures and avoid pains; there is nothing supernatural to worry about in life, no purpose or meaning.

Huge spectrum of religion, from the profoundly religious views of the Upanishads to the uncompromising materialism of Charvaka. Mahavira, the founder of Jainism. This title means 'the great hero'. Born into the warrior-ruler caste around 540 BC. Not sure of facts, but born a prince and dissatisfied with what he saw around him. At about the age of 30, he gave up his kingdom, family, and riches to become an ascetic in search of meaning of life. He eventually becomes enlightened around the age of 40. He spends his years teaching others what he has learned.

He accepted Samsara, which is reincarnation, but added a twist. Rather than believing that only humans, animals, and gods have souls, nearly everything is alive. Everything is made of Jibba, small, living intelligences, which are trapped in matter. Five great categories of beings in this world: 5-sense creatures, 4-sense creatures, 3-sense creatures, etc. The biggest category is 1-sense creatures, which include plants, microscopic organisms, wind, water, drops, and fires. The one thing they all have in common is they all feel pain. When a match is struck to produce a flame, a new being with a soul comes into existence. When the flame is extinguished, the soul leaves and will be reborn, perhaps as a human.

Do you remember when you were a tree and were chopped down, or when you were a fish and caught in a net? This kind of thinking.

There is no all-encompassing Braman. There is only an endless cycle of souls caught in unending agony.

He also has his version of Karma and Moksha. Cool and thoughtless acts attract heavy karma and weigh it down towards lower levels of existence at its time of death. Acts of kindness and generosity make a lighter soul and allow it to move up the ladder towards higher levels of existence. Acts of self-sacrifice remove bad karma from a soul, which brings the soul closer to Moksha, or liberation from existence. With enough self-sacrifice, one will reach Nirvana and never be reborn again. A soul in Nirvana has infinite knowledge, infin perception, infinite energy, and infinite bliss.

What does a Jainist want to do to lead the good life? Don't cause suffering. Five great vows: no violence, no stealing, no sexual immorality, no falsehood, and no grasping. In addition, there are two broad paths available: the ordinary path and the extraordinary path. Jains of monogamous, sweep the ground, wear face-masks, strain water, no root vegetables, donate extra money. They avoid farming, become merchants or bankers. The extraordinary path means becoming a monk - celibacy, walk barefoot everywhere, beg for food, accept insults, and possibly end their lives by starving to death.

Moksha comes at the end of a 14 stage development. Faith is stage number 4.

This may seem extreme, but it is directed towards oneself rather than, for example, cleansing the world of heretics.

His class tradition was conquest, but rather than physical conquest, he chose spiritual conquest. If you believe the few basics, the rest of the religion is rather logical. It's focus on suffering exists in many religions.

Epistamology is the study of how we know what we know. Jains have the theory of many-sided-ness. Rather than saying 'x is y', it is more precise to say 'x may be y'. Each person has a different standpoint, illustrated by the story of the 5 men who touch an elephant and say what beast they touched. All judgements are limited and perspective.

Is the world finite or infinite? In terms of substance, it is finite. From the standpoint of time and modification, it is infinite. So, the world is both finite and infinite at the same time.

Considering this idea of multiple viewpoints further, for the degree of happiness you have when you win a competition, someone else is equally disappointed that they did not win.

The Jains were seen as very extreme. An alternative path is given by Buddhism, which is also called "The Middle Way", which is between the Upanishads religion and the Jains ideas.


Lecture 4 - The Buddha and the Middle Way
-------

The second major school in India is Buddhism, which is between extreme asceticism and ordinary life or materialism.

Buddha was born in modern Nepal, lived from 563 to 483 BC. Contemporary of Mahavira, and was also a prince. Kingdoms and warfare existed, nequities in this society, caste system existed, and the Vedic religion started to be seen as ritual and without deep, spiritual answers. The first biography of the Buddha was written by a man named Ashvigoza in Sanskrit in 100 AD, which is 500 years after his time.

Buddha's mother had a hard time giving birth. She had a dream one day of a white elephant who entered her side, and she found herself pregnant. The real miracle was when he was born, as his mother was born without any pain at all. As soon as he was born, he took seven steps, then spoke - "I was born for enlightenment, for the good of this world. This is my last birth." In all Buddha stories, he was seen doing miraculous things. In his last incarnation, he was born as Sidartha Gautama. His mother died shortly after giving birth, so he was raised by his step-aunt. He was prophesied to be either a great king or a holy man.

His father wanted him to be a king, so he kept him in the palace and never let him out. He became married an had a good time in the palace for many years. He became curious about life outside the palace walls, so his father arranged things so that he wouldn't see anything unpleasant, but when he went out he saw someone who was old. He asked his chariot driver what was wrong with the man, and the driver said this happens to everyone if you are lucky enough to get so old. He went out again and saw a sick and decrepid person, and went out again later and saw death for the first time. He realized the life he was living was pretty artificial. He went out again and saw a wandering ascetic, and realized it's possible to achieve peace and contentment even if the world is full of suffering.

He decided to become an ascetic, and left the palace at night when he was 29. He wandered with other ascetics for 6 years and was very diligent. He limited his food, stories say to one grain of rice per day, but he came to a limit of his progress. "I'm not getting any wiser, I'm just hungry,", he said. He devotes himself to sitting under a tree until he has some answers, and he goes into a series of deeper and deeper meditative states. He sees his previous lives, the previous lives of others, then he becomes enlightened. The word 'Buddha' means 'the enlightened one' or 'the awakened one'. He didn't want to just learn how to live with suffering or block it out, he wanted to eliminate it entirely. When he first became enlightened, he wanted to join the gods, but they encouraged him to stay on earth to teach what he has learned to others. His first sermon was given to his companions at a place called Sarna in India, which starts spinning the wheel of the law. The Buddha spends the next 45 years wandering around, teaching what he has learned and organizing his followers. At the age of 80, he becomes sick after eating some food and he died, passing into Nirvana.

At his first sermon, he describes the Four Noble Truths:
1) All life is suffering
2) All suffering is caused by desire
3) If you can stop desire, you can stop suffering
4) You can stop desire by following the Eightfold Path

"Birth is suffering, decay is suffering, illness is suffering, death is suffering. The presence we ought of objects we hate is suffering, and separation of objects we love is suffering. Not to obtain what we ought to desire is suffering."

The Eightfold Path:
- Right views
- Right intention
- Right speech
- Right action
- Right livelihood
- Right effort
- Right mindfulness
- Right concentration

This list of eight can be divided into three parts: Wisdom (views, intention), Conduct (speech, action, livelihood), Mental Development (effort, mindfulness, concentration).

The Buddha didn't talk much about Nirvana, but rather focuses on the process instead of the goal. Nirvana is an indescribable state - Where does a flame go when it goes out?

He retains the metaphysics of the Upanishads, Samsara, Karma, Darma, and Moksha, but unlike Hinduism, the Buddha taught that nothing has a soul. All sentient beings are composite, transient, and soulless. Any grasping at permanence will end in failure and suffering. Enlightenment is simply the realization that no part of you is everlasting, instead you are a combination of the five aggregates: body, feelings, perceptions, dispositions, consciousness. These are always changing all the time, and will be recombined and formed into another body. If we can overcome the illusion that there is an entity that is me, that has needs and desires, then my aggregates will dissipate at my death and will find peace and permanence in Nirvana. The key to eliminating selfish desire is that there is no self.

Three characteristics:
- All things are impermanent
- All things are not satisfactory
- There is no self. Rebirth does not involve a soul, but it's like a candle lighting another candle.

Five aggregates, called the Scandas:
- Body
- Sensations
- Perceptions
- Dispositions
- Consciousness

The Four Immeasurables:
- Friendliness
- Compassion
- Sympathetic joy
- Equinimity

Three Refuges:
- Buddha
- Darma
- Sanga

Three Poisons:
- Greed
- Hate
- Delusion

Five Precepts:
- No harming or killing
- No stealing
- No sexual immorality
- No lying
- No intoxicants

To move beyond the normal life:
- Live life of celibacy
- Don't eat after 10
- No use of money
- Spend time in scripture and meditation
- 250 rules for monks

The Sanga is the longest continuously existing social organization in the world.

Why is Buddhism attractive?
- It's not brand new; it still draws on existing ideas of Darma, Moksha, etc. There's a famous sermon given by the Buddha called the Fire Sermon. You're on fire and you need to do something about it.
- Buddha was agnostic about many things. Didn't offend other ideas. Many heavens and many hells, will die in hell and be born again into this world.
- Buddha taught in vernacular languages, rather than in old languages as in Bramanism and the Vedas. The Buddha scriptures are quite beautiful.
- Buddhist teachings are logical, empirical and practical.


Lecture 5 - The Bhagavad Gita - The Way of Action







